story_begin = {}

function story_begin.start()
    createEncounter("story_1", 15, 15);
    showDialog("story_begin_dia", [[Hello captain.
We have received an urgent top secret |mission.|
An experimental new ship conducting covert operations in the free sectors has dissappeared. Since you're one of the few ships in the area and not a warship, which would attract undue attention, we want you to investigate it's last reported location.
Be careful, as you are well aware, the free sectors are hostile and dangerous.]]);
    return true;
end

story_1 = {}

function story_1.start()
    addRM(50);
    showDialog("story_1", "You discover what seems to be the wreckage of multiple ships. Analyzing the debris it is unclear if it is from the covert ship. Luckly we discover a few fresh ion trails, clearly multiple ships have been here recently.");
    createEncounter("story_decoy", 15, 15);
    createEncounter("story_decoy", 15, 15);
    createEncounter("story_2", 15, 15);
    return true;
end

story_decoy = {}

function story_decoy.start()
    showDialog("story_decoy_dia", "The ion trail becomes too sparse at this point. There is nothing in this sector. Either the trail is gone or it was a false trail.");
    return true;
end

story_2 = {}

function story_2.start()
    spawnEnemyShip();
    showDialog("story_2_dia", "You find a ship at the end of the ion trail.");
    removeEncounters("story_decoy");
    onFlee("story_2.flee");
    onDestroy("story_2.destroy");
    return true;
end

function story_2.flee()
    showDialog("story_2_flee", "A warp statsis field is preventing you from engaging your warp engines. You cannot flee.");
    cancelWarp();
end

function story_2.destroy()
    onFlee(nil);
end
