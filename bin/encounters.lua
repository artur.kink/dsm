-- All encounters must be defined in modules.
-- The encounter entry is the start() function.
-- The start function must return true or false.
-- This is the methodology of checking if the encounter meets
-- starting requirements. Returning false will avoid starting the
-- encounter.
--
-- Events:
-- onHail
-- onFlee
--      Called when you player attempts to warp away.
--      Calling cancelWarp() will cancel the warp.
-- onAttack
-- onDestroy


anomaly = {}

function anomaly.start()
    mycrew = getRandomCrew();
    addCrewSelectOption("anomaly_opt1", 1, "anomaly.find", "Send crew to investigate");
    addOption("anomaly_opt2", "", "Could be dangerous, lets move on");
    showDialog("anomaly_dia1", ":1: notices a bizarre astrophysical anomaly", getCrewName(mycrew));
    return true;
end

function anomaly.find(crew)
    if random(100) < 80 then
        if hasProficiency(crew, "astrophysics") or random(100) < 20 then
            increaseCrewProficiencyLevel(crew, "astrophysics", 10);
            createEncounter("findfuel", 10, 10);
            showDialog("anomaly_dia2", "Found the source of the anomaly coming from a nearby sector");
        else
            showDialog("anomaly_dia3", "Could not figure out the source of the anomaly.");
        end
    else
        showDialog("anomaly_dia4", "As you spend time researching the anomaly, a massive gravitational wave suddenly shoots out and rocks the hull of your ship. We should leave before another one strikes.");
        damageShipArmor(PlayerShip, 30);
    end
end

findfuel = {}

function findfuel.start()
    addFuel(10);
    showDialog("find_dia", "You discover a cosmic eddy that can be converted into fuel");
    return true;
end

ship = {}

function ship.start()
    showDialog("ship_dia", "As you drop out of warp a ship approaches you.");
    spawnEnemyShip();
    return true;
end

example = {}

function example.start()
    addOption("ex_opt1", "example.approach", "Approach Ship");
    addOption("ex_opt2", "example.avoid", "Avoid Ship");
    showDialog("ex_dia", "There is a ship on the other side of the system.");
    return true;
end

function example.approach()
    if random(100) < 60 then
        showDialog("ex_dia2", "You'll regret this!");
        spawnEnemyShip();
        onDestroy("example.destroy");
        onFlee("example.flee");
    else
        showDialog("ex_dia3", "The ship notices you approaching and retreats before you can reach it.");
    end
end

function example.avoid()
    onHail("example.hail");
end

function example.hail()
    showDialog("ex_hail", "The ship seems to have received your hail and approaches.");
    spawnEnemyShip();
end

function example.destroy()
    onFlee();
end

function example.flee()
    showDialog("ex_flee", "The ship does not seem to follow you");
end

life = {}

function life.start()
    addCrewSelectOption("life_opt1", 1, "life.investigate", "Send crew down to investigate");
    addOption("life_opt2", "", "Avoid planet");
    showDialog("life_dia1", "A life form on the planet calls to you.");
    return true;
end

function life.investigate(crew)
    if random(100) < 20 then
        showDialog("life_dia2", "A crew beams down to the planet surface. After tracking down the life signs, a black goo monster of pure evil appears. :1: dies when the monster strikes at them.", getCrewName(crew));
        killCrew(crew);
    else
        giveReward();
        showDialog("life_dia3", "A life form greets you. In return for letting it learn more about your lifeform, it offers you a gift.");
    end
end

distress_signal = {}

function distress_signal.start()
    addOption("distress_signal_opt1", "distress_signal.rescue", "Attempt rescue");
    addOption("distress_signal_opt2", "", "Ignore");
    showDialog("distress_signal_dia1", "You recieve a recorded distress signal, coming from a small ship, likely an escape pod. Attempts at two-way communication fail... it's possible they didn't make it.");
    return true;
end

function distress_signal.rescue()
    rescuechance = random(100);
    if rescuechance < 33 then
        showDialog("distress_signal_dia2", "It was a trap! A large warship warps into range!");
        -- to do: spawn ship by class
        spawnEnemyShip();
    elseif rescuechance < 66 then
        showDialog("distress_signal_dia3", "It's no use, it appears their life-support systems had already failed and all that remains is a frozen corpse.");
    else
        -- to do: check max crew
        createPlayerCrew();
        showDialog("distress_signal_dia4", "It appears their radio was damaged but they are still alive. An escapee from a slaver ship agrees to join your crew.");
    end
end

trader = {}

function trader.start()
    spawnEnemyShip();
    setEnemyHostile(false);
    addOption("trader_opt1", "trader.listen", "Listen to their offer");
    addOption("trader_opt2", "trader.no", "Not interested");
    showDialog("trader_dia1", "A ship hails you. The captain is requesting to trade with you.");
    return true;
end

function trader.no()
    result = random(100);
    if result < 50 then
        showDialog("trader_dia2", "Hmph, alright...");
    elseif result < 75 then
        showDialog("trader_dia3", "If you dont want to listen to reason, then well take what we need by force!");
        setEnemyHostile(true);
    else
        showDialog("trader_dia4", "The ship captain issues an order to his crew. Suddenly explosions rock your ship, you had wandered into a mine field. The enemy ship escapes before you have a chance to react.");
        damageShipArmor(PlayerShip, 20);
        spawnFire(PlayerShip, 5);
        spawnFire(PlayerShip, 5);
        warpEnemyShip();
    end
end


function trader.listen()
    rm = random(5) + 20;
    if getRM() >= rm then
        addOption("trader_opt3", "trader.trade", "Accept");
        addOption("trader_opt2", "trader.no", "Not interested");
    else
        addOption("trader_opt4", "trader.no", "Sorry, we don't have that much RM");
    end
    addIntVariable("rm", rm);
    showDialog("trader_dia5", "We need some scrap, were ready to offer you 10 fuel for :1: scrap.", tostring(rm));
end

function trader.trade()
    useRM(getIntVariable("rm"));
    if random(100) < 15 then
        setEnemyHostile(true);
        spawnFire(PlayerShip, 15);
        showDialog("trader_dia6", "As the fuel is transported aboard it explodes in a giant fireball, it was a trap. The enemy ship is charging its weapons");
    else
        addFuel(10);
        showDialog("trader_dia7",  "Thanks, it has been a pleasure doing business with you");
        onAttack("trader.attack");
    end
end

function trader.attack()
    showDialog("trader_dia8", "How dare you betray me? But I was ready for this, you see, I have booby trapped the fuel. Suddenly the fuel you transported abroad explodes");
    spawnFire(PlayerShip, 5);
    spawnFire(PlayerShip, 5);
    onAttack("");
end

radiation = {}

function radiation.start()
    if isAsteroids() then
        crew = getRandomCrew();
        addOption("radiation_opt1", "radiation.mine", "Attempt to mine the asteroids");
        addOption("radiation_opt2", "", "Leave as soon as possible");
        showDialog("radiation_dia1", "The asteroid belt is made of extremely radioactive material. It seems to be seeping through the shield, it could definitely be deadly. :1: notifies you that we could mine the asteroids for some fuel, however it would take some time.", getCrewName(crew))
        addTurnCallback("radiation_turn", "radiation.turn", 0);
        onFlee("radiation.flee");
        setSpawnEnemyShips(false);
        return true;
    else
        return false;
    end
end

function radiation.mine()
    showDialog("radiation_dia2", "Well get to work right away, this might take more than 5 turns");
    addTurnCallback("radiation_finish", "radiation.finish", random(5, 6));
end

function radiation.turn()
    damageShipShield(PlayerShip, 15);
    flash(1000, 0, 255, 0);
    crews = getPlayerCrew();
    for k,v in pairs(crews) do
        damageCrew(v, random(3, 5));
    end
    if random(100) < 45 then
        spawnFire(PlayerShip, 1);
    end
    addTurnCallback("radiation_turn", "radiation.turn", 1);
end

function radiation.finish()
    addFuel(20);
    showDialog("radiation_dia3", "We have successfully mined some fuel captain.");
end

function radiation.flee()
    removeTurnCallback("radiation_turn");
    removeTurnCallback("radiation_finish");
end

solar = {}

function solar.start()
    if isStar() then
        showDialog("solar_dia1", "The nearby star looks like its about to generate a massive solar flare. Suddenly the star bursts a masdive flare towards you.");
        spawnFire(PlayerShip, 3);
        spawnFire(PlayerShip, 3);
        damageShipShield(PlayerShip, 900);
        setSafe(false);
        addTurnCallback("solar_turn", "solar.turn", 0);
        onFlee("solar.flee");
    else
        return false;
    end
    return true;
end

function solar.turn()
    spawnFire(PlayerShip, 2);
    flash(1000, 255, 0, 0);
    damageShipShield(PlayerShip, 100);
    addTurnCallback("solar_turn", "solar.turn", 1);
    return true;
end

function solar.flee()
    removeTurnCallback("solar_turn");
end

bounty_hunter = {}

function bounty_hunter.start()
    spawnEnemyShip();
    addOption("bounty_hunter_opt1", "bounty_hunter.no", "I don't do business with bounty hunters");
    addOption("bounty_hunter_opt2", "bounty_hunter.listen", "Listen to his offer");
    showDialog("bounty_hunter_dia1", "A ship hails you.\n\"You know you have a bounty on your head, but maybe I don't have to kill you... if you can match the price\"");
    return true;
end

function bounty_hunter.no()
    showDialog("bounty_hunter_dia2", "\"You'll regret this, I never fail to bring my bounty in!\"");
    onDestroy("bounty_hunter.destroy");
end

function bounty_hunter.destroy()
    showDialog("bounty_hunter_dia3", "Right before the ship exploded, the bounty hunter sends an S.O.S signal to a nearby system. It's likely help is on its way... we should leave quickly");
    onFlee("bounty_hunter.flee");
    setSafe(false);
    setSpawnEnemyShips(false);
    addTurnCallback("bounty_hunter_turn", "bounty_hunter.turn", 2);
end

function bounty_hunter.turn()
    spawnEnemyShip();
    showDialog("bounty_hunter_dia4", "A ship warps into view.\n\"How could my brother have lost??? You will pay for this!\"");
end

function bounty_hunter.flee()
    removeTurnCallback("bounty_hunter_turn");
end

function bounty_hunter.listen()
    if getRM() < 50 then
        addOption("bounty_hunter_opt3", "bounty_hunter.no", "Thats more than I have...");
    else
        addOption("bounty_hunter_opt1", "bounty_hunter.no", "I don't do business with bounty hunters");
        addOption("bounty_hunter_opt4", "bounty_hunter.deal", "Alright, lets make a deal");
    end
    showDialog("bounty_hunter_dia5", "For 50RM I'll let you walk.");
end

function bounty_hunter.deal()
    useRM(50)
    showDialog("bounty_hunter_dia6", "Pleasure doing business with you!");
    warpEnemyShip();
end

surprise = {}

function surprise.start()
    spawnEnemyShip();
    damageShip(PlayerShip, 50);
    showDialog("suprise_dia", "As you drop out of warp, a ship that has been clearly waiting for someone unleashes a surprise attack. You sustain heavy damage");
    return true;
end

anthro = {}

function anthro.start()
    spawnEnemyShip();
    setEnemyHostile(false);
    addOption("anthro_opt1", "anthro.no", "Refuse");
    addOption("anthro_opt2", "anthro.yes", "Invite them aboard");
    showDialog("anthro_dia1", "A nearby ship hails you.\n\"Hello, were a group of anthropologists. We're interested in all species, could we meet?\"\nThey appear friendly.");
    return true;
end

function anthro.no()
    showDialog("anthro_dia2", "\"We're sorry to hear that\"");
    if random(2) == 0 then
        warpEnemyShip();
    end
end

function anthro.yes()
    result = random(100);
    if result < 90 then
        giveReward();
        showDialog("anthro_dia3", "You invite the group on board. After a pleasant cultural exchange they thank you with a gift.");
        warpEnemyShip();
    elseif result < 95 and getRM() >= 5 then
        useRM(5);
        warpEnemyShip();
        showDialog("anthro_dia4", "You invite the group on board. After a pleasant cultural exchange they leave. As the ship leaves you notice some RM is missing, it was a con job...");
    else
        setEnemyHostile(true);
        spawnEnemyCrew(PlayerShip, 3);
        showDialog("anthro_dia5", "As the anthrophologists board you ship, they pull out their weapons... it was all a lie");
    end
end

debris = {}

function debris.start()
    addOption("debris_opt1", "debris.investigate", "Investigate");
    addOption("debris_opt2", "", "Avoid it, it could be dangerous");
    showDialog("debris_dia1", "You see the debris of a destroyed ship");
    return true;
end

function debris.investigate()
    result = random(100);
    if result < 50 then
        showDialog("debris_dia2", "You find some salvageable recycleble material.");
        addRM(30);
    elseif result < 75 then
        showDialog("debris_dia3", "You find some salvageable material, but in the process you take some damage traversing the debris field");
        addRM(20);
        damageShipArmor(PlayerShip, 20);
    else
        showDialog("debris_dia3", "You find nothing and in the process you take some damage traversing the debris field");
        damageShipArmor(PlayerShip, 20);
    end
end

pod = {}

function pod.start()
    addOption("pod_opt1", "pod.board", "Bring the pod onboard");
    addOption("pod_opt2", "", "Leave the pod, it could be dangerous");
    showDialog("pod_dia1", "You find an escape pod, its still emitting an emergency signal. It does not respond to your hails, it looks pretty messed up");
    return true;
end

function pod.board()
    result = random(100);
    if result < 20 then
        showDialog("pod_dia2", "A person emerges from the pod, they pull out their blaster");
        spawnEnemyCrew(PlayerShip, 1);
        setEnemyHostile(true);
    elseif result < 30 then
        showDialog("pod_dia3", "A person emerges from the pod, they're thankful for saving their life and offer to join your crew");
        createPlayerCrew();
    elseif result < 40 then
        crew = getRandomCrew();
        damageShipArmor(PlayerShip, 10);
        damageCrew(crew, 45);
        spawnFire(PlayerShip, 10);
        showDialog("pod_dia4", "As you open the pod, it explodes. :1: takes serious damage.", getCrewName(crew));
    else
        addRM(15);
        showDialog("pod_dia5", "The pod is empty, you recycle it for some material.");
    end
end

cling = {}

function cling.start()
    addCrewSelectOption("cling_opt1", 1, "cling.remove", "Send crew to remove it");
    addOption("cling_opt2", "cling.shoot", "Attempt to shoot it off");
    showDialog("cling_dia1", "An object attaches itself to your ship's hull.");
    return true;
end

function cling.shoot()
    result = random(100);
    if result <= 50 then
        showDialog("cling_dia2", "The object is destroyed without any damage to your ship.");
    else
        damageShipArmor(PlayerShip, 30);
        showDialog("cling_dia3", "As the object gets shot, it explodes in a massive explosion. Your hull takes heavy damage.");
    end
end

function cling.remove(crew)
    result = random(100);
    hasProf = hasProficiency(crew, "repair");
    increaseCrewProficiencyLevel(crew, "repair", 5);
    if result < 60 or (hasProf and result < 80) then
        addRM(5);
        showDialog("cling_dia4", ":1: successfully removes the object. It appears to have been an old mine that failed to activate. You recycle it.", getCrewName(crew));
    elseif result < 95 or (hasProf and result < 98) then
        damageShipArmor(PlayerShip, 30);
        showDialog("cling_dia5", "As :1: tries to remove the object, it activates. Luckly :1: manages to get away from it as it explodes. Your ship takes damage.", getCrewName(crew));
    else
        killCrew(crew);
        showDialog("cling_dia5", "As :1: tries to remove the object, it activates. :1: manages to disconnect the object from the ship before it explodes and kills them.\nTheir service will not be forgotten.", getCrewName(crew));
    end

end

ruins = {}

function ruins.start()
    addCrewSelectOption("ruins_opt1", 1, "ruins.explore", "Send down a crew to explore");
    addOption("ruins_opt2", "", "It is of no interest to us");
    showDialog("ruins_dia1", "You spot the ancient ruins of a city on the planet surface");
    return true;
end

function ruins.explore(crew)
    result = random(100);
    if result < 60 then
        if hasProficiency(crew, "archeology") then
            addFuel(10);
            increaseCrewProficiencyLevel(crew, "archeology", 10);
        elseif hasProficiency(crew, "history") then
            addFuel(3+random(7));
            increaseCrewProficiencyLevel(crew, "history", 10);
        else
            addFuel(3);
        end
        showDialog("ruins_dia2", "Searching the ruins you found a ancient deposit of fuel");
    elseif result < 95 then
        showDialog("ruins_dia3", "It seems the ruins are too ancient, there is nothing of interest here");
    else
        if getRM() >= 50 then
            addOption("ruins_opt3", "ruins.ok", "Accept their offer");
        else
            addOption("ruins_opt4", "ruins.no", "We don't have that much");
        end
        addCrewVariable("crew", crew);
        showDialog("ruins_dia4", "A group of bandits capture you crew and keep them hostage. They're demanding 50RM in return for :1:'s life", getCrewName(crew));
    end
end

function ruins.ok()
    useRM(50);
    showDialog("ruins_dia5", "Your crew is released. The bandits leave no trace of themselves that you can follow");
end

function ruins.no()
    killCrew(getCrewVariable("crew"));
    showDialog("ruins_dia6", "The bandits terminate the communication. You spend hours searching for them or your crew... but there are no leads.\nYou suspect the worst.");
end

station = {}

function station.start()
    showDialog("station_dia", "Welcome to our station, would you like to trade?");
    onHail("station.hail");
    onAttack("station.attack");
    onScan("station.scan");
    return true;
end

function station.hail()
    showDialog("station_hail", "We're open for trading");
end

function station.attack()
    showDialog("station_attack", "How dare |you| attack us!? You will pay for this!");
    onAttack(nil);
    onHail(nil);
    onScan(nil);
end

function station.scan()
    setEnemyHostile(true);
    onAttack(nil);
    onHail(nil);
    onScan(nil);
    showDialog("station_scan", "We consider your scanning of us a hostile action!");
end

deliver = {}

function deliver.start()
    amount = getIntVariable("deliver");
    if amount ~= nil then
        return false;
    end
    spawnEnemyShip();
    setEnemyHostile(false);
    addOption("deliver_opt1", "deliver.accept", "Accept");
    addOption("deliver_opt2", "", "Refuse");
    showDialog("deliver_dia", "A ship hails you. They ask for your help to deliver some goods.");
    return true;
end

function deliver.accept()
    addItem("good", 5);
    addIntVariable("deliver", 5, true);
    addIntVariable("deliver_turn", getTurnNumber(), true);
    createEncounter("delivered", 10, 20);
end

-- delivered created by the deliver encounter
delivered = {}

function delivered.start()
    amount = getIntVariable("deliver");
    if amount == nil then
        return false;
    end

    spawnEnemyShip();
    removeVariable("deliver");
    turn = getIntVariable("deliver_turn");
    removeVariable("deliver_turn");
    if amount > getItemAmount("good") then
        showDialog("delivered_dia1", "A ship hails you. After explaining why you don't have the agreed upon goods, they charge their weapons.");
    elseif turn + 10 < getTurnNumber() then
        showDialog("delivered_dia2", "A ship hails you. They thank you for delivering the goods. However since the deliver took so long they do not pay you the premium");
        setEnemyHostile(false);
        useItem("good", amount);
        addRM(5);
    else
        showDialog("delivered_dia3", "A ship hails you. They thank you for delivering the goods.");
        setEnemyHostile(false);
        useItem("good", amount);
        addRM(5);
        giveReward();
    end
    return true;
end

probe = {}

function probe.start()
    addOption("probe_opt1", "probe.download", "Attempt to download the probe data");
    addOption("probe_opt2", "", "Leave the probe, it could be dangerous");
    showDialog("probe_dia1", "You spot a harmless looking probe flying nearby. You might be able to download its data remotely.");
    return true;
end

function probe.download()
    result = random(100);
    if random(100) <= 50 then
        exploreStarMap(7, 10);
        showDialog("probe_dia2", "You download the probe data, it contained a lot of nearby star map data.");
    else
        spawnEnemyShip();
        showDialog("probe_dia3", "An enemy ship appears, the probe must have given away our location.");
    end
end
