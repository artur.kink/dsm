function start()
    spawnEnemyShip();
    setTimeout(10000);
end

function frame()
    if EnemyShip == nil then
        stop(false);
        return
    end
    shield = getShieldSystem(EnemyShip);
    if shield ~= nil then
        damageSystem(shield, 50);
    end
    scanner = getScannerSystem(EnemyShip);
    if scanner ~= nil then
        damageSystem(scanner, 50);
    end
    engine = getEngineSystem(EnemyShip);
    if engine ~= nil then
        damageSystem(engine, 50);
    end
    endTurn();
end
