function start()
    setTimeout(2000);
end

function frame()
    setAsteroids(true);
    if isAsteroids() == false then
        stop(false);
    end
    setClouds(true);
    if isAsteroids() == false or isClouds() == false then
        stop(false)
    end
    setStar(true);
    if isAsteroids() == false or isClouds() == false or isStar() == false then
        stop(false)
    end
    setStar(false);
    if isAsteroids() == false or isClouds() == false or isStar() == true then
        stop(false)
    end
    setClouds(false);
    if isAsteroids() == false or isClouds() == true or isStar() == true then
        stop(false)
    end
    setAsteroids(false);
    if isAsteroids() == true or isClouds() == true or isStar() == true then
        stop(false)
    end
    stop(true)
end
