function start()
    setTimeout(1000);
end

function frame()
    rm = getRM();
    addRM(50);
    if rm + 50 ~= getRM() then
        stop(false);
        return;
    end
    useRM(50);
    if rm ~= getRM() then
        stop(false);
        return;
    end

    fuel = getFuel();
    addFuel(50);
    if fuel + 50 ~= getFuel() then
        stop(false);
        return;
    end
    useFuel(50);
    if fuel ~= getFuel() then
        stop(false);
        return;
    end

    parts = getItemAmount("sensor_part");
    addItem("sensor_part", 2); 
    if parts + 2 ~= getItemAmount("sensor_part") then
        stop(false);
        return;
    end

    useItem("sensor_part", 2); 
    if parts ~= getItemAmount("sensor_part") then
        stop(false);
        return;
    end

    stop(true);
end

function timeout()
    return false;
end
