function start()
    setSpawnEnemyShips(false);
    setSafe(false);
    addTurnCallback("turn", "callback", 0);
    setTimeout(1000);
end

function frame()
    endTurn();
end

function callback()
    damageShip(PlayerShip, 50);
    addTurnCallback("turn", "callback", 1);
end

function gameover()
    return true;
end

function timeout()
    return false;
end
