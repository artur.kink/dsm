local int = 0;
local crew = nil;
function start()
    setTimeout(1000);
    int = 1;
    crew = getRandomCrew();
    addCrewVariable("crew", crew, true);
    addIntVariable("int", 1, true);
    addStringVariable("str", "test", true);
    setSpawnEnemyShips(false);
end

function frame()
    i = getIntVariable("int");
    int = i+1;
    addIntVariable("int", int);

    crewcopy = getCrewVariable("crew");
    removeVariable("crew");
    addCrewVariable("crew", crewcopy);

    endTurn();
end

function timeout()
    io.write("int var: " .. getIntVariable("int") .. " local: " .. int .. "\n");
    if getIntVariable("int") ~= int then
        return false;
    end

    io.write("crew var: " .. getCrewName(getCrewVariable("crew")) .. " local: " .. getCrewName(crew) .. "\n");
    if getCrewVariable("crew") ~= crew or crew == nil then
        io.write("fail\n");
        return false;
    end

    if getStringVariable("str") ~= "test" then
        io.write("fail\n");
        return false;
    end

    return true;
end
