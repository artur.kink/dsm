local turn;
function start()
    setTimeout(1000);
    setSpawnEnemyShips(false);
    turn = getTurnNumber();
end

function frame()
    if turn ~= getTurnNumber() then
        io.write("Saved Turn: " .. turn .. " Current: " .. getTurnNumber() .. "\n");
        stop(false);
    end
    endTurn();
    turn = turn + 1;
end

function timeout()
    io.write("Saved Turn: " .. turn .. " Current: " .. getTurnNumber() .. "\n");
    if turn < 1 or turn ~= getTurnNumber() then
        return false;
    end
    return true;
end
