function start()
    setTimeout(5000);
    setSpawnEnemyShips(false);
end

function frame()
    if EnemyShip ~= nil then
        io.write("Expecting no ship\n");
        stop(false);
        return;
    end
    spawnEnemyShip();
    warpEnemyShip();
end

function gameover()
    return false;
end
