function start()
    setTimeout(1000);
end

function frame()
    shieldAmount = getShipShield(PlayerShip);
    shield = getShieldSystem(PlayerShip);
    if shield == nil or shieldAmount == 0 then
        io.write("Expected shield\n");
        stop(false);
        return;
    end
    damageSystem(shield, 1000);
    -- todo: this should be changed to ~= 0 once fixed in game
    if getShipShield(PlayerShip) == 0 then
        io.write("Shield should be 0\n");
        stop(false);
        return;
    end
    stop(true);
end

function timeout()
    return false;
end
