-- This is a file of lua helper functions that
-- do not need to be implemented in the game source.


-- Get random player crew
function getRandomCrew()
    crews = getPlayerCrew()
    return crews[random(#crews)]
end
