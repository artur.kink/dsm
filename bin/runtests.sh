#!/bin/bash
#./test_content

runtest(){
    echo "Running test $1"
    timeout --signal=SIGINT --kill-after=60 60 ./game -t $@
    if [ "$?" -ne "0" ]
    then
        echo "Expected test $1 to terminate with 0"
        exit 1
    fi
    echo "Done tests $1"
}

for f in tests/*.lua
do
    runtest "$f" $@
done
