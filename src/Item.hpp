#ifndef _ITEM_HPP
#define _ITEM_HPP

class Item{
public:
    char* name;
    const char* printName;
    int amount;
    unsigned int icon;
    int recycle;
    char* system;
    int rarity;
    int price;

    Item(){
        name = NULL;
        printName = NULL;
        system = NULL;
        amount = 0;
        icon = 0;
        recycle = 0;
        rarity = 0;
        price = 0;
    }
};


#endif
