#include "Game.hpp"

#include <iostream>
#include <signal.h>

Game* game;

static void interrupt_handler(int signal){
    static int stopCount = 0;
    if(stopCount > 0)
        exit(1);
    game->stop();
    stopCount++;
}

int main(int argc, char** argv){

    std::cout << "Starting main...\n";
    if(signal(SIGINT, interrupt_handler) == SIG_ERR){
        return 1;
    }

    game = new Game();
    for(int i = 1; i < argc; i++){
        if(strcmp(argv[i], "-t") == 0){
            if(++i < argc){
                game->setTest(argv[i]);
            }else{
                std::cout << "Expected file argument with -t\n";
                return 1;
            }
        }else if(strcmp(argv[i], "-nodraw") == 0){
            game->setNoDraw();
            std::cout << "No draw mode\n";
        }
    }

    int status = game->init();
    if(status == 0)
        status = game->start();
    std::cout << "Terminating main...\n";
    return status;
}
