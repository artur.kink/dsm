#ifndef _CREW_HPP
#define _CREW_HPP

#include "SDLIncludes.hpp"

#include "FileManager.hpp"

#include "glm/vec2.hpp"
#include <vector>
#include <cstdint>
#include <cstdlib>

#include "Globals.hpp"

class Ship;

class Skill{
public:
    const int firstLevelExp = 50;
    Skill(){
        level = 0;
        exp = 0;
        levelExp = firstLevelExp;
    }
    int exp;
    int levelExp;
    int level;

    const float expGrowth = 1.5f;
    const int maxLevel = 10;

    /**
     * Increase skill by amount of exp given.
     * @return true if leveled up
     */
    bool increase(int amount){
        if(amount <= 0)
            return false;
        if(level >= maxLevel)
            return false;
        exp += amount;
        if(exp >= levelExp){
            level++;
            levelExp += (int)((float)levelExp * expGrowth);
            return true;
        }
        return false;
    }

    void setLevel(int l){
        exp = firstLevelExp;
        if(l > maxLevel)
            l = maxLevel;
        level = l;
        for(int i = 0; i < l; i++){
            levelExp += (int)((float)levelExp * expGrowth);
        }
    }
};

static const char* SkillNames[5] = {
    "Combat",
    "Command",
    "Operations",
    "Engineering",
    "Science"
};

class Crew{
public:
    SDL_Point position;
    glm::vec2 drawPosition;
    bool turnDone;
    bool lockedToStation;
    int sprite;

    int health;
    int maxHealth;

    uint8_t walkDistance;
    uint8_t maxWalkDistance;

    char name[20];

    bool moving;
    enum Actions{
        None = 0,
        Attacking = 2,
        Repairing = 3,
        Healing = 4
    };
    bool playerCrew;
    bool hostile;

    Actions action;
    Actions repeatAction;

    int actionTime;
    int actionAmount;
    int actionTick;
    std::vector<SDL_Point> moveMap;

    SDL_Point targetPosition;

    Ship *ship;

    enum Skills{
        Combat = 0,
        Command = 1,
        Operations = 2,
        Engineering = 3,
        Science = 4,
        NumSkills = 5
    };

    //Skills
    Skill skills[Skills::NumSkills];

    std::vector<int> proficiencies;

    Crew();
    Crew(const char* n);
    void damage(int amount);
    void clearTurn();
    void setPosition(const SDL_Point& p);

    void saveData(FileManager* file);
    void loadData(FileManager* file);
};

#endif
