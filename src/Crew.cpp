#include "Crew.hpp"

Crew::Crew(){
    maxWalkDistance = 8;
    maxHealth = 50;
    ship = NULL;
    moving = false;
    action = Actions::None;
    repeatAction = Actions::None;
    lockedToStation = false;
    hostile = false;
};

Crew::Crew(const char* n){
    strcpy(name, n);
    turnDone = false;
    lockedToStation = false;
    moving = false;
    action = Actions::None;
    repeatAction = Actions::None;

    health = maxHealth = 50;
    
    playerCrew = true;
    hostile = false;
    ship = NULL;

    maxWalkDistance = 8;
    walkDistance = maxWalkDistance;
}

void Crew::setPosition(const SDL_Point& p){
    position = p;
    drawPosition.x = p.x*TILESIZE;
    drawPosition.y = p.y*TILESIZE;
}

void Crew::damage(int amount){
    if(amount > health){
        health = 0;
    }else{
        health -= amount;
    }
}

void Crew::clearTurn(){
    turnDone = false;
    lockedToStation = false;
    walkDistance = maxWalkDistance;
}

void Crew::saveData(FileManager* file){
    file->saveString(name);
    file->saveBool(playerCrew);
    file->saveBool(hostile);
    file->saveSDLPoint(position);
    file->saveBool(turnDone);
    file->saveBool(lockedToStation);
    file->saveInt32(health);
    file->saveInt32(maxHealth);

    file->saveInt8(walkDistance);
    file->saveInt8(maxWalkDistance);

    file->saveInt32(sprite);

    for(int i = 0; i < Crew::NumSkills; i++){
        Skill& skill = skills[i];
        file->saveInt32(skill.exp);
        file->saveInt32(skill.levelExp);
        file->saveInt32(skill.level);
    }
}

void Crew::loadData(FileManager* file){
    file->loadString(name);

    playerCrew = file->loadBool();
    hostile = file->loadBool();
    position = file->loadSDLPoint();
    drawPosition.x = position.x*32;
    drawPosition.y = position.y*32;
    turnDone = file->loadBool();
    lockedToStation = file->loadBool();
    health = file->loadInt32();
    maxHealth = file->loadInt32();

    walkDistance = file->loadInt8();
    maxWalkDistance = file->loadInt8();

    sprite = file->loadInt32();

    for(int i = 0; i < Crew::NumSkills; i++){
        Skill& skill = skills[i];
        skill.exp = file->loadInt32();
        skill.levelExp = file->loadInt32();
        skill.level = file->loadInt32();
    }
}
