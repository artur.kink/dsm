#include "AudioManager.hpp"

AudioManager::AudioManager(){
    autoPlayMusic = false;
    musicCounter++;
    initialized = false;
}

void AudioManager::init(){
    if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0){
        SDL_Log("Mix_OpenAudo: %s", Mix_GetError());
        return;
    }

    int reserve = Mix_ReserveChannels(RESERVE_CHANNELS);
    if(reserve != RESERVE_CHANNELS)
        SDL_Log("Failed to reserve audio channels");
    initialized = true;
    soundEnabled = true;
}

void AudioManager::load(const char* fileName){
    if(!initialized)
        return;
    char* text = FileManager::loadFileText(fileName);
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* rootNode = doc.first_node("audio");
    for(rapidxml::xml_node<>* audioNode = rootNode->first_node(); audioNode; audioNode = audioNode->next_sibling()){
        if(strcmp("music", audioNode->name()) == 0){
            Mix_Music* music = Mix_LoadMUS(audioNode->first_attribute("file")->value());
            if(music)
                addMusic(audioNode->first_attribute("name")->value(), music);
            else
                SDL_Log("Failed to load music %s", audioNode->first_attribute("file")->value());
        }else if(strcmp("sound", audioNode->name()) == 0){
            Mix_Chunk* chunk = Mix_LoadWAV(audioNode->first_attribute("file")->value());
            if(chunk)
                addSound(audioNode->first_attribute("name")->value(), chunk);
            else
                SDL_Log("Failed to load sound %s", audioNode->first_attribute("file")->value());
        }
    }

    delete text;
}

void AudioManager::cleanup(){
    if(initialized){
        Mix_HaltMusic(); 
        Mix_HaltChannel(-1);
    }
}

void AudioManager::addMusic(const char* name, Mix_Music* m){
    music.insert(std::pair<std::string, Mix_Music*>(name, m));
}

void AudioManager::addSound(const char* name, Mix_Chunk* sound){
    sounds.insert(std::pair<std::string, Mix_Chunk*>(name, sound));
}

void AudioManager::update(){
    if(!initialized)
        return;
    if(autoPlayMusic && Mix_PlayingMusic() == 0){
        auto it = music.begin();
        std::advance(it, musicCounter);
        Mix_PlayMusic((*it).second, 0);
        musicCounter++;
        if(musicCounter >= music.size())
            musicCounter = 0;
    }
}

void AudioManager::playAllMusic(){
    if(music.empty())
        autoPlayMusic = false;
    else
        autoPlayMusic = true;
}

void AudioManager::stopMusic(){
    autoPlayMusic = false;
    Mix_HaltMusic(); 
}

bool AudioManager::isMusicPlaying(){
    return autoPlayMusic;
}

void AudioManager::playMusic(const char* name){
    if(!initialized)
        return;
    Mix_Music* m = music[(char*)name];
    if(m == NULL)
        SDL_Log("Failed to find music %s", name);
    else if(Mix_PlayMusic(m, -1) == -1)
        SDL_Log("Failed to play music: %s", Mix_GetError());
}

bool AudioManager::isSoundEnabled(){
    return soundEnabled;
}

void AudioManager::enableSound(bool enable){
    soundEnabled = enable;
}

void AudioManager::playSound(const char* name, int channel, bool checkAvailable){
    if(!initialized || !soundEnabled)
        return;
    Mix_Chunk* sound = sounds[(char*)name];
    if(sound == NULL)
        SDL_Log("Failed to find sound %s", name);
    else{
        if(checkAvailable && channel != -1 && Mix_Playing(channel) != 0)
            return;
        if(Mix_PlayChannel(channel, sound, 0) == -1)
            SDL_Log("Failed to play sound: %s", Mix_GetError());
    }
}

AudioManager::~AudioManager(){

}
