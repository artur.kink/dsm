#include "LuaHandler.hpp"
#include "Game.hpp"
#include "FileManager.hpp"

LuaHandler::LuaHandler(){
    luaState = NULL;
}

void LuaHandler::init(){
    if(luaState)
        lua_close(luaState);
    luaState = luaL_newstate();
    luaL_openlibs(luaState);

    lua_pushcfunction(luaState, l_removeEnemyShip);
    lua_setglobal(luaState, "removeEnemyShip");
    lua_pushcfunction(luaState, l_spawnEnemyShip);
    lua_setglobal(luaState, "spawnEnemyShip");
    lua_pushcfunction(luaState, l_warpEnemyShip);
    lua_setglobal(luaState, "warpEnemyShip");
    lua_pushcfunction(luaState, l_showDialog);
    lua_setglobal(luaState, "showDialog");
    lua_pushcfunction(luaState, l_addOption);
    lua_setglobal(luaState, "addOption");
    lua_pushcfunction(luaState, l_addCrewSelectOption);
    lua_setglobal(luaState, "addCrewSelectOption");
    lua_pushcfunction(luaState, l_addItem);
    lua_setglobal(luaState, "addItem");
    lua_pushcfunction(luaState, l_addRM);
    lua_setglobal(luaState, "addRM");
    lua_pushcfunction(luaState, l_addFuel);
    lua_setglobal(luaState, "addFuel");
    lua_pushcfunction(luaState, l_giveReward);
    lua_setglobal(luaState, "giveReward");
    lua_pushcfunction(luaState, l_useItem);
    lua_setglobal(luaState, "useItem");
    lua_pushcfunction(luaState, l_useRM);
    lua_setglobal(luaState, "useRM");
    lua_pushcfunction(luaState, l_useFuel);
    lua_setglobal(luaState, "useFuel");
    lua_pushcfunction(luaState, l_getItemAmount);
    lua_setglobal(luaState, "getItemAmount");
    lua_pushcfunction(luaState, l_getFuel);
    lua_setglobal(luaState, "getFuel");
    lua_pushcfunction(luaState, l_getRM);
    lua_setglobal(luaState, "getRM");
    lua_pushcfunction(luaState, l_onHail);
    lua_setglobal(luaState, "onHail");
    lua_pushcfunction(luaState, l_onDestroy);
    lua_setglobal(luaState, "onDestroy");
    lua_pushcfunction(luaState, l_onFlee);
    lua_setglobal(luaState, "onFlee");
    lua_pushcfunction(luaState, l_onAttack);
    lua_setglobal(luaState, "onAttack");
    lua_pushcfunction(luaState, l_onScan);
    lua_setglobal(luaState, "onScan");
    lua_pushcfunction(luaState, l_addTurnCallback);
    lua_setglobal(luaState, "addTurnCallback");
    lua_pushcfunction(luaState, l_removeTurnCallback);
    lua_setglobal(luaState, "removeTurnCallback");
    lua_pushcfunction(luaState, l_createEncounter);
    lua_setglobal(luaState, "createEncounter");
    lua_pushcfunction(luaState, l_cancelWarp);
    lua_setglobal(luaState, "cancelWarp");
    lua_pushcfunction(luaState, l_beginEncounter);
    lua_setglobal(luaState, "beginEncounter");
    lua_pushcfunction(luaState, l_removeEncounters);
    lua_setglobal(luaState, "removeEncounters");
    lua_pushcfunction(luaState, l_setEnemyHostile);
    lua_setglobal(luaState, "setEnemyHostile");
    lua_pushcfunction(luaState, l_getCrewName);
    lua_setglobal(luaState, "getCrewName");
    lua_pushcfunction(luaState, l_findProficiencyCrew);
    lua_setglobal(luaState, "findProficiencyCrew");
    lua_pushcfunction(luaState, l_getPlayerCrew);
    lua_setglobal(luaState, "getPlayerCrew");
    lua_pushcfunction(luaState, l_addCrewVariable);
    lua_setglobal(luaState, "addCrewVariable");
    lua_pushcfunction(luaState, l_getCrewVariable);
    lua_setglobal(luaState, "getCrewVariable");
    lua_pushcfunction(luaState, l_addSystemVariable);
    lua_setglobal(luaState, "addSystemVariable");
    lua_pushcfunction(luaState, l_addIntVariable);
    lua_setglobal(luaState, "addIntVariable");
    lua_pushcfunction(luaState, l_getIntVariable);
    lua_setglobal(luaState, "getIntVariable");
    lua_pushcfunction(luaState, l_addStringVariable);
    lua_setglobal(luaState, "addStringVariable");
    lua_pushcfunction(luaState, l_getStringVariable);
    lua_setglobal(luaState, "getStringVariable");
    lua_pushcfunction(luaState, l_removeVariable);
    lua_setglobal(luaState, "removeVariable");
    lua_pushcfunction(luaState, l_killCrew);
    lua_setglobal(luaState, "killCrew");
    lua_pushcfunction(luaState, l_getCrewHealth);
    lua_setglobal(luaState, "getCrewHealth");
    lua_pushcfunction(luaState, l_damageCrew);
    lua_setglobal(luaState, "damageCrew");
    lua_pushcfunction(luaState, l_healCrew);
    lua_setglobal(luaState, "healCrew");
    lua_pushcfunction(luaState, l_hasProficiency);
    lua_setglobal(luaState, "hasProficiency");
    lua_pushcfunction(luaState, l_getCrewProficiencyLevel);
    lua_setglobal(luaState, "getCrewProficiencyLevel");
    lua_pushcfunction(luaState, l_increaseCrewProficiencyLevel);
    lua_setglobal(luaState, "increaseCrewProficiencyLevel");
    lua_pushcfunction(luaState, l_createPlayerCrew);
    lua_setglobal(luaState, "createPlayerCrew");
    lua_pushcfunction(luaState, l_spawnEnemyCrew);
    lua_setglobal(luaState, "spawnEnemyCrew");
    lua_pushcfunction(luaState, l_damageShip);
    lua_setglobal(luaState, "damageShip");
    lua_pushcfunction(luaState, l_damageShipShield);
    lua_setglobal(luaState, "damageShipShield");
    lua_pushcfunction(luaState, l_damageShipArmor);
    lua_setglobal(luaState, "damageShipArmor");
    lua_pushcfunction(luaState, l_repairShip);
    lua_setglobal(luaState, "repairShip");
    lua_pushcfunction(luaState, l_regenShipShield);
    lua_setglobal(luaState, "regenShipShield");
    lua_pushcfunction(luaState, l_getShipArmor);
    lua_setglobal(luaState, "getShipArmor");
    lua_pushcfunction(luaState, l_getShipShield);
    lua_setglobal(luaState, "getShipShield");
    lua_pushcfunction(luaState, l_spawnFire);
    lua_setglobal(luaState, "spawnFire");
    lua_pushcfunction(luaState, l_removeFire);
    lua_setglobal(luaState, "removeFire");
    lua_pushcfunction(luaState, l_spawnAsteroid);
    lua_setglobal(luaState, "spawnAsteroid");
    lua_pushcfunction(luaState, l_damageSystem);
    lua_setglobal(luaState, "damageSystem");
    lua_pushcfunction(luaState, l_repairSystem);
    lua_setglobal(luaState, "repairSystem");
    lua_pushcfunction(luaState, l_getShieldSystem);
    lua_setglobal(luaState, "getShieldSystem");
    lua_pushcfunction(luaState, l_getScannerSystem);
    lua_setglobal(luaState, "getScannerSystem");
    lua_pushcfunction(luaState, l_getEngineSystem);
    lua_setglobal(luaState, "getEngineSystem");
    lua_pushcfunction(luaState, l_getWeaponSystems);
    lua_setglobal(luaState, "getWeaponSystems");
    lua_pushcfunction(luaState, l_setSafe);
    lua_setglobal(luaState, "setSafe");
    lua_pushcfunction(luaState, l_setSpawnEnemyShips);
    lua_setglobal(luaState, "setSpawnEnemyShips");
    lua_pushcfunction(luaState, l_isAsteroids);
    lua_setglobal(luaState, "isAsteroids");
    lua_pushcfunction(luaState, l_setAsteroids);
    lua_setglobal(luaState, "setAsteroids");
    lua_pushcfunction(luaState, l_isClouds);
    lua_setglobal(luaState, "isClouds");
    lua_pushcfunction(luaState, l_setClouds);
    lua_setglobal(luaState, "setClouds");
    lua_pushcfunction(luaState, l_isStar);
    lua_setglobal(luaState, "isStar");
    lua_pushcfunction(luaState, l_setStar);
    lua_setglobal(luaState, "setStar");
    lua_pushcfunction(luaState, l_exploreStarMap);
    lua_setglobal(luaState, "exploreStarMap");
    

    lua_pushcfunction(luaState, l_playSound);
    lua_setglobal(luaState, "playSound");
    lua_pushcfunction(luaState, l_flash);
    lua_setglobal(luaState, "flash");
    lua_pushcfunction(luaState, l_random);
    lua_setglobal(luaState, "random");
    lua_pushcfunction(luaState, l_getTurnNumber);
    lua_setglobal(luaState, "getTurnNumber");

    // Lua Debug and test functions
    lua_pushcfunction(luaState, l_changeLanguage);
    lua_setglobal(luaState, "changeLanguage");
    lua_pushcfunction(luaState, l_increaseDifficulty);
    lua_setglobal(luaState, "increaseDifficulty");
    lua_pushcfunction(luaState, l_reset);
    lua_setglobal(luaState, "reset");
    lua_pushcfunction(luaState, l_endTurn);
    lua_setglobal(luaState, "endTurn");
    lua_pushcfunction(luaState, l_setTimeout);
    lua_setglobal(luaState, "setTimeout");
    lua_pushcfunction(luaState, l_stop);
    lua_setglobal(luaState, "stop");
    lua_pushcfunction(luaState, l_saveGame);
    lua_setglobal(luaState, "saveGame");
    lua_pushcfunction(luaState, l_loadGame);
    lua_setglobal(luaState, "loadGame");
    lua_pushcfunction(luaState, l_newGame);
    lua_setglobal(luaState, "newGame");

    loadScript("helpers.lua");
}

/**
 * Load Lua script with given name
 */
bool LuaHandler::loadScript(const char* name){
    char* scriptText = FileManager::loadFileText(name);
    if(!scriptText){
        return false;
    }
    if(luaL_loadstring(luaState, scriptText)){
        SDL_Log("Failed to load lua script %s", name);
        delete scriptText;
        return false;
    }
    delete scriptText;
    if(lua_pcall(luaState, 0, 0, 0)){
        SDL_Log("Failed to parse lua script %s", name);
        return false;
    }
    return true;
}

/**
 * Find function and push onto stack.
 * @return true if found, false otherwise
 */
bool LuaHandler::getLuaFunction(const char* name){
    char module[50];
    const char* func = NULL;
    int i = 0;
    while(name[i] != '\0'){
        if(name[i] == '.'){
            module[i] = '\0';
            func = (name + i + 1);
            break;
        }
        module[i] = name[i];
        i++;
    }
    if(func != NULL){
        lua_getglobal(luaState, module);
        if(lua_gettop(luaState) == 0){
            lua_remove(luaState, -1);
            return false;
        }
        lua_getfield(luaState, -1, func);
        lua_remove(luaState, -2);
    }else{
        lua_getglobal(luaState, name);
    }
    if(lua_gettop(luaState) == 0)
        return false;
    if(lua_isfunction(luaState,lua_gettop(luaState))){
        return true;
    }
    lua_remove(luaState, -1);
    return false;
}

int LuaHandler::executeCommand(const char* command){
    int status = luaL_loadstring(luaState, command);
    if(status){
        SDL_Log("Failed to load lua string");
    }else{
        status = lua_pcall(luaState, 0, LUA_MULTRET, 0);
        SDL_Log("lua command result: %d", status);
    }
    return status;
}

/**
 * Set PlayerShip global for Lua
 */
void LuaHandler::setPlayerShip(Ship* ship){
    assert(ship != NULL);
    lua_pushlightuserdata(luaState, ship);
    lua_setglobal(luaState, "PlayerShip");
}

/**
 * Set EnemyShip global for Lua
 */
void LuaHandler::setEnemyShip(Ship* ship){
    if(ship == NULL){
        lua_pushnil(luaState);
        lua_setglobal(luaState, "EnemyShip");
    }else{
        lua_pushlightuserdata(luaState, ship);
        lua_setglobal(luaState, "EnemyShip");
    }
}

// Lua bindings
int LuaHandler::l_spawnEnemyShip(lua_State* state){
    const ShipDescription* description = NULL;
    if(lua_gettop(state) == 1){
        description = Game::game->shipLoader.getShipDescription(lua_tostring(state, -1));
    }
    Game::game->spawnEnemyShip(description);
    return 0;
}

int LuaHandler::l_removeEnemyShip(lua_State* state){
    if(Game::game->enemyShip){
        Game::game->removeShip(Game::game->enemyShip);
        Game::game->enemyShip = NULL;
        Game::game->centerViews();
    }
    return 0;
}

int LuaHandler::l_warpEnemyShip(lua_State* state){
    Game::game->warpEnemyShip();
    return 0;
}

int LuaHandler::l_addOption(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -nargs);
    const char* callback = lua_tostring(state, -(nargs-1));
    const char* text = lua_tostring(state, -(nargs-2));
    std::vector<const char*> variables;
    for(int i = nargs-2; i > 0; i--){
        variables.push_back(lua_tostring(state, -i));
    }
    Game::game->addOption(name, text, callback, &variables);
    return 0;
}

int LuaHandler::l_addCrewSelectOption(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -(nargs));
    int numCrew = lua_tonumber(state, -(nargs-1));
    const char* callback = lua_tostring(state, -(nargs-2));
    const char* text = lua_tostring(state, -(nargs-3));
    std::vector<const char*> variables;
    for(int i = nargs-2; i > 0; i--){
        variables.push_back(lua_tostring(state, -i));
    }
    Game::game->addCrewSelectOption(name, text, numCrew, callback, &variables);
    return 0;
}

int LuaHandler::l_showDialog(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -nargs);
    const char* dialog = lua_tostring(state, -(nargs-1));
    std::vector<const char*> variables;
    for(int i = nargs-2; i > 0; i--){
        variables.push_back(lua_tostring(state, -i));
    }
    Game::game->showDialog(name, dialog, &variables);
    return 0;
}

int LuaHandler::l_addItem(lua_State* state){
    const char* name = lua_tostring(state, -2);
    int amount = lua_tointeger(state, -1);
    Game::game->addItem(name, amount, true);
    return 0;
}

int LuaHandler::l_useItem(lua_State* state){
    const char* name = lua_tostring(state, -2);
    int amount = lua_tointeger(state, -1);
    Game::game->useItem(name, amount, true);
    return 0;
}

int LuaHandler::l_useRM(lua_State* state){
    int amount = lua_tointeger(state, -1);
    Game::game->useRM(amount);
    Game::game->gainedRM -= amount;
    return 0;
}

int LuaHandler::l_useFuel(lua_State* state){
    int amount = lua_tointeger(state, -1);
    Game::game->useFuel(amount);
    Game::game->gainedFuel -= amount;
    return 0;
}

int LuaHandler::l_addRM(lua_State* state){
    int amount = lua_tointeger(state, -1);
    Game::game->addRM(amount);
    Game::game->gainedRM += amount;
    return 0;
}

int LuaHandler::l_addFuel(lua_State* state){
    int amount = lua_tointeger(state, -1);
    Game::game->addFuel(amount);
    Game::game->gainedFuel += amount;
    return 0;
}

int LuaHandler::l_giveReward(lua_State* state){
    Game::game->giveReward();
    return 0;
}

int LuaHandler::l_getItemAmount(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Item* item = Game::game->findInventoryItem(name);
    if(item == NULL)
        lua_pushnumber(state, 0);
    else
        lua_pushnumber(state, item->amount);
    return 1;
}

int LuaHandler::l_getRM(lua_State* state){
    lua_pushnumber(state, Game::game->getRM());
    return 1;
}

int LuaHandler::l_getFuel(lua_State* state){
    lua_pushnumber(state, Game::game->getFuel());
    return 1;
}

int LuaHandler::l_onHail(lua_State* state){
    const char* callback = lua_tostring(state, -1);
    if(callback != NULL)
        strcpy(Game::game->hailCallback, callback);
    else
        Game::game->hailCallback[0] = '\0';
    return 0;
}

int LuaHandler::l_onDestroy(lua_State* state){
    const char* callback = lua_tostring(state, -1);
    if(callback != NULL)
        strcpy(Game::game->destroyCallback, callback);
    else
        Game::game->destroyCallback[0] = '\0';
    return 0;
}

int LuaHandler::l_onFlee(lua_State* state){
    const char* callback = lua_tostring(state, -1);
    if(callback != NULL)
        strcpy(Game::game->fleeCallback, callback);
    else
        Game::game->fleeCallback[0] = '\0';
    return 0;
}

int LuaHandler::l_onAttack(lua_State* state){
    const char* callback = lua_tostring(state, -1);
    if(callback != NULL)
        strcpy(Game::game->attackCallback, callback);
    else
        Game::game->attackCallback[0] = '\0';
    return 0;
}

int LuaHandler::l_onScan(lua_State* state){
    const char* callback = lua_tostring(state, -1);
    if(callback != NULL)
        strcpy(Game::game->scanCallback, callback);
    else
        Game::game->scanCallback[0] = '\0';
    return 0;
}

int LuaHandler::l_addTurnCallback(lua_State* state){
    const char* name = lua_tostring(state, -3);
    const char* callback = lua_tostring(state, -2);
    int turns = lua_tointeger(state, -1);
    Game::game->addTurnCallback(name, callback, Game::game->turnCounter + turns);
    return 0;
}

int LuaHandler::l_removeTurnCallback(lua_State* state){
    const char* name = lua_tostring(state, -1);
    auto callbackIt = Game::game->turnCallbacks.begin();
    while(callbackIt!= Game::game->turnCallbacks.end()){
        Game::TurnCallback& callback = *callbackIt;
        if(strcmp(callback.name, name) == 0){
            callbackIt = Game::game->turnCallbacks.erase(callbackIt);
        }else{
            ++callbackIt;
        }
    }
    return 0;
}

int LuaHandler::l_createEncounter(lua_State* state){
    const char* name = lua_tostring(state, -3);
    int min = lua_tointeger(state, -2);
    int max = lua_tointeger(state, -1);
    Game::game->createEncounter(name, min, max);
    return 0;
}

int LuaHandler::l_removeEncounters(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Game::game->removeEncounters(name);
    return 0;
}

int LuaHandler::l_cancelWarp(lua_State* state){
    Game::game->cancelWarp();
    return 0;
}

int LuaHandler::l_beginEncounter(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Encounter* encounter = Game::game->encounterManager.getEncounter(name);
    if(encounter){
        bool result = Game::game->beginEncounter(encounter);
        lua_pushboolean(state, result);
    }else{
        lua_pushboolean(state, false);
    }
    return 1;
}

int LuaHandler::l_setEnemyHostile(lua_State* state){
    bool hostile = lua_toboolean(state, -1);
    Game::game->setEnemyHostile(hostile, Game::game->enemyShip);
    Game::game->isSafe = false; // Re-evaluate safeness
    return 0;
}

int LuaHandler::l_getCrewName(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -1);
    lua_pushstring(state, crew->name);
    return 1;
}

int LuaHandler::l_findProficiencyCrew(lua_State* state){
    const char* proficiency = lua_tostring(state, -1);
    Crew* crew = Game::game->findProficiencyCrew(proficiency);
    if(crew == NULL)
        lua_pushnil(state);
    else
        lua_pushlightuserdata(state, crew);
    return 1;
}

int LuaHandler::l_getPlayerCrew(lua_State* state){
    lua_createtable(state, Game::game->playerCrews.size(), 0);
    for(int i = 0; i < Game::game->playerCrews.size(); i++){
        lua_pushnumber(state, i);
        lua_pushlightuserdata(state, Game::game->playerCrews[i]);
        lua_settable(state, -3);
    }
    return 1;
}

int LuaHandler::l_addCrewVariable(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -nargs);
    Crew* crew = (Crew*)lua_touserdata(state, -(nargs - 1));
    bool global = false;
    if(nargs == 3)
        global = lua_toboolean(state, -1);
    Game::game->addCrewVariable(name, crew, global);
    return 0;
}

int LuaHandler::l_getCrewVariable(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Variable* variable = Game::game->getVariable(name);
    if(variable == NULL)
        lua_pushnil(state); 
    else
        lua_pushlightuserdata(state, variable->crew);
    return 1;
}

int LuaHandler::l_addSystemVariable(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -nargs);
    System* system = (System*)lua_touserdata(state, -(nargs -1));
    bool global = false;
    if(nargs == 3)
        global = lua_toboolean(state, -1);
    Game::game->addSystemVariable(name, system, global);
    return 0;
}

int LuaHandler::l_addIntVariable(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -nargs);
    int i = lua_tonumber(state, -(nargs - 1));
    bool global = false;
    if(nargs == 3)
        global = lua_toboolean(state, -1);
    Game::game->addIntVariable(name, i, global);
    return 0;
}

int LuaHandler::l_getIntVariable(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Variable* variable = Game::game->getVariable(name);
    if(variable == NULL)
        lua_pushnil(state);
    else
        lua_pushnumber(state, variable->i);
    return 1;
}

int LuaHandler::l_addStringVariable(lua_State* state){
    int nargs = lua_gettop(state);
    const char* name = lua_tostring(state, -nargs);
    const char* str = lua_tostring(state, -(nargs - 1));
    bool global = false;
    if(nargs == 3)
        global = lua_toboolean(state, -1);
    Game::game->addStringVariable(name, str, global);
    return 0;
}

int LuaHandler::l_getStringVariable(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Variable* variable = Game::game->getVariable(name);
    if(variable == NULL)
        lua_pushnil(state);
    else
        lua_pushstring(state, variable->str);
    return 1;
}

int LuaHandler::l_removeVariable(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Game::game->removeVariable(name);
    return 0;
}

int LuaHandler::l_killCrew(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -1);
    Game::game->killCrew(crew);
    return 0;
}

int LuaHandler::l_hasProficiency(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -2);
    const char* name = lua_tostring(state, -1);
    lua_pushboolean(state, Game::game->hasProficiency(crew, name));
    return 1;
}

int LuaHandler::l_getCrewProficiencyLevel(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -2);
    const char* name = lua_tostring(state, -1);
    Proficiency* prof = Game::game->findProficiency(name);
    if(prof){
        lua_pushnumber(state, crew->skills[prof->skill].level); 
    }else{
        lua_pushnumber(state, 0);
    }
    return 1;
}

int LuaHandler::l_increaseCrewProficiencyLevel(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -3);
    const char* name = lua_tostring(state, -2);
    int amount = lua_tonumber(state, -1);
    Proficiency* prof = Game::game->findProficiency(name);
    if(prof){
        crew->skills[prof->skill].increase(amount);
    }
    return 0;
}

int LuaHandler::l_createPlayerCrew(lua_State* state){
    Crew* newCrew = NULL;
    while(newCrew == NULL){
        int position = Game::game->gameRandom.max(4);
        CrewDescription& crewd = Game::game->crews[position][Game::game->gameRandom.max(Game::game->crews[position].size())];

        bool found = false;
        for(int i = 0; i < Game::game->playerCrews.size(); i++){
            Crew* crew = Game::game->playerCrews[i];
            if(strcmp(crew->name, crewd.name) == 0){
                found = true;
                break;
            }
        }
        if(found)
            continue;

        newCrew = Game::game->createCrewFromDescription(crewd);
        Game::game->addCrew(newCrew, Game::game->ship, Game::game->findSpawnPoint(Game::game->ship), true);
    }
    lua_pushlightuserdata(state, newCrew);
    return 1;
}

int LuaHandler::l_spawnEnemyCrew(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    int amount = lua_tointeger(state, -1);
    int x = Game::game->gameRandom.max(ship->width);
    int y = Game::game->gameRandom.max(ship->height);
    for(int i = 0; i < amount; i++){
        SDL_Point spawnPoint = Game::game->findClosestFreePoint(ship, x, y);
        if(spawnPoint.x != -1){
            Crew* crew = Game::game->createRandomCrew();
            crew->hostile = true;
            Game::game->addCrew(crew, ship, spawnPoint, false);
        }else{
            break;
        }
    }
    Game::game->isSafe = false;
    return 0;
}

int LuaHandler::l_getCrewHealth(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -1);
    lua_pushnumber(state, crew->health);
    return 1;
}

int LuaHandler::l_damageCrew(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -2);
    int amount = lua_tointeger(state, -1);
    Game::game->damageCrew(crew, amount);
    return 0;
}

int LuaHandler::l_healCrew(lua_State* state){
    Crew* crew = (Crew*)lua_touserdata(state, -2);
    int amount = lua_tointeger(state, -1);
    crew->health += amount;
    if(crew->health > crew->maxHealth)
        crew->health = crew->maxHealth;
    return 0;
}

int LuaHandler::l_damageShip(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    if(ship){
        int amount = lua_tointeger(state, -1);
        if(ship->shield->shield >= amount){
            ship->shield->shield -= amount;
            amount  = 0;
        }else{
            int newAmount = amount - ship->shield->shield;
            ship->shield->shield = 0;
            amount = newAmount;
        }

        if(ship->armor >= amount){
            ship->armor -= amount;
        }else{
            ship->armor = 0;
        }
        if(ship->armor == 0)
            Game::game->destroyShip(ship->playerShip);
    }
    return 0;
}

int LuaHandler::l_damageShipArmor(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    if(ship){
        int amount = lua_tointeger(state, -1);
        if(ship->playerShip)
            Game::game->gainedArmor -= amount;
        if(ship->armor >= amount)
            ship->armor -= amount;
        else
            ship->armor = 0;

        if(ship->armor == 0)
            Game::game->destroyShip(ship->playerShip);
    }
    return 0;
}

int LuaHandler::l_damageShipShield(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    if(ship && ship->shield){
        int amount = lua_tointeger(state, -1);
        if(ship->shield->shield >= amount)
            ship->shield->shield -= amount;
        else
            ship->shield->shield = 0;
    }
    return 0;
}

int LuaHandler::l_repairShip(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    if(ship){
        int amount = lua_tointeger(state, -1);
        Game::game->repairShip(ship, amount);
    }
    return 0;
}

int LuaHandler::l_regenShipShield(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    if(ship && ship->shield){
        int amount = lua_tointeger(state, -1);
        if(ship->getShield() + amount > ship->getShieldMax())
            ship->shield->shield = ship->getShieldMax();
        else
            ship->shield->shield += amount;
    }
    return 0;
}

int LuaHandler::l_getShipArmor(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    lua_pushnumber(state, ship->armor);
    return 1;
}

int LuaHandler::l_getShipShield(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    if(ship->shield)
        lua_pushnumber(state, ship->shield->shield);
    else
        lua_pushnumber(state, 0);
    return 1;
}

int LuaHandler::l_spawnFire(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -2);
    int size = lua_tointeger(state, -1);
    int x = Game::game->gameRandom.max(ship->width);
    int y = Game::game->gameRandom.max(ship->height);
    for(int i = 0; i < size; i++){
        SDL_Point firePoint = Game::game->findClosestFreePoint(ship, x, y);
        if(firePoint.x != -1)
            ship->getTile(firePoint.x, firePoint.y).setFire();
        else
            break;
    }
    Game::game->isSafe = false;
    return 0;
}

int LuaHandler::l_removeFire(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            Tile& tile = ship->getTile(x, y);
            if(tile.fire != 0)
                tile.fire = 0;
        }
    }
    return 0;
}

int LuaHandler::l_spawnAsteroid(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    Game::game->spawnAsteroid(ship);
    Game::game->isSafe = false;
    return 0;
}

int LuaHandler::l_damageSystem(lua_State* state){
    System* system = (System*)lua_touserdata(state, -2);
    int amount = lua_tointeger(state, -1);
    // Todo Damage should disable system if destroyed
    Game::game->damageSystem(system, amount);
    return 1;
}

int LuaHandler::l_repairSystem(lua_State* state){
    System* system = (System*)lua_touserdata(state, -2);
    int amount = lua_tointeger(state, -1);
    // Todo Should enable system if health was 0 
    Game::game->repairSystem(system, amount);
    return 1;
}

int LuaHandler::l_getShieldSystem(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    if(ship->shield)
        lua_pushlightuserdata(state, ship->shield);
    else
        lua_pushnil(state);
    return 1;
}

int LuaHandler::l_getScannerSystem(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    if(ship->scanner)
        lua_pushlightuserdata(state, ship->scanner);
    else
        lua_pushnil(state);
    return 1;
}

int LuaHandler::l_getEngineSystem(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    if(ship->engine)
        lua_pushlightuserdata(state, ship->engine);
    else
        lua_pushnil(state);
    return 1;
}

int LuaHandler::l_getWeaponSystems(lua_State* state){
    Ship* ship = (Ship*)lua_touserdata(state, -1);
    lua_createtable(state, ship->weapons.size(), 0);
    for(int i = 0; i < ship->weapons.size(); i++){
        lua_pushnumber(state, i);
        lua_pushlightuserdata(state, ship->weapons[i]);
        lua_settable(state, -3);
    }
    return 1;
}

int LuaHandler::l_setSafe(lua_State* state){
    Game::game->isPositionSafe = lua_toboolean(state, -1);
    Game::game->isSafe = false; // Re-evaluate safeness
    return 0;
}

int LuaHandler::l_setSpawnEnemyShips(lua_State* state){
    Game::game->spawnEnemyShips = lua_toboolean(state, -1);
    return 0;
}

int LuaHandler::l_isAsteroids(lua_State* state){
    if(Game::game->starMap->getTile(Game::game->mapPosition).isAsteroids())
        lua_pushboolean(state, true);
    else
        lua_pushboolean(state, false);
    return 1;
}

int LuaHandler::l_setAsteroids(lua_State* state){
    bool set = lua_toboolean(state, -1);
    Game::game->starMap->getTile(Game::game->mapPosition).setType(StarMap::smt_Asteroids, set);
    Game::game->createBackground();
    Game::game->isSafe = false; // Re-evaluate safeness
    return 0;
}

int LuaHandler::l_isClouds(lua_State* state){
    if(Game::game->starMap->getTile(Game::game->mapPosition).isClouds())
        lua_pushboolean(state, true);
    else
        lua_pushboolean(state, false);
    return 1;
}

int LuaHandler::l_setClouds(lua_State* state){
    bool set = lua_toboolean(state, -1);
    Game::game->starMap->getTile(Game::game->mapPosition).setType(StarMap::smt_Clouds, set);
    Game::game->createBackground();
    return 0;
}

int LuaHandler::l_isStar(lua_State* state){
    if(Game::game->starMap->getTile(Game::game->mapPosition).isStar())
        lua_pushboolean(state, true);
    else
        lua_pushboolean(state, false);
    return 1;
}

int LuaHandler::l_setStar(lua_State* state){
    bool set = lua_toboolean(state, -1);
    Game::game->starMap->getTile(Game::game->mapPosition).setType(StarMap::smt_Star, set);
    Game::game->createBackground();
    return 0;
}

int LuaHandler::l_exploreStarMap(lua_State* state){
    int near = lua_tonumber(state, -2);
    int far = lua_tonumber(state, -1);
    Game::game->exploreStarMap(Game::game->starMap, Game::game->mapPosition.x, Game::game->mapPosition.y, near, far);
    return 0;
}

int LuaHandler::l_playSound(lua_State* state){
    const char* name = lua_tostring(state, -1);
    Game::game->audioManager.playSound(name);
    return 0;
}

int LuaHandler::l_flash(lua_State* state){
    int duration = lua_tonumber(state, -4);
    uint8_t r = (uint8_t)lua_tonumber(state, -3);
    uint8_t g = (uint8_t)lua_tonumber(state, -2);
    uint8_t b = (uint8_t)lua_tonumber(state, -1);
    SDL_Color color = {r, g, b, 255};
    Game::game->flash(duration, color);
    return 0;
}

int LuaHandler::l_random(lua_State* state){
    int nargs = lua_gettop(state);
    if(nargs == 2){
        int min = lua_tonumber(state, -2);
        int max = lua_tonumber(state, -1);
        lua_pushnumber(state, Game::game->gameRandom.range(min, max));
    }else{
        int max = lua_tonumber(state, -1);
        lua_pushnumber(state, Game::game->gameRandom.max(max));
    }
    return 1;
}

int LuaHandler::l_getTurnNumber(lua_State* state){
    lua_pushnumber(state, Game::game->turnCounter);
    return 1;
}

// Lua Debug functions

int LuaHandler::l_changeLanguage(lua_State* state){
    const char* lang = lua_tostring(state, -1);
    StringLibrary::instance->setLanguage(lang);
    Game::game->loadText();
    return 0;
}

int LuaHandler::l_increaseDifficulty(lua_State* state){
    Game::game->difficulty.increase(lua_tonumber(state, -1));
    return 0;
}

int LuaHandler::l_reset(lua_State* state){
    Game::game->resetLua = true;
    return 0;
}

int LuaHandler::l_endTurn(lua_State* state){
    Game::game->endTurn();
    return 0;
}

int LuaHandler::l_setTimeout(lua_State* state){
    Game::game->setTimeout(lua_tonumber(state, -1));
    return 0;
}

int LuaHandler::l_stop(lua_State* state){
    if(lua_toboolean(state, -1) == false)
        Game::game->returnCode = 1;
    Game::game->stop();
    return 0;
}

int LuaHandler::l_saveGame(lua_State* state){
    Game::game->saveGame(lua_tostring(state, -1));
    return 0;
}

int LuaHandler::l_loadGame(lua_State* state){
    Game::game->clearGame();
    Game::game->loadGame(lua_tostring(state, -1));
    Game::game->startGame();
    return 0;
}

int LuaHandler::l_newGame(lua_State* state){
    Game::game->clearGame();
    Game::game->createPlayer();
    Game::game->newGame();
    return 0;
}
