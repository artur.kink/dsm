#ifndef _RANDOM_HPP
#define _RANDOM_HPP

#include <random>

class Random{
private:

    std::mt19937 rand;

public:

    void setSeed(int64_t s){
        rand.seed(s);
    }

    bool percentage(int p){
        return max(100) < p;
    }

    bool oneIn(int amount){
        return max(amount) == 0;
    }

    int max(int m){
        if(m == 0)
            return 0;
        std::uniform_int_distribution<> distribution(0, m-1);
        return distribution(rand);
    }

    int range(int min, int max){
        std::uniform_int_distribution<> distribution(min, max);
        return distribution(rand);
    }

    float floatRange(float min, float max){
        std::uniform_real_distribution<> distribution(min, max);
        return distribution(rand);
    }
};

#endif
