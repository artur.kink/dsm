#ifndef _PARTICLE_HPP
#define _PARTICLE_HPP

#include "SDLIncludes.hpp"
#include "glm/glm.hpp"

class Ship;

class Particle{
public:
    Ship* ship;
    glm::vec2 position;
    glm::vec2 velocity;
    int64_t start;
    int duration;
    SDL_Color color;

    Particle(Ship* ship, glm::vec2& pos, glm::vec2& vel, int64_t s, int dur, SDL_Color c){
        this->ship = ship;
        position = pos;
        velocity = vel;
        start = s;
        duration = dur;
        color = c;
    }

    glm::vec2 getPosition(int64_t frameTime){
        return position + velocity*(float)(frameTime-start);
    }
};

class ParticleManager{
private:

public:
    int numParticles;
    Particle** particles;
    static const int MAX_PARTICLES = 1000;

    ParticleManager();

    void update(int64_t frameTime);
    void addParticle(Particle* particle);
    

};

#endif
