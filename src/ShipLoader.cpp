#include "ShipLoader.hpp"

void ShipLoader::load(const char* fileName){
    char* text = FileManager::loadFileText(fileName);
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* shipsNode = doc.first_node("ships");

    for(rapidxml::xml_node<>* layoutNode = shipsNode->first_node("layout"); layoutNode; layoutNode = layoutNode->next_sibling("layout")){
        ShipLayout* layout = new ShipLayout();
        layout->width = atoi(layoutNode->first_attribute("width")->value());
        layout->height = atoi(layoutNode->first_attribute("height")->value());
        layout->tiles = new int*[layout->height];

        const char* nameAttr = layoutNode->first_attribute("name")->value();
        layout->name = new char[strlen(nameAttr)+1];
        strcpy(layout->name, nameAttr);
        const char* body = layoutNode->value();

        int row = 0;
        while(*body){
            while(*body && *body != '[')
                ++body;
            if(*body == '\0')
                break;
            ++body;
            if(row >= layout->height){
                SDL_Log("Ship layout '%s' width mismatch", layout->name);
                break;
            }
            layout->tiles[row] = new int[layout->width];
            int col = 0;
            while(*body && *body != ']'){
                //fprintf(stderr, "%c", *body);
                if(col >= layout->width){
                    SDL_Log("Ship layout '%s' width mismatch", layout->name);
                    break;
                }
                if(*body == ' '){
                    layout->tiles[row][col] = Tile::Empty;
                }else if(*body == 'e'){
                    layout->tiles[row][col] = Tile::Walkable;
                }else if(*body == 'w'){
                    layout->tiles[row][col] = (Tile::Walkable | Tile::Station_Weapons);
                }else if(*body == 'c'){
                    layout->tiles[row][col] = (Tile::Walkable | Tile::Station_Captain);
                }else if(*body == 'o'){
                    layout->tiles[row][col] = (Tile::Walkable | Tile::Station_Ops);
                }else if(*body == 'd'){
                    layout->tiles[row][col] = Tile::Walkable;
                    layout->doors.push_back(SDL_Point{col, row});
                }else if((*body >= '1' && *body <= '9') || 
                         *body == '!' || *body == '@' || *body == '#' || *body == '$' || *body == '%' ||
                         *body == '^' || *body == '&' || *body == '*' || *body == '('){
                    if(*body >= '1' && *body <= '9')
                        layout->tiles[row][col] = Tile::Walkable;
                    else
                        layout->tiles[row][col] = Tile::External;
                    auto it = layout->rooms.find(*body);
                    if(it != layout->rooms.end()){
                        SDL_Rect& room = it->second; 
                        if(room.x > col){
                            room.x = col;
                        }
                        if(room.y > row){
                            room.y = row;
                        }
                        if(room.w < col){
                            room.w = col;
                        }
                        if(room.h < row){
                            room.h = row;
                        }
                    }else{
                        SDL_Rect room = {col, row, col, row};
                        layout->rooms.insert(std::pair<char, SDL_Rect>(*body, room));
                    }
                }
                col++;
                ++body;
            }
            //fprintf(stderr, "\n");
            row++;
        }
        auto it = layout->rooms.begin();
        while(it != layout->rooms.end()){
            //SDL_Log("Room '%c' %d,%d %dx%d", it->first, it->second.x, it->second.y,
            //        it->second.w - it->second.x + 1, it->second.h - it->second.y + 1);
            it++;
        }
        layouts.push_back(layout);
    }

    for(rapidxml::xml_node<>* ship = shipsNode->first_node("ship"); ship; ship = ship->next_sibling("ship")){
        ShipDescription* description = new ShipDescription();
        const char* layoutAttr = ship->first_attribute("layout")->value();
        description->layout = NULL;
        for(int l = 0; l < layouts.size(); l++){
            if(strcmp(layouts[l]->name, layoutAttr) == 0){
                description->layout = layouts[l];
                break;
            }
        }
        if(description->layout == NULL){
            SDL_Log("Could not find ship layout '%s'", layoutAttr);
            continue;
        }
        const char* nameAttr = ship->first_attribute("name")->value();
        description->name = new char[strlen(nameAttr)+1];
        strcpy(description->name, nameAttr);
        description->armor = atoi(ship->first_attribute("armor")->value());
        description->power = atoi(ship->first_attribute("power")->value());
        if(ship->first_attribute("crew"))
            description->crew = atoi(ship->first_attribute("crew")->value());

        for(rapidxml::xml_node<>* system = ship->first_node("system"); system; system = system->next_sibling("system")){
            ShipDescription::ShipSystem shipSystem;
            const char* systemName = system->first_attribute("name")->value();
            shipSystem.name = new char[strlen(systemName)+1];
            strcpy(shipSystem.name, systemName);
            rapidxml::xml_attribute<>* roomAttr = system->first_attribute("room");
            if(roomAttr)
                shipSystem.room = atoi(roomAttr->value());
            else
                shipSystem.room = -1;
            shipSystem.power = 0;
            if(system->first_attribute("power")){
                const char* power = system->first_attribute("power")->value();
                if(strcmp(power, "max") == 0)
                    shipSystem.power = 255;
                else
                    shipSystem.power = atoi(power);
            }
            description->systems.push_back(shipSystem);
        }
        ships.push_back(description);
    }

    for(rapidxml::xml_node<>* list = shipsNode->first_node("list"); list; list = list->next_sibling("list")){
        std::vector<const ShipDescription*>* shipList = new std::vector<const ShipDescription*>();
        for(rapidxml::xml_node<>* ship = list->first_node("ship"); ship; ship = ship->next_sibling("ship")){
            shipList->push_back(getShipDescription(ship->first_attribute("name")->value()));
        }

        const char* listName = list->first_attribute("name")->value();
        shipLists.insert(std::pair<std::string, std::vector<const ShipDescription*>*>(listName, shipList));
    }
    delete text;
}

Ship* ShipLoader::loadShipLayout(const ShipLayout* layout){
    Ship* ship = new Ship(layout->width, layout->height);
    ship->setNumRooms(layout->rooms.size());

    int numNormalRooms = 0;
    auto it = layout->rooms.begin();
    while(it != layout->rooms.end()){
        if(it->first >= '1' && it->first <= '9' && (int)(it->first - '1') > numNormalRooms)
            numNormalRooms = (int)(it->first - '1');
        ++it;
    }

    ship->captainPosition = {-1, -1};
    ship->opsPosition = {-1, -1};

    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            ship->tiles[y][x].type = layout->tiles[y][x];
            if(ship->tiles[y][x].isCaptainStation()){
                ship->captainPosition.x = x;
                ship->captainPosition.y = y;
            }else if(ship->tiles[y][x].isOpsStation()){
                ship->opsPosition.x = x;
                ship->opsPosition.y = y;
            }
        }
    }

    it = layout->rooms.begin();
    while(it != layout->rooms.end()){
        const SDL_Rect* rect = &it->second;
        int roomNum = 0;
        if(it->first >= '1' && it->first <= '9')
            roomNum = (int)(it->first - '1');
        else if(it->first == '!')
            roomNum = numNormalRooms + 1;
        else if(it->first == '@')
            roomNum = numNormalRooms + 2;
        else if(it->first == '#')
            roomNum = numNormalRooms + 3;
        else if(it->first == '$')
            roomNum = numNormalRooms + 4;
        else if(it->first == '%')
            roomNum = numNormalRooms + 5;
        else if(it->first == '^')
            roomNum = numNormalRooms + 6;
        else if(it->first == '&')
            roomNum = numNormalRooms + 7;
        else if(it->first == '*')
            roomNum = numNormalRooms + 8;
        else if(it->first == '(')
            roomNum = numNormalRooms + 9;
        Room* room = &ship->rooms[roomNum];
        room->x = it->second.x;
        room->y = it->second.y;
        room->width = it->second.w - it->second.x + 1;
        room->height = it->second.h - it->second.y + 1;
        for(int y = rect->y; y < rect->h+1; y++){
            for(int x = rect->x; x < rect->w+1; x++){
                ship->tiles[y][x].room = roomNum+1;
                if(ship->tiles[y][x].isExternal())
                    room->isExternal = true;
            }
        }
        ++it;
    }

    for(int d = 0; d < layout->doors.size(); d++){
        ship->addDoor(new Door(), layout->doors[d].x, layout->doors[d].y);
    }
    return ship;
}

const ShipDescription* ShipLoader::getShipDescription(const char* name){
    for(int i = 0; i < ships.size(); i++){
        if(strcmp(name, ships[i]->name) == 0){
            return ships[i];
        }
    }
    return NULL;
}

std::vector<const ShipDescription*>* ShipLoader::getShipList(const char* name){
    auto it = shipLists.find(name);
    if(it != shipLists.end())
        return it->second;
    return NULL;
}
