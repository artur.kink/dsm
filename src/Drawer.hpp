#ifndef _DRAWER_HPP
#define _DRAWER_HPP

#include "SDLIncludes.hpp"
#include "Sprites.hpp"

#define MAXZOOM 1.25f
#define MINZOOM 0.5f

enum TextAlign{
    Left,
    Center,
    Right
};

static SDL_Color black = {0, 0, 0, 255};
static SDL_Color white = {255, 255, 255, 255};

class Drawer{
private:
    SDL_Window* window;
    char* drawString;

    int textureWidth;
    int textureHeight;

public:
    SDL_Renderer* renderer;
    // Temporary target textures
    SDL_Texture* drawTexture;
    SDL_Texture* enemyTexture;
    SDL_Texture* uiTexture;
    SDL_Texture* windowTexture;

    int displayWidth;
    int displayHeight;

    int uiWidth;
    int uiHeight;
    float uiZoom;

    // Loaded image textures
    SDL_Texture* tilesTexture;
    SDL_Texture* crewTexture;
    SDL_Texture* iconTexture;
    SDL_Texture* backgroundTexture;

    int fontHeight;
    TTF_Font* font;
    TTF_Font* messageFont;

    Drawer();
    int init();
    void cleanup();
    int resize(int w, int h);

    void setFont(TTF_Font* f);
    void setMessageFont(TTF_Font* f);

    void drawRect(int x, int y, int w, int h, const SDL_Color& color);
    void drawRectBorder(int x, int y, int w, int h, int b, const SDL_Color& color, const SDL_Color& borderColor);
    void drawLine(int x1, int y1, int x2, int y2, const SDL_Color& color);
    void drawSprite(const Sprite* sprite, int x, int y);
    void drawSpriteAlpha(const Sprite* sprite, int x, int y, uint8_t alpha);
    void drawSprite(const Sprite* sprite, const SDL_Rect* dest);
    void drawSprite(const Sprite* sprite, int x, int y, int angle, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void drawSprite(SDL_Texture* texture, const SDL_Rect* src, int x, int y);
    void drawProgressBar(int value, int max, int x, int y, unsigned int w, unsigned int h, SDL_Color color, SDL_Color border);
    void drawProgressBar(int value, int partial, int max, int x, int y, unsigned int w, unsigned int h, SDL_Color color, SDL_Color partialColor, SDL_Color border);
    void drawIcon(int icon, int x, int y);
    int drawText(int x, int y, TextAlign align, const SDL_Color& color, TTF_Font* drawFont, const char* text);
    int drawTextf(int x, int y, TextAlign align, const SDL_Color& color, TTF_Font* drawFont, const char* format, ...);

    void clear();
    void prepareUI();
    void presentUI();
    void prepareWindowTexture();

    // Font helpers
    int getTextWidth(int textLen, const TTF_Font* font) const;
    int getTextWidth(const char* string, TTF_Font* font) const;
    int getTextCentered(int textLen, const TTF_Font* font, int width) const; 
    int getTextCentered(const char* string, TTF_Font* font, int width) const; 
};

#endif
