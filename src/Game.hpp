#ifndef _GAME_HPP
#define _GAME_HPP

#ifndef ANDROID
#define SDL_MAIN_HANDLED
#endif
#include "SDLIncludes.hpp"


#include <vector>
#include <cstdint>

#include "glm/glm.hpp"
#include "glm/vec2.hpp"

#include "rapidxml.hpp"

#include "LuaHandler.hpp"

#include "Globals.hpp"
#include "StringLibrary.hpp"

#include "Helpers.hpp"
#include "Drawer.hpp"
#include "FileManager.hpp"
#include "AudioManager.hpp"
#include "Sprites.hpp"
#include "Controls.hpp"
#include "Random.hpp"
#include "Particle.hpp"

#include "Item.hpp"
#include "ShipLoader.hpp"
#include "SystemLoader.hpp"
#include "EncounterManager.hpp"
#include "Crew.hpp"
#include "Ship.hpp"

class Window{
public:
    int offsetX;
    int offsetY;

    int width;
    int height;

    bool visible;

    bool animate;
    int animationWidth;

    /** Did animation finish in this frame */
    bool doneAnimation;

    Window(){
        visible = false;
        animate = false;
        width = height = 0;
    }
};

class PowerWindow : public Window{
public:
    std::vector<System*> systems;
    int selection;
};

class MapWindow : public Window{
public:
    int mapOffsetX;
    int mapOffsetY;

    std::vector<SDL_Point> reachable;
    std::vector<SDL_Point> secondary;
    SDL_Point selection;
    int distance;
    void center(const SDL_Point& point);
};

struct Action{
public:
    const char* name;
    enum ActionCode{
        useSystem = 0,
        hail,
        evade,
        defense,
        offense,
        heal,
        repair
    };
    ActionCode code;
    System* system;
    const Sprite* icon;
    bool available;
};

class ActionsWindow : public Window{
public:
    std::vector<Action> actions;
};

class MenuButton{
public:
    SDL_Point point;
    const char* text;
    int len;
    bool visible;
    MenuButton(const char* t){
        visible = true;
        setText(t);
    }
    void setText(const char* t){
        text = t;
        len = strlen(text);
    }
};

class InventoryWindow : public Window{
public:
    int selection;
};

class SystemWindow : public Window{
public:
    System* system;
};

class CrewWindow : public Window{
public:
    Crew* crew;
};

class Shot{
public:
    bool waitingToFire;
    int64_t startTime;
    bool hit;
    bool missed;
    bool destroy;
    bool waiting;
    int size;
    float distance;

    int damage;
    int fireChance;

    bool inEnemyView;
    bool asteroid;

    Ship* source;
    Ship* dest;

    SDL_Point start;
    SDL_Point target;
};

class Laser{
public:
    int64_t startTime;

    Ship* source;
    Ship* dest;
    Shot* fireAtShot;

    SDL_Point sourceStart;

    SDL_Point targetEnd;
    float targetAngle;

    int damage;
    int64_t damageTime;
    bool missed;
};

class DialogWindow : public Window{
public:
    int64_t openTime;
    bool animateText;
    int dialogLen;

    enum WordFlag{
        Highlight = 1,
        NewLine = 2,
    };

    struct Word{
        char* word;
        int length;
        int width;
        uint8_t flags;
    };
    char dialog[2048];
    Word dialogWords[200];

    struct Option{
        char text[256];
        Word words[20];
        char callback[100];
        int crewSelect;
    };
    Option options[3];
    int numOptions;

    int optionSelection;

    Encounter* encounter;
};

class CrewSelectionWindow : public Window{
public:
    int numSelect;
    char callback[100];
    bool selectedCrews[20];
    int numSelectedCrews;
};

class LuaWindow : public Window{
public:
    int backtrack;
    std::vector<std::string> history;
    char luaCommand[200];

    LuaWindow(){
        luaCommand[0] = '\0';
    }
};

class Station{
public:
    Ship* ship;

    // Services
    int repairPrice;
    int healPrice;
    int mapPrice;

    int fuelPrice;
    int fuelAmount;

    std::vector<Item> items;
};

class StarMap{
public:
    int width;
    int height;

    enum StarMapTile{
        smt_Empty = 0x0,
        smt_Star = 0x01,
        smt_Station = 0x02,
        smt_Enemy = 0x04,
        smt_Asteroids = 0x08,
        smt_Clouds = 0x10,
        smt_Visited = 0x20,
        smt_Visible = 0x40,
        smt_Unexplored = 0x80
    };
    struct StarTile{
        uint8_t tile;

        bool checked; // Temp flag used by map search

        Station* station;
        Encounter* encounter;
        StarTile(){
            tile = smt_Empty;
            station = NULL;
            encounter = NULL;
        }

        void setType(StarMapTile type, bool set){
            if(set){
                tile |= type;
            }else{
                tile &= (~type);
            }
        }

        bool isStar() const{
            return (tile & smt_Star) != 0;
        }
        bool isAsteroids() const{
            return (tile & smt_Asteroids) != 0;
        }
        bool isClouds() const{
            return (tile & smt_Clouds) != 0;
        }
        bool isStation() const{
            return (tile & smt_Station) != 0;
        }
        bool isVisible() const{
            return (tile & smt_Visible) != 0;
        }
        bool isUnexplored() const{
            return (tile & smt_Unexplored) != 0;
        }
        bool isVisited() const{
            return (tile & smt_Visited) != 0;
        }
    };
    StarTile** tiles;

    StarMap(int w, int h){
        width = w;
        height = h;

        tiles = new StarTile*[width];
        for(int x = 0; x < width; x++){
            tiles[x] = new StarTile[height];
            for(int y = 0; y < height; y++){
                tiles[x][y].tile = 0; 
                tiles[x][y].station = NULL;
            }
        }
    }

    StarTile& getTile(SDL_Point point){
        return tiles[point.x][point.y];
    }

    ~StarMap(){
        for(int x = 0; x < width; x++){
            delete tiles[x];
        }
        delete tiles;
    }
};

class Proficiency{
public:
    char* name;
    const char* printName;
    int skill;

    Proficiency(const char* n, int s){
        skill = s;
        name = new char[strlen(n)+1];
        strcpy(name, n);
    }
};

class CrewDescription{
public:
    char name[20];
    int sprite;
    bool selected;
    std::vector<int> proficiencies;
};

class ShipView{
public:
    float zoom;
    glm::vec2 offset;
    glm::vec2 original;

    int64_t time;
    bool isShaking;
};

class Variable{
public:
    char* name;
    bool global;
    Crew* crew;
    System* system;
    char* str;
    int i;

    Variable(const char* n, bool g){
        name = new char[strlen(n)+1];
        strcpy(name, n);
        global = g;
        crew = NULL;
        system = NULL;
        str = NULL;
        i = 0;
    }

    ~Variable(){
        if(str)
            delete str;
        delete name;
    }

    const char* getDataName(){
        if(crew)
            return crew->name;
        else if(system)
            return system->name;
        else if(str)
            return str;
        else
            return "UNKNOWN";
    }
};

class EnemyMove{
public:
    SDL_Point target;
    enum MoveAction{
        None,
        Attack,
        Repair,
        Move,
        Heal
    };
    MoveAction action;
    bool immediate;
    EnemyMove(){
        action = MoveAction::None;
    }
};


class MoveMap{
public:
    enum MoveAction{
        None = 0x00,
        Repairable = 0x01,
        Attackable = 0x02,
        Healable = 0x04,
        Movable = 0x08
    };
    struct MoveTile{
        uint8_t distance;
        uint8_t action;
    };
    MoveTile** map;
    bool hasRepair;
    bool hasHeal;

    void clear();

    bool isRepairable(int x, int y) const{
        return map[y][x].action == MoveAction::Repairable;
    }

    bool isAttackable(int x, int y) const{
        return map[y][x].action == MoveAction::Attackable;
    }

    bool isHealable(int x, int y) const{
        return map[y][x].action == MoveAction::Healable;
    }

    int getDistance(int x, int y) const{
        return map[y][x].distance;
    }

    bool isActionable(int x, int y) const{
        return map[y][x].action != MoveAction::None;
    }

    bool canMoveThrough(int x, int y) const{
        return map[y][x].distance != 0;
    }

    bool isMovable(int x, int y) const{
        return map[y][x].action == MoveAction::Movable;
    }

    void setAction(int x, int y, MoveAction action){
        if(action == MoveAction::Healable)
            hasHeal = true;
        if(action == MoveAction::Repairable)
            hasRepair = true;
        map[y][x].action = action; 
    }
   
    void setDistance(int x, int y, int distance){
        map[y][x].distance = (uint8_t)distance;
    }
};

class Asteroid{
public:
    glm::vec2 position;
    const Sprite* sprite;
    float angle;
    bool foreground;
    float speed;
    float rotationSpeed;

    Asteroid(const Sprite* s, int x, int y, Random& rand){
        sprite = s;
        position.x = x;
        position.y = y;
        randomize(rand);
    }
    void randomize(Random& rand){
        angle = rand.max(360);
        foreground = rand.oneIn(4);
        speed = rand.range(30, 60);
        rotationSpeed = rand.range(20, 40);
    }
};

class Cloud{
public:
    glm::vec2 position;
    const Sprite* sprite;
    glm::vec2 speed;
    uint8_t alpha;
    Cloud(const Sprite* s, Random& rand, int w, int h){
        alpha = 128;
        sprite = s;
        randomize(rand, w, h);
    }
    void randomize(Random& rand, int w, int h){
        position.x = rand.max(w);
        position.y = rand.max(h);
        speed.x = rand.floatRange(-0.02f, 0.02f);
        speed.y = rand.floatRange(-0.02f, 0.02f);
    }
};

class Difficulty{
public:
    int difficulty;
    void reset(){
        difficulty = 0;
    }
    void increase(int amount){
        difficulty += amount;
    }

    int getDPT(){
        return 14 + difficulty/50;
    }
};

class Game{
private:
    bool gameRunning;
    bool isPaused;

    StringLibrary stringLibrary;

    // Cached strings used extensively in UI
    struct CachedStrings{
        const char* actions;
        const char* amount;
        const char* cargo;    
        const char* command;
        const char* cooldown;
        const char* damage;
        const char* danger;
        const char* end_turn;
        const char* enemy_turn;
        const char* engineering;
        const char* fuel;
        const char* game_over;
        const char* health;
        const char* incoming;
        const char* install;
        const char* operations;
        const char* paused;
        const char* player_turn;
        const char* power;
        const char* randomize;
        const char* record_distance;
        const char* record_encounters;
        const char* record_ships;
        const char* recycle;
        const char* recycled_material;
        const char* safe;
        const char* science;
        const char* starmap;
        const char* start;
        const char* store;
        const char* upgrade;
        const char* victory;
        const char* warp;
    };
    CachedStrings cachedStrings;

    Drawer drawer;
    AudioManager audioManager;
    SpriteManager spriteManager;
    // Cached sprites used extensively in UI
    struct CachedSprites{
        const SpriteList* asteroids;
        const SpriteList* buttons;
        const SpriteList* clouds;
        const Sprite* corner;
        const SpriteList* fire;
        const Sprite* heal;
        const SpriteList* missile;
        const Sprite* portrait;
        const Sprite* repair;
        const Sprite* shield;
        const Sprite* target;
        const Sprite* safe;
        const Sprite* unsafe;
        const Sprite* sun;
        const SpriteList* grid;
        const Sprite* mapasteroid;
        const SpriteList* walk_arrow;
        const SpriteList* window;
    };
    CachedSprites cachedSprites;

    bool resetLua;
    friend LuaHandler;
    LuaHandler luaHandler;
    LuaWindow luaWindow;

    // Timing
    int64_t frameTime;
    int64_t frameDelta;
    int64_t realFrameTime;
    int64_t realStartTime;
    bool proceedFrame; ///< Run update for this frame even if isPaused
    unsigned int fps;

    // There are two random classes.
    // gameRandom must only be used for game logic such as deciding what kind of ship to spawn.
    // random may be used for non game logic such as spawning particles.
    // Using gameRandom for game logic can allow reproduction of games
    Random random;
    Random gameRandom;

    bool clickedInPlayerView;

    bool debug;
    bool debugPlayerView;

    bool noDraw;
    bool isTest;
    int returnCode;
    int timeout;
    int timeoutStart;

    Crew* selectedCrew;

    MoveMap moveMap;
    /** Temporary map used to keep track of already checked tiles */
    mutable uint8_t** checkMap; 

    int8_t selectedAction;
    Weapon* selectedWeapon;
    bool selectedScanner;
    bool selectedTeleporter;
    Crew* teleporterCrew;

    SystemLoader systemLoader;
    ShipLoader shipLoader;

    std::vector<CrewDescription> crews[4];
    int selectedCrews[4];

    ItemLibrary itemLibrary;
    std::vector<Item> items;
    int RMAmount;
    int fuelAmount;

    System* selectedSystem;
    const SystemDescription* installingSystem;

    std::vector<Proficiency> proficiencies;

    bool isSafe;
    Ship* ship;
    bool enemyDefeated;
    Ship* enemyShip;
    Station* station;
    std::vector<Crew*> enemyCrews;
    std::vector<Crew*> playerCrews;

    std::vector<Shot> shots;
    std::vector<Laser> lasers;
    std::vector<Asteroid> asteroids;
    std::vector<Cloud> clouds;

    int64_t turnTimer;
    bool playerTurn;
    bool endedTurn;
    int turnCounter;

    Difficulty difficulty;

    struct Record{
        int encounterCounter = 0;
        int shipDestroyedCounter = 0;
        int distance = 0;
    };
    Record currentRecord;
	Record alltimeRecord;

    ActionsWindow actionsWindow;
    PowerWindow powerWindow;
    MapWindow mapWindow;
    InventoryWindow inventoryWindow;
    SystemWindow systemWindow;
    Window stationWindow;
    CrewWindow crewWindow;
    DialogWindow dialogWindow;
    CrewSelectionWindow crewSelectionWindow;

    // Victory view
    bool displayVictory;
    bool generateVictory;

    std::vector<Item> gainedItems; //< List of items gained from destroying ship
    int gainedRM;
    int gainedFuel;
    int gainedArmor;

    EncounterManager encounterManager;
    char scanCallback[50];
    char hailCallback[50];
    char destroyCallback[50];
    char fleeCallback[50];
    char attackCallback[50];
    struct TurnCallback{
        int turn;
        char name[50];
        char callback[50];
    };
    std::vector<TurnCallback> turnCallbacks;
    std::vector<Variable*> variables;

    SDL_Point mapPosition;
    bool isPositionSafe;
    bool spawnEnemyShips;
    StarMap* starMap;

    bool inWarp;
    SDL_Point warpDestination;
    bool doWarp;
    bool playerWarp;
    int64_t shipAnimationTimer;
    bool destroyingShip;
    bool playerDestroy;

    ShipView playerView;
    float playerViewSize;
    ShipView enemyView;

    SDL_Point mouseTile;
    int cursor;

    struct Message{
        const Ship* ship;
        int x;
        int y;
        SDL_Texture* texture;
        int64_t timer;
    };
    std::vector<Message> messages;

    struct Effect{
        const Ship* ship;
        SDL_Point point;
        const SpriteList* list;
        int64_t timer;
        int duration;
    };
    std::vector<Effect> effects;

    ParticleManager particleManager;

    SDL_Color flashColor;
    int flashStart;
    int flashDuration;

    bool gameOver;
    bool showLoadGame;

    enum GameView{
        Menu,
        Options,
        Settings,
        Hangar,
        Playing
    };
    GameView currentView;
    std::vector<MenuButton> mainButtons;
    int mainSelection;

    std::vector<MenuButton> optionsButtons;
    int optionsSelection;

    std::vector<MenuButton> settingsButtons;
    int settingsSelection;
    GameView settingsParent;

public:
    static Game* game;

    Game();

    int init();

    int start();
    void run();
    void stop();

    void clearGame();
    void cleanup();
 
    void processEvents();
    void pause();
    void update();
    bool performEnemyTurn();
    void updateHangar();
    bool updateCrews(const std::vector<Crew*>& crews);
    void updateMove(Crew* crew);
    void updateScroll();
    void constrainZoom();
    void updateWindow(Window& window);
    void updateView(ShipView& view);
    void shake(ShipView& view);

    Crew* createCrewFromDescription(const CrewDescription& crew);
    void setTest(const char* fileName);
    void setTimeout(int t);
    void setNoDraw();

    bool loadConfig();
    void saveConfig();

    // Game content
    int getAvailableRoom(const Ship* ship, const SystemDescription* system);
    Ship* getShip(const ShipDescription* description);
    Ship* loadShipLayout(const char* file);
    void loadContentNames();
    void loadProficiencies();
    void loadCrews();
    void loadText();

    int getPlayerMouseX() const;
    int getPlayerMouseY() const;

    bool mouseInEnemyView() const;
    int getEnemyMouseX() const;
    int getEnemyMouseY() const;

    void createBackground();
    void endEnemyTurn();
    void endTurn();
    void endPlayerTurn();
    void endSelectedCrewTurn();
    void removeMenuSelections();

    void addEffect(const SpriteList* list, const Ship* ship, int x, int y, int duration = 0);
    void scanEffect(Ship* ship, int x, int y, int size, SDL_Color color);

    void addMessage(const char* text);
    void addMessage(const Ship* ship, int x, int y, const char* text);

    void flash(int duration, SDL_Color color);

    // Items
    Item* findInventoryItem(const char* name);
    Item createItem(const char* name, int amount);
    void addItem(const char* name, int amount, bool saveChange = false);
    void addItem(const Item& item);
    void addRM(int amount);
    void addFuel(int amount);
    int getRM();
    int getFuel();
    bool useFuel(int amount);
    bool useRM(int amount);
    bool useItem(const char* name, int amount, bool saveChange = false);

    void addCrew(Crew* crew, Ship* ship, const SDL_Point& point, bool player);
    void killCrew(Crew* crew);
    void warp(int x, int y);
    void cancelWarp();
    void removeShip(Ship* ship);
    void damageTile(Ship* ship, int x, int y, int amount, bool damageShip = true);
    void damageCrew(Crew* crew, int amount);
    void damageSystem(System* system, int amount);
    void updateFire(Ship* ship);
    void fireShot(Ship* source, Ship* dest, Weapon* weapon, Crew* crew, int startX, int startY, int endX, int endY);
    void fireLaserAtShot(Ship* source, Weapon* weapon, Crew* crew, int startX, int startY, Shot* targetShot, bool missed);
    void spawnAsteroids(Ship* ship);
    void spawnAsteroid(Ship* ship);
    bool attackShip(Ship* ship, int amount, int x, int y, int size, int fire);
    void repairShip(Ship* ship, int amount);
    void repair(Crew* crew, int x, int y);
    bool performRepair(Ship* ship, Crew* crew, int x, int y);
    void repairSystem(System* system, int amount);
    void heal(Crew* crew, int x, int y);
    bool performHeal(Crew* crew, int amount, int x, int y);
    void attack(Crew* crew, int x, int y); 
    void hail();
    bool checkIsSafe() const;
    void checkAndClearIfSafe();
    void setEnemyHostile(bool hostile, Ship* ship = NULL);
    SDL_Point findSpawnPoint(Ship* ship);
    int getWarpDistance();
    int getNumReachablePoints(StarMap* map, const SDL_Point& point, int distance) const;
    SDL_Point populateMap(StarMap* map, const SDL_Point& src, const SDL_Point& dest, bool mustFinish);

    void resetViews();
    void centerViews();
    void spawnStation();
    void createVictory();
    void giveReward();
    void destroyShip(bool player);
    void warpEnemyShip();
    void addRandomSystem(Ship* ship, System::SystemType type);
    void spawnEnemyShip(const ShipDescription* description = NULL, bool station = false);
    void createEnemyCrew(int num);

    void calculateActions();
    void addAction(const char* name, Action::ActionCode code, System* system, const Sprite* sprite, bool available);

    Crew* createRandomCrew();
    void selectCrew(Crew* crew);
    bool teleportCrew(Crew* crew, Ship* ship, int x, int y);
    void moveCrew(Crew* crew, Ship* ship, int x, int y, bool instant = false);
    void calculateCrewMovePath(Crew* crew, int x, int y);
    SDL_Point findClosestAdjacentMovePoint(const Crew* crew, int x, int y) const;
    SDL_Point findClosestMovablePoint(const Crew* crew, int x, int y) const;
    SDL_Point findClosestFreePoint(Ship* ship, int x, int y);
    bool hasTarget(Ship* ship, bool player, int x, int y) const;
    void calculateMoveMap(const Crew* crew, bool canRepair = false, bool canHeal = false);
    void calculateShipVisibility(Ship* ship, bool player);
    void clearEnemyVisibility();
    void exploreStarMap(StarMap* starMap, int x, int y, int nearDistance, int farDistance);

	Crew* getPlayerCrew(const char* name);
    bool hasProficiency(const Crew* crew, const char* proficiency);
    Crew* findProficiencyCrew(const char* proficiency);
    Proficiency* findProficiency(const char* name);
    int skillLerp(const Crew* crew, int skill, int min, int max) const;

    // Encounters
    bool beginEncounter(Encounter* encounter);
    int copyAndReplaceVariables(char* dest, DialogWindow::Word* words, const char* src, const std::vector<const char*>* variables = NULL);
    void addCrewVariable(const char* name, Crew* crew, bool global);
    void addSystemVariable(const char* name, System* system, bool global);
    void addIntVariable(const char* name, int i, bool global);
    void addStringVariable(const char* name, const char* str, bool global);
    Variable* getVariable(const char* name);
    void removeVariable(const char* name);
    void clearVariables(bool all);
    void showDialog(const char* name, const char* str, const std::vector<const char*>* variables = NULL);
    void addOption(const char* name, const char* str, const char* callback, const std::vector<const char*>* variables = NULL);
    void addCrewSelectOption(const char* name, const char* str, int numCrew, const char* callback, const std::vector<const char*>* variables = NULL);
    void createEncounter(const char* name, int minDistance, int maxDistance, bool forward = true);
    void removeEncounters(const char* name);
    void addTurnCallback(const char* name, const char* callback, int turn);
    void clearDialog();

    void createPlayer();
    void newGame();
    void startGame();
    void endGame();

    // Saving/Loading game
    char* getPreferredPath(const char* filename);
    void saveGame(const char* fileName = NULL);
    void saveGameShip(const Ship* ship, FileManager* file);
    void saveDialogWords(FileManager* file, DialogWindow::Word* words);
    void saveCommands();
    void saveRecord(FileManager* file, Record& record);
    Ship* loadGameShip(FileManager* file);
    bool canLoadGame();
    void loadGame(const char* fileName = NULL);
    void loadCommands();
    void loadRecord(FileManager* file, Record& record);

    // UI
    int getUIMouseX() const;
    int getUIMouseY() const;
    bool isMouseInUIRect(int x, int y, int w, int h) const;
    void showMapWindow();
    void findReachableMapPoints(const SDL_Point& position, int distance, bool explored, std::vector<SDL_Point>& list);
    void showWindow(Window& window);
    void centerWindow(Window& window);
    bool isInWindow(const Window& window) const;
    SDL_Point getWindowMousePos(const Window& window) const;
    Window* getOpenWindow();
    bool isInButton(const MenuButton& button) const;
    void updateMenuSelection(std::vector<MenuButton>& buttons, int& menuSelection);

    // Drawing
    void drawShip(const Ship* ship, int mouseX, int mouseY, bool isPlayerView);
    void drawMessages(const Ship* ship, int offsetX = 0, int offsetY = 0);
    void drawEffects(const Ship* ship, int offsetX = 0, int offsetY = 0);
    void drawParticles(const Ship* ship, int offsetX = 0, int offsetY = 0);
    void drawAsteroids(bool foreground);
    void drawClouds();
    void drawShots(bool enemyView);
    void drawLasers(bool enemyView);
    void drawIncoming(bool enemyView, int x);
    void draw();
    void drawSelectionCrews(const std::vector<CrewDescription>& crews, int x, int y);
    void drawHangar();
    void drawWindow(const Window& window);

};

#endif
