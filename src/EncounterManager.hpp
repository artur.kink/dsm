#ifndef _ENCOUNTERMANAGER_HPP
#define _ENCOUNTERMANAGER_HPP

#include <vector>
#include <map>
#include <string>
#include <cstdint>
#include <cstring>
#include <cstdlib>

#include "FileManager.hpp"

class Encounter{
public:
    char* name;

    int lastEncountered;
    
    Encounter(){
        name = NULL;
        lastEncountered = -1;
    }
};

class EncounterManager{
protected:
    std::vector<Encounter*> encounters;
    std::map<std::string, std::vector<Encounter*>*> encounterLists;
public:

    void load(const char* fileName);
    Encounter* loadEncounter(rapidxml::xml_node<>* encounterNode);

    void addEncounter(Encounter* encounter);
    Encounter* getEncounter(const char* name);

    void addEncounterList(const char* name, std::vector<Encounter*>* list);
    std::vector<Encounter*>* getEncounterList(const char* name);
};

#endif
