#include "Drawer.hpp"

Drawer::Drawer(){
    window = NULL;
    textureWidth = textureHeight = 0;
    drawTexture = NULL;
    enemyTexture = NULL;
    uiTexture = NULL;
    windowTexture = NULL;
}

int Drawer::init(){
    SDL_Log("Creating Window");
    SDL_DisplayMode display;
    SDL_GetCurrentDisplayMode(0, &display);
    SDL_Log("Display: %dx%d", display.w, display.h);
#ifdef ANDROID

    // Set GL channels to match textures to avoid color issues on some android devices
    // https://bugzilla.libsdl.org/show_bug.cgi?id=2291
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

    displayWidth = display.w;
    displayHeight = display.h;
    window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, displayWidth, displayHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN);
#else
    displayWidth = 1280;
    displayHeight = 720;
    window = SDL_CreateWindow("Game", 10, 10, displayWidth, displayHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
#endif

    if(window == NULL){
        SDL_Log("SDL_CreateWindow: %s", SDL_GetError());
        SDL_Quit();
        return 2;
    }
    SDL_Log("Window Created\n");

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if(renderer == NULL){
        SDL_DestroyWindow(window);
        SDL_Log("SDL_CreateRenderer Error: %s", SDL_GetError());
        SDL_Quit();
        return 3;
    }

    SDL_RendererInfo rendererInfo;
    SDL_GetRendererInfo(renderer, &rendererInfo);
    SDL_Log("Max Texture Size: %dx%d", rendererInfo.max_texture_width, rendererInfo.max_texture_height);

    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    SDL_Log("Initializing TTF\n");
    if(TTF_Init()){
        SDL_Log("TTF_Init Errort: %s", SDL_GetError());
        return 5;
    }
    SDL_Log("TTF Initialized\n");

    drawTexture = NULL;
    enemyTexture = NULL;
    uiTexture = NULL;
    windowTexture = NULL;
    if(resize(displayWidth, displayHeight)){
        SDL_Log("Failed to create draw texture: %s", SDL_GetError());
        return 1;
    }

    SDL_Log("Loading Textures");
    tilesTexture = IMG_LoadTexture(renderer, "tile.png");
    if(tilesTexture == NULL){
        SDL_Log("IMG_LoadTexture: %s", SDL_GetError());
        return 1;
    }

    crewTexture = IMG_LoadTexture(renderer, "crew.png");
    if(crewTexture == NULL){
        SDL_Log("IMG_LoadTexture: %s", SDL_GetError());
        return 1;
    }

    backgroundTexture = IMG_LoadTexture(renderer, "background.png");
    if(backgroundTexture == NULL){
        SDL_Log("IMG_LoadTexture: %s", SDL_GetError());
        return 1;
    }

    iconTexture = IMG_LoadTexture(renderer, "icons.png");
    if(iconTexture == NULL){
        SDL_Log("IMG_LoadTexture: %s", SDL_GetError());
        return 1;
    }

    drawString = new char[1024];
    return 0;
}

int Drawer::resize(int w, int h){

    displayWidth = w;
    displayHeight = h;

#ifdef ANDROID
    uiZoom = 0.5f;
    uiWidth = displayWidth*uiZoom;
    uiHeight = displayHeight*uiZoom;
#else
    uiZoom = 1.0f;
    uiWidth = displayWidth;
    uiHeight = displayHeight;
#endif

    if(drawTexture && w < textureWidth && h < textureHeight){
        return 0;
    }

    textureWidth = w;
    textureHeight = h;
    SDL_Log("Create new draw textures");

    if(drawTexture)
        SDL_DestroyTexture(drawTexture);
    if(enemyTexture)
        SDL_DestroyTexture(enemyTexture);

    drawTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, displayWidth*MAXZOOM, displayHeight*MAXZOOM);
    SDL_SetTextureBlendMode(drawTexture, SDL_BLENDMODE_BLEND);
    enemyTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, displayWidth*MAXZOOM, displayHeight*MAXZOOM);
    SDL_SetTextureBlendMode(enemyTexture, SDL_BLENDMODE_BLEND);
    if(drawTexture == NULL || enemyTexture == NULL){
        return 1;
    }

    if(uiTexture)
        SDL_DestroyTexture(uiTexture);
    uiTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, uiWidth, uiHeight);
    SDL_SetTextureBlendMode(uiTexture, SDL_BLENDMODE_BLEND);

    if(windowTexture)
        SDL_DestroyTexture(windowTexture);
    windowTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, uiWidth, uiHeight);
    SDL_SetTextureBlendMode(windowTexture, SDL_BLENDMODE_BLEND);
    return 0;
}

void Drawer::cleanup(){
    if(renderer){
        SDL_DestroyRenderer(renderer);
        renderer = NULL; 
    }

    if(window){
        SDL_DestroyWindow(window);
        window = NULL;
    }

    TTF_Quit();
}

void Drawer::setFont(TTF_Font* f){
    font = f;
    // Calculate font height
    TTF_SizeUTF8(font, "I", NULL, &fontHeight);
    fontHeight++;
}

void Drawer::setMessageFont(TTF_Font* f){
    messageFont = f;
}

void Drawer::drawRect(int x, int y, int w, int h, const SDL_Color& color){
    SDL_Rect rect = {x, y, w, h};
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(renderer, &rect);
}

void Drawer::drawRectBorder(int x, int y, int w, int h, int b, const SDL_Color& color, const SDL_Color& borderColor){
    drawRect(x, y, w, h, borderColor);
    drawRect(x + b, y + b, w - b*2, h - b*2, color);
}

void Drawer::drawLine(int x1, int y1, int x2, int y2, const SDL_Color& color){
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}

void Drawer::drawSprite(SDL_Texture* texture, const SDL_Rect* src, int x, int y){
    SDL_Rect dest = {x, y, src->w, src->h};
    SDL_RenderCopy(renderer, texture, src, &dest);
}

void Drawer::drawSprite(const Sprite* sprite, int x, int y){
    SDL_Rect dest = {x, y, sprite->rect.w, sprite->rect.h};
    SDL_RenderCopy(renderer, sprite->texture, &sprite->rect, &dest);
}

void Drawer::drawSpriteAlpha(const Sprite* sprite, int x, int y, uint8_t alpha){
    uint8_t origAlpha = 255;
    SDL_GetTextureAlphaMod(sprite->texture, &origAlpha);
    SDL_SetTextureAlphaMod(sprite->texture, alpha);
    SDL_Rect dest = {x, y, sprite->rect.w, sprite->rect.h};
    SDL_RenderCopy(renderer, sprite->texture, &sprite->rect, &dest);
    SDL_SetTextureAlphaMod(sprite->texture, origAlpha);
}

void Drawer::drawSprite(const Sprite* sprite, const SDL_Rect* dest){
    SDL_RenderCopy(renderer, sprite->texture, &sprite->rect, dest);
}

void Drawer::drawSprite(const Sprite* sprite, int x, int y, int angle, SDL_RendererFlip flip){
    SDL_Rect dest = {x, y, sprite->rect.w, sprite->rect.h};
    SDL_Point center = {sprite->rect.w/2, sprite->rect.h/2};
    SDL_RenderCopyEx(renderer, sprite->texture, &sprite->rect, &dest, angle, &center, flip);
}

void Drawer::drawProgressBar(int value, int max, int x, int y, unsigned int w, unsigned int h, SDL_Color color, SDL_Color border){
    int barWidth = w-2;
    int progressWidth = barWidth*(value/(float)max);

    drawRect(x, y, w, h, border);
    drawRect(x+1, y+1, progressWidth, h-2, color);
    drawRect(x+1+progressWidth, y+1, barWidth-progressWidth, h-2, black);
}

void Drawer::drawProgressBar(int value, int partial, int max, int x, int y, unsigned int w, unsigned int h, SDL_Color color, SDL_Color partialColor, SDL_Color border){
    int barWidth = w-2;
    int progressWidth = barWidth*(value/(float)max);
    int partialWidth = barWidth*(partial/(float)max);
    partialWidth = partialWidth - progressWidth;

    drawRect(x, y, w, h, border);
    drawRect(x+1, y+1, progressWidth, h-2, color);
    drawRect(x+1+progressWidth, y+1, partialWidth, h-2, partialColor);
    drawRect(x+1+progressWidth+partialWidth, y+1, barWidth-progressWidth-partialWidth, h-2, black);
}

void Drawer::drawIcon(int icon, int x, int y){
    SDL_Rect src = {icon * 32, 0, 32, 32};
    SDL_Rect dest = {x, y, 32, 32};
    SDL_RenderCopy(renderer, iconTexture, &src, &dest);
}

/**
 * @return Width in pixels of drawn text
 */
int Drawer::drawText(int x, int y, TextAlign align, const SDL_Color& color, TTF_Font* drawFont, const char* text){
    if(text == NULL || text[0] == '\0')
        return 0;

    if(drawFont == NULL)
        drawFont = font;

    SDL_Surface* textSurface = TTF_RenderUTF8_Solid(drawFont, text, color);
    SDL_Texture* textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);

    int textW, textH;
    SDL_QueryTexture(textTexture, NULL, NULL, &textW, &textH);
    SDL_Rect textRect = {0, 0, textW, textH};
    SDL_Rect destRect = {x, y, textW, textH};
    if(align == TextAlign::Center)
        destRect.x -= textW/2;
    else if(align == TextAlign::Right)
        destRect.x -= textW;

    SDL_SetTextureAlphaMod(textTexture, color.a);
    SDL_RenderCopy(renderer, textTexture, &textRect, &destRect);

    SDL_FreeSurface(textSurface);
    SDL_DestroyTexture(textTexture);
    return textW;
}

int Drawer::drawTextf(int x, int y, TextAlign align, const SDL_Color& color, TTF_Font* drawFont, const char* format, ...){
    va_list args;
    va_start(args, format);
    vsprintf(drawString, format, args);
    va_end(args);
    return drawText(x, y, align, color, font, drawString);
}

void Drawer::clear(){
    SDL_SetRenderTarget(renderer, NULL);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
}

void Drawer::prepareUI(){
    SDL_SetRenderTarget(renderer, uiTexture);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
}

void Drawer::presentUI(){
    SDL_SetRenderTarget(renderer, NULL);
    SDL_Rect src = {0, 0, uiWidth, uiHeight};
    SDL_Rect dest = {0, 0, displayWidth, displayHeight};
    SDL_RenderCopy(renderer, uiTexture, &src, &dest);
}

void Drawer::prepareWindowTexture(){
    SDL_SetRenderTarget(renderer, windowTexture);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
}

/**
 * Since we use monospace font, the width is constant
 */
int Drawer::getTextWidth(int textLen, const TTF_Font* font) const{
    if(font == messageFont)
        return textLen * 16;
    else
        return textLen * 8;
}

int Drawer::getTextWidth(const char* string, TTF_Font* font) const{
    int width = 0;
    TTF_SizeUTF8(font, string, &width, NULL);
    return width;
}

/**
 * Given a string length, calculate the position to draw the text in order
 * to center it within the given width.
 * A given width of 0 will give the text offset to center on 0.
 */
int Drawer::getTextCentered(int textLen, const TTF_Font* font, int width) const{
    return width/2 - getTextWidth(textLen, font)/2;
}

int Drawer::getTextCentered(const char* string, TTF_Font* font, int width) const{
    return width/2 - getTextWidth(string, font)/2;
}

