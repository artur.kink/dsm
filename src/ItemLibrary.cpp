#include "ItemLibrary.hpp"

void ItemLibrary::load(const char* fileName){
    char* text = FileManager::loadFileText(fileName);
    if(text == NULL)
        return;
    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* itemsNode = doc.first_node("items");
    for(rapidxml::xml_node<>* itemNode = itemsNode->first_node(); itemNode; itemNode = itemNode->next_sibling()){
        Item item;
        item.name = new char[strlen(itemNode->first_attribute("name")->value())+1];
        strcpy(item.name, itemNode->first_attribute("name")->value());
        item.icon = atoi(itemNode->first_attribute("icon")->value());
        if(itemNode->first_attribute("system")){
            item.system = new char[strlen(itemNode->first_attribute("system")->value())+1];
            strcpy(item.system, itemNode->first_attribute("system")->value());
        }
        if(itemNode->first_attribute("recycle")){
            item.recycle = atoi(itemNode->first_attribute("recycle")->value());
        }
        if(itemNode->first_attribute("rarity")){
            item.rarity = atoi(itemNode->first_attribute("rarity")->value());
        }
        if(itemNode->first_attribute("price")){
            item.price = atoi(itemNode->first_attribute("price")->value());
        }
        items.push_back(item);
    }
    delete text;
}

Item ItemLibrary::findItemDefinition(const char* name) const{
    for(int i = 0; i < items.size(); i++){
        if(strcmp(items[i].name, name) == 0){
            return items[i];
        }
    }
    SDL_Log("ERROR: Expected item '%s' not found", name);
    Item dummy;
    dummy.amount = 0;
    return dummy;
}

Item ItemLibrary::findSystemItem(const char* system){
    for(int i = 0; i < items.size(); i++){
        if(items[i].system && strcmp(items[i].system, system) == 0)
            return items[i];
    }
    SDL_Log("ERROR: Expected item '%s' not found", system);
    Item dummy;
    dummy.amount = 0;
    return dummy;
}
