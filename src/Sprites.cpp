#include "Sprites.hpp"

void SpriteManager::load(const char* fileName, SDL_Renderer* renderer){
    char* text = FileManager::loadFileText(fileName);
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* rootNode = doc.first_node("sprites");

    for(rapidxml::xml_node<>* spriteNode = rootNode->first_node(); spriteNode; spriteNode = spriteNode->next_sibling()){
        if(strcmp("list", spriteNode->name()) == 0){
            SpriteList* list = new SpriteList();
            rapidxml::xml_attribute<>* framerate = spriteNode->first_attribute("framerate");
            if(framerate)
                list->framerate = atoi(framerate->value());
            for(rapidxml::xml_node<>* listNode = spriteNode->first_node(); listNode; listNode = listNode->next_sibling()){
                list->sprites.push_back(loadSprite(listNode, renderer));
            }
            addList(list, spriteNode->first_attribute("name")->value());
        }else{
            addSprite(loadSprite(spriteNode, renderer));
        }
    }

    delete text;
}

const Sprite* SpriteManager::loadSprite(rapidxml::xml_node<>* node, SDL_Renderer* renderer){
    rapidxml::xml_attribute<>* nameAttr = node->first_attribute("name");
    const char* name = "";
    if(nameAttr != NULL){
        name = nameAttr->value();
    }
    Sprite* sprite = new Sprite(name);
    sprite->rect.x = atoi(node->first_attribute("x")->value());
    sprite->rect.y = atoi(node->first_attribute("y")->value());
    sprite->rect.w = atoi(node->first_attribute("w")->value());
    sprite->rect.h = atoi(node->first_attribute("h")->value());
    SDL_Texture* texture = getTexture(node->first_attribute("texture")->value());
    if(texture == NULL){
        texture = IMG_LoadTexture(renderer, node->first_attribute("texture")->value());
    }
    sprite->texture = texture;
    return sprite;
}


void SpriteManager::addTexture(SDL_Texture* texture, const char* name){
    textures.insert(std::pair<std::string, SDL_Texture*>(name, texture));
}

SDL_Texture* SpriteManager::getTexture(const char* name){
    return textures[(char*)name];
}

void SpriteManager::addSprite(const Sprite* sprite){
    sprites.push_back(sprite);
}

void SpriteManager::addList(const SpriteList* list, const char* name){
    lists.insert(std::pair<std::string, const SpriteList*>(name, list));
}

const SpriteList* SpriteManager::getList(const char* name){
    return lists[(char*)name];
}

const Sprite* SpriteManager::getSprite(const char* name){
    for(int i = 0; i < sprites.size(); i++){
        if(strcmp(name, sprites[i]->name) == 0)
            return sprites[i];
    }
    return NULL;
}

Sprite::Sprite(const char* n){
    name = new char[strlen(n)+1];
    strcpy(name, n);
}

const Sprite* SpriteList::getFrame(int64_t startTime, int64_t time) const{
    return sprites[((time-startTime)/framerate)%sprites.size()];
}

const Sprite* SpriteList::getSprite(const char* name) const{
    for(int i = 0; i < sprites.size(); i++){
        if(strcmp(name, sprites[i]->name) == 0)
            return sprites[i];
    }
    return NULL;
}

const Sprite* SpriteList::getSprite(int i) const{
    return sprites[i];
}
