#include "StringLibrary.hpp"

StringLibrary* StringLibrary::instance = NULL;

StringLibrary::StringLibrary(){
    instance = this;
    font = NULL;
    messageFont = NULL;
    language = new char[3];
}

void StringLibrary::setLanguage(const char* lang){
    strcpy(language, lang);
}

void StringLibrary::load(const char* fileName, bool noDraw){
    if(!noDraw){
        if(font != NULL)
            TTF_CloseFont(font);
        if(messageFont != NULL)
            TTF_CloseFont(messageFont);
    }

    // Cleanup loaded text and font
    for(int i = 0; i < (int)StringType::str_NumTypes; i++){
        std::vector<String*>& strings = this->strings[i];
        for(int i = 0; i < strings.size(); i++){
            delete strings[i];
        }
        strings.clear();
    }

    char* text = FileManager::loadFileText("text.xml");
    if(text == NULL){
        SDL_Log("Could not find text.xml");
        return;
    }

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* rootNode = doc.first_node("text");
    rapidxml::xml_node<>* languageNode = rootNode->first_node(language);

    rapidxml::xml_attribute<>* spacesAttr = languageNode->first_attribute("spaces");
    if(spacesAttr && strcmp(spacesAttr->value(), "no") == 0)
        spacesInDialog = false;
    else
        spacesInDialog = true;

    for(rapidxml::xml_node<>* textNode = languageNode->first_node(); textNode; textNode = textNode->next_sibling()){
        rapidxml::xml_attribute<>* nameAttribute = textNode->first_attribute("name");
        std::vector<String*>* stringList = NULL;
        if(!noDraw && strcmp(textNode->name(), "font") == 0){
            TTF_Font** loadFont = NULL;
            if(strcmp(nameAttribute->value(), "main") == 0){
                loadFont = &font;
            }else{
                loadFont = &messageFont;
            }
            *loadFont = TTF_OpenFont(textNode->first_attribute("file")->value(), atoi(textNode->first_attribute("size")->value()));
            if(*loadFont == NULL){
                SDL_Log("TTF_OpenFont Error: %s", SDL_GetError());;
                return;
            }
        }else if(strcmp(textNode->name(), "source") == 0){
            stringList = &strings[StringType::str_Source];
        }else if(strcmp(textNode->name(), "item") == 0){
            stringList = &strings[StringType::str_Item];
        }else if(strcmp(textNode->name(), "system") == 0){
            stringList = &strings[StringType::str_System];
        }else if(strcmp(textNode->name(), "proficiency") == 0){
            stringList = &strings[StringType::str_Proficiency];
        }else if(strcmp(textNode->name(), "encounter") == 0){
            stringList = &strings[StringType::str_Encounter];
        }
        
        if(stringList){
            for(rapidxml::xml_node<>* stringNode = textNode->first_node(); stringNode; stringNode = stringNode->next_sibling()){
                String* string = new String();
                rapidxml::xml_attribute<>* nameAttribute = stringNode->first_attribute("name");
                string->name = std::string(nameAttribute->value());
                string->str = new char[strlen(stringNode->value())+1];
                strcpy(string->str, stringNode->value());
                stringList->push_back(string);
            }
        }
    }

    delete text;
}

const char* StringLibrary::getString(const char* name, StringType type){
    std::vector<String*>& strings = this->strings[(int)type];
    for(int i = 0; i < strings.size(); i++){
        if(strcmp(name, strings[i]->name.c_str()) == 0)
            return strings[i]->str;
    }
    SDL_Log("Did not find string %s in type %d", name, (int)type);
    return "ERROR";
}
