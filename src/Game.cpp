#include <stdio.h>
#include <queue>
#include <algorithm>
#include <list>

// For glm::rotate
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/rotate_vector.hpp"

#include "Game.hpp"

Game* Game::game = NULL;

SDL_Color red = {255, 0, 0, 255};
SDL_Color red_highlight = {255, 0, 0, 128};
SDL_Color blue = {64, 64, 255, 255};
SDL_Color blue_highlight = {64, 64, 255, 128};
SDL_Color gray = {64, 64, 64, 255};
SDL_Color yellow = {255, 255, 0, 255};
SDL_Color yellow_highlight = {255, 255, 0, 128};
SDL_Color green = {0, 255, 0, 255};
SDL_Color green_highlight = {0, 255, 0, 128};
SDL_Color green_dark_highlight = {0, 192, 0, 160};
SDL_Color black_highlight = {0, 0, 0, 160};
SDL_Color shield_regen_green = {0, 255, 0, 96};

SDL_Color skillColors[Crew::NumSkills] = {
    {255, 255, 255, 255},
    {196, 30, 30, 255},
    {0, 196, 30, 255},
    {0, 30, 196, 255},
    {224, 128, 0, 255}
};

SDL_Color system_bg = {44, 44, 44, 255};
// Size of tile in map window
#define MAPTILESIZE 40

#define CREW_SELECTION_WIDTH 175
#define CREW_SELECTION_HEIGHT 74

#ifdef RAPIDXML_NO_EXCEPTIONS
namespace rapidxml{
/**
 * Custom handler for rapidxml parse errors to avoid C++ exceptions(disabled on android)
 */
extern void parse_error_handler(const char* what, void* where){
    SDL_Log("XML Parse Error: '%s'", what);
}
}
#endif

Game::Game(){
    game = this;
    gameRunning = false;

    debug = false;
    debugPlayerView = true;
    currentView = GameView::Menu;
    noDraw = false;
    timeout = false;
    returnCode = 0;
    luaHandler.init();
}

/**
 * Initialize game
 */
int Game::init(){

    if(noDraw){
        return 0;
    }

    SDL_Log("Initializing SDL");

    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0){
        SDL_Log("SDL_Init Error: %s\n", SDL_GetError());
        return 1;
    }
    SDL_Log("SDL Initialized");

    drawer.init();
    audioManager.init();

    SDL_Log("Initialized");
    return 0;
}

void Game::endGame(){
    gameOver = true;

    // If dialog window is showing, then we likely lost the game in an encounter.
    // Keep the encounter open, but remove options. This way player can see the
    // encounter result that lead to game over.
    dialogWindow.numOptions = 0;
    addOption("", "Close", "");

    saveCommands();

    // Wipe save file
    char* path = getPreferredPath("save.dat");
    FileManager* file = new FileManager(path, "wb");
    delete path;
    if(!file->isOpen()){
        SDL_Log("Failed to open save file");
        delete file;
        return;
    }
    file->saveInt16(0);
    delete file;

    path = getPreferredPath("record.dat");
    file = new FileManager(path, "rb");

    if(file->isOpen()){
        loadRecord(file, alltimeRecord);
    }
    delete file;

    if(currentRecord.encounterCounter > alltimeRecord.encounterCounter){
        alltimeRecord.encounterCounter = currentRecord.encounterCounter;
    }
    if(currentRecord.shipDestroyedCounter > alltimeRecord.shipDestroyedCounter){
        alltimeRecord.shipDestroyedCounter = currentRecord.shipDestroyedCounter;
    }
    if(currentRecord.distance > alltimeRecord.distance){
        alltimeRecord.distance = currentRecord.distance;
    }
    file = new FileManager(path, "wb");
    saveRecord(file, alltimeRecord);
    delete file;
    delete path;
}

void Game::clearGame(){
    gameOver = false;
    currentView = GameView::Menu;
    showLoadGame = canLoadGame();
    delete ship;
    ship = NULL;

    if(enemyShip)
        delete enemyShip;
    enemyShip = NULL; 
    station = NULL;

    items.clear();
    for(int i = 0; i < enemyCrews.size(); i++){
        delete enemyCrews[i];
    }
    enemyCrews.clear();
    for(int i = 0; i < playerCrews.size(); i++){
        delete playerCrews[i];
    }
    playerCrews.clear();

    shots.clear();
    lasers.clear();
    for(int i = 0; i < messages.size(); i++){
        SDL_DestroyTexture(messages[i].texture);
    }
    messages.clear();

    if(starMap){
        for(int x = 0; x < starMap->width; x++){
            for(int y = 0; y < starMap->height; y++){
                if(starMap->tiles[x][y].station)
                    delete starMap->tiles[x][y].station;
            }
        }
        delete starMap;
        starMap = NULL;
    }
    clearVariables(true);
}

void Game::cleanup(){

    SDL_Log("Cleaning up");

    drawer.cleanup();
    audioManager.cleanup();
    Mix_Quit();
    SDL_Quit();
    SDL_Log("Cleanup done");
}

int Game::start(){
    SDL_Log("Starting");
    gameRunning = true;
    run();
    SDL_Log("Done run");
    cleanup();
    return returnCode;
}

void Game::stop(){
    SDL_Log("Stopping");
    gameRunning = false;
}

void Game::setTest(const char* fileName){
    isTest = true;
    if(!luaHandler.loadScript(fileName)){
        returnCode = 1;
    }
}

void Game::setTimeout(int t){
    timeoutStart = realFrameTime;
    timeout = t;
}

void Game::setNoDraw(){
    noDraw = true;
}

void Game::saveConfig(){
    char* path = getPreferredPath("config.ini");
    FileManager* file = new FileManager(path, "w");
    delete path;

    if(!file->isOpen()){
        delete file;
        return;
    }

    file->saveRawString("MUSIC=") ;
    if(audioManager.isMusicPlaying())
        file->saveRawString("YES\n");
    else
        file->saveRawString("NO\n");

    file->saveRawString("SOUND=");
    if(audioManager.isSoundEnabled())
        file->saveRawString("YES\n");
    else
        file->saveRawString("NO\n");

    delete file;
}

bool Game::loadConfig(){
    char* path = getPreferredPath("config.ini");
    char* text = FileManager::loadFileText(path);
    delete path;

    if(!text){
        return false;
    }

    char* key = strtok(text,"\n\r=");
    while(key){
        char* value = strtok(NULL,"\n\r=");
        SDL_Log("Config: Key: '%s' Value: '%s'", key, value);
        if(strcmp(key, "SOUND") == 0){
            if(strcmp(value, "NO") == 0){
                audioManager.enableSound(false);
            }else{
                audioManager.enableSound(true);
            }
        }else if(strcmp(key, "MUSIC") == 0){
            if(strcmp(value, "YES") == 0){
                audioManager.playAllMusic();
            }else{
                audioManager.stopMusic();
            }
        }
        key = strtok(NULL,"\n\r=");
    }
    delete text;
    return true;
}

#define SAVEVERSION 2
bool Game::canLoadGame(){
    char* filePath = getPreferredPath("save.dat");
    SDL_RWops* file =  SDL_RWFromFile(filePath, "rb");
    delete filePath;

    if(file){
        int8_t val = 0;
        SDL_RWread(file, &val, sizeof(val), 1);
        SDL_RWclose(file);
        if(val != SAVEVERSION){
            return false;
        }
        return true;
    }
    return false;
}

void Game::loadGame(const char* fileName){
    FileManager* file = NULL;
    if(fileName == NULL){
        char* path = getPreferredPath("save.dat");
        file = new FileManager(path, "rb");
        delete path;
    }else{
        file = new FileManager(fileName, "rb");
    }
    if(!file->isOpen()){
        delete file;
        return;
    }
    int16_t val = file->loadInt16();
    if(val != SAVEVERSION){
        delete file;
        return;
    }

    gameRandom.setSeed(file->loadInt64());
    frameTime = file->loadInt64();

    turnCounter = file->loadInt32();

    playerTurn = file->loadBool();
    endedTurn = file->loadBool();
    inWarp = file->loadBool();
    doWarp = file->loadBool();
    playerWarp = file->loadBool();
    destroyingShip = file->loadBool();
    playerDestroy = file->loadBool();
    if(doWarp){
        warpDestination = file->loadSDLPoint();
    }
    enemyDefeated = file->loadBool(); 
    generateVictory = file->loadBool();
    spawnEnemyShips = file->loadBool();
    difficulty.difficulty = file->loadInt32();

    // Statistics
    loadRecord(file, currentRecord);

    ship = loadGameShip(file);
    
    bool loadEnemyShip = file->loadBool();
    if(loadEnemyShip){
        enemyShip = loadGameShip(file);
        // Load Visibility maps
        for(int y = 0; y < enemyShip->height; y++){
            for(int x = 0; x < enemyShip->width; x++){
                enemyShip->tiles[y][x].visibility = file->loadInt8();
            }
        }
        for(int y = 0; y < ship->height; y++){
            for(int x = 0; x < ship->width; x++){
                ship->tiles[y][x].visibility |= file->loadInt8();
            }
        }
    }else{
        enemyShip = NULL;
    }
    luaHandler.setEnemyShip(enemyShip);

    // Shots
    {
    int8_t numShots = file->loadInt8();
    for(int i = 0; i < numShots; i++){
        Shot shot;
        shot.waitingToFire = file->loadBool();
        shot.hit = file->loadBool();
        shot.missed = file->loadBool();
        shot.destroy = file->loadBool();
        shot.waiting = file->loadBool();
        shot.inEnemyView = file->loadBool();
        shot.asteroid = file->loadBool();

        shot.fireChance = 20;
        shot.damage = file->loadInt32();
        shot.size = file->loadInt32();

        int8_t shipInfo = file->loadInt8();
        if(shipInfo == 1)
            shot.source = ship;
        else if(shipInfo == 2)
            shot.source = enemyShip;
        else
            shot.source = NULL;

        shipInfo = file->loadInt8();
        if(shipInfo == 1)
            shot.dest = ship;
        else if(shipInfo == 2)
            shot.dest = enemyShip;
        else
            shot.dest = NULL;

        shot.start = file->loadSDLPoint();
        shot.target = file->loadSDLPoint();
        shot.distance = UINT32_MAX; 
        shot.startTime = frameTime;
        shots.push_back(shot);
    }
    }

    // Items
    RMAmount = file->loadInt32();
    fuelAmount = file->loadInt32();
    int16_t numItems = file->loadInt16();
    for(int i = 0; i < numItems; i++){
        char str[256];
        file->loadString(str);

        Item item = itemLibrary.findItemDefinition(str);
        item.amount = file->loadInt32();
        addItem(item);
    }

    // Star Map
    int32_t mapWidth = file->loadInt32();
    int32_t mapHeight = file->loadInt32();
    starMap = new StarMap(mapWidth, mapHeight);
    for(int x = 0; x < mapWidth; x++){
        for(int y = 0; y < mapHeight; y++){
            StarMap::StarTile& tile = starMap->tiles[x][y];
            tile.tile = file->loadInt8();
            bool hasStation = file->loadBool();
            if(hasStation){
                Station* station = new Station();
                tile.station = station;

                station->repairPrice = file->loadInt8();
                station->healPrice = file->loadInt8();
                station->mapPrice = file->loadInt8();
                station->fuelPrice = file->loadInt8();
                station->fuelAmount = file->loadInt8();
                int numItems = file->loadInt8();
                for(int i = 0; i < numItems; i++){
                    char name[256];
                    file->loadString(name);
                    int amount = file->loadInt8();
                    station->items.push_back(createItem(name, amount));
                }
            }
            bool hasEncounter = file->loadBool();
            if(hasEncounter){
                char name[256];
                file->loadString(name);
                tile.encounter = encounterManager.getEncounter(name);
            }
        }
    }
    
    // Current position
    isPositionSafe = file->loadBool();
    mapPosition = file->loadSDLPoint();
    if(starMap->tiles[mapPosition.x][mapPosition.y].station)
        station = starMap->tiles[mapPosition.x][mapPosition.y].station;

    // Encounter info
    bool dialogVisible = file->loadBool();
    if(dialogVisible){
        char* dialogText = file->loadString();
        int8_t numOptions = file->loadInt8();
        for(int i = 0; i < numOptions; i++){
            // Option text
            char optionText[256]; 
            file->loadString(optionText);
            // Option callback
            char callback[256];
            file->loadString(callback);
            int crewSelect = file->loadInt8();
            if(crewSelect != 0)
                addCrewSelectOption(NULL, optionText, crewSelect, callback);
            else
                addOption(NULL, optionText, callback);
        }
        showDialog(NULL, dialogText);
        delete dialogText;
    }
    bool crewSelectVisible = file->loadBool();
    if(crewSelectVisible){
        crewSelectionWindow.numSelect = file->loadInt8();
        file->loadString(crewSelectionWindow.callback);
        showWindow(crewSelectionWindow);
    }

    // Event callbacks
    file->loadString(attackCallback);
    file->loadString(hailCallback);
    file->loadString(fleeCallback);
    file->loadString(destroyCallback);
    file->loadString(scanCallback);
    int numTurnCallbacks = file->loadInt32();
    for(int i = 0; i < numTurnCallbacks; i++){
        int turn = file->loadInt8();
        char name[50];
        file->loadString(name);
        char callback[50];
        file->loadString(callback);
        addTurnCallback(name, callback, turn);
    }

    // Variables
    int32_t numVariables = file->loadInt32();
    for(int i = 0; i < numVariables; i++){
        char* name = file->loadString();
        
        bool global = file->loadBool();
        int8_t type = file->loadInt8();
        
        if(type == 0){
            char* crewName = file->loadString();
            Crew* crew = getPlayerCrew(crewName);
            addCrewVariable(name, crew, global);
            delete crewName;
        }else if(type == 1){
            // TODO add
            addSystemVariable(name, NULL, global);
        }else if(type == 2){
            char* str = file->loadString();
            addStringVariable(name, str, global);
            delete str;
        }else if(type == 3){
            int num = file->loadInt32();
            addIntVariable(name, num, global);
        }
        delete name;
    }
    
    delete file;
    loadCommands();
}

char* Game::getPreferredPath(const char* filename){
    char* path = SDL_GetPrefPath("dsm", "dsm");
    if(!path){
        SDL_Log("Could not find preferred path");
        return NULL;
    }
    char* filePath = new char[256];
    strcpy(filePath, path);
    strcat(filePath, filename);
    SDL_free(path);
    return filePath;
}

/**
 * @todo Saving every turn can cause considerable lag. Might have to either
 * avoid every turn save, or serialize to a buffer and save to file in a thread.
 */
void Game::saveGame(const char* fileName){
    FileManager* file = NULL;
    if(fileName == NULL){
        char* path = getPreferredPath("save.dat");
        file = new FileManager(path, "wb");
        delete path;
    }else{
        file = new FileManager(fileName, "wb");
    }
    if(!file->isOpen()){
        SDL_Log("Failed to open save file");
        delete file;
        return;
    }

    // Invalidate save file if game over
    if(ship->armor <= 0){
        file->saveInt16(0);
        delete file;
        return; 
    }
    file->saveInt16(SAVEVERSION); // Start with SAVEVERSION to mark valid save file

    // Create a new random seed for reproducability
    int64_t newSeed = SDL_GetTicks();
    gameRandom.setSeed(newSeed);
    file->saveInt64(newSeed);
    file->saveInt64(frameTime);

    file->saveInt32(turnCounter);
    file->saveBool(playerTurn);
    file->saveBool(endedTurn);
    file->saveBool(inWarp);
    file->saveBool(doWarp);
    file->saveBool(playerWarp);
    file->saveBool(destroyingShip);
    file->saveBool(playerDestroy);
    if(doWarp){
        file->saveSDLPoint(warpDestination);
    }
    file->saveBool(enemyDefeated);
    file->saveBool(generateVictory);
    file->saveBool(spawnEnemyShips);
    file->saveInt32(difficulty.difficulty);

    // Statistics
    saveRecord(file, currentRecord);

    saveGameShip(ship, file); 
    if(enemyShip){
        // true to load enemy ship
        file->saveBool(true);
        saveGameShip(enemyShip, file); 
        // Need to save visibility maps
        for(int y = 0; y < enemyShip->height; y++){
            for(int x = 0; x < enemyShip->width; x++){
                file->saveInt8(enemyShip->tiles[y][x].visibility);
            }
        }
        for(int y = 0; y < ship->height; y++){
            for(int x = 0; x < ship->width; x++){
                file->saveInt8(ship->tiles[y][x].visibility & Tile::VisibilityFlags::Enemy_Full);
            }
        }
    }else{
        // false to not load enemy ship
        file->saveBool(false);
    }

    // Shots
    file->saveInt8(shots.size());
    for(int i = 0; i < shots.size(); i++){
        const Shot& shot = shots[i];
        file->saveBool(shot.waitingToFire);
        file->saveBool(shot.hit);
        file->saveBool(shot.missed);
        file->saveBool(shot.destroy);
        file->saveBool(shot.waiting);
        file->saveBool(shot.inEnemyView);
        file->saveBool(shot.asteroid);

        file->saveInt32(shot.damage);
        file->saveInt32(shot.size);

        if(shot.source == ship)
            file->saveInt8(1);
        else if(shot.source == enemyShip)
            file->saveInt8(2);
        else
            file->saveInt8(3);

        if(shot.dest == ship)
            file->saveInt8(1);
        else if(shot.dest == enemyShip)
            file->saveInt8(2);
        else
            file->saveInt8(3);

        file->saveSDLPoint(shot.start);
        file->saveSDLPoint(shot.target);
    }

    // Items
    file->saveInt32(RMAmount);
    file->saveInt32(fuelAmount);
    int32_t numItems = items.size();
    file->saveInt16(numItems);
    for(int i = 0; i < numItems; i++){
        const Item& item = items[i];
        file->saveString(item.name);
        file->saveInt32(item.amount);
    }

    // Star Map
    file->saveInt32(starMap->width);
    file->saveInt32(starMap->height);
    for(int x = 0; x < starMap->width; x++){
        for(int y = 0; y < starMap->height; y++){
            const StarMap::StarTile& tile = starMap->tiles[x][y];
            file->saveInt8(tile.tile);
            if(tile.station){
                file->saveBool(true);
                const Station* station = tile.station;
                file->saveInt8(station->repairPrice);
                file->saveInt8(station->healPrice);
                file->saveInt8(station->mapPrice);
                file->saveInt8(station->fuelPrice);
                file->saveInt8(station->fuelAmount);
                file->saveInt8(station->items.size());
                for(int i = 0; i < station->items.size(); i++){
                    file->saveString(station->items[i].name);
                    file->saveInt8(station->items[i].amount);
                }
            }else{
                file->saveBool(false);
            }

            if(tile.encounter){
                file->saveBool(true);
                file->saveString(tile.encounter->name);
            }else{
                file->saveBool(false);
            }
        }
    }

    // Current position
    file->saveBool(isPositionSafe);
    file->saveSDLPoint(mapPosition);

    // Encounter info
    file->saveBool(dialogWindow.visible);
    if(dialogWindow.visible){
        saveDialogWords(file, dialogWindow.dialogWords);
        file->saveInt8(dialogWindow.numOptions);
        for(int i = 0; i < dialogWindow.numOptions; i++){
            // Option text
            saveDialogWords(file, dialogWindow.options[i].words);
            file->saveString(dialogWindow.options[i].callback);
            file->saveInt8(dialogWindow.options[i].crewSelect);
        }
    }
    file->saveBool(crewSelectionWindow.visible);
    if(crewSelectionWindow.visible){
        file->saveInt8(crewSelectionWindow.numSelect);
        file->saveString(crewSelectionWindow.callback);
    }

    // Event callbacks
    file->saveString(attackCallback);
    file->saveString(hailCallback);
    file->saveString(fleeCallback);
    file->saveString(destroyCallback);
    file->saveString(scanCallback);
    file->saveInt32(turnCallbacks.size());
    for(int i = 0; i < turnCallbacks.size(); i++){
        const TurnCallback& callback = turnCallbacks[i];
        file->saveInt8(callback.turn);
        file->saveString(callback.name);
        file->saveString(callback.callback);
    }
    
    // Variables
    file->saveInt32(variables.size());
    for(int i = 0; i < variables.size(); i++){
        const Variable* variable = variables[i];
        file->saveString(variable->name);
        file->saveBool(variable->global);

        if(variable->crew){
            file->saveInt8(0);
            file->saveString(variable->crew->name);
        }else if(variable->system){
            file->saveInt8(1);
            file->saveString(variable->system->name);
        }else if(variable->str){
            file->saveInt8(2);
            file->saveString(variable->str);
        }else{ //int
            file->saveInt8(3);
            file->saveInt32(variable->i);
        }
    }

    delete file;
    saveCommands();
}

void Game::saveDialogWords(FileManager* file, DialogWindow::Word* words){
    int16_t length = 0;
    int i = 0;
    while(words[i].word != NULL){
        length += words[i].length + 1; // 1 for space
        if((words[i].flags & DialogWindow::Highlight) != 0)
            length += 2;
        i++;
    }
    file->saveInt16(length);
    i = 0;
    while(words[i].word != NULL){
        if((words[i].flags & DialogWindow::Highlight) != 0)
            file->saveInt8('|');

        file->saveBytes(words[i].word, words[i].length);

        if((words[i].flags & DialogWindow::Highlight) != 0)
            file->saveInt8('|');
        if((words[i].flags & DialogWindow::NewLine) != 0)
            file->saveInt8('\n');
        else
            file->saveInt8(' ');

        i++;
    }
}

void Game::saveGameShip(const Ship* ship, FileManager* file){
    file->saveBool(ship->playerShip);
    file->saveString(ship->name);

    file->saveInt32(ship->armor);
    file->saveInt32(ship->armorMax);
    file->saveInt8(ship->fireArmorDamageCooldown);
    file->saveInt32(ship->power);
    file->saveInt32(ship->freePower);
    file->saveInt32((int)ship->mode);
    file->saveInt32(ship->modeCooldown);

    // Systems
    file->saveInt8(ship->allSystems.size());
    for(int i = 0; i < ship->numRooms; i++){
        if(ship->rooms[i].system != NULL){
            file->saveInt8(i);
            System* system = ship->rooms[i].system;
            file->saveString(system->name);
            system->saveData(file);
        }
    }

    // Crews
    int8_t numCrews = ship->crews.size();
    file->saveInt8(numCrews);
    for(int i = 0; i < numCrews; i++){
        Crew* crew = ship->crews[i];
        crew->saveData(file);
        
        // Proficiencies
        int8_t numProficiencies = crew->proficiencies.size();
        file->saveInt8(numProficiencies);
        for(int p = 0; p < numProficiencies; p++){
            Proficiency& proficiency = proficiencies[crew->proficiencies[p]];
            file->saveString(proficiency.name);
        }
    }

    // Doors
    // The door locations are saved on ship layout, but extra
    // data must be saved
    int8_t numDoors = ship->doors.size();
    file->saveInt8(numDoors);
    for(int i = 0; i < numDoors; i++){
        Door* door = ship->doors[i];
        file->saveInt8(door->health);
        file->saveBool(door->isOpen);
        file->saveBool(door->forceOpen);
        file->saveBool(door->turnDone);
    }

    // Fire
    int32_t numFire = 0;
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            if(ship->getTile(x, y).fire != 0)
                numFire++;
        }
    }
    file->saveInt32(numFire);
    for(int8_t y = 0; y < ship->height; y++){
        for(int8_t x = 0; x < ship->width; x++){
            if(ship->getTile(x, y).fire != 0){
                file->saveInt8(x);
                file->saveInt8(y);
                file->saveInt8(ship->getTile(x, y).fire);
            }
        }
    }
}

void Game::saveCommands(){
    char* path = getPreferredPath("commands.dat");
    FileManager* file = new FileManager(path, "wb");
    delete path;
    if(!file->isOpen()){
        SDL_Log("Failed to open commands file");
        delete file;
        return;
    }
    file->saveInt8((int8_t)luaWindow.history.size());
    for(int i = 0; i < luaWindow.history.size(); i++){
        file->saveString(luaWindow.history[i].c_str());
    }
    delete file;
}

void Game::saveRecord(FileManager* file, Record& record){
    file->saveInt32(record.encounterCounter);
    file->saveInt32(record.shipDestroyedCounter);
    file->saveInt32(record.distance);
}

void Game::loadRecord(FileManager* file, Record& record){
    record.encounterCounter = file->loadInt32();
    record.shipDestroyedCounter = file->loadInt32();
    record.distance = file->loadInt32();
}

Ship* Game::loadGameShip(FileManager* file){
    bool playerShip = file->loadBool();

    char str[256];
    file->loadString(str);

    Ship* ship = NULL;
    const ShipDescription* description = shipLoader.getShipDescription(str);
    ship = shipLoader.loadShipLayout(description->layout);

    if(ship == NULL)
        return NULL;
    ship->playerShip = playerShip;
    if(playerShip)
        ship->view = &playerView;
    else
        ship->view = &enemyView;
    strcpy(ship->name, str);

    ship->armor = file->loadInt32();
    ship->armorMax = file->loadInt32();
    ship->fireArmorDamageCooldown = file->loadInt8();
    int32_t power = file->loadInt32();
    int32_t freePower = file->loadInt32();
    ship->mode = (Ship::ShipMode)file->loadInt32();
    ship->modeCooldown = file->loadInt32();

    int8_t numSystems = file->loadInt8();
    for(int i = 0; i < numSystems; i++){
        int8_t room = file->loadInt8();
        file->loadString(str);

        System* system = systemLoader.getSystem(str);
        ship->addSystem(system, room+1);
        system->loadData(file);
    }

    ship->power = power;
    ship->freePower = freePower;

    // Crews
    int32_t numCrews = file->loadInt8();
    for(int i = 0; i < numCrews; i++){
        Crew* crew = new Crew();
        crew->loadData(file);
        addCrew(crew, ship, crew->position, crew->playerCrew);

        // Proficiencies
        int8_t numProficiencies = file->loadInt8();
        for(int p = 0; p < numProficiencies; p++){
            char name[50];
            file->loadString(name);

            for(int j = 0; j < proficiencies.size(); j++){
                const Proficiency& prof = proficiencies[j];
                if(strcmp(prof.name, name) == 0){
                    crew->proficiencies.push_back(j);
                    break;
                }
            }
        }
    }
    
    // Doors
    // The door locations are saved on ship layout, but extra
    // data must be saved
    int8_t numDoors = file->loadInt8();
    for(int i = 0; i < numDoors; i++){
        Door* door = ship->doors[i];
        door->health = file->loadInt8();
        door->isOpen = file->loadBool();
        door->forceOpen = file->loadBool();
        door->turnDone = file->loadBool();
    }

    // Fire
    int32_t numFire = file->loadInt32();
    for(int i = 0; i < numFire; i++){
        int8_t x = file->loadInt8();
        int8_t y = file->loadInt8();
        ship->getTile(x, y).fire = file->loadInt8();
    }

    return ship;
}

void Game::loadCommands(){
    char* path = getPreferredPath("commands.dat");
    FileManager* file = new FileManager(path, "rb");
    delete path;
    if(!file->isOpen()){
        SDL_Log("Failed to open commands file");
        delete file;
        return;
    }
    int numCommands = file->loadInt8();
    luaWindow.history.clear();
    for(int i = 0; i < numCommands; i++){
        char command[256];
        file->loadString(command);
        luaWindow.history.push_back(std::string(command));
    }
    delete file;
}

int Game::getAvailableRoom(const Ship* ship, const SystemDescription* system){
    int retRoom = -1;
    // Find a unassigned room 
    int room = gameRandom.max(ship->numRooms);
    for(int roomCounter = 0; roomCounter < ship->numRooms; roomCounter++){
        room++;
        if(room > ship->numRooms)
            room = 1;

        const Room& r = ship->rooms[room-1];
        if(r.system != NULL ||
           (system->type == System::Weapon && !ship->rooms[room-1].isExternal) ||
           (system->type != System::Weapon && ship->rooms[room-1].isExternal))
            continue;

        const SystemLayout* layout = system->layout;
        if(layout->width > r.width || layout->height > r.height)
            continue;

        retRoom = room;
        break;
    }
    return retRoom;
}

Ship* Game::getShip(const ShipDescription* description){
    Ship* ship = shipLoader.loadShipLayout(description->layout);
    ship->armor = ship->armorMax = description->armor;
    ship->freePower = description->power;
    strcpy(ship->name, description->name);

    // Add non random location systems
    for(int s = 0; s < description->systems.size(); s++){
        const ShipDescription::ShipSystem& system = description->systems[s];
        if(system.room != -1){
            System* createdSystem = systemLoader.getSystem(system.name);
            ship->addSystem(createdSystem, system.room);
            if(system.power == 255){
                ship->directPower(createdSystem, createdSystem->maxPower); 
            }else if(system.power != 0)
                ship->directPower(createdSystem, system.power);
        }
    }

    // Add random location systems
    for(int s = 0; s < description->systems.size(); s++){
        const ShipDescription::ShipSystem& system = description->systems[s];
        if(system.room == -1){
            System* createdSystem = systemLoader.getSystem(system.name);
            int room = getAvailableRoom(ship, systemLoader.getSystemDescription(system.name));
            assert(room != -1);
            ship->addSystem(createdSystem, room);
            if(system.power == 255)
                ship->directPower(createdSystem, createdSystem->maxPower); 
            else if(system.power != 0)
                ship->directPower(createdSystem, system.power);
        }
    }
    return ship;
}

void Game::loadContentNames(){
    for(int i = 0; i < itemLibrary.items.size(); i++){
        itemLibrary.items[i].printName = stringLibrary.getString(itemLibrary.items[i].name, StringLibrary::str_Item);
    }
    for(int i = 0; i < items.size(); i++){
        items[i].printName = stringLibrary.getString(items[i].name, StringLibrary::str_Item);
    }
    for(int i = 0; i < proficiencies.size(); i++){
        proficiencies[i].printName = stringLibrary.getString(proficiencies[i].name, StringLibrary::str_Proficiency);
    }
    if(ship){
        for(int s = 0; s < ship->allSystems.size(); s++){
            System* system = ship->allSystems[s];
            system->printName = stringLibrary.getString(system->name, StringLibrary::str_System);
        }
    }
    for(int s = 0; s < systemLoader.systems.size(); s++){
        SystemDescription* system = systemLoader.systems[s];
        for(int i = 0; i < system->upgradeItems.size(); i++){
            Item& item = system->upgradeItems[i];
            item.printName = stringLibrary.getString(item.name, StringLibrary::str_Item);
        }
    }
}

void Game::loadProficiencies(){
    char* text = FileManager::loadFileText("proficiencies.xml");
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* rootNode = doc.first_node("proficiencies");

    for(rapidxml::xml_node<>* proficiencyNode = rootNode->first_node(); proficiencyNode; proficiencyNode = proficiencyNode->next_sibling()){
        rapidxml::xml_attribute<>* skillAttribute = proficiencyNode->first_attribute("skill");
        for(int i = 0; i < Crew::NumSkills; i++){
            if(strcmp(skillAttribute->value(), SkillNames[i]) == 0){
                proficiencies.push_back(Proficiency(proficiencyNode->first_attribute("name")->value(), (Crew::Skills)i));
            }
        }
    }

    delete text;
}

void Game::loadCrews(){
    char* text = FileManager::loadFileText("crew.xml");
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* rootNode = doc.first_node("crews");

    for(rapidxml::xml_node<>* crewNode = rootNode->first_node(); crewNode; crewNode = crewNode->next_sibling()){
        CrewDescription crew;
        strcpy(crew.name, crewNode->first_attribute("name")->value());
        crew.sprite = atoi(crewNode->first_attribute("sprite")->value());
        const char* value = crewNode->first_attribute("position")->value();
        for(rapidxml::xml_node<>* profNode = crewNode->first_node(); profNode; profNode = profNode->next_sibling()){
            const char* name = profNode->first_attribute("name")->value();
            for(int j = 0; j < proficiencies.size(); j++){
                Proficiency& prof = proficiencies[j];
                if(strcmp(prof.name, name) == 0){
                    crew.proficiencies.push_back(j);
                    break;
                }
            }
        }
        if(strcmp(value, "command") == 0)
            crews[0].push_back(crew);
        else if(strcmp(value, "operations") == 0)
            crews[1].push_back(crew);
        else if(strcmp(value, "engineering") == 0)
            crews[2].push_back(crew);
        else if(strcmp(value, "science") == 0)
            crews[3].push_back(crew);
    }

    delete text;
}

void Game::loadText(){

    stringLibrary.load("text.xml", noDraw);

    drawer.setFont(stringLibrary.font);
    drawer.setMessageFont(stringLibrary.messageFont);

    cachedStrings.actions = stringLibrary.getString("actions");
    cachedStrings.amount= stringLibrary.getString("amount");
    cachedStrings.cargo = stringLibrary.getString("cargo"); 
    cachedStrings.command = stringLibrary.getString("command"); 
    cachedStrings.cooldown = stringLibrary.getString("cooldown");
    cachedStrings.damage = stringLibrary.getString("damage");
    cachedStrings.danger = stringLibrary.getString("danger");
    cachedStrings.end_turn = stringLibrary.getString("end_turn"); 
    cachedStrings.enemy_turn = stringLibrary.getString("enemy_turn");
    cachedStrings.engineering = stringLibrary.getString("engineering");
    cachedStrings.fuel = stringLibrary.getString("fuel");
    cachedStrings.game_over = stringLibrary.getString("game_over");
    cachedStrings.health = stringLibrary.getString("health");
    cachedStrings.incoming = stringLibrary.getString("incoming");
    cachedStrings.install = stringLibrary.getString("install");
    cachedStrings.operations = stringLibrary.getString("operations");
    cachedStrings.paused = stringLibrary.getString("paused");
    cachedStrings.player_turn = stringLibrary.getString("player_turn");
    cachedStrings.power = stringLibrary.getString("power");
    cachedStrings.randomize = stringLibrary.getString("randomize");
    cachedStrings.record_distance = stringLibrary.getString("record_distance");
    cachedStrings.record_encounters = stringLibrary.getString("record_encounters");
    cachedStrings.record_ships = stringLibrary.getString("record_ships");
    cachedStrings.recycle = stringLibrary.getString("recycle");
    cachedStrings.recycled_material = stringLibrary.getString("recycled_material");
    cachedStrings.safe = stringLibrary.getString("safe");
    cachedStrings.science = stringLibrary.getString("science");
    cachedStrings.starmap = stringLibrary.getString("starmap");
    cachedStrings.start = stringLibrary.getString("start");
    cachedStrings.store = stringLibrary.getString("store");
    cachedStrings.upgrade = stringLibrary.getString("upgrade");
    cachedStrings.victory = stringLibrary.getString("victory");
    cachedStrings.warp = stringLibrary.getString("warp");

    loadContentNames();
}

void Game::run(){

    // Load game data
    // Items must be loaded before systems, because of a dependency
    stringLibrary.setLanguage("en");
    loadText();
    spriteManager.load("sprites.xml", drawer.renderer);
    itemLibrary.load("items.xml");
    shipLoader.load("ships.xml");
    systemLoader.load("systems.xml", itemLibrary, spriteManager);
    loadProficiencies();
    loadCrews();
    luaHandler.loadScript("encounters.lua");
    luaHandler.loadScript("story.lua");
    if(!isTest)
        luaHandler.loadScript("game.lua");
    encounterManager.load("encounters.xml");
    audioManager.load("audio.xml");
    loadContentNames();
    loadCommands();
    cachedSprites.asteroids = spriteManager.getList("asteroids");
    cachedSprites.buttons = spriteManager.getList("buttons");
    cachedSprites.clouds = spriteManager.getList("clouds");
    cachedSprites.corner = spriteManager.getSprite("corner");
    cachedSprites.missile = spriteManager.getList("missile");
    cachedSprites.fire = spriteManager.getList("fire");
    cachedSprites.shield = spriteManager.getSprite("shield");
    cachedSprites.portrait = spriteManager.getSprite("portrait");
    cachedSprites.repair = spriteManager.getSprite("repair");
    cachedSprites.heal = spriteManager.getSprite("heal");
    cachedSprites.target = spriteManager.getSprite("target");
    cachedSprites.safe = spriteManager.getSprite("safe");
    cachedSprites.unsafe = spriteManager.getSprite("unsafe");
    cachedSprites.sun = spriteManager.getSprite("sun");
    cachedSprites.grid = spriteManager.getList("grid");
    cachedSprites.walk_arrow = spriteManager.getList("walk_arrow");
    cachedSprites.window = spriteManager.getList("window");
    cachedSprites.mapasteroid = spriteManager.getSprite("mapasteroid");

    showLoadGame = canLoadGame();
    mainSelection = 0;
    mainButtons.push_back(MenuButton("LOAD GAME"));
    mainButtons.push_back(MenuButton("NEW GAME"));
    mainButtons.push_back(MenuButton("SETTINGS"));
    mainButtons.push_back(MenuButton("EXIT"));

    optionsSelection = 0;
    optionsButtons.push_back(MenuButton("CONTINUE"));
    optionsButtons.push_back(MenuButton("SETTINGS"));
    optionsButtons.push_back(MenuButton("MAIN MENU"));

    settingsSelection = 0;
    settingsButtons.push_back(MenuButton("MUSIC ON"));
    settingsButtons.push_back(MenuButton("SOUND ON"));
    settingsButtons.push_back(MenuButton("BACK"));

    moveMap.map = new MoveMap::MoveTile*[MAXMAPHEIGHT];
    checkMap = new uint8_t*[MAXMAPHEIGHT];
    for(int y = 0; y < MAXMAPHEIGHT; y++){
        moveMap.map[y] = new MoveMap::MoveTile[MAXMAPWIDTH];
        checkMap[y] = new uint8_t[MAXMAPWIDTH];
    }

    if(!loadConfig()){
        audioManager.playAllMusic();
        saveConfig();
    }

    int64_t secondTimer = 0;
    int fpsCounter = 0;
    SDL_Log("Starting main loop");
    realFrameTime = SDL_GetTicks();
    realStartTime = realFrameTime;
    proceedFrame = false;
    random.setSeed(realFrameTime);
    gameRandom.setSeed(realFrameTime);

    if(isTest){
        createPlayer();
        newGame();
        if(luaHandler.getLuaFunction("start")){
            if(lua_pcall(luaHandler.luaState, 0, 0, 0) != 0){
                returnCode = 2;
                return;
            }
        }
    }

    resetViews();
    while(gameRunning){
        int64_t previousRealTime = realFrameTime;
        realFrameTime = SDL_GetTicks();
        int64_t realDelta = realFrameTime - previousRealTime;
        
        if(!isPaused){
            frameDelta = realDelta;
        }else{
            frameDelta = 0;
            proceedFrame = false;
            if(Controls::instance()->keyWasPressed(SDL_SCANCODE_F)){
                proceedFrame = true;
                frameDelta = realDelta;
            }
        }
        frameTime += frameDelta;

        fpsCounter++;
        if(realFrameTime - secondTimer > 1000){
            fps = fpsCounter;
            fpsCounter = 0;
            secondTimer = realFrameTime;
        }

        if(!noDraw){
            Controls::instance()->update(frameTime);
            processEvents();
            audioManager.update();
        }

        update();
    
        if(!noDraw)
            draw();
#ifndef ANDROID
        SDL_Delay(1); 
#endif
        if(isTest && timeout != 0){
            if(realFrameTime - timeoutStart > timeout){
                SDL_Log("Timed out, stopping");
                if(luaHandler.getLuaFunction("timeout")){
                    if(lua_pcall(luaHandler.luaState, 0, 1, 0) != 0)
                        returnCode = 2;
                    else{
                        bool result = lua_toboolean(luaHandler.luaState, -1);
                        if(!result)
                            returnCode = 1;
                    }
                }
                stop();
            } 
        }
    }

    if(returnCode == 0 && currentView == GameView::Playing && playerTurn)
        saveGame();
}

void Game::createPlayer(){
    ship = getShip(shipLoader.getShipDescription("player"));
    ship->playerShip = true;
    ship->view = &playerView;
    for(int i = 0; i < 5; i++){
        int position = gameRandom.max(4);
        addCrew(createCrewFromDescription(crews[position][gameRandom.max(crews[position].size())]), ship, findSpawnPoint(ship), true);
    }
}

void Game::newGame(){
    // Clear all non temporary game state to starting values
    currentRecord.encounterCounter = 0;
    currentRecord.shipDestroyedCounter = 0;
    currentRecord.distance = 0;
    
    playerTurn = true;
    endedTurn = false;

    turnCounter = 0;
    RMAmount = 0;
    fuelAmount = 0;

    difficulty.reset();

    // Star map is widthxheight
    mapWindow.selection.x = -1;
    mapWindow.selection.y = -1;
    starMap = new StarMap(200, 50);

    mapPosition.x = 6;
    mapPosition.y = starMap->height/2;
    SDL_Point curPoint = mapPosition;
    std::vector<SDL_Point> points;
    points.push_back(mapPosition);
    // Add main game path 
    for(int x = 0; x < 4; x++){
        SDL_Point dest = {curPoint.x + starMap->width/5, gameRandom.range(5, starMap->height-5)};
        curPoint = populateMap(starMap, curPoint, dest, true);
        points.push_back(curPoint);
    }
    SDL_Point finalPoint = SDL_Point{starMap->width - 5, gameRandom.range(5, starMap->height-5)};
    populateMap(starMap, curPoint, finalPoint, true);
    points.push_back(finalPoint);

    // Extra random paths
    for(int i = 0; i < 4; i++){
        int start = i;
        SDL_Point intermediate = {points[start].x + starMap->width/10, (points[start].y + points[start+1].y)/2 + gameRandom.max(starMap->height/2)};
        if(intermediate.y > starMap->height)
            intermediate.y -= starMap->height;
        SDL_Point reached = populateMap(starMap, points[start], intermediate, true);
        populateMap(starMap, reached, points[start+1], true);
    }

    for(int y = 0; y < starMap->height; y++){
        for(int x = 0; x < starMap->width; x++){
            StarMap::StarTile& tile = starMap->tiles[x][y];
            tile.tile |= StarMap::smt_Unexplored;

            if(tile.isStar()){
                if(gameRandom.oneIn(7))
                    tile.tile |= StarMap::smt_Station;
                if(!tile.isStation() && gameRandom.percentage(33))
                    tile.tile |= StarMap::smt_Asteroids;
                if(gameRandom.percentage(25))
                    tile.tile |= StarMap::smt_Clouds;
            }
        }
    }

    starMap->getTile(mapPosition).tile = StarMap::smt_Star;
    mapWindow.mapOffsetY = mapPosition.y*32-5*32;
    exploreStarMap(starMap, mapPosition.x, mapPosition.y, 3, 8);
    inWarp = false;
    doWarp = false;
    isSafe = false;
    isPositionSafe = true;
    spawnEnemyShips = true;
    generateVictory = false;
    clearDialog();
    clearVariables(true);
    hailCallback[0] = '\0';
    destroyCallback[0] = '\0';
    fleeCallback[0] = '\0';
    attackCallback[0] = '\0';
    scanCallback[0] = '\0';

    startGame();
}

/**
 * Set game ready to be played.
 * Clears only temporary game state, such as UI.
 */
void Game::startGame(){
    currentView = GameView::Playing;
    selectedAction = -1;
    selectedCrew = NULL;
    selectedWeapon = NULL;
    selectedScanner = false;
    selectedTeleporter = false;

    installingSystem = NULL;
    displayVictory = false;

    // Player ship always explored
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            ship->getTile(x, y).visibility |= Tile::VisibilityFlags::Explored;
        }
    }
    luaHandler.setPlayerShip(ship);
    calculateShipVisibility(ship, true);
    calculateShipVisibility(ship, false);

    createBackground();
    gameOver = false;
    playerView.isShaking = false;
    enemyView.isShaking = false;
    mapWindow.selection.x = -1;
    mapWindow.selection.y = -1;
    resetViews();

    if(inWarp || destroyingShip)
        shipAnimationTimer = frameTime;
}

void Game::processEvents(){
    SDL_Event event;
    while(SDL_PollEvent(&event)){
        switch(event.type){
            case SDL_WINDOWEVENT:
                switch(event.window.event){
                    case SDL_WINDOWEVENT_CLOSE:
                    if(isTest)
                        returnCode = 1;
                    stop();
                    break;
                    case SDL_WINDOWEVENT_RESIZED:
                    SDL_Log("Resized to %dx%d", event.window.data1, event.window.data2);
                    drawer.resize(event.window.data1, event.window.data2);
                    if(currentView == GameView::Playing)
                        createBackground();
                    if(ship)
                        centerViews();
                    break;
                }
                break;
            case SDL_FINGERUP:
                Controls::instance()->removeFinger();
                break;
            case SDL_FINGERDOWN:
                Controls::instance()->addFinger();
                break;
            case SDL_MULTIGESTURE:
                //SDL_Log("Multi gesture, fingers: %d", event.mgesture.numFingers);  
                Controls::instance()->setPinch(event.mgesture.dDist);
                break;
            case SDL_TEXTINPUT:
#ifndef ANDROID
                if(luaWindow.visible){
                    strcat(luaWindow.luaCommand, event.text.text);
                }
#endif
                break;
#ifdef ANDROID
            case SDL_APP_WILLENTERBACKGROUND:
                if(currentView == GameView::Playing){
                    saveGame();
                }
                break;
#endif
        }
    }
}

void Game::removeMenuSelections(){
    selectedAction=-1;
    selectedTeleporter = false;
    teleporterCrew = NULL;
    selectedWeapon = NULL;
    selectedScanner = false;

    installingSystem = NULL;

    powerWindow.visible = false;
    powerWindow.selection = 0;

    mapWindow.selection.x = -1;
    mapWindow.selection.y = -1;
    mapWindow.visible = false;

    inventoryWindow.visible = false;

    systemWindow.visible = false;

    stationWindow.visible = false;

    crewWindow.visible = false;
    crewWindow.crew = NULL;

    dialogWindow.visible = false;
    clearDialog();

    crewSelectionWindow.visible = false;
}

void Game::endSelectedCrewTurn(){
    selectedAction = -1;
    selectedCrew->turnDone = true;
    selectedCrew = NULL;
    removeMenuSelections();

    moveMap.clear();
}

void Game::endEnemyTurn(){
    for(int i = 0; i < enemyCrews.size(); i++){
        enemyCrews[i]->clearTurn();
    }

    if(enemyShip){
        updateFire(enemyShip);
        enemyShip->endTurn();
        if(enemyShip->armor == 0)
            destroyShip(false);
        spawnAsteroids(enemyShip);
    }

    playerTurn = true;
    turnTimer = frameTime;
    saveGame();
}

void Game::endTurn(){
    endedTurn = true;

    for(int i = 0; i < playerCrews.size(); i++){
        Crew* crew = playerCrews[i];
        if(crew->repeatAction != Crew::None && crew->turnDone == false){
            if(crew->repeatAction == Crew::Repairing)
                repair(crew, crew->targetPosition.x, crew->targetPosition.y);
            else if(crew->repeatAction == Crew::Healing)
                heal(crew, crew->targetPosition.x, crew->targetPosition.y);
            else if(crew->repeatAction == Crew::Attacking)
                attack(crew, crew->targetPosition.x, crew->targetPosition.y);
            return;
        }
    }

    // Call backs may add or remove existing callbacks
    // To avoid issues in iteration we restart on every
    // callback called
    while(true){
        bool restart = false;
        auto it = turnCallbacks.begin();
        while(it != turnCallbacks.end()){
            TurnCallback& callback = *it;
            if(callback.turn == turnCounter){
                char callbackName[200];
                strcpy(callbackName, callback.callback);
                turnCallbacks.erase(it);
                clearDialog();
                if(luaHandler.getLuaFunction(callbackName))
                    lua_pcall(luaHandler.luaState, 0, 0, 0);
                restart = true;
                // Callback opened dialog so stop ending turn until it is closed
                if(dialogWindow.visible){
                    return;
                }
                break;
            }else{
                it++;
            }
        }
        if(!restart)
            break;
    }

    for(int i = 0; i < shots.size(); i++){
        Shot& shot = shots[i];
        if(shot.waiting && (!shot.inEnemyView || enemyCrews.size() == 0)){
            shot.waiting = false;
            shot.startTime = frameTime;
            return;
        }
        if(shot.waiting == false)
            return;
    }
    selectedCrew = NULL;
    selectedWeapon = NULL;
    selectedScanner = false;
    selectedTeleporter = false;

    endPlayerTurn();
    spawnAsteroids(ship);

    bool enemyHasMoves = false;
    for(int i = 0; i < enemyCrews.size();i++){
        if(enemyCrews[i]->hostile){
            enemyHasMoves = true;
            break;
        }
    }
    if(enemyHasMoves){
        playerTurn = false;
        turnTimer = frameTime;
        calculateShipVisibility(ship, false);
    }else{
        if(enemyShip){
            updateFire(enemyShip);
            enemyShip->endTurn();
        }
    }
    if(enemyShip && enemyShip->armor == 0){
        destroyShip(false);
    }else if(spawnEnemyShips){
        if(!inWarp && enemyShip == NULL && gameRandom.oneIn(20)){
            if(enemyCrews.size() == 0)
                clearEnemyVisibility();
            spawnEnemyShip();
            addMessage(stringLibrary.getString("ship_appeared"));
        }
    }
    if(enemyShip)
        calculateShipVisibility(enemyShip, true);
    
    endedTurn = false;

    checkAndClearIfSafe();

    turnCounter++;
    saveGame();
}

void Game::endPlayerTurn(){
    for(uint8_t i = 0; i < playerCrews.size(); i++){
        Crew* crew = playerCrews[i];
        crew->clearTurn();
    }
    updateFire(ship);
    ship->endTurn();
}

void Game::createBackground(){
    asteroids.clear();
    clouds.clear();
    if(starMap->getTile(mapPosition).isAsteroids()){
        for(int i = 0; i < 30; i++){
            asteroids.push_back(Asteroid(cachedSprites.asteroids->getSprite(random.max(cachedSprites.asteroids->sprites.size())),
                                         random.max(drawer.displayWidth), random.max(drawer.displayHeight), random));
        }
    }
    if(starMap->getTile(mapPosition).isClouds()){
        for(int i = 0; i < 30; i++){
            clouds.push_back(Cloud(cachedSprites.clouds->getSprite(random.max(cachedSprites.clouds->sprites.size())), random, drawer.displayWidth, drawer.displayHeight));
        }
    }
}

/**
 * Damage everything in given tile.
 */
void Game::damageTile(Ship* ship, int x, int y, int amount, bool damageShip){
    Crew* crew = ship->getTileCrew(x, y);
    if(crew){
        damageCrew(crew, amount);
    }

    if(!damageShip)
        return;

    Door* door = ship->getDoor(x, y);
    if(door){
        door->health -= amount;
        if(door->health < 0)
            door->health = 0;
        if(door->health == 0)
            door->isOpen = true;
    }
    System* system = ship->getTileSystem(x, y);
    if(system){
        damageSystem(system, amount);
        if(system->isBroken()){
            ship->directPower(system, -system->power);
            if(system == ship->scanner){
                calculateShipVisibility(ship, ship->playerShip);
            }
        }
    }
}

void Game::damageSystem(System* system, int amount){
    if(system->health <= amount)
        system->health = 0;
    else
        system->health -= amount;
}

void Game::damageCrew(Crew* crew, int amount){
    crew->damage(amount);
    if(crew->health == 0){
        killCrew(crew);
    }
}

void Game::updateFire(Ship* ship){
    bool hasFire = false;
    // Spread fire
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            Tile& tile = ship->getTile(x, y);
            if(tile.isEmpty() == false && tile.isSystem() == false && tile.isExternal() == false && tile.canCreateFire() &&
               ship->getTileCrew(x, y) == NULL){
                if(tile.getDoor() && tile.getDoor()->isOpen == false)
                    continue;

                if(y > 0 && !ship->getTile(x, y-1).isNewFire() && ship->getTile(x, y-1).isFire() && gameRandom.percentage(25)){
                    tile.setFire(); 
                }
                if(y < ship->height-1 && !ship->getTile(x, y+1).isNewFire() && ship->getTile(x, y+1).isFire() && gameRandom.percentage(25)){
                    tile.setFire(); 
                }
                if(x > 0 && !ship->getTile(x-1, y).isNewFire() && ship->getTile(x-1, y).isFire() && gameRandom.percentage(25)){
                    tile.setFire(); 
                }
                if(x < ship->width-1 && !ship->getTile(x+1, y).isNewFire() && ship->getTile(x+1, y).isFire() && gameRandom.percentage(25)){
                    tile.setFire(); 
                }
            }
        }
    }

    // Apply fire damage and update lifespan
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            Tile& tile = ship->getTile(x, y);
            if(tile.isFire() && !tile.isNewFire()){
                hasFire = true;
                if(y > 0)
                    damageTile(ship, x, y-1, 2);
                if(y < ship->height-1)
                    damageTile(ship, x, y+1, 2);
                if(x > 0)
                    damageTile(ship, x-1, y, 2);
                if(x < ship->width-1)
                    damageTile(ship, x+1, y, 2);
                
                tile.fire--;
            }else if(tile.isFireCooldown()){
                tile.fire--;
            }else if(tile.isNewFire()){
                tile.fire--;
            }
        }
    }
    if(hasFire && ship->fireArmorDamageCooldown == 0){
        ship->armor -= 1;
        ship->fireArmorDamageCooldown = 3;
    }else if(hasFire){
        ship->fireArmorDamageCooldown--;
    }
}

void Game::fireLaserAtShot(Ship* source, Weapon* weapon, Crew* crew, int startX, int startY, Shot* targetShot, bool missed){
    Laser laser;
    laser.source = source;
    laser.dest = NULL;
    laser.sourceStart.x = startX + weapon->fireOffset.x;
    laser.sourceStart.y = startY + weapon->fireOffset.y;
    laser.targetEnd.x = 0;
    laser.targetEnd.y = 0;
    laser.startTime = frameTime;
    laser.targetAngle = random.range(-40, 40);
    laser.damage = weapon->getDamage();
    laser.damageTime = 0;
    laser.fireAtShot = targetShot;
    laser.missed = missed;
    lasers.push_back(laser);
}

void Game::fireShot(Ship* source, Ship* dest, Weapon* weapon, Crew* crew, int startX, int startY, int endX, int endY){

    if(weapon->weaponType == Weapon::Laser){
        Laser laser;
        laser.source = source;
        laser.dest = dest;
        laser.sourceStart.x = startX + weapon->fireOffset.x;
        laser.sourceStart.y = startY + weapon->fireOffset.y;
        laser.targetEnd.x = endX;
        laser.targetEnd.y = endY;
        laser.startTime = frameTime;
        laser.damage = weapon->getDamage();
        if(source->mode == Ship::ShipMode::Offensive)
            laser.damage += 2;
        if(dest->mode == Ship::ShipMode::Defensive)
            laser.damage -= 2;
        laser.damageTime = 0;
        laser.fireAtShot = NULL;
        int evasion = dest->getEvasion();
        if(starMap->getTile(mapPosition).isClouds())
            evasion += 20;
        laser.missed = gameRandom.percentage(evasion);
        if(laser.missed){
            addMessage(stringLibrary.getString("missed"));
            laser.targetAngle = random.range(-10, 10);
        }else{
            laser.targetAngle = random.range(-40, 40);
        }
        lasers.push_back(laser);
    }else{
        for(int i = 0; i < weapon->shots; i++){
            Shot shot;
            if(i == 0){
                shot.waitingToFire = false;
                audioManager.playSound("launch");
            }else{
                shot.waitingToFire = true;
            }
            shot.hit = false;
            shot.missed = false;
            shot.destroy = false;
            shot.waiting = false;
            shot.asteroid = false;
            shot.source = source;
            shot.dest = dest;

            shot.inEnemyView = !source->playerShip;

            // Todo: Boost damage by proficiency?
            shot.damage = weapon->getDamage();// + crew->skills[Crew::Weapons].level;
            if(source->mode == Ship::ShipMode::Offensive)
                shot.damage += 2;
            shot.fireChance = weapon->fireChance;
            shot.size = weapon->damageSize;
            shot.startTime = frameTime;
            shot.start.x = startX + weapon->fireOffset.x;
            shot.start.y = startY + weapon->fireOffset.y;
            shot.target.x = endX;
            shot.target.y = endY;
            shots.push_back(shot);
        }
    }
}

void Game::spawnAsteroids(Ship* ship){
    if(!starMap->getTile(mapPosition).isAsteroids())
        return;

    if(gameRandom.oneIn(2))
        return;

    int numAsteroids = gameRandom.max(3);
    for(int i = 0; i < numAsteroids; i++){
        spawnAsteroid(ship);
    }
}

void Game::spawnAsteroid(Ship* ship){
    Shot asteroid;
    asteroid.hit = false;
    asteroid.destroy = false;
    asteroid.missed = false;
    asteroid.waiting = true;
    asteroid.asteroid = true;
    asteroid.source = NULL;
    asteroid.dest = ship;
    asteroid.inEnemyView = !ship->playerShip;
    asteroid.damage = 10;
    asteroid.size = 3;
    asteroid.startTime = frameTime;
    asteroid.start.y = -100 - ship->view->offset.y;
    asteroid.start.x = random.max((drawer.displayWidth*ship->view->zoom)/2);
    do{
        asteroid.target.x = gameRandom.max(ship->width);
        asteroid.target.y = gameRandom.max(ship->height);
    }while(ship->getTile(asteroid.target).isEmpty());
    asteroid.waitingToFire = false;
    asteroid.distance = UINT32_MAX;
    shots.push_back(asteroid);
}

/**
 * @return true if ship destroyed.
 */
bool Game::attackShip(Ship* ship, int amount, int x, int y, int size, int fire){

    if(amount < 0)
        amount = 0;

    if(ship->shield == NULL || ship->shield->shield == 0){

        for(int sy = 0; sy < ship->height; sy++){
            for(int sx = 0; sx < ship->width; sx++){
                int distance = 0;
                const Tile& tile = ship->getTile(sx, sy);
                if(tile.isEmpty() == false && (distance = getTileDistance(x, y, sx, sy)) < size){
                    int damage = amount;
                    int fireChance = fire;
                    if(distance > 0){
                        damage = amount/(distance*4);
                        fireChance = fireChance/(distance*2);
                    }
                    damageTile(ship, sx, sy, damage);
                    if(fireChance != 0 && tile.isFire() == false && tile.isExternal() == false && gameRandom.percentage(fireChance)){
                        SDL_Point firePoint = findClosestFreePoint(ship, x, y);
                        if(firePoint.x != -1)
                            ship->getTile(firePoint.x, firePoint.y).setFire();
                    }
                }
            }
        }

        if(ship->armor <= amount)
            ship->armor = 0;
        else
            ship->armor -= amount;

    }else{
        if(ship->shield->shield <= amount)
            ship->shield->shield = 0;
        else
            ship->shield->shield -= amount;
    }

    if(!ship->playerShip && ship->armor == 0){
        destroyShip(false);
        return true;
    }
    shake(*ship->view);
    return false;
}

void Game::repairShip(Ship* ship, int amount){
    if(ship->playerShip)
        gainedArmor += (ship->armor - ship->armorMax);
    ship->armor += amount;
    if(ship->armor > ship->armorMax)
        ship->armor = ship->armorMax;
}

/**
 * Have crew repair given position
 */
void Game::repair(Crew* crew, int x, int y){
    SDL_Point point = findClosestAdjacentMovePoint(crew, x, y);
    moveCrew(crew, crew->ship, point.x, point.y);

    crew->action = Crew::Repairing;
    crew->repeatAction = Crew::None;
    crew->actionAmount = 10;
    if(hasProficiency(crew, "repairs")){
        crew->actionAmount += skillLerp(crew, Crew::Engineering, 0, 10);
    }
    if(isSafe)
        crew->actionAmount = 1000;
    crew->actionTick = 35;
    crew->targetPosition.x = x;
    crew->targetPosition.y = y;
    crew->actionTime = 0;
    crew->skills[Crew::Engineering].increase(5);
    crew->turnDone = true;
}

bool Game::performRepair(Ship* ship, Crew* crew, int x, int y){
    Door* door = ship->getDoor(x, y);
    if(door){
        if(door->health == 0 && door->forceOpen == false && ship->getTileCrew(x, y) == NULL)
            door->isOpen = false;
        door->health += 1;
        if(door->health >= door->maxHealth){
            door->health = door->maxHealth;
            return true;
        }
        return false;
    }
    System* system = ship->getTileSystem(x, y);
    if(system){
        bool calculateVisibility = (system == ship->scanner && !system->isAvailable());
        bool directPower = system->isBroken();
        repairSystem(system, 1);
        if(!system->isBroken() && directPower){
            ship->directPower(system, system->directedPower);
            if(system->type == System::Weapon){
                ((Weapon*)system)->cooldown += 1;
            }
        }
        if(calculateVisibility && system->isAvailable()){
            calculateShipVisibility(ship, ship->playerShip);
        }

        if(system->health >= system->maxHealth){
            system->health = system->maxHealth;
            return true;
        }
        return false;
    }
    return true;
}

void Game::repairSystem(System* system, int amount){
    system->health += amount;
    if(system->health > system->maxHealth)
        system->health = system->maxHealth;
}

void Game::heal(Crew* crew, int x, int y){
    if(crew->ship->getTileCrew(x, y) != crew){
        SDL_Point point = findClosestAdjacentMovePoint(crew, x, y);
        moveCrew(crew, crew->ship, point.x, point.y);
    }
    crew->action = Crew::Healing;
    crew->repeatAction = Crew::None;
    crew->actionAmount = 10 + skillLerp(crew, Crew::Science, 0, 10);
    if(isSafe)
        crew->actionAmount = 1000;
    crew->actionTick = 35;
    crew->targetPosition.x = x;
    crew->targetPosition.y = y;
    crew->actionTime = 0;
    crew->skills[Crew::Science].increase(5);
    crew->turnDone = true;
}

bool Game::performHeal(Crew* crew, int amount, int x, int y){
    Crew* targetCrew = crew->ship->getTileCrew(x, y);
    if(targetCrew){
        targetCrew->health += amount;
        if(targetCrew->health >= targetCrew->maxHealth){
            targetCrew->health = targetCrew->maxHealth;
            return true;
        }
        return false;
    }
    return true;
}

/**
 * Have crew attack given position
 */
void Game::attack(Crew* crew, int x, int y){
    if(!hasTarget(crew->ship, crew->playerCrew, x, y)){
        crew->action = Crew::None;
        crew->repeatAction = Crew::None;
        return;
    }
    crew->action = Crew::Attacking;
    crew->repeatAction = Crew::None;
    crew->actionAmount = 10 + crew->skills[Crew::Combat].level;
    crew->actionTick = 350/crew->actionAmount;
    crew->targetPosition.x = x;
    crew->targetPosition.y = y;
    crew->actionTime = 0;
    crew->skills[Crew::Combat].increase(5);
    crew->turnDone = true;
}

void Game::setEnemyHostile(bool hostile, Ship* ship){
    if(ship != NULL){
        for(int i = 0; i < ship->crews.size(); i++){
            if(!ship->crews[i]->playerCrew)
                ship->crews[i]->hostile = hostile;
        }
    }else{
        for(int i = 0; i < enemyCrews.size(); i++){
            enemyCrews[i]->hostile = hostile;
        }
    }
    isSafe = false;
}

void Game::hail(){
    if(hailCallback[0] != '\0' && luaHandler.getLuaFunction(hailCallback)){
        clearDialog();
        lua_pcall(luaHandler.luaState, 0, 0, 0);
    }else{
        showDialog("hail", "Your hail is ignored");
    }
}

bool Game::checkIsSafe() const{

    if(!isPositionSafe)
        return false;

    if(playerWarp && inWarp)
        return false;

    if(dialogWindow.visible)
        return false;

    if(starMap->getTile(mapPosition).isAsteroids())
        return false;

    for(int i = 0; i < enemyCrews.size(); i++){
        if(enemyCrews[i]->hostile)
            return false;
    }

    if(shots.size() != 0)
        return false;

    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            if(ship->getTile(x, y).isFire())
                return false;
        }
    }

    return true;
}

void Game::checkAndClearIfSafe(){
    if(!isSafe && !checkIsSafe())
        return;

    isSafe = true;

    for(int i = 0; i < ship->weapons.size(); i++){
        ship->weapons[i]->cooldown = 0;
    }
    if(ship->engine){
        ship->engine->cooldown = 0;
    }
    if(ship->scanner){
        ship->scanner->cooldown = 0;
        ship->scanner->isScanning = false;
    }
    if(ship->shield){
        ship->shield->shield = ship->shield->shieldMax;
        ship->shield->shieldRegenCooldown = 0;
    }
    for(uint8_t i = 0; i < playerCrews.size(); i++){
        Crew* crew = playerCrews[i];
        crew->clearTurn();
    }

    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            if(ship->getTile(x, y).getDoor())
                ship->getTile(x, y).getDoor()->turnDone = false;
        }
    }
}

void Game::resetViews(){
#ifdef ANDROID
    playerView.zoom = 0.75f;
#else
    playerView.zoom = 1.0f;
#endif
    enemyView.zoom = playerView.zoom;
    playerViewSize = 0.55f;
    if(ship)
        centerViews();
}

void Game::centerViews(){
    if(enemyShip == NULL){
        playerView.offset.x = (((float)drawer.displayWidth/2)*playerView.zoom - (ship->width*HALFTILESIZE));
        playerView.offset.y = (((float)drawer.displayHeight/2)*playerView.zoom - (ship->height*HALFTILESIZE));
        return;
    }
    playerView.offset.x = (((float)(drawer.displayWidth*playerViewSize)/2)*playerView.zoom - (ship->width*HALFTILESIZE));
    playerView.offset.y = (((float)drawer.displayHeight/2)*playerView.zoom - (ship->height*HALFTILESIZE));

    enemyView.offset.x = (((float)(drawer.displayWidth - drawer.displayWidth*playerViewSize)/2)*enemyView.zoom - (enemyShip->width*HALFTILESIZE));
    enemyView.offset.y = (((float)drawer.displayHeight/2)*enemyView.zoom - (enemyShip->height*HALFTILESIZE));
}

void Game::spawnStation(){
    enemyDefeated = false;
    StarMap::StarTile& tile = starMap->getTile(mapPosition);
    if(tile.station){
        station = tile.station;
    }else{
        station = new Station();
        tile.station = station;

        station->repairPrice = 15;
        station->healPrice = 15;
        station->mapPrice = 15;
        station->fuelPrice = gameRandom.range(4, 6);
        station->fuelAmount = gameRandom.range(4, 10);
        while(station->items.size() < 3){
            Item item = itemLibrary.items[gameRandom.max(itemLibrary.items.size())];
            if(item.price == 0)
                continue;
            if(item.rarity != 0 && gameRandom.oneIn(item.rarity))
                continue;
            bool found = false;
            for(int i = 0; i < station->items.size(); i++){
                if(strcmp(item.name, station->items[i].name) == 0){
                    found = true;
                    break;
                }
            }
            if(found)
                continue;
            if(item.system != NULL)
                item.amount = 1;
            else
                item.amount = gameRandom.range(2, 10);
            station->items.push_back(item);
        }
    }
    if(station->ship)
        enemyShip = station->ship;
    else{
        std::vector<const ShipDescription*>* stations = shipLoader.getShipList("stations");
        const ShipDescription* description = (*stations)[gameRandom.max(stations->size())];
        spawnEnemyShip(description, true);
    }
    station->ship = enemyShip;
}

void Game::createVictory(){
    gainedItems.clear();
    gainedRM = 0;
    gainedFuel = 0;
    giveReward();

    displayVictory = true;
    enemyDefeated = true;
    generateVictory = false;
}

void Game::giveReward(){
    int rm = gameRandom.range(5, 13);
    int fuel = gameRandom.range(2, 6);
    gainedRM += rm;
    gainedFuel += fuel;
    addFuel(fuel);
    addRM(rm);
    int items = 1;
    if(gameRandom.oneIn(5))
        items++;
    int i = 0;
    while(i < items){
        Item item = itemLibrary.items[gameRandom.max(itemLibrary.items.size())];
        if(item.rarity != 0 && gameRandom.oneIn(item.rarity)){
            item.amount = 1;
            gainedItems.push_back(item);
            addItem(item);
            i++;
        }
    }
}

void Game::destroyShip(bool player){
    addMessage(stringLibrary.getString("destroyed"));
    shipAnimationTimer = frameTime;
    destroyingShip = true;
    playerDestroy = player;
}

void Game::warpEnemyShip(){
    if(enemyShip == NULL)
        return;
    playerWarp = false;
    inWarp = true;
    shipAnimationTimer = frameTime;
    audioManager.playSound("warp");
}

void Game::addRandomSystem(Ship* ship, System::SystemType type){
    RandomVectorIterator<SystemDescription*> iter(systemLoader.systems, gameRandom);
    while(iter.hasNext()){
        const SystemDescription* description = iter.getNext();
        if(description->type == type && getAvailableRoom(ship, description) != -1){
            System* system = (System*)systemLoader.getSystem(description);
            ship->addSystem(system, getAvailableRoom(ship, description));
            ship->directPower(system, system->maxPower);
            break;
        }
    }
}

void Game::spawnEnemyShip(const ShipDescription* description, bool station){
    if(enemyShip)
        removeShip(enemyShip);

    // Clear enemy view of player ship
    if(enemyCrews.size() == 0){
        for(int y = 0; y < ship->height; y++){
            for(int x = 0; x < ship->width; x++){
                ship->getTile(x, y).visibility &= Tile::VisibilityFlags::Full;
            }
        }
    }
    enemyDefeated = false;

    if(description == NULL){
        std::vector<const ShipDescription*>* enemyShips = shipLoader.getShipList("enemy");
        description = (*enemyShips)[gameRandom.max(enemyShips->size())];
    }

    enemyShip = getShip(description);

    // Add a shield if no shield
    if(enemyShip->shield == NULL && enemyShip->numRooms > enemyShip->allSystems.size()){
        int shieldDifficulty = difficulty.difficulty;
        if(station)
            shieldDifficulty += 50;
        const SystemDescription* description = NULL;
        RandomVectorIterator<SystemDescription*> iter(systemLoader.shields, gameRandom);
        while(iter.hasNext()){
            const SystemDescription* shield = iter.getNext();
            if(shield->difficulty == -1)
                continue;
            if(description == NULL){
                description = shield;
            }else if(description &&
                    std::abs(description->difficulty - shieldDifficulty) > std::abs(shield->difficulty - shieldDifficulty) &&
                    getAvailableRoom(enemyShip, shield) != -1){
                description = shield;
            }
        }

        if(description != NULL){
            Shield* shield = (Shield*)systemLoader.getSystem(description);
            enemyShip->addSystem(shield, getAvailableRoom(enemyShip, description));
            enemyShip->directPower(shield, shield->maxPower);
        }
    }

    // Add engine if no engine
    if(!station && enemyShip->engine == NULL){
        const SystemDescription* description = systemLoader.engines[gameRandom.max(systemLoader.engines.size())];
        Engine* engine = (Engine*)systemLoader.getSystem(description);
        enemyShip->addSystem(engine, getAvailableRoom(enemyShip, description));
        enemyShip->directPower(engine, engine->maxPower);
    }

    // Calculate current damage per turn
    int dpt = 0;
    for(int i = 0; i < enemyShip->weapons.size(); i++){
        dpt += enemyShip->weapons[i]->getDamagePerTurn();
    }
    int targetDpt = difficulty.getDPT();
    if(station)
        targetDpt *= 1.75f;

    int deltaDpt = targetDpt - dpt;
    // Add random weapons
    if(deltaDpt >= 4 && enemyShip->freePower > 0){
        // We find the number of free weapon rooms
        // Then we decide a random number of those rooms to use
        // We try to fill each room with a weapon
        int freeWeaponRooms = 0;
        for(int i = 0; i < enemyShip->numRooms; i++){
            if(enemyShip->rooms[i].isExternal && enemyShip->rooms[i].system == NULL)
                freeWeaponRooms++;
        }

        SDL_Log("DPT: %d TargetDPT: %d Delta: %d", dpt, targetDpt, deltaDpt);
        if(freeWeaponRooms > 0){

            // Constrain rooms to reasonable damage
            if(deltaDpt/freeWeaponRooms <= 4)
                freeWeaponRooms = deltaDpt/4;

            // Get number of rooms to fill
            int useRooms = freeWeaponRooms;
            if(deltaDpt <= 5)
                useRooms = 1;
            else if(useRooms > 1)
                useRooms = gameRandom.max(freeWeaponRooms)+1;

            int weaponDpt = deltaDpt/useRooms;
            SDL_Log("UseRooms: %d, WeaponTarget: %d", useRooms, weaponDpt);
            for(int i = 0; i < enemyShip->numRooms && useRooms > 0; i++){
                const Room& room = enemyShip->rooms[i];
                if(room.system == NULL && room.isExternal){
                    const SystemDescription* cur = NULL;
                    RandomVectorIterator<SystemDescription*> iter(systemLoader.weapons, gameRandom);
                    //Add a little extra randomness to current weapon selection target dpt
                    int curWeaponTarget = weaponDpt;
                    if(dpt + weaponDpt <= targetDpt)
                        curWeaponTarget = weaponDpt+gameRandom.range(-1, 2);
                    SDL_Log("Current weapon selection dpt target: %d", curWeaponTarget);
                    while(iter.hasNext()){
                        const SystemDescription* description = iter.getNext();
                        if(description->getDamagePerTurn() < 2 || description->getDamagePerTurn() > curWeaponTarget+2)
                            continue;
                        if(cur == NULL && getAvailableRoom(enemyShip, description) != -1){
                            cur = description;
                        }else if(cur &&
                                std::abs(cur->getDamagePerTurn() - curWeaponTarget) > std::abs(description->getDamagePerTurn() - curWeaponTarget) &&
                                getAvailableRoom(enemyShip, description) != -1){
                            SDL_Log("\tSeleting %s dpt %d as best candidate compared to previous dpt %d", description->name, description->getDamagePerTurn(), cur->getDamagePerTurn());
                            cur = description;
                        }
                    }
                    if(cur){
                        Weapon* newWeapon = (Weapon*)systemLoader.getSystem(cur);
                        enemyShip->addSystem(newWeapon, i+1);
                        enemyShip->directPower(newWeapon, newWeapon->maxPower);
                        dpt += newWeapon->getDamagePerTurn();
                        SDL_Log("\tAdding Weapon %s new Dpt: %d", newWeapon->name, dpt);
                        useRooms--;
                    }else{
                        break;
                    }
                }
            }
            SDL_Log("Final DPT: %d vs Target: %d", dpt, targetDpt);
        }
    }

    // Add other systems
    if(enemyShip->scanner == NULL && gameRandom.percentage(50)){
        addRandomSystem(enemyShip, System::Scanner);
    }
    if(enemyShip->sickbay == NULL && gameRandom.percentage(50)){
        addRandomSystem(enemyShip, System::Sickbay);
    }
    if(enemyShip->teleporter == NULL && gameRandom.percentage(99)){
        addRandomSystem(enemyShip, System::Teleporter);
    }

    enemyShip->playerShip = false;
    enemyShip->view = &enemyView;
    luaHandler.setEnemyShip(enemyShip);
    for(int y = 0; y < enemyShip->height; y++){
        for(int x = 0; x < enemyShip->width; x++){
            enemyShip->getTile(x, y).visibility = Tile::VisibilityFlags::Enemy_Explored;
        }
    }

    // If enemy ship has no scanners, it keeps doors open to increase visibility
    if(enemyShip->scanner == NULL){
        for(int y = 0; y < enemyShip->height; y++){
            for(int x = 0; x < enemyShip->width; x++){
                Door* door = enemyShip->getTile(x, y).getDoor();
                if(door){
                    door->isOpen = true;
                    door->forceOpen = true;
                }
            }
        }
    }

    createEnemyCrew(description->crew);
    if(!station)
        setEnemyHostile(true);
    calculateShipVisibility(enemyShip, false);
    centerViews();
    isSafe = false;
}

void Game::createEnemyCrew(int num){
    std::vector<SDL_Point> spawnPoints;
    spawnPoints.push_back(enemyShip->captainPosition);
    for(int y = 0; y < enemyShip->height; y++){
        for(int x = 0; x < enemyShip->width; x++){
            if(enemyShip->getTile(x, y).isWeaponsStation()){
                SDL_Point point = {x, y};
                spawnPoints.push_back(point);
            }
        }
    }

    if(enemyShip->opsPosition.x != -1)
        spawnPoints.push_back(enemyShip->opsPosition);

    for(int i = 0; i < num; i++){
        SDL_Point spawnPoint;
        if(i < spawnPoints.size())
            spawnPoint = spawnPoints[i];
        else
            spawnPoint = findClosestFreePoint(enemyShip, spawnPoints[0].x, spawnPoints[0].y);

        addCrew(createRandomCrew(), enemyShip, spawnPoint, false);
    }
}

void Game::pause(){
    isPaused = true;
}

void Game::update(){

    if(currentView == GameView::Menu){

        mainButtons[0].visible = showLoadGame;
        for(int i = 0; i < mainButtons.size(); i++){
            mainButtons[i].point.x = drawer.uiWidth/2;
            mainButtons[i].point.y = drawer.uiHeight/2 + i*90 - 60;
        }

        updateMenuSelection(mainButtons, mainSelection);

        int selection = -1;
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
            selection = mainSelection;
        }
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            audioManager.playSound("select");
            for(int i = 0; i < mainButtons.size(); i++){
                if(mainButtons[i].visible && isInButton(mainButtons[i])){
                    selection = i;
                    break;
                }
            }
        }
        if(selection == 1){
            currentView = GameView::Hangar;
            resetViews();
            if(ship){
                removeShip(ship);
            }
            ship = getShip(shipLoader.getShipDescription("player"));
            ship->playerShip = true;
            ship->view = &playerView;
            calculateShipVisibility(ship, true);
            for(int y = 0; y < 4; y++){
                selectedCrews[y] = 0;
                for(int x = 0; x < crews[y].size(); x++)
                    crews[y][x].selected = false;
            }
        }else if(selection == 0){
            loadGame();
            startGame();
        }else if(selection == 2){
            settingsParent = currentView;
            currentView = GameView::Settings;
            settingsSelection = 0;
        }else if(selection == 3){
            stop();
        }
        return;
    }else if(currentView == GameView::Options){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            currentView = GameView::Playing;
            return;
        }

        updateMenuSelection(optionsButtons, optionsSelection);

        for(int i = 0; i < optionsButtons.size(); i++){
            optionsButtons[i].point.x = drawer.uiWidth/2;
            optionsButtons[i].point.y = drawer.uiHeight/2 + i*90 - 60;
        }

        int selection = -1;
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
            selection = optionsSelection;
        }
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            audioManager.playSound("select");
            for(int i = 0; i < optionsButtons.size(); i++){
                if(optionsButtons[i].visible && isInButton(optionsButtons[i])){
                    selection = i;
                    break;
                }
            }
        }
        if(selection == 0){
            currentView = GameView::Playing;
        }else if(selection == 1){
            settingsParent = currentView;
            currentView = GameView::Settings;
            settingsSelection = 0;
        }else if(selection == 2){
            saveGame();
            clearGame();
        }
        return;
    }else if(currentView == GameView::Settings){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            currentView = settingsParent;
            return;
        }

        if(audioManager.isMusicPlaying()){
            settingsButtons[0].setText("MUSIC ON");
        }else{
            settingsButtons[0].setText("MUSIC OFF");
        }
        if(audioManager.isSoundEnabled()){
            settingsButtons[1].setText("SOUND ON");
        }else{
            settingsButtons[1].setText("SOUND OFF");
        }

        updateMenuSelection(settingsButtons, settingsSelection);

        for(int i = 0; i < settingsButtons.size(); i++){
            settingsButtons[i].point.x = drawer.uiWidth/2;
            settingsButtons[i].point.y = drawer.uiHeight/2 + i*90 - 60;
        }

        int selection = -1;
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
            selection = settingsSelection;
        }
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            audioManager.playSound("select");
            for(int i = 0; i < settingsButtons.size(); i++){
                if(settingsButtons[i].visible && isInButton(settingsButtons[i])){
                    selection = i;
                    break;
                }
            }
        }
        if(selection == 0){
            if(audioManager.isMusicPlaying()){
                audioManager.stopMusic();
            }else{
                audioManager.playAllMusic();
            }
            saveConfig();
        }else if(selection == 1){
            if(audioManager.isSoundEnabled()){
                audioManager.enableSound(false);
            }else{
                audioManager.enableSound(true);
            }
            saveConfig();
        }else if(selection == 2){
            currentView = settingsParent;
        }
        return;
    }else if(currentView == GameView::Hangar){
        updateHangar();
        return;
    }
    
    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_TAB))
        debug = !debug;

    if(Controls::instance()->keyIsDown(SDL_SCANCODE_TAB) &&
       Controls::instance()->keyWasPressed(SDL_SCANCODE_E)){
        debugPlayerView = !debugPlayerView;    
    }

    if(!gameOver && 
       (ship->armor == 0 || playerCrews.size() == 0)){
        endGame();
        return;
    }

    updateWindow(powerWindow);
    updateWindow(mapWindow);
    updateWindow(inventoryWindow);
    updateWindow(systemWindow);
    updateWindow(stationWindow);
    updateWindow(crewWindow);
    updateWindow(dialogWindow);
    updateWindow(crewSelectionWindow);
    updateWindow(luaWindow);

#ifndef ANDROID
    if(!luaWindow.visible && Controls::instance()->keyWasPressed(SDL_SCANCODE_L)){
        showWindow(luaWindow);
        luaWindow.luaCommand[0] = '\0';
    }
    if(luaWindow.visible){
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_BACKSPACE)){
            if(luaWindow.luaCommand[0] != '\0'){
                luaWindow.luaCommand[strlen(luaWindow.luaCommand)-1] = '\0';
            }
        }

        bool loadCommand = false;
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_UP) && luaWindow.history.size() > 0){
            luaWindow.backtrack++;
            if(luaWindow.backtrack > luaWindow.history.size())
                luaWindow.backtrack = luaWindow.history.size();
            loadCommand = true;
        }else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_DOWN)){
            luaWindow.backtrack--;
            if(luaWindow.backtrack <= 0){
                luaWindow.backtrack = 0;
                luaWindow.luaCommand[0] = '\0';
            }
            loadCommand = true;
        }

        if(loadCommand && luaWindow.backtrack != 0){
            strcpy(luaWindow.luaCommand, luaWindow.history[luaWindow.history.size() - luaWindow.backtrack].c_str());
        }

        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            luaWindow.visible = false;
        }else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN) &&
                 luaWindow.luaCommand[0] != '\0'){
            SDL_Log("Execute lua command: '%s'", luaWindow.luaCommand);
            luaHandler.executeCommand(luaWindow.luaCommand);
            if(luaWindow.history.size() < 50){
                luaWindow.history.push_back(luaWindow.luaCommand);
            }else{
                luaWindow.history.erase(luaWindow.history.begin());
                luaWindow.history.push_back(luaWindow.luaCommand);
            }
            luaWindow.backtrack = 0;
            luaWindow.luaCommand[0] = '\0';
            if(resetLua){
                resetLua = false;
                luaHandler.init();
                luaHandler.loadScript("encounters.lua");
                luaHandler.loadScript("story.lua");
                luaHandler.loadScript("game.lua");
            }
        }
        return;
    }
#endif

    // Update enemy warp
    if(inWarp && !playerWarp){
        if(frameTime - shipAnimationTimer > 1000){
            inWarp = false;
            removeShip(enemyShip);
            enemyShip = NULL;
            centerViews();
        }
        if(dialogWindow.visible == false){
            return;
        }
    }else if(destroyingShip && !playerDestroy){
        if(frameTime - shipAnimationTimer > 1000){
            destroyingShip = false;
            removeShip(enemyShip);
            currentRecord.shipDestroyedCounter++;
            difficulty.increase(1);
            if(station != NULL){
                delete station;
                station = NULL;
                StarMap::StarTile& tile = starMap->getTile(mapPosition);
                tile.station = NULL;
                tile.tile &= (~StarMap::smt_Station);
            }
            enemyShip = NULL;
            playerTurn = true;
            centerViews();

            // If enemy was already defeated,
            // do not create new victory on ship destroy
            if(enemyDefeated == false){
                generateVictory = true;
            }else{
                enemyDefeated = false;
            }

            if(destroyCallback[0] != '\0' && luaHandler.getLuaFunction(destroyCallback)){
                clearDialog();
                lua_pcall(luaHandler.luaState, 0, 0, 0);
                destroyCallback[0] = '\0';
            }
        }
        if(dialogWindow.visible == false){
            return;
        }
    }
    if(dialogWindow.visible){

        if(dialogWindow.animateText &&
           realFrameTime - dialogWindow.openTime < dialogWindow.dialogLen*15){
            if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) ||
               Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
                dialogWindow.animateText = false;
            }
            return;
        }

        int selection = -1;
        if(dialogWindow.numOptions > 0){
            if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_UP)){
                dialogWindow.optionSelection--;
                if(dialogWindow.optionSelection < 0)
                    dialogWindow.optionSelection = dialogWindow.numOptions-1;
            }else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_DOWN)){
                dialogWindow.optionSelection++;
                if(dialogWindow.optionSelection >= dialogWindow.numOptions)
                    dialogWindow.optionSelection = 0;
            }else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
                selection = dialogWindow.optionSelection; 
            }
        }

        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            if(!isInWindow(dialogWindow)){
                if(dialogWindow.numOptions == 0){
                    removeMenuSelections();
                }
                return;
            }
            if(dialogWindow.numOptions > 0){
                selection = getUIMouseY() - dialogWindow.offsetY - dialogWindow.height;
                selection = selection/-40;
                selection = (dialogWindow.numOptions-1) - selection;
            }
        }else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_1)){
            selection = 0;
        }else if(dialogWindow.numOptions > 1 && Controls::instance()->keyWasPressed(SDL_SCANCODE_2)){
            selection = 1;
        }else if(dialogWindow.numOptions > 2 && Controls::instance()->keyWasPressed(SDL_SCANCODE_3)){
            selection = 2;
        }

        if(selection >= 0 && selection < dialogWindow.numOptions){
            SDL_Log("Selection: %d '%s' Callback: '%s'", selection, dialogWindow.options[selection].text, dialogWindow.options[selection].callback);
            if(dialogWindow.options[selection].crewSelect != 0){
                strcpy(crewSelectionWindow.callback, dialogWindow.options[selection].callback);
                crewSelectionWindow.numSelect = dialogWindow.options[selection].crewSelect;
                showWindow(crewSelectionWindow);
                crewSelectionWindow.numSelectedCrews = 0;
                for(int i = 0; i < playerCrews.size(); i++)
                    crewSelectionWindow.selectedCrews[i] = false;
                dialogWindow.visible = false;
                dialogWindow.numOptions = 0;
            }else if(dialogWindow.options[selection].callback[0] != '\0' && luaHandler.getLuaFunction(dialogWindow.options[selection].callback)){
                removeMenuSelections();
                lua_pcall(luaHandler.luaState, 0, 0, 0);
                checkAndClearIfSafe();
            }else{
                removeMenuSelections();
                checkAndClearIfSafe();
            }
        }
        return;
    }

    if(gameOver){
        if(isTest){
            if(luaHandler.getLuaFunction("gameover")){
                if(lua_pcall(luaHandler.luaState, 0, 1, 0) != 0)
                    returnCode = 2;
                else{
                    bool result = lua_toboolean(luaHandler.luaState, -1);
                    if(!result)
                        returnCode = 1;
                }
            }
            stop();
        }
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) ||
           Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE) ||
           Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
            clearGame();
        }
        return;
    }


    // Check for pause
    if(isPaused){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_P)){
            isPaused = false;
        }
        if(!proceedFrame)
            return;
    }else{
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_P)){
            pause();
            return;
        }
    }

#ifdef ANDROID
    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_AC_BACK)){
        saveGame();
        currentView = GameView::Options;
        return;
    }
#endif

    updateView(playerView);
    if(enemyShip)
        updateView(enemyView);

    if(frameTime - turnTimer < 500){
        return;
    }

    if(crewSelectionWindow.visible){
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) && isInWindow(crewSelectionWindow)){
            SDL_Point pos = getWindowMousePos(crewSelectionWindow);
            if(pos.y > crewSelectionWindow.height - 32 && crewSelectionWindow.numSelectedCrews >= 1){
                removeMenuSelections();
                if(luaHandler.getLuaFunction(crewSelectionWindow.callback)){
                    for(int i = 0; i < playerCrews.size(); i++){
                        if(crewSelectionWindow.selectedCrews[i]){
                            lua_pushlightuserdata(luaHandler.luaState, playerCrews[i]);
                        }
                    }
                    for(int i = 0; i < crewSelectionWindow.numSelect - crewSelectionWindow.numSelectedCrews; i++)
                        lua_pushnil(luaHandler.luaState);
                    lua_pcall(luaHandler.luaState, crewSelectionWindow.numSelectedCrews, 0, 0);
                }
                checkAndClearIfSafe();
                return;
            }
            int selection = (pos.x / 170) + pos.y/(64+drawer.fontHeight)*4;
            if(selection >= 0 && selection < playerCrews.size()){
                if(crewSelectionWindow.selectedCrews[selection]){
                    crewSelectionWindow.selectedCrews[selection] = false;
                    crewSelectionWindow.numSelectedCrews--;
                }else if(crewSelectionWindow.numSelectedCrews < crewSelectionWindow.numSelect){
                    crewSelectionWindow.selectedCrews[selection] = true;
                    crewSelectionWindow.numSelectedCrews++;
                }
            }
        }
        return;
    }

    if(generateVictory){
        createVictory();
        return;
    }

    Shot* waitingToFireShot = NULL;
    bool allShotsWaiting = true;
    for(auto it = shots.begin(); it != shots.end();){
        Shot& shot = *it;
        if(shot.waiting){
            ++it;
            continue;
        }
        if(shot.waitingToFire){
            waitingToFireShot = &shot;
            ++it;
            continue;
        }
        if(frameTime - shot.startTime > 5000 || (!shot.missed && shot.hit) || noDraw){
            int evasion = shot.dest->getEvasion();
            if(starMap->getTile(mapPosition).isClouds())
                evasion += 20;
            if(gameRandom.percentage(evasion)){
                addMessage(shot.dest, shot.target.x*TILESIZE + HALFTILESIZE, shot.target.y*TILESIZE + HALFTILESIZE, stringLibrary.getString("missed"));
                shot.missed = true;
            }else{
                addMessage(shot.dest, shot.target.x*TILESIZE + HALFTILESIZE, shot.target.y*TILESIZE + HALFTILESIZE, stringLibrary.getString("hit"));
                addEffect(spriteManager.getList("explosion"), shot.dest, shot.target.x*TILESIZE + HALFTILESIZE - 24, shot.target.y*TILESIZE + HALFTILESIZE - 24);
                glm::vec2 start(shot.target.x*TILESIZE + HALFTILESIZE, shot.target.y*TILESIZE + HALFTILESIZE); 
                for(int p = 0; p < 150; p++){
                    glm::vec2 vel(random.floatRange(-0.3f, 0.3f), random.floatRange(-0.3f, 0.3f));
                    particleManager.addParticle(new Particle(shot.dest, start, vel, frameTime, 750, red));
                }
                if(shot.dest->mode == Ship::ShipMode::Defensive){
                    shot.damage -= 2;
                }
                audioManager.playSound("explosion", AudioManager::CHANNEL_DAMAGE);
                // If ship destroyed break out of loop since shots might be invalidated
                if(attackShip(shot.dest, shot.damage, shot.target.x, shot.target.y, shot.size, shot.fireChance)){
                    shots.erase(it);
                    return;
                }
            }
        }
        if(frameTime - shot.startTime > 5000 || (shot.hit && !shot.missed) || shot.destroy){
            it = shots.erase(it);
        }else{
            allShotsWaiting = false;
            ++it;
        }
    }

    if(waitingToFireShot != NULL && allShotsWaiting == true){
        waitingToFireShot->waitingToFire = false;
        waitingToFireShot->startTime = frameTime;
        audioManager.playSound("launch");
    }

    for(auto it = lasers.begin(); it != lasers.end(); ){
        Laser& laser = *it;

        audioManager.playSound("laser", AudioManager::CHANNEL_WEAPON, true);
        glm::vec2 start(laser.targetEnd.x*TILESIZE + HALFTILESIZE + random.max(HALFTILESIZE), laser.targetEnd.y*TILESIZE + HALFTILESIZE + random.max(HALFTILESIZE));
        glm::vec2 vel(random.floatRange(-0.3f, 0.3f), random.floatRange(-0.3f, 0.3f));
        laser.damageTime += frameDelta;
        if(!laser.missed && laser.dest != NULL)
            particleManager.addParticle(new Particle(laser.dest, start, vel, frameTime, 500, red));
        if(laser.damageTime > 50){
            laser.damage--;
            laser.damageTime -= 50;
            if(!laser.missed && laser.dest != NULL &&
               attackShip(laser.dest, 1, laser.targetEnd.x, laser.targetEnd.y, 1, 0)){
                laser.damage = 0;
            }
        }

        if(laser.damage == 0 || frameTime - laser.startTime > 5000){
            if(laser.missed == false && laser.dest == NULL && laser.fireAtShot != NULL){
                for(auto sit = shots.begin(); sit != shots.end(); ++sit){
                    if(laser.fireAtShot == &*sit){
                        shots.erase(sit);
                        audioManager.playSound("explosion", AudioManager::CHANNEL_DAMAGE);
                        break;
                    }
                }
            }
            it = lasers.erase(it);
        }else{
            ++it;
        }
    }
    if(lasers.size() > 0)
        return;

    if(updateCrews(playerCrews))
        return;
    if(updateCrews(enemyCrews))
        return;

    if(inWarp && playerWarp){
        if(frameTime - shipAnimationTimer > 1000){
            inWarp = false;
            turnCounter++;
            addMessage(stringLibrary.getString("done_warp"));
            endPlayerTurn();
            ship->engine->cooldown = 3;
            int farVisibility = 3;
            int nearVisibility = 1;
            if(ship->scanner){
                farVisibility = ship->scanner->farVisibility;
                nearVisibility = ship->scanner->nearVisibility;
                // Add a scanners proficiency bonus
                if(ship->opsPosition != SDL_Point{-1, -1}){
                    Crew* crew = ship->getTileCrew(ship->opsPosition.x, ship->opsPosition.y);
                    if(crew && hasProficiency(crew, "scanners")){
                        farVisibility++;
                    }
                }
            }
            exploreStarMap(starMap, mapPosition.x, mapPosition.y, nearVisibility, farVisibility);
            StarMap::StarTile& tile = starMap->getTile(mapPosition);
            if(tile.encounter != NULL){
                beginEncounter(tile.encounter);
                tile.encounter = NULL;
            }else if(tile.isStation()){
                spawnStation();
                std::vector<Encounter*>* stationEncounters = encounterManager.getEncounterList("station");
                while(true){
                    Encounter* encounter = (*stationEncounters)[gameRandom.max(stationEncounters->size())];
                    if(beginEncounter(encounter))
                        break;
                }
            }else if(!tile.isVisited() && gameRandom.percentage(80)){
                std::vector<Encounter*>* randomEncounters = encounterManager.getEncounterList("random");
                while(true){
                    Encounter* encounter = (*randomEncounters)[gameRandom.max(randomEncounters->size())];
                    if(encounter->lastEncountered != currentRecord.encounterCounter && beginEncounter(encounter))
                        break;
                }
            }else if(tile.isVisited() && gameRandom.percentage(75)){
                spawnEnemyShip();
            }
            tile.tile |= StarMap::smt_Visited;

            createBackground();
            checkAndClearIfSafe();
            calculateShipVisibility(ship, true);

            saveGame();
        }
        return;
    }
    if(doWarp){
        warp(warpDestination.x, warpDestination.y);
        return;
    }

    if(Controls::instance()->wasMouseButtonPressed(Controls::MOUSE_LEFT)){
        clickedInPlayerView = getUIMouseX() < drawer.uiWidth*playerViewSize;
    }

    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_C)){
        centerViews();
    }
    if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_EQUALS)){
        if(enemyShip == NULL || getUIMouseX() < drawer.uiWidth*playerViewSize){
            playerView.zoom -= 0.05f;
        }else{
            enemyView.zoom -= 0.05f;
        }
        constrainZoom();
    }else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_MINUS)){
        if(enemyShip == NULL || getUIMouseX() < drawer.uiWidth*playerViewSize){
            playerView.zoom += 0.05f;
        }else{
            enemyView.zoom += 0.05f;
        }
        constrainZoom();
    }

    if(playerTurn == false){

        updateScroll();

        if(frameTime - turnTimer < 650)
            return;
        for(int i = 0; i < shots.size(); i++){
            if(!shots[i].waiting)
                return;
        }
        if(lasers.size() > 0)
            return;

        if(performEnemyTurn())
            return;

        for(int i = 0; i < shots.size(); i++){
            Shot& shot = shots[i];
            if(shot.waiting && shot.inEnemyView){
                shot.waiting = false;
                shot.startTime = frameTime;
                return;
            }
        }
        SDL_Log("Ending enemy turn");
        endEnemyTurn();

        return;
    }

    if(endedTurn){
        endTurn();
        return;
    }
    checkAndClearIfSafe();

    if(!enemyDefeated && enemyShip && enemyCrews.size() == 0){
        createVictory();
    }

    if(isTest){
        if(luaHandler.getLuaFunction("frame")){
            if(lua_pcall(luaHandler.luaState, 0, 0, 0) != 0){
                returnCode = 2;
                return;
            }
        }
        return;
    }

    if(displayVictory){
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) ||
           Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE) ||
           Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN))
            displayVictory = false;
        return;
    }

    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_I)){
        if(inventoryWindow.visible){
            removeMenuSelections();
        }else if(!getOpenWindow()){
            showWindow(inventoryWindow);
            inventoryWindow.selection = -1;
        }
        return;
    }
    if(inventoryWindow.visible){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            removeMenuSelections();
            return;
        }
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            if(!isInWindow(inventoryWindow)){
                removeMenuSelections();
                return;
            }
            
            SDL_Point pos = getWindowMousePos(inventoryWindow);
            if(pos.y < 33*6+drawer.fontHeight){
                inventoryWindow.selection = pos.x/33 + ((pos.y-15)/33)*10;
                if(inventoryWindow.selection >= items.size()){
                    inventoryWindow.selection = -1;
                    audioManager.playSound("invalid");
                }else{
                    audioManager.playSound("select");
                }
            }else if(pos.y > inventoryWindow.height - 33 && pos.x > 270){
                Item& item = items[inventoryWindow.selection];
                addRM(item.recycle);
                if(item.amount == 1)
                    inventoryWindow.selection = -1;
                useItem(item.name, 1);
                addMessage(stringLibrary.getString("recycled"));
            }else if(pos.y > inventoryWindow.height - 33 && pos.x < 270 && inventoryWindow.selection != -1){
                const Item& selectedItem = items[inventoryWindow.selection];
                if(selectedItem.system != NULL){
                    if(!checkIsSafe()){
                        addMessage(stringLibrary.getString("install_safe"));
                        return;
                    }
                    removeMenuSelections(); // Remove any selections to avoid confusion
                    selectedCrew = NULL;
                    installingSystem = systemLoader.getSystemDescription(selectedItem.system);
                    
                }
            }

        }
        return;
    }

    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_M)){
        if(mapWindow.visible)
            removeMenuSelections();
        else if(!getOpenWindow()){
            showMapWindow();
        }
        return;
    }
    if(mapWindow.visible){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            removeMenuSelections();
            return;
        }
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_C)){
            mapWindow.center(mapPosition);
        }

        if(Controls::instance()->isMouseButtonDown(Controls::MOUSE_LEFT) &&
           Controls::instance()->getMouseButtonPressLength(Controls::MOUSE_LEFT) > 200){
            mapWindow.mapOffsetX -= Controls::instance()->getMouseXDiff()*1.5f;
            mapWindow.mapOffsetY -= Controls::instance()->getMouseYDiff()*1.5f;
            if(mapWindow.mapOffsetX < 0)
                mapWindow.mapOffsetX = 0;
            if(mapWindow.mapOffsetY < 0)
                mapWindow.mapOffsetY = 0;

            if(mapWindow.mapOffsetY > starMap->height*MAPTILESIZE - mapWindow.height)
                mapWindow.mapOffsetY = starMap->height*MAPTILESIZE - mapWindow.height;
            if(mapWindow.mapOffsetX > starMap->width*MAPTILESIZE - mapWindow.width)
                mapWindow.mapOffsetX = starMap->width*MAPTILESIZE - mapWindow.width;
            return;
        }
        
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_LEFT)){
            mapWindow.mapOffsetX -= MAPTILESIZE;
        }else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_RIGHT)){
            mapWindow.mapOffsetX += MAPTILESIZE;
        }else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_UP)){
            mapWindow.mapOffsetY -= MAPTILESIZE;
        }else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_DOWN)){
            mapWindow.mapOffsetY += MAPTILESIZE;
        }
        // Force offsets within window
        if(mapWindow.mapOffsetX < 0)
            mapWindow.mapOffsetX = 0;
        if(mapWindow.mapOffsetY < 0)
            mapWindow.mapOffsetY = 0;
        if(mapWindow.mapOffsetY > starMap->height*MAPTILESIZE - mapWindow.height)
            mapWindow.mapOffsetY = starMap->height*MAPTILESIZE - mapWindow.height;
        if(mapWindow.mapOffsetX > starMap->width*MAPTILESIZE - mapWindow.width)
            mapWindow.mapOffsetX = starMap->width*MAPTILESIZE - mapWindow.width;

        bool beginWarp = false;
        if(mapWindow.selection.x != -1 && Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN)){
           beginWarp = true;
        }

        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) && Controls::instance()->getMouseButtonPressLength(Controls::MOUSE_LEFT) < 200){
            if(!isInWindow(mapWindow)){
                removeMenuSelections();
                return;
            }
            SDL_Point pos = getWindowMousePos(mapWindow);
            if(enemyShip == NULL && mapWindow.reachable.size() == 0 &&
               isPointInRect(pos, mapWindow.width-70, mapWindow.height-70, 64, 64)){
                spawnEnemyShip();
                removeMenuSelections();
                return;
            }
            if(mapWindow.selection != SDL_Point{-1, -1} && mapWindow.selection != mapPosition
               && isPointInRect(pos, mapWindow.width-70, mapWindow.height-70-drawer.fontHeight, 64, 64)){
                beginWarp = true;
            }else{
                mapWindow.selection.x = (pos.x + mapWindow.mapOffsetX%MAPTILESIZE)/MAPTILESIZE + mapWindow.mapOffsetX/MAPTILESIZE;
                mapWindow.selection.y = (pos.y + mapWindow.mapOffsetY%MAPTILESIZE)/MAPTILESIZE + mapWindow.mapOffsetY/MAPTILESIZE;
                if(mapWindow.selection.x >= starMap->width || mapWindow.selection.y > starMap->height){
                    mapWindow.selection = SDL_Point{-1, -1};
                    mapWindow.distance = 0;
                    mapWindow.secondary.clear();
                    return;
                }
                StarMap::StarTile& tile = starMap->getTile(mapWindow.selection);
                if((!tile.isStar() && tile.encounter == NULL) || (!debug && tile.isUnexplored())){
                    audioManager.playSound("invalid");
                    mapWindow.selection = SDL_Point{-1, -1};
                    mapWindow.distance = 0;
                    mapWindow.secondary.clear();
                    return;
                }
                mapWindow.distance = getDistance(mapPosition, mapWindow.selection);
                mapWindow.secondary.clear();
                if(mapWindow.selection != mapPosition){
                    findReachableMapPoints(mapWindow.selection, getWarpDistance(), !debug, mapWindow.secondary);
                }
                audioManager.playSound("select");
                return;
            }
        }
        if(beginWarp){
            if(inWarp){
                SDL_Log("Potential error, attempting to warp while already in warp\n");
            }else if(!ship->engine || !ship->engine->isAvailable() || ship->engine->cooldown != 0){
                addMessage("ENGINE UNAVAILABLE");
                audioManager.playSound("invalid");
            }else if(!debug && mapWindow.distance > ship->engine->warp){
                addMessage(stringLibrary.getString("too_far"));
                audioManager.playSound("invalid");
            }else if(mapWindow.distance != 0 && (debug || useFuel(mapWindow.distance))){
                audioManager.playSound("select");
                warpDestination = mapWindow.selection;
                removeMenuSelections();
                doWarp = true;
                if(fleeCallback[0] != '\0' && luaHandler.getLuaFunction(fleeCallback)){
                    lua_pcall(luaHandler.luaState, 0, 1, 0);
                }
            }
        }

        return;
    }
    if(powerWindow.visible){
    
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) && !isInWindow(powerWindow)){
            endSelectedCrewTurn();
            return;
        }

        SDL_Point pos = getWindowMousePos(powerWindow);
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
           pos.x > powerWindow.width - 128 &&
           pos.x < powerWindow.width){
            powerWindow.selection = (pos.y - 15)/46;
            if(pos.x > powerWindow.width - 64){
                ship->directPower(powerWindow.systems[powerWindow.selection], 1);
            }else{
                ship->directPower(powerWindow.systems[powerWindow.selection], -1);
            }
            return;
        }
        

        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            endSelectedCrewTurn();
            return;
        }
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_UP)){
            if(powerWindow.selection == 0)
                powerWindow.selection = powerWindow.systems.size() - 1;
            else
                powerWindow.selection--;
        }
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_DOWN)){
            if(powerWindow.selection >= powerWindow.systems.size() - 1)
                powerWindow.selection = 0;
            else
                powerWindow.selection++;
        }
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_LEFT)){
            ship->directPower(powerWindow.systems[powerWindow.selection], -1);
        }else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_RIGHT)){
            ship->directPower(powerWindow.systems[powerWindow.selection], 1);
        }
        return;
    }

    if(systemWindow.visible){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            removeMenuSelections();
            return;
        }
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) && !isInWindow(systemWindow)){
            removeMenuSelections();
            return;
        }
        System* system = systemWindow.system;
        SDL_Point pos = getWindowMousePos(systemWindow);
        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
           pos.y > systemWindow.height-24){

           if(pos.x < systemWindow.width/2 &&
              system->upgrade != NULL){
            
                // Check if we have all the items required
                bool haveItems = true;
                if(getRM() < system->upgradeRM)
                    haveItems = false;
                for(int i = 0; i < system->upgradeItems->size(); i++){
                    const Item& upgradeItem = (*system->upgradeItems)[i];
                    Item* inventoryItem = findInventoryItem(upgradeItem.name);
                    if(!inventoryItem || inventoryItem->amount < upgradeItem.amount){
                        haveItems = false;
                        break;
                    }
                }
                if(haveItems){
                    for(int i = 0; i < system->upgradeItems->size(); i++){
                        const Item& upgradeItem = (*system->upgradeItems)[i];
                        useItem(upgradeItem.name, upgradeItem.amount);
                    }
                    if(system->upgradeRM != 0)
                        useRM(system->upgradeRM);

                    System* newSystem = systemLoader.getSystem(system->upgrade);
                    ship->replaceSystem(system, newSystem);
                    systemWindow.system = newSystem;
                    addMessage(stringLibrary.getString("upgraded"));
                }else{
                    addMessage(stringLibrary.getString("upgrade_items"));
                }
            }else if(pos.x > systemWindow.width/2){
                if(!checkIsSafe()){
                    addMessage(stringLibrary.getString("uninstall_safe"));
                    return;
                }
                Item systemItem = itemLibrary.findSystemItem(system->name);
                bool isScanner = ship->scanner == system;
                ship->removeSystem(system);
                if(isScanner)
                    calculateShipVisibility(ship, true);
                systemItem.amount = 1;
                addItem(systemItem);
                systemWindow.visible = false;
                addMessage(stringLibrary.getString("uninstalled"));
                // Reselect crew to avoid stale actions
                if(selectedCrew)
                    selectCrew(selectedCrew);
            }
        }
        return;
    }

    if(stationWindow.visible){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            removeMenuSelections();
            return;
        }

        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            if(!isInWindow(stationWindow)){
                removeMenuSelections();
                return;
            }else{
                SDL_Point pos = getWindowMousePos(stationWindow);
                int selection = (pos.y - 13 - 27)/46;
                SDL_Log("Selection: %d", selection);
                if(pos.x < 256){
                    if(selection == 0 && useRM(station->repairPrice)){
                        repairShip(ship, 15);
                        addMessage(stringLibrary.getString("repaired"));
                    }else if(selection == 1 && useRM(station->healPrice)){
                        for(int i = 0; i < playerCrews.size(); i++){
                            playerCrews[i]->health = playerCrews[i]->maxHealth;
                        }
                    }else if(selection == 2 && useRM(station->mapPrice)){
                        exploreStarMap(starMap, mapPosition.x, mapPosition.y, 5, 10);
                        addMessage(stringLibrary.getString("scanning"));
                    }
                }else if(selection <= station->items.size()){
                    if(selection == 0 && station->fuelPrice != 0){
                        if(station->fuelAmount > 0 && useRM(station->fuelPrice)){
                            addFuel(1);
                            station->fuelAmount--;
                            addMessage(stringLibrary.getString("bought"));
                        }
                    }else{
                        if(station->fuelPrice != 0)
                            selection--;
                        Item& item = station->items[selection];
                        if(item.amount > 0 && useRM(item.price)){
                            item.amount--;
                            Item buyItem = item;
                            buyItem.amount = 1;
                            addItem(buyItem);
                            addMessage(stringLibrary.getString("bought"));
                        }
                    }
                }
            }
        }
        return;
    }

    if(crewWindow.visible){
        if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE) ||
           Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
            removeMenuSelections();
            return;
        }

        if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
            if(!isInWindow(crewWindow)){
                removeMenuSelections();
                return;
            }
        }
        return;
    }

    if(Controls::instance()->keyIsDown(SDL_SCANCODE_LSHIFT) ||
       Controls::instance()->keyIsDown(SDL_SCANCODE_RSHIFT)){
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_LEFT))
            playerViewSize -= 0.02f;
        else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_RIGHT))
            playerViewSize += 0.02f;
    }else{
        if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_LEFT))
            playerView.offset.x -= 16;
        else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_RIGHT))
            playerView.offset.x += 16;
        else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_UP))
            playerView.offset.y -= 16;
        else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_DOWN))
            playerView.offset.y += 16;
    }

    if(station != NULL && isSafe &&
       Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
       isMouseInUIRect(drawer.uiWidth-70*4, drawer.uiHeight-86, 64, 64)){
        showWindow(stationWindow);
        return;
    }

    if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
       isMouseInUIRect(drawer.uiWidth-70*3, drawer.uiHeight-86, 64, 64)){
        showWindow(inventoryWindow);
        inventoryWindow.selection = -1;
        return;
    }

    if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
       isMouseInUIRect(drawer.uiWidth-70*2, drawer.uiHeight-86, 64, 64)){
        showMapWindow();
        return;
    }

    if(!isSafe && (Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN) ||
       (Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
        isMouseInUIRect(drawer.uiWidth-70, drawer.uiHeight-86, 64, 64)))){
        SDL_Log("Ending turn");
        audioManager.playSound("select");
        endTurn();
        return;
    }

    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE)){
        if(selectedCrew != NULL){
            selectedCrew = NULL;
            removeMenuSelections();
            return;
        }else{
            currentView = GameView::Options;
            return;
        }
    }

    int selection = 0;
    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_1))
        selection = 1;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_2))
        selection = 2;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_3))
        selection = 3;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_4))
        selection = 4;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_5))
        selection = 5;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_6))
        selection = 6;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_7))
        selection = 7;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_8))
        selection = 8;
    else if(Controls::instance()->keyWasPressed(SDL_SCANCODE_9))
        selection = 9;
    if(isInWindow(actionsWindow) && selection == 0 && Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
        selection = 1 + (getUIMouseX() - actionsWindow.offsetX)/64;
    }

    if(selection != 0 && selectedCrew != NULL && selectedCrew->turnDone == false && (selection-1) < actionsWindow.actions.size()){
        selection -= 1;
        if(actionsWindow.actions[selection].available){
            Action& action = actionsWindow.actions[selection];
            if(selectedAction == selection){
                removeMenuSelections();
                return;
            }
            removeMenuSelections();
            if(action.system && action.system->type == System::SystemType::Weapon){
                selectedWeapon = (Weapon*)action.system;
                audioManager.playSound("select");
                selectedAction = selection;
            }else if(action.system == ship->scanner){
                selectedScanner = true;
                selectedAction = selection;
            }else if(ship->teleporter && action.system == ship->teleporter){
                selectedTeleporter = true;
                selectedAction = selection;
            }else if(action.system == ship->engine){
                showWindow(powerWindow);
                powerWindow.systems.clear();
                for(int i = 0; i < ship->allSystems.size(); i++){
                    if(ship->allSystems[i]->maxPower > 0)
                        powerWindow.systems.push_back(ship->allSystems[i]);
                }
                selectedAction = selection;
            }else if(action.code == Action::evade){
                ship->setMode(Ship::ShipMode::Evasive);
                addMessage("Evasive Maneuvers");
                endSelectedCrewTurn();
            }else if(action.code == Action::defense){
                ship->setMode(Ship::ShipMode::Defensive);
                addMessage("Defensive Position");
                endSelectedCrewTurn();
            }else if(action.code == Action::offense){
                ship->setMode(Ship::ShipMode::Offensive);
                addMessage("Attack Formation");
                endSelectedCrewTurn();
            }else if(action.code == Action::hail){
                hail();
            }else if(action.code == Action::heal && action.system != NULL){
                System* system = ship->getTileSystem((int)selectedCrew->position.x, (int)selectedCrew->position.y);
                performHeal(selectedCrew, ((Sickbay*)system)->heal, selectedCrew->position.x, selectedCrew->position.y);
                endSelectedCrewTurn();
            }else if(action.code == Action::heal){
                calculateMoveMap(selectedCrew, false, true);
                if(!moveMap.hasHeal)
                    addMessage(stringLibrary.getString("no_heal"));
            }else if(action.code == Action::repair){
                calculateMoveMap(selectedCrew, true, false);
                if(!moveMap.hasRepair)
                    addMessage(stringLibrary.getString("no_repair"));
            }else{
                endSelectedCrewTurn();
            }
            return;
        }else{
            audioManager.playSound("invalid");
        }
    }else if(selection != 0 && selectedCrew == NULL){
        if((selection-1) < playerCrews.size()){
            selectCrew(playerCrews[selection-1]);
        }
    }

    if(selectedWeapon && selectedWeapon->weaponType == Weapon::Laser && Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
        int shotCounter = 0;
        for(int i = 0; i < shots.size(); i++){
            Shot& shot = shots[i];
            if(shot.waiting && !shot.inEnemyView){
                int x = drawer.uiWidth*playerViewSize;
                if(enemyShip == NULL)
                    x = drawer.uiWidth;
                int textWidth = drawer.getTextWidth(8, drawer.font);
                if(isMouseInUIRect(x - textWidth, 32 + 36*shotCounter, textWidth, 36)){
                    SDL_Log("Fire laser at shot %d", i);
                    bool missed = gameRandom.oneIn(5);
                    if(missed)
                        addMessage(stringLibrary.getString("missed"));
                    else
                        addEffect(spriteManager.getList("explosion"), NULL, x-textWidth, 32 + 36*shotCounter);
                    fireLaserAtShot(ship, selectedWeapon, selectedCrew, 
                                    selectedWeapon->posX, selectedWeapon->posY, &shot, missed);
                    selectedWeapon->beginCooldown();
                    selectedCrew->lockedToStation = true;
                    selectCrew(selectedCrew);
                    return;
                } 
                shotCounter++;
            }
        }
    }

    SDL_Point prevMouseTile = mouseTile;
    mouseTile.x = (getPlayerMouseX()-playerView.offset.x)/TILESIZE,
    mouseTile.y = (getPlayerMouseY()-playerView.offset.y)/TILESIZE;

    // Is click on player ship tile
    bool isPlayerTile = !mouseInEnemyView() &&
                        mouseTile.x >= 0 && mouseTile.x < ship->width &&
                        mouseTile.y >= 0 && mouseTile.y < ship->height;
    bool isEnemyTile = false;
    if(!isPlayerTile){
        mouseTile.x = (getEnemyMouseX()-enemyView.offset.x)/TILESIZE;
        mouseTile.y = (getEnemyMouseY()-enemyView.offset.y)/TILESIZE;
        isEnemyTile = mouseInEnemyView() &&
                      enemyShip && mouseTile.x >= 0 && mouseTile.y < enemyShip->width &&
                      mouseTile.y >= 0 && mouseTile.y < enemyShip->height;
    }

#ifndef ANDROID
    // Calculate new selected crew movement path if mouse tile changed
    if(selectedCrew && !selectedCrew->moving && (isPlayerTile || isEnemyTile) && prevMouseTile != mouseTile &&
       ((selectedCrew->ship->playerShip && isPlayerTile) || (!selectedCrew->ship->playerShip && isEnemyTile))){
        calculateCrewMovePath(selectedCrew, mouseTile.x, mouseTile.y);
    }else if(selectedCrew && !selectedCrew->moving && !isPlayerTile && !isEnemyTile){
        selectedCrew->moveMap.clear();
    }
#endif

    if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
        // Select crew from portrait
        for(int i = 0; i < playerCrews.size(); i++){
            if(isMouseInUIRect(6 + i*70, drawer.uiHeight - 62 - 24, 64, 64)){
                if(Controls::instance()->wasMouseButtonDoubleClicked(Controls::MOUSE_LEFT)){
                    showWindow(crewWindow);
                    crewWindow.crew = playerCrews[i];
                }else{
                    selectCrew(playerCrews[i]);
                    audioManager.playSound("select");
                }
                return;
            }
        }

        selectedSystem = NULL;

        if(selectedTeleporter){
            if(isEnemyTile && enemyShip->shield->shield == 0){
                Crew* crew = enemyShip->getTileCrew(mouseTile.x, mouseTile.y);
                if(teleporterCrew == NULL && crew && crew->playerCrew){
                    teleporterCrew = crew;
                    return;
                }else if(teleporterCrew != NULL && teleporterCrew != crew){
                    if(teleportCrew(teleporterCrew, enemyShip, mouseTile.x, mouseTile.y)){
                        endSelectedCrewTurn();
                    }else{
                        teleporterCrew = NULL;
                    }
                    return;
                }
            }else if(isEnemyTile && enemyShip->shield->shield > 0){
                addMessage(stringLibrary.getString("shield_teleport"));
            }else if(isPlayerTile){
                Crew* crew = ship->getTileCrew(mouseTile.x, mouseTile.y);
                if(teleporterCrew == NULL && crew && crew->playerCrew){
                    teleporterCrew = crew;
                    return;
                }else if(teleporterCrew != NULL && teleporterCrew != crew){
                    if(teleportCrew(teleporterCrew, ship, mouseTile.x, mouseTile.y)){
                        endSelectedCrewTurn();
                    }else{
                        teleporterCrew = NULL;
                    }
                    return;
                }
            }else{
                teleporterCrew = NULL;
            }
            return;
        }

        if(selectedWeapon && isEnemyTile &&
           !enemyShip->getTile(mouseTile).isEmpty()){
            SDL_Log("Shot %s at enemy ship", selectedWeapon->name);
            setEnemyHostile(true);
            fireShot(ship, enemyShip, selectedWeapon, selectedCrew,
                     selectedWeapon->posX, selectedWeapon->posY, mouseTile.x, mouseTile.y);
            // Todo:: Level up a skill?
            //selectedCrew->skills[Crew::Weapons].increase(5);
            selectedWeapon->beginCooldown();
            selectedCrew->lockedToStation = true;
            selectCrew(selectedCrew);

            if(enemyShip && attackCallback[0] != '\0' && luaHandler.getLuaFunction(attackCallback)){
                clearDialog();
                lua_pcall(luaHandler.luaState, 0, 0, 0);
            }
            return;
        }else if(selectedWeapon){
            removeMenuSelections();
            return;
        }

        if(selectedScanner && isEnemyTile){
            scanEffect(enemyShip, mouseTile.x, mouseTile.y, ship->scanner->getSize(), blue);
            if(starMap->getTile(mapPosition).isClouds() && gameRandom.percentage(50)){
                addMessage(stringLibrary.getString("clouds_interference"));
                endSelectedCrewTurn();
                return;
            }
            selectedCrew->skills[Crew::Operations].increase(5);
            ship->scanner->isScanning = true;
            ship->scanner->cooldown = 2;
            ship->scanner->scanX = mouseTile.x;
            ship->scanner->scanY = mouseTile.y;
            calculateShipVisibility(enemyShip, true);
            endSelectedCrewTurn();
            if(scanCallback[0] != '\0' && luaHandler.getLuaFunction(scanCallback)){
                clearDialog();
                lua_pcall(luaHandler.luaState, 0, 0, 0);
            }
            return;
        }else if(selectedScanner){
            removeMenuSelections();
            return;
        }

        Crew* tileCrew = NULL;
        int tileX, tileY;
        Ship* tileShip = NULL;
        bool isValidTile = false;
        if(isPlayerTile){
            SDL_Log("Player Tile %d,%d", mouseTile.x, mouseTile.y);
            tileCrew = ship->getTileCrew(mouseTile.x, mouseTile.y);
            tileX = mouseTile.x;
            tileY = mouseTile.y;
            tileShip = ship;
            isValidTile = true;
        }else if(isEnemyTile){
            SDL_Log("Enemy Tile %d,%d", mouseTile.x, mouseTile.y);
            tileCrew = enemyShip->getTileCrew(mouseTile.x, mouseTile.y);
            tileX = mouseTile.x;
            tileY = mouseTile.y;
            tileShip = enemyShip;
            isValidTile = true;
        }
        if(debug && tileCrew)
            SDL_Log("Tile Crew: %p '%s'", tileCrew, tileCrew->name);
        
        if(isValidTile){
            if(installingSystem){
                if(isPlayerTile && ship->tiles[tileY][tileX].room != 0){
                    const Room& room = ship->rooms[ship->tiles[tileY][tileX].room-1];
                    const SystemLayout* layout = installingSystem->layout;
                    if(room.system == NULL &&
                       ((room.isExternal == true && installingSystem->type == System::SystemType::Weapon) ||
                        (room.isExternal == false && installingSystem->type != System::SystemType::Weapon)) &&
                       room.width >= layout->width && room.height >= layout->height){
                        for(int y = 0; y < ship->height; y++){
                            for(int x = 0; x < ship->width; x++){
                                if(ship->getTile(x, y).room == ship->getTile(tileX, tileY).room && ship->getTileCrew(x, y) != NULL){
                                    addMessage(stringLibrary.getString("install_crew"));
                                    return;
                                }
                            }
                        }
                        ship->addSystem(systemLoader.getSystem(installingSystem->name), ship->getTile(tileX, tileY).room);
                        useItem(items[inventoryWindow.selection].name, 1);
                        addMessage(stringLibrary.getString("installed"));
                    }
                }
                removeMenuSelections();
                return;
            }else if(selectedCrew != NULL && tileCrew == NULL && tileShip == selectedCrew->ship && moveMap.isMovable(tileX, tileY)){
                moveCrew(selectedCrew, selectedCrew->ship, tileX, tileY);
                if(!isSafe){
                    int moveDistance = moveMap.getDistance(tileX, tileY);
                    if(moveDistance > selectedCrew->walkDistance)
                        selectedCrew->walkDistance = 0;
                    else
                        selectedCrew->walkDistance -= moveDistance;
                }
                selectedCrew->repeatAction = Crew::None;
            }else if(selectedCrew != NULL && moveMap.isRepairable(tileX, tileY)){
                repair(selectedCrew, tileX, tileY);
                endSelectedCrewTurn();
            }else if(selectedCrew != NULL && tileCrew != NULL && tileCrew->playerCrew == true && moveMap.isHealable(tileX, tileY)){
                heal(selectedCrew, tileX, tileY);
                endSelectedCrewTurn();
            }else if(tileCrew != NULL && tileCrew->playerCrew == true && tileCrew->turnDone == false){
                selectCrew(tileCrew);
            }else if(selectedCrew != NULL && tileCrew != NULL && tileCrew->playerCrew == false && moveMap.isAttackable(tileX, tileY)){
                tileCrew->hostile = true;
                isSafe = false;
                attack(selectedCrew, tileX, tileY);
                endSelectedCrewTurn();
            }else if(selectedCrew != NULL && tileCrew == NULL && moveMap.isAttackable(tileX, tileY) && tileShip->getTileSystem(tileX, tileY) != NULL){
                setEnemyHostile(true);
                attack(selectedCrew, tileX, tileY);
                endSelectedCrewTurn();
            }else if(selectedCrew != NULL && tileShip->playerShip == false && moveMap.isAttackable(tileX, tileY) && tileShip->getDoor(tileX, tileY) != NULL){
                setEnemyHostile(true);
                attack(selectedCrew, tileX, tileY);
                endSelectedCrewTurn();
            }else if(selectedCrew != NULL && moveMap.isAttackable(tileX, tileY) && tileShip->getTile(tileX, tileY).isFire()){
                attack(selectedCrew, tileX, tileY);
                endSelectedCrewTurn();
            }else if(selectedCrew == NULL && isPlayerTile && tileCrew == NULL && ship->getDoor(tileX, tileY)){
                Door* door = ship->getDoor(tileX, tileY); 
                if(door->health != 0 && door->turnDone != true){
                    SDL_Log("Open/Close door");
                    door->turnDone = true;
                    door->isOpen = !door->isOpen;
                    door->forceOpen = !door->forceOpen;
                    calculateShipVisibility(ship, true);
                    calculateShipVisibility(ship, false);
                }
            }else if(isPlayerTile && ship->getTileSystem(tileX, tileY)){
                selectedSystem = ship->getTileSystem(tileX, tileY);
                if(Controls::instance()->wasMouseButtonDoubleClicked(Controls::MOUSE_LEFT)){
                    showWindow(systemWindow);
                    systemWindow.system = selectedSystem;
                }
                selectedCrew = NULL;
            }else{
                selectedCrew = NULL;
            }
        }

    }

    updateScroll();
}

SDL_Point Game::findSpawnPoint(Ship* ship){
    // Find free point
    SDL_Point spawnPoint = {-1, -1};
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            const Tile& tile = ship->getTile(x, y);
            if(tile.isStation() && ship->getTileCrew(x, y) == NULL){
                spawnPoint.x = x; 
                spawnPoint.y = y; 
                break;
            }
        }
    }

    if(spawnPoint.x == -1){
        spawnPoint = findClosestFreePoint(ship, gameRandom.max(ship->width), gameRandom.max(ship->height));
    }
    return spawnPoint;
}

int Game::getNumReachablePoints(StarMap* map, const SDL_Point& point, int distance) const{
    int reachablePoints = 0;
    for(int x = std::max(0, point.x-distance); x < std::min(point.x+distance, map->width); x++){
        for(int y = std::max(0, point.y-distance); y < std::min(point.y+distance, map->height); y++){
            if(map->tiles[x][y].isStar() && getDistance(SDL_Point{x, y}, point) <= distance){
                reachablePoints++;
            }
        }
    }
    return reachablePoints;
}

SDL_Point Game::populateMap(StarMap* map, const SDL_Point& src, const SDL_Point& dest, bool mustFinish){
    int points = 0;
    SDL_Point curPoint = src;
    int reachableChecks = 0;
    int destDistance = getDistance(curPoint, dest);
    while(destDistance > 4){
        SDL_Point newPoint = {curPoint.x + gameRandom.range(-4, 4), curPoint.y + gameRandom.range(-4, 4)};
        int newDistance = 0;
        if(newPoint.y >= 0 && newPoint.y < map->height && newPoint.x >= 0 && newPoint.x < map->width &&
           !map->tiles[newPoint.x][newPoint.y].isStar() &&
           getDistance(curPoint, newPoint) <= 4 &&
           (newDistance = getDistance(newPoint, dest)) < destDistance){
            reachableChecks++;
            // Check area around new point and make sure that it contains
            // only 1 other reachable point
            if(reachableChecks < 20 ){
                if(getNumReachablePoints(map, newPoint, 4) > 1)
                    continue;
            }else if(!mustFinish){
                return curPoint;
            }
            curPoint = newPoint;
            map->tiles[newPoint.x][newPoint.y].tile |= StarMap::smt_Star;
            points += 1;
            reachableChecks = 0;
            destDistance = newDistance;
        }
    }
    map->tiles[dest.x][dest.y].tile |= StarMap::smt_Star;
    return dest;
}

int Game::getWarpDistance(){
    if(ship->engine != NULL)
        return ship->engine->warp;
    return 0;
}

Crew* Game::createCrewFromDescription(const CrewDescription& crew){
    Crew* newCrew = new Crew(crew.name);
    for(int p = 0; p < crew.proficiencies.size(); p++){
        newCrew->proficiencies.push_back(crew.proficiencies[p]);
    }
    newCrew->sprite = crew.sprite;
    return newCrew;
}

void Game::updateHangar(){
    if(Controls::instance()->keyWasPressed(SDL_SCANCODE_ESCAPE) ||
       Controls::instance()->keyWasPressed(SDL_SCANCODE_AC_BACK)){
        clearGame();
        return;
    }

    // Randomize button
    if((Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN) && playerCrews.size() < 5) ||
       (Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
        isMouseInUIRect(drawer.uiWidth-97 - 72, drawer.uiHeight-96, 64, 64))){
        audioManager.playSound("select");

        // Unselect all and randomize selection if already fully selected
        if(playerCrews.size() == 5){
            for(int y = 0; y < 4; y++){
                for(int c = 0; c < crews[y].size(); c++){
                    CrewDescription& crew = crews[y][c];
                    if(crew.selected){
                        crew.selected = false;
                        selectedCrews[y]--;
                        for(auto it = playerCrews.begin(); it != playerCrews.end(); ++it){
                            Crew* playerCrew = *it;
                            if(strcmp(playerCrew->name, crew.name) == 0){
                                ship->removeCrew(playerCrew);
                                playerCrews.erase(it);
                                break;
                            }
                        }
                    }
                }
            }
        }

        while(playerCrews.size() != 5){
            int y = random.max(4);
            if(selectedCrews[y] >= 2)
                continue;

            int x = random.max(crews[y].size());
            if(crews[y][x].selected)
                continue;

            CrewDescription& crew = crews[y][x];
            crew.selected = true;
            selectedCrews[y]++;
            addCrew(createCrewFromDescription(crew), ship, findSpawnPoint(ship), true);
        }

        return;
    }

    if(Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT)){
        for(int y = 0; y < 4; y++){
            for(int c = 0; c < crews[y].size(); c++){
                int offsetX = ((ship->width*TILESIZE + 128)/playerView.zoom)*drawer.uiZoom;
                if(isMouseInUIRect(offsetX + c * (CREW_SELECTION_WIDTH+5), 45 + y*30 + 64*y + drawer.fontHeight, CREW_SELECTION_WIDTH, CREW_SELECTION_HEIGHT)){
                    CrewDescription& crew = crews[y][c]; 
                    if(crew.selected){
                        crew.selected = false;
                        selectedCrews[y]--;
                        for(auto it = playerCrews.begin(); it != playerCrews.end(); ++it){
                            Crew* playerCrew = *it;
                            if(strcmp(playerCrew->name, crew.name) == 0){
                                ship->removeCrew(playerCrew);
                                playerCrews.erase(it);
                                break;
                            }
                        }
                        audioManager.playSound("select");
                    }else if(playerCrews.size() < 5 && selectedCrews[y] < 2){
                        crew.selected = true;
                        selectedCrews[y]++;
                        addCrew(createCrewFromDescription(crew), ship, findSpawnPoint(ship), true);
                        audioManager.playSound("select");
                    }else{
                        audioManager.playSound("invalid");
                    }
                    return;
                }
            }
        }
    }

    if((Controls::instance()->keyWasPressed(SDL_SCANCODE_RETURN) ||
        (Controls::instance()->wasMouseButtonReleased(Controls::MOUSE_LEFT) &&
         isMouseInUIRect(drawer.uiWidth-97, drawer.uiHeight-96, 64, 64)))  &&
       playerCrews.size() == 5){
        audioManager.playSound("select");

        newGame();
        if(luaHandler.getLuaFunction("start")){
            lua_pcall(luaHandler.luaState, 0, 0, 0);
        }
        return;
    }

}

bool Game::performEnemyTurn(){
    if(enemyShip && enemyShip->scanner == NULL){
        // Close doors if danger spotted on ship
        for(int y = 0; y < enemyShip->height; y++){
            for(int x = 0; x < enemyShip->width; x++){
                const Tile& tile = enemyShip->getTile(x, y);
                if(tile.isVisible(false) && hasTarget(enemyShip, false, x, y)){
                    for(int y = 0; y < enemyShip->height; y++){
                        for(int x = 0; x < enemyShip->width; x++){
                            Door* door = enemyShip->getTile(x, y).getDoor();
                            if(door && door->isOpen && door->health != 0 && enemyShip->getTileCrew(x, y) == NULL){
                                door->isOpen = false;
                                door->forceOpen = false;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    bool madeMove = false;
    for(int i = 0; i < enemyCrews.size(); i++){
        Crew* crew = enemyCrews[i];
        if(crew->turnDone)
            continue;
        if(!crew->hostile){
            crew->turnDone = true;
            continue;
        }

        bool doNotMove = false;
        bool canScan = false;
        bool canTeleport = false;
        if(crew->ship == enemyShip){
            const Tile& tile = enemyShip->getTile(crew->position);
            if(tile.isWeaponsStation()){
                doNotMove = true;
                Weapon* fireWeapon = NULL;
                for(int w = 0; w < enemyShip->weapons.size(); w++){
                    if(enemyShip->weapons[w]->isUsable()){
                        fireWeapon = enemyShip->weapons[w];
                        break;
                    }
                }
                if(fireWeapon){
                    while(true){
                        int x = gameRandom.max(ship->width);
                        int y = gameRandom.max(ship->height);
                        const Tile& tile = ship->getTile(x, y);
                        if(tile.isRoom() && (tile.isExternal() == false || ship->getTileSystem(x, y)) &&
                           (tile.isExplored(false) == false || ship->getTileSystem(x, y))){
                            fireShot(enemyShip, ship, fireWeapon, crew, fireWeapon->posX, fireWeapon->posY, x, y);
                            fireWeapon->beginCooldown();
                            crew->lockedToStation = true;
                            return true;
                        }
                    }
                }
            }else if(tile.isCaptainStation()){
                if(enemyShip->mode == Ship::ShipMode::None){
                    int rand = gameRandom.max(3);
                    if(rand == 0)
                        enemyShip->setMode(Ship::ShipMode::Evasive);
                    else if(rand == 1)
                        enemyShip->setMode(Ship::ShipMode::Defensive);
                    else if(rand == 2)
                        enemyShip->setMode(Ship::ShipMode::Offensive);
                    crew->turnDone = true;
                }
            }else if(tile.isOpsStation()){
                if(!starMap->getTile(mapPosition).isClouds() && enemyShip->scanner && enemyShip->scanner->isUsable())
                    canScan = true;
                System* teleporter = enemyShip->teleporter;
                if(teleporter && teleporter->isUsable() && (ship->shield == NULL || ship->shield->shield == 0)){
                    canTeleport = true;
                    doNotMove = true;
                }else if(teleporter && !teleporter->isBroken())
                    doNotMove = true;
            }
        }

        calculateMoveMap(crew, true, true);

        EnemyMove move;
        move.action = EnemyMove::None;
        SDL_Point explorePoint = {-1, -1}; // Point to explore if no other actions
        for(int y = 0; y < crew->ship->height; y++){
            for(int x = 0; x < crew->ship->width; x++){
                const Tile& tile = crew->ship->getTile(x, y);
                if(tile.isEmpty())
                    continue;
                if(moveMap.isAttackable(x, y)){
                    if(move.action != EnemyMove::Attack || move.immediate == false){
                        move.action = EnemyMove::Attack;
                        move.target.x = x;
                        move.target.y = y;
                        move.immediate = true;
                    }else if(move.action == EnemyMove::Attack){
                        Crew* tileCrew = crew->ship->getTileCrew(x, y);
                        if(tileCrew){
                            move.action = EnemyMove::Attack;
                            move.target.x = x;
                            move.target.y = y;
                            move.immediate = true;
                        }
                    }
                }else if(moveMap.isHealable(x, y)){
                    if(move.action != EnemyMove::Attack || move.immediate == false){
                        move.action = EnemyMove::Heal;
                        move.target.x = x;
                        move.target.y = y;
                        move.immediate = true;
                    }
                }else if(moveMap.isRepairable(x, y)){
                    if(move.action == EnemyMove::None || (move.immediate == false && move.action == EnemyMove::Repair)){
                        move.action = EnemyMove::Repair;
                        move.target.x = x;
                        move.target.y = y;
                        move.immediate = true;
                    }else if(move.action == EnemyMove::Repair && move.immediate){
                        SDL_Point prev = findClosestAdjacentMovePoint(crew, move.target.x, move.target.y);
                        SDL_Point cur = findClosestAdjacentMovePoint(crew, x, y);
                        if(moveMap.getDistance(cur.x, cur.y) < moveMap.getDistance(prev.x, prev.y)){
                            move.target.x = x;
                            move.target.y = y;
                        }
                    }
                }else{
                    if(tile.isExplored(false)){
                        System* tileSystem = crew->ship->getTileSystem(x, y);
                        if(crew->ship->playerShip == false && tileSystem && tileSystem->health != tileSystem->maxHealth){
                            if(move.action == EnemyMove::None){
                                move.action = EnemyMove::Repair;
                                move.target.x = x;
                                move.target.y = y;
                                move.immediate = false;
                            }
                        }
                    }
                    if(tile.isVisible(false)){
                        Crew* tileCrew = NULL;
                        if((tileCrew = crew->ship->getTileCrew(x, y)) && tileCrew->playerCrew &&
                           move.action != EnemyMove::Attack){
                            move.action = EnemyMove::Attack;
                            move.target.x = x;
                            move.target.y = y;
                            move.immediate = false;
                        }else if(crew->ship->playerShip == false && crew->ship->getTile(x, y).isFire() && move.action == EnemyMove::None){
                            move.action = EnemyMove::Attack;
                            move.target.x = x;
                            move.target.y = y;
                            move.immediate = false;
                        }
                    }
                    if(!tile.isExplored(false)){
                        explorePoint.x = x;
                        explorePoint.y = y;
                    }
                    if(crew->ship->playerShip == false &&
                       tile.isWeaponsStation() && crew->ship->getTileCrew(x, y) == NULL){
                        explorePoint.x = x;
                        explorePoint.y = y;
                    }
                }
            }
        }
        if(move.action != EnemyMove::None && move.immediate){
            if(move.action == EnemyMove::Attack){
                attack(crew, move.target.x, move.target.y);
                madeMove = true;
            }else if(!doNotMove && move.action == EnemyMove::Repair){
                repair(crew, move.target.x, move.target.y);
                madeMove = true;
            }else if(move.action == EnemyMove::Heal){
                heal(crew, move.target.x, move.target.y);
                madeMove = true;
            }
        }else if(!doNotMove && move.action != EnemyMove::None && move.immediate == false){
            SDL_Point movePoint = findClosestMovablePoint(crew, move.target.x, move.target.y);
            if(movePoint.x != -1 && movePoint != crew->position){
                moveCrew(crew, crew->ship, movePoint.x, movePoint.y, false);
                crew->walkDistance -= moveMap.getDistance(movePoint.x, movePoint.y);
            }else{
                crew->turnDone = true;
            }
            madeMove = true;
        }else if(canTeleport){
            for(int c = 0; c < enemyCrews.size(); c++){
                Crew* targetCrew = enemyCrews[c];
                if(targetCrew != crew &&
                   ((targetCrew->ship->playerShip == true && targetCrew->health < 15) ||
                    (targetCrew->ship->playerShip == false &&
                     targetCrew->ship->getTile(targetCrew->position).isStation() == false &&
                     targetCrew->health >= targetCrew->maxHealth-5))){
                    Ship* targetShip = NULL;
                    if(targetCrew->ship->playerShip == true)
                        targetShip = enemyShip;
                    else
                        targetShip = ship; 
                    
                    if(teleportCrew(targetCrew, targetShip, gameRandom.max(targetShip->width), gameRandom.max(targetShip->height))){
                        madeMove = true;
                        crew->turnDone = true;
                        if(targetShip == ship){
                            addMessage(stringLibrary.getString("intruders"));
                        }
                        break;
                    }
                }
            }
        }else if(canScan){
            enemyShip->scanner->isScanning = true;
            enemyShip->scanner->cooldown = 2;
            enemyShip->scanner->scanX = gameRandom.range(2, ship->width-1);
            enemyShip->scanner->scanY = gameRandom.range(2, ship->height-1);
            int tries = 0;
            while(tries < 10){
                int x = gameRandom.range(2, ship->width-1);
                int y = gameRandom.range(2, ship->height-1);
                const Tile& tile = ship->getTile(x, y);
                if(tile.isRoom() && !tile.isExternal() && tile.isExplored(false) == false){
                    enemyShip->scanner->scanX = x;
                    enemyShip->scanner->scanY = y;
                    scanEffect(ship, x, y, enemyShip->scanner->getSize(), red);
                    break;
                }
                tries++;
            }
            madeMove = true;
            crew->turnDone = true;
            calculateShipVisibility(ship, false);
        }else if(!doNotMove){
            if(crew->walkDistance != 0 && explorePoint.x != -1){
                SDL_Point movePoint = findClosestMovablePoint(crew, explorePoint.x, explorePoint.y);
                if(movePoint.x != -1 && movePoint != crew->position){
                    moveCrew(crew, crew->ship, movePoint.x, movePoint.y, false);
                    crew->walkDistance -= moveMap.getDistance(movePoint.x, movePoint.y);
                }else{
                    crew->turnDone = true;
                }
                madeMove = true;
            }else{
                crew->turnDone = true;
            }
        }
        if(madeMove)
            return true;
    }
    return false;
}

bool Game::updateCrews(const std::vector<Crew*>& crews){
    for(int i = 0; i < crews.size(); i++){
        Crew* crew = crews[i];
        if(crew->moving){
            updateMove(crew);
            if(crew->moving){
                return true;
            }
        }
        if(crew->action == Crew::None)
            continue;

        bool isCrewVisible = debug || crew->ship->getTile(crew->targetPosition).isVisible(true) ||
                             crew->ship->getTile(crew->position).isVisible(true);
        if(noDraw)
            isCrewVisible = false;
        if(isCrewVisible)
            crew->actionTime += frameDelta;

        if(crew->action == Crew::Repairing){
            bool doneRepairing = false;
            while((!isCrewVisible || crew->actionTime >= crew->actionTick) && crew->actionAmount != 0 && !doneRepairing){
                if(isCrewVisible)
                    crew->actionTime -= crew->actionTick;
                crew->actionAmount--;
                doneRepairing = performRepair(crew->ship, crew, crew->targetPosition.x, crew->targetPosition.y); 
            }

            if(crew->actionAmount == 0 || doneRepairing){
                if(crew->playerCrew && !doneRepairing){
                    crew->repeatAction = Crew::Repairing;
                    crew->action = Crew::None;
                }else{
                    crew->action = Crew::None;
                }
                if(hasProficiency(crew, "repairs")){
                    if(gameRandom.max(100) < skillLerp(crew, Crew::Engineering, 0, 20))
                        repairShip(ship, 1);
                }
            }
        }else if(crew->action == Crew::Healing){
            bool doneHealing = false;
            while((!isCrewVisible || crew->actionTime >= crew->actionTick) && crew->actionAmount != 0 && !doneHealing){
                if(isCrewVisible)
                    crew->actionTime -= crew->actionTick;
                crew->actionAmount--;
                doneHealing = performHeal(crew, 1, crew->targetPosition.x, crew->targetPosition.y); 
            }

            if(crew->actionAmount == 0 || doneHealing){
                if(crew->playerCrew && !doneHealing){
                    crew->repeatAction = Crew::Healing;
                    crew->action = Crew::None;
                }else{
                    crew->action = Crew::None;
                }
            }

        }else if(crew->action == Crew::Attacking){
            if(isCrewVisible)
                audioManager.playSound("laser", AudioManager::CHANNEL_WEAPON);
            while((!isCrewVisible || crew->actionTime >= crew->actionTick) && crew->actionAmount != 0){
                if(isCrewVisible)
                    crew->actionTime -= crew->actionTick;
                crew->actionAmount--;
                damageTile(crew->ship, crew->targetPosition.x, crew->targetPosition.y, 1, crew->playerCrew != crew->ship->playerShip);
            }
            if(crew->actionAmount == 0){
                crew->action = Crew::None;
                if(crew->ship->getTile(crew->targetPosition).isFire()){
                    if(hasProficiency(crew, "firefighting"))
                        crew->ship->getTile(crew->targetPosition).fire = skillLerp(crew, Crew::Combat, 1, Tile::fireCooldown);
                    else
                        crew->ship->getTile(crew->targetPosition).fire = 0;
                    crew->skills[Crew::Combat].increase(2);
                    crew->repeatAction = Crew::None;
                }else if(crew->playerCrew && hasTarget(crew->ship, true, crew->targetPosition.x, crew->targetPosition.y)){
                    crew->repeatAction = Crew::Attacking;
                }else{
                    crew->repeatAction = Crew::None;
                }
            }else{
                return true;
            }
        }

        if(crew->action != Crew::None)
            return true;
    }

    return false;
}

void Game::updateMove(Crew* crew){
    int moveAmount = frameDelta;
    while(moveAmount > 0){
        int stepSize = 5;
        if(moveAmount < 5)
            stepSize = moveAmount;
        moveAmount -= stepSize;

        SDL_Point& point = crew->moveMap.back();
        // If crew is moving from and to a tile that is not visible to player then
        // we avoid decrementing moveAmount to process the move immediately
        if(noDraw || (!debug && crew->ship->getTile(point).isVisible(true) == false &&
           crew->ship->getTile(crew->position).isVisible(true) == false)){
            moveAmount += 5;
            stepSize = 5;
        }

        glm::vec2 dir(crew->position.x - point.x, crew->position.y - point.y);
        Door* door = crew->ship->getDoor(point.x, point.y);
        if(door)
            door->isOpen = true;
        crew->drawPosition -= (dir)/(5.0f/stepSize);
        if(glm::distance(crew->drawPosition, glm::vec2(point.x*TILESIZE, point.y*TILESIZE)) < 1){
            Door* previousDoor = crew->ship->getDoor(crew->position.x, crew->position.y);
            SDL_Point previousPoint = crew->position;
            crew->setPosition(point);
            if(previousDoor && !previousDoor->forceOpen && previousDoor->health != 0 &&
               ship->getTileCrew(previousPoint.x, previousPoint.y) == NULL)
                previousDoor->isOpen = false;
            crew->moveMap.pop_back();
            if(crew->playerCrew){
                calculateShipVisibility(crew->ship, true);
            }
            if(crew->moveMap.size() == 0){
                crew->moving = false;
                if(crew->playerCrew == false)
                    calculateShipVisibility(crew->ship, false);
                // Reselect to recalculate all crew actions
                if(selectedCrew)
                    selectCrew(selectedCrew);
                break;
            }
        }
    }
}

void Game::updateScroll(){
    if(Controls::instance()->getIsMultiTouch()){
        if(Controls::instance()->getPinchDist() != 0.0f){
            if(!enemyShip || clickedInPlayerView)
                playerView.zoom -= Controls::instance()->getPinchDist();
            else
                enemyView.zoom -= Controls::instance()->getPinchDist();
            constrainZoom();
        }
    }else if(Controls::instance()->isMouseButtonDown(Controls::MOUSE_LEFT) &&
             Controls::instance()->getMouseButtonPressLength(Controls::MOUSE_LEFT) > 100){
        if(!enemyShip || clickedInPlayerView){
            playerView.offset.x += Controls::instance()->getMouseXDiff()*playerView.zoom;
            playerView.offset.y += Controls::instance()->getMouseYDiff()*playerView.zoom;
        }else{
            enemyView.offset.x += Controls::instance()->getMouseXDiff()*enemyView.zoom;
            enemyView.offset.y += Controls::instance()->getMouseYDiff()*enemyView.zoom;
        }
    }
}

void Game::constrainZoom(){
    if(playerView.zoom < MINZOOM)
        playerView.zoom = MINZOOM;
    else if(playerView.zoom > MAXZOOM)
        playerView.zoom = MAXZOOM;

    if(enemyView.zoom < MINZOOM)
        enemyView.zoom = MINZOOM;
    else if(enemyView.zoom > MAXZOOM)
        enemyView.zoom = MAXZOOM;
    SDL_Log("PlayerZoom: %f, EnemyZoom: %f", playerView.zoom, enemyView.zoom);
}


void Game::updateWindow(Window& window){
    window.doneAnimation = false;
    if(window.animate){
        window.animationWidth += frameDelta*4;
        if(window.animationWidth >= window.width){
            window.animate = false;
            window.doneAnimation = true;
        }
    }
}

void Game::updateView(ShipView& view){
    if(view.isShaking){
        glm::vec2 shk(((float)random.range(-100, 100))/20.0f, ((float)random.range(-100, 100))/20.0f);
        view.offset = view.original + shk;
        if(frameTime - view.time > 150){
            view.isShaking = false;
            view.offset = view.original;
        }
    }
}

void Game::shake(ShipView& view){
    if(!view.isShaking){
        view.original = view.offset;
    }
    view.isShaking = true;
    view.time = frameTime;
}

bool Game::teleportCrew(Crew* crew, Ship* ship, int x, int y){
    SDL_Point teleportPoint = findClosestFreePoint(ship, x, y);
    if(teleportPoint.x != -1){
        crew->repeatAction = Crew::None;
        moveCrew(crew, ship, teleportPoint.x, teleportPoint.y, true);
        return true;
    }
    return false;
}

void Game::moveCrew(Crew* crew, Ship* ship, int x, int y, bool instant){

    Ship* currentShip = crew->ship;
    if(crew->position == currentShip->captainPosition){
        currentShip->setMode(Ship::ShipMode::None);
    }

    // If ships don't match then make sure its instant move
    if(ship != currentShip)
        instant = true;
    if(instant){
        crew->setPosition(SDL_Point{x, y});
        currentShip->removeCrew(crew);
        ship->addCrew(crew);
        crew->ship = ship;
        calculateShipVisibility(ship, crew->playerCrew);
    }else if(crew->position.x != x || crew->position.y != y){
        crew->moving = true;
        calculateCrewMovePath(crew, x, y);
    }
}

void Game::calculateCrewMovePath(Crew* crew, int x, int y){
    int checkX = x;
    int checkY = y;
    crew->moveMap.clear();
    // Build list of tiles to move to by searching backwards from
    // destination tile
    while(moveMap.canMoveThrough(checkX, checkY) &&
          (crew->position.x != checkX || crew->position.y != checkY)){
        SDL_Point point = {checkX, checkY};
        crew->moveMap.push_back(point);
        int checkDistance = moveMap.getDistance(checkX, checkY);
        if(checkY > 0 && moveMap.canMoveThrough(checkX, checkY-1) && moveMap.getDistance(checkX, checkY-1) < checkDistance){
            checkY -= 1;
            continue;
        }else if(checkX > 0 && moveMap.canMoveThrough(checkX-1, checkY) && moveMap.getDistance(checkX-1, checkY) < checkDistance){
            checkX -= 1;
            continue;
        }else if(checkX < ship->width-1 && moveMap.canMoveThrough(checkX+1, checkY) && moveMap.getDistance(checkX+1, checkY) < checkDistance){
            checkX += 1;
            continue;
        }else if(checkY < ship->height-1 && moveMap.canMoveThrough(checkX, checkY+1) && moveMap.getDistance(checkX, checkY+1) < checkDistance){
            checkY += 1;
            continue;
        }
        break;
    }
}

void Game::exploreStarMap(StarMap* starMap, int x, int y, int nearDistance, int farDistance){
    glm::vec2 pos(x, y);
    for(int tx = 0; tx < starMap->width; tx++){
        for(int ty = 0; ty < starMap->height; ty++){
            glm::vec2 point(tx, ty);
            StarMap::StarTile& tile = starMap->tiles[tx][ty];
            if(glm::distance(pos, point) < farDistance){
                if(tile.isUnexplored())
                    tile.tile ^= StarMap::smt_Unexplored;
                if(getTileDistance(tx, ty, x, y) < nearDistance)
                    starMap->tiles[tx][ty].tile |= StarMap::smt_Visible;
            }
        }
    }
}

void Game::calculateShipVisibility(Ship* ship, bool player){

    // If own ship and have scanner and not in clouds then give full visibility
    if(player == ship->playerShip && ship->scanner && ship->scanner->isAvailable() &&
       (currentView != GameView::Playing || !starMap->getTile(mapPosition).isClouds())){
        for(int y = 0; y < ship->height; y++){
            for(int x = 0; x < ship->width; x++){
                if(player){
                    ship->getTile(x, y).visibility |= Tile::VisibilityFlags::Full;
                }else{
                    ship->getTile(x, y).visibility |= Tile::VisibilityFlags::Enemy_Full;
                }
            }
        }
        return;
    }

    // Clear ship visibility
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            if(player && ship->getTile(x, y).isExplored()){
                ship->getTile(x, y).visibility = (Tile::VisibilityFlags::Explored | (ship->getTile(x, y).visibility & Tile::VisibilityFlags::Enemy_Full));
            }else if(!player && ship->getTile(x, y).isExplored(false)){
                ship->getTile(x, y).visibility = (Tile::VisibilityFlags::Enemy_Explored | (ship->getTile(x, y).visibility & Tile::VisibilityFlags::Full));
            }
        }
    }

    // Apply scanner if active
    Scanner* scanner = this->ship->scanner;
    if(player == false){
        if(enemyShip)
            scanner = enemyShip->scanner;
        else
            scanner = NULL;
    }

    if(scanner && scanner->isScanning && ship->playerShip != player){
        int size = scanner->getSize()+1;
        for(int y = 0; y < ship->height; y++){
            for(int x = 0; x < ship->width; x++){
                if(x > scanner->scanX-size && x < scanner->scanX+size &&
                   y > scanner->scanY-size && y < scanner->scanY+size){
                    if(player)
                        ship->getTile(x, y).visibility |= Tile::VisibilityFlags::Full;
                    else
                        ship->getTile(x, y).visibility |= Tile::VisibilityFlags::Enemy_Full;
                }
            }
        }
    }


    // Calculate new visibility based on crews on ship
    for(int i = 0; i < ship->crews.size(); i++){
        Crew* crew = ship->crews[i];
        if(crew->playerCrew == player){
            int minX = std::max(0, crew->position.x-5);
            int minY = std::max(0, crew->position.y-5);
            int maxX = std::min(crew->position.x+5, (int)ship->width-1);
            int maxY = std::min(crew->position.y+5, (int)ship->height-1);
            for(int y = minY; y <= maxY; y++){
                for(int x = minX; x <= maxX; x++){
                    if(!crew->ship->getTile(x, y).isEmpty()){
                        // Shadow cast from crew to all points within range
                        // Based on https://playtechs.blogspot.ca/2007/03/raytracing-on-grid.html
                        SDL_Point delta = {abs(x - crew->position.x), abs(y - crew->position.y)};
                        SDL_Point check = {crew->position.x, crew->position.y};
                        int numPoints = 1 + delta.x + delta.y;
                        int increaseX = (x > crew->position.x) ? 1 : -1;
                        int increaseY = (y > crew->position.y) ? 1 : -1;
                        int next = delta.x - delta.y;
                        delta.x *= 2;
                        delta.y *= 2;
                        for(; numPoints > 0; numPoints--){
                            if(crew->ship->getTile(check).isEmpty())
                                break;
                            if(player)
                                ship->getTile(check).visibility |= Tile::VisibilityFlags::Full;
                            else
                                ship->getTile(check).visibility |= Tile::VisibilityFlags::Enemy_Full;
                            const Door* door = ship->getDoor(check.x, check.y);
                            if(door && door->isOpen == false)
                                break;

                            if(next > 0){
                                check.x += increaseX;
                                next -= delta.y;
                            }else{
                                check.y += increaseY;
                                next += delta.x;
                            }
                        }
                    }
                }
            }
        }
    }
}

void Game::clearEnemyVisibility(){
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            ship->getTile(x, y).visibility = (ship->getTile(x, y).visibility & Tile::VisibilityFlags::Full);
        }
    }
}

/**
 * Find closest non blocked, empty tile to given point.
 */
SDL_Point Game::findClosestFreePoint(Ship* ship, int x, int y){

    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            checkMap[y][x] = 0;
        }
    }

    std::queue<SDL_Point> queue;
    SDL_Point start = {x, y};
    queue.push(start);
    while(!queue.empty()){
        SDL_Point point = queue.front();
        queue.pop();
        const Tile& tile = ship->getTile(point);
        if(!tile.isBlocked() && !tile.getDoor() && !tile.isFire() && ship->getTileCrew(point.x, point.y) == NULL){
            return point; 
        }

        std::vector<SDL_Point> points;

        if(point.x > 0 && checkMap[point.y][point.x-1] == 0){
            SDL_Point p = {point.x-1, point.y};
            checkMap[p.y][p.x] = 1;
            points.push_back(p);
        }
        if(point.y > 0 && checkMap[point.y-1][point.x] == 0){
            SDL_Point p = {point.x, point.y-1};
            checkMap[p.y][p.x] = 1;
            points.push_back(p);
        }
        if(point.x < ship->width-1 && checkMap[point.y][point.x+1] == 0){
            SDL_Point p = {point.x+1, point.y};
            checkMap[p.y][p.x] = 1;
            points.push_back(p);
        }
        if(point.y < ship->height-1 && checkMap[point.y+1][point.x] == 0){
            SDL_Point p = {point.x ,point.y+1};
            checkMap[p.y][p.x] = 1;
            points.push_back(p);
        }

        int addPoints = points.size();
        if(addPoints != 0){
            int addStart = gameRandom.max(addPoints);
            for(int i = 0; i < addPoints; i++){
                queue.push(points[addStart%addPoints]);
                addStart++;
            }
        }

    }
    SDL_Point p = {-1, -1};
    return p;
}

/**
 * Find closest tile that crew can move to.
 */
SDL_Point Game::findClosestMovablePoint(const Crew* crew, int x, int y) const{

    if(crew->ship->getTileCrew(x, y) == crew){
        SDL_Point p = {x, y};
        return p;
    }

    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            checkMap[y][x] = 0;
        }
    }

    Ship* ship = crew->ship;
    std::queue<SDL_Point> queue;
    SDL_Point start = {x, y};
    queue.push(start);
    while(!queue.empty()){
        SDL_Point point = queue.front();
        queue.pop();
        if(crew->ship->getTile(point).isEmpty())
            continue;
        if(!crew->ship->getTile(point).isBlocked() && moveMap.isMovable(point.x, point.y)){
            return point; 
        }
        if(point.x > 0 && checkMap[point.y][point.x-1] == 0){
            SDL_Point p = {point.x-1, point.y};
            checkMap[p.y][p.x] = 1;
            queue.push(p);
        }
        if(point.y > 0 && checkMap[point.y-1][point.x] == 0){
            SDL_Point p = {point.x, point.y-1};
            checkMap[p.y][p.x] = 1;
            queue.push(p);
        }
        if(point.x < ship->width-1 && checkMap[point.y][point.x+1] == 0){
            SDL_Point p = {point.x+1, point.y};
            checkMap[p.y][p.x] = 1;
            queue.push(p);
        }
        if(point.y < ship->height-1 && checkMap[point.y+1][point.x] == 0){
            SDL_Point p = {point.x ,point.y+1};
            checkMap[p.y][p.x] = 1;
            queue.push(p);
        }
    }
    SDL_Point p = {-1, -1};
    return p;
}

/**
 * Find closest moveable point adjacent to given point.
 * @return {-1, -1} if no adjacent move point
 */
SDL_Point Game::findClosestAdjacentMovePoint(const Crew* crew, int x, int y) const{
    if(crew->ship->getTileCrew(x, y) == crew){
        SDL_Point p = {x, y};
        return p;
    }
    uint8_t closest = 0xFF;
    SDL_Point closestPoint = {-1, -1};
    Ship* ship = crew->ship;

    if(x > 0 && moveMap.isMovable(x-1, y) && moveMap.getDistance(x-1, y) < closest && ship->getTileCrew(x-1, y) == NULL){
        closest = moveMap.getDistance(x-1, y);
        closestPoint.x = x-1;
        closestPoint.y= y;
    }
    if(x > 0 && ship->getTileCrew(x-1, y) == crew){
        SDL_Point p = {x-1, y};
        return p;
    }
    if(y > 0 && moveMap.isMovable(x, y-1) && moveMap.getDistance(x, y-1) < closest && ship->getTileCrew(x, y-1) == NULL){
        closest = moveMap.getDistance(x, y-1);
        closestPoint.x = x;
        closestPoint.y = y-1;
    }
    if(y > 0 && ship->getTileCrew(x, y-1) == crew){
        SDL_Point p = {x, y-1};
        return p;
    }
    if(x < ship->width-1 && moveMap.isMovable(x+1, y) && moveMap.getDistance(x+1, y) < closest && ship->getTileCrew(x+1, y) == NULL){
        closest = moveMap.getDistance(x+1, y);
        closestPoint.x = x+1;
        closestPoint.y = y;
    }
    if(x < ship->width-1 && ship->getTileCrew(x+1, y) == crew){
        SDL_Point p = {x+1, y};
        return p;
    }
    if(y < ship->height-1 && moveMap.isMovable(x, y+1) && moveMap.getDistance(x, y+1) < closest && ship->getTileCrew(x, y+1) == NULL){
        closest = moveMap.getDistance(x, y+1);
        closestPoint.x = x;
        closestPoint.y = y+1;
    }
    if(y < ship->height-1 && ship->getTileCrew(x, y+1) == crew){
        SDL_Point p = {x, y+1};
        return p;
    }
    return closestPoint;
}

void Game::killCrew(Crew* crew){
    SDL_Log("Killing crew");
    crew->ship->removeCrew(crew);
    if(crew->playerCrew){
        for(auto it = playerCrews.begin(); it != playerCrews.end(); ++it){
            if(*it == crew){
                playerCrews.erase(it);
                break;
            }
        }
        if(crew->ship->playerShip == false)
            calculateShipVisibility(crew->ship, true);
    }else{

        // Clear repeat actions for anyone attacking this crew member
        for(auto it = playerCrews.begin(); it != playerCrews.end(); ++it){
            if((*it)->repeatAction == Crew::Attacking &&
               (*it)->targetPosition == crew->position){
                (*it)->repeatAction = Crew::None;
            }
        }

        for(auto it = enemyCrews.begin(); it != enemyCrews.end(); ++it){
            if(*it == crew){
                enemyCrews.erase(it);
                break;
            }
        }
        if(crew->ship->playerShip == true)
            calculateShipVisibility(ship, false);
    }
    delete crew;
}

void Game::warp(int x, int y){
    addMessage(stringLibrary.getString("warped"));
    audioManager.playSound("warp");
    int distance = glm::distance(glm::vec2(mapPosition.x, mapPosition.y), glm::vec2(x, y));
    SDL_Log("Distance: %d", distance);
    currentRecord.distance += distance;
    mapPosition.x = x;
    mapPosition.y = y;
    clearVariables(false);
    hailCallback[0] = '\0';
    destroyCallback[0] = '\0';
    fleeCallback[0] = '\0';
    attackCallback[0] = '\0';
    scanCallback[0] = '\0';
    isSafe = false;
    isPositionSafe = true;
    spawnEnemyShips = true;
    inWarp = true;
    playerWarp = true;
    doWarp = false;
    enemyDefeated = false;
    shipAnimationTimer = frameTime;
    ship->setMode(Ship::ShipMode::None);
    shots.clear();
    lasers.clear();
    asteroids.clear();
    selectedCrew = NULL;
    if(enemyShip){
        removeShip(enemyShip);
        enemyShip = NULL;
        centerViews();
    }
    if(station){
        station->ship = NULL;
        station = NULL;
    }
}

void Game::cancelWarp(){
    doWarp = false;
}

Item* Game::findInventoryItem(const char* name){
    for(int i = 0; i < items.size(); i++){
        if(strcmp(items[i].name, name) == 0){
            return &items[i];
        }
    }
    return NULL;
}

Item Game::createItem(const char* name, int amount){
    Item item = itemLibrary.findItemDefinition(name);
    item.amount = amount;
    return item;
}

bool Game::useRM(int amount){
    if(RMAmount >= amount){
        RMAmount -= amount;
        return true;
    }
    return false;
}

bool Game::useFuel(int amount){
    if(fuelAmount >= amount){
        fuelAmount -= amount;
        return true;
    }
    return false;
}

bool Game::useItem(const char* name, int amount, bool saveChange){
    for(auto it = items.begin(); it != items.end(); it++){
        Item& item = *it;
        if(strcmp(item.name, name) == 0){
            if(item.amount < amount)
                return false;
            item.amount -= amount;
            if(item.amount <= 0){
                items.erase(it);
            }
            if(saveChange){
                Item gainedItem = item;
                gainedItem.amount = -amount;
                gainedItems.push_back(gainedItem);
            }
            return true;
        }
    }
    return false;
}

void Game::addItem(const char* name, int amount, bool saveChange){
    if(amount == 0)
        return;
    Item item = itemLibrary.findItemDefinition(name);
    item.amount = amount;
    addItem(item);
    if(saveChange)
        gainedItems.push_back(item);
}

void Game::addItem(const Item& item){
    if(item.name == NULL)
        return;
    if(item.amount == 0)
        return;

    for(int i = 0; i < items.size(); i++){
        if(strcmp(items[i].name, item.name) == 0){
            items[i].amount += item.amount;
            return;
        }
    }
    items.push_back(item);
}

void Game::addRM(int amount){
    RMAmount += amount;
}

void Game::addFuel(int amount){
    fuelAmount += amount;
}

int Game::getRM(){
    return RMAmount;
}

int Game::getFuel(){
    return fuelAmount;
}

void Game::addCrew(Crew* crew, Ship* ship, const SDL_Point& point, bool player){
    crew->playerCrew = player;
    crew->ship = ship;
    crew->setPosition(point);
    ship->crews.push_back(crew);
    
    if(player)
        playerCrews.push_back(crew);
    else
        enemyCrews.push_back(crew);
}

void Game::removeShip(Ship* ship){
    if(!ship->playerShip){
        luaHandler.setEnemyShip(NULL);
    }
    for(auto it = ship->crews.begin(); it != ship->crews.end(); ++it){
        Crew* crew = *it;
        if(crew->playerCrew){
            for(auto cit = playerCrews.begin(); cit != playerCrews.end(); ++cit){
                if(*cit == crew){
                    delete *cit;
                    playerCrews.erase(cit);
                    break;
                }
            }
        }else{
            for(auto cit = enemyCrews.begin(); cit != enemyCrews.end(); ++cit){
                if(*cit == crew){
                    delete *cit;
                    enemyCrews.erase(cit);
                    break;
                }
            }
        }
    }
    delete ship;

    for(auto it = shots.begin(); it != shots.end();){
        Shot& shot = *it;
        if(shot.dest == ship){
            it = shots.erase(it);
            continue;
        }else if(shot.source == ship){
            shot.source = NULL;
        }
        ++it;
    }
}

void Game::addAction(const char* name, Action::ActionCode code, System* system, const Sprite* icon, bool available){
    Action action;
    action.name = name;
    action.code = code;
    action.system = system;
    action.icon = icon;
    action.available = available;
    actionsWindow.actions.push_back(action);
}

void Game::calculateActions(){
    actionsWindow.actions.clear();
    if(selectedCrew == NULL){
        return;
    }

    if(selectedCrew->ship->playerShip){
        Ship* ship = selectedCrew->ship;
        addAction("REPAIR", Action::repair, NULL, spriteManager.getSprite("repair_action"), selectedCrew->lockedToStation == false);
        if(hasProficiency(selectedCrew, "medicine"))
            addAction("HEAL", Action::heal, NULL, spriteManager.getSprite("heal_action"), selectedCrew->lockedToStation == false);
        const Tile& tile = ship->getTile(selectedCrew->position);
        if(tile.isWeaponsStation()){
            for(int i = 0; i < ship->weapons.size(); i++){
                const Sprite* sprite = NULL;
                if(ship->weapons[i]->weaponType == Weapon::Laser)
                    sprite = spriteManager.getSprite("laser_action");
                else
                    sprite = spriteManager.getSprite("missile_action");
                addAction(ship->weapons[i]->printName, Action::useSystem, ship->weapons[i], sprite, ship->weapons[i]->isUsable());
            }
        }else if(tile.isOpsStation()){
            if(ship->scanner){
                addAction(stringLibrary.getString("scan"), Action::useSystem, ship->scanner, spriteManager.getSprite("scan_action"), ship->scanner->isUsable());
            }
            if(ship->teleporter){
                addAction(stringLibrary.getString("teleport"), Action::useSystem, ship->teleporter, spriteManager.getSprite("tele_action"), ship->teleporter->isUsable());
            }
            addAction(stringLibrary.getString("power"), Action::useSystem, ship->engine, spriteManager.getSprite("power_action"), ship->engine);
        }else if(tile.isCaptainStation()){
            addAction(stringLibrary.getString("hail"), Action::hail, NULL, spriteManager.getSprite("hail_action"), enemyShip != NULL || hailCallback[0] != '\0');
            bool flightActions = ship->mode == Ship::None && ship->modeCooldown == 0 && ship->engine && ship->engine->isUsable();
            addAction("EVADE", Action::evade, NULL, spriteManager.getSprite("evade_action"), flightActions);
            addAction("DEFENSE", Action::defense, NULL, spriteManager.getSprite("defense_action"), flightActions);
            addAction("OFFENSE", Action::offense, NULL, spriteManager.getSprite("offense_action"), flightActions);
        }else if(tile.isSystem()){
            System* system = ship->getTileSystem((int)selectedCrew->position.x, (int)selectedCrew->position.y);
            if(system->type == System::SystemType::Sickbay){
                addAction("SICKBAY", Action::heal, system, spriteManager.getSprite("heal_action"), true);
            }
        }
    }
}

Crew* Game::createRandomCrew(){
    Crew* crew = new Crew("");
    crew->sprite = random.max(4);
    crew->proficiencies.push_back(gameRandom.max(proficiencies.size()));
    crew->proficiencies.push_back(gameRandom.max(proficiencies.size()));
    return crew;
}

void Game::selectCrew(Crew* crew){
    removeMenuSelections();

    moveMap.clear();
    crew->moveMap.clear();

    if(crew->turnDone){
        selectedCrew = NULL;
        return;
    }
    selectedCrew = crew;
    calculateActions();
    if(crew->lockedToStation){
        bool actionsAvailable = false;
        for(int i = 0; i < actionsWindow.actions.size(); i++){
            if(actionsWindow.actions[i].available){
                actionsAvailable = true;
                break;
            }
        }
        if(!actionsAvailable){
            crew->turnDone = true;
            selectedCrew = NULL;
            return;
        }
    }
    calculateMoveMap(selectedCrew);
}

bool Game::beginEncounter(Encounter* encounter){
    char start[50];
    strcpy(start, encounter->name);
    strcat(start, ".start");
    clearDialog();
    if(luaHandler.getLuaFunction(start)){
        lua_pcall(luaHandler.luaState, 0, 1, 0);
        if(lua_gettop(luaHandler.luaState) == 0){
            SDL_Log("Error with encounter '%s' expecting a return", encounter->name);
            return false;
        }
        bool ret = lua_toboolean(luaHandler.luaState, -1);
        lua_pop(luaHandler.luaState, 1);
        if(ret){
            currentRecord.encounterCounter++;
            difficulty.increase(1);
            encounter->lastEncountered = currentRecord.encounterCounter;
        }
        return ret;
    }
    return false;
}

void Game::createEncounter(const char* name, int minDistance, int maxDistance, bool forward){
    Encounter* encounter = encounterManager.getEncounter(name);
    if(encounter == NULL){
        SDL_Log("Error: Could not find encounter %s to create", name);
        return;
    }
    // Avoid situations where small distance range can create unreachable points
    if(maxDistance - minDistance < 4)
        maxDistance += 4;

    // Cleared temp checked map
    for(int x = 0; x < starMap->width; x++){
        for(int y = 0; y < starMap->height; y++){
            starMap->tiles[x][y].checked = false;
        }
    }

    // Find reachable points within minDistance to maxDistance
    std::queue<std::pair<int, SDL_Point>> queue;
    std::vector<SDL_Point> points;
    queue.push(std::pair<int, SDL_Point>(0, mapPosition));
    while(!queue.empty()){
        std::pair<int, SDL_Point> point = queue.front();
        queue.pop();
        starMap->getTile(point.second).checked = true;
        std::vector<SDL_Point> reachable;
        findReachableMapPoints(point.second, getWarpDistance(), false, reachable);
        for(int i = 0; i < reachable.size(); i++){
            if(starMap->getTile(reachable[i]).checked == true)
                continue;
            int distance = getDistance(point.second, reachable[i]);
            int totalDistance = distance + point.first;
            if(totalDistance < maxDistance){
                queue.push(std::pair<int, SDL_Point>(distance + point.first, reachable[i]));
            }
            if(totalDistance >= minDistance && totalDistance <= maxDistance){
                points.push_back(reachable[i]);
                starMap->getTile(reachable[i]).checked = true;
            }
        }
    }

    // Prefer to use points that are not visible or explored
    // to prevent backtracking
    std::vector<SDL_Point> notExplored;
    std::vector<SDL_Point> notVisible;
    for(int i = 0; i < points.size(); i++){
        StarMap::StarTile& tile = starMap->getTile(points[i]); 
        if(tile.isUnexplored())
            notExplored.push_back(points[i]);
        if(!tile.isVisible())
            notVisible.push_back(points[i]);
    }
    std::vector<SDL_Point>& targetPoints = points;
    if(notVisible.size() > 0)
        targetPoints = notVisible;
    else if(notExplored.size() > 0)
        targetPoints = notExplored;

    // Create encounter at random point around a random reachable point
    while(true){
        SDL_Point p = targetPoints[gameRandom.max(targetPoints.size())];
        p.x += gameRandom.max(4);
        p.y += gameRandom.max(4);
        if(p.x >= 0 && p.x < starMap->width &&
           p.y >= 0 && p.y < starMap->height){
            StarMap::StarTile& tile = starMap->getTile(p); 
            if(!tile.isStation() && tile.encounter == NULL){
                if(getNumReachablePoints(starMap, SDL_Point{p.x, p.y}, 4) != 0){
                    tile.encounter = encounter;
                    exploreStarMap(starMap, p.x, p.y, 0, 2);
                    return;
                }
            }
        }
    }
}

/**
 * Remove all encounters with matching name from map
 */
void Game::removeEncounters(const char* name){
    for(int x = 0; x < starMap->width; x++){
        for(int y = 0; y < starMap->height; y++){
            StarMap::StarTile& tile = starMap->tiles[x][y];
            if(tile.encounter && strcmp(tile.encounter->name, name) == 0){
                tile.encounter = NULL;
            }
        }
    }
}

void Game::addTurnCallback(const char* name, const char* callback, int turn){
    TurnCallback turnCallback;
    turnCallback.turn = turn;
    strcpy(turnCallback.name, name);
    strcpy(turnCallback.callback, callback);
    turnCallbacks.push_back(turnCallback);
    SDL_Log("Added turn callback for turn %d, currentTurn: %d", turn, turnCounter);
}

void Game::clearDialog(){
    gainedItems.clear();
    gainedRM = 0;
    gainedFuel = 0;
    gainedArmor = 0;
    dialogWindow.numOptions = 0;
}

Crew* Game::getPlayerCrew(const char* name){
    for(int i = 0; i < playerCrews.size(); i++){
        if(strcmp(playerCrews[i]->name, name) == 0)
            return playerCrews[i];
    }
    return NULL;
}

bool Game::hasProficiency(const Crew* crew, const char* proficiency){
    for(int p = 0; p < crew->proficiencies.size(); p++){
        if(strcmp(proficiencies[crew->proficiencies[p]].name, proficiency) == 0){
            return true;
        }
    }
    return false;
}

Crew* Game::findProficiencyCrew(const char* proficiency){
    for(int i = 0; i < playerCrews.size(); i++){
        Crew* crew = playerCrews[i];
        if(hasProficiency(crew, proficiency))
            return crew;
    }
    return NULL;
}

Proficiency* Game::findProficiency(const char* name){
    for(int i = 0; i < proficiencies.size(); i++){
        if(strcmp(proficiencies[i].name, name) == 0)
            return &proficiencies[i];
    }
    return NULL;
}

int Game::skillLerp(const Crew* crew, int skill, int min, int max) const{
    int diff = max - min;
    int amount = ((float)crew->skills[skill].level/10.f)*diff;
    return amount + min;
}

int Game::copyAndReplaceVariables(char* dest, DialogWindow::Word* words, const char* src, const std::vector<const char*>* variables){
    int copyDest = 0;
    int wordCount = 0;
    words[0].word = dest;
    words[0].length = 0;
    words[0].flags = 0;
    bool prevIsSpace = false;
    int spaceCounter = 0;
    bool highlight = false;
    bool inVar = false;
    char variable[128];
    int varCount = 0;
    for(int i = 0; i < strlen(src); i++){
        if(src[i] != ' ' && prevIsSpace && spaceCounter != 0){
            for(int s = 0; s < spaceCounter-1; s++){
                dest[copyDest] = ' ';
                copyDest++;
            }
            
            words[wordCount].length = spaceCounter-1;
            dest[copyDest] = '\0';
            wordCount++;
            words[wordCount].word = dest + (copyDest + 1);
            words[wordCount].length = 0;
            words[wordCount].flags = 0;
            copyDest++;
            prevIsSpace = false;
            spaceCounter = 0;
        }

        if((src[i] == ' ' && prevIsSpace == false) || src[i] == '\n'){
            dest[copyDest] = '\0';
            if(src[i] == '\n')
                words[wordCount].flags |= DialogWindow::NewLine;
            wordCount++;
            words[wordCount].word = dest + (copyDest + 1);
            words[wordCount].length = 0;
            words[wordCount].flags = 0;
            copyDest++;
            prevIsSpace = true;
            spaceCounter = 0;
        }else if(src[i] == ' '){
            spaceCounter++;
        }else if(src[i] == '|'){
            highlight = !highlight;
        }else if(src[i] == ':'){
            if(inVar){
                variable[varCount] = '\0';
                int length = 0;
                if(variable[0] >= '0' && variable[0] <= '9'){
                    int num = atoi(variable);
                    if(variables && variables->size() <= num){
                        strcpy(dest+copyDest, (*variables)[num-1]);
                        length = strlen((*variables)[num-1]);
                    }else{
                        SDL_Log("ERROR missing variable");
                    }
                }else{
                    Variable* var = getVariable(variable);
                    strcpy(dest+copyDest, var->getDataName());
                    length = strlen(var->getDataName());
                }
                words[wordCount].length += length;
                words[wordCount].flags |= DialogWindow::Highlight;
                copyDest += length;
                inVar = false;
            }else{
                varCount = 0;
                inVar = true;
            }
            prevIsSpace = false;
        }else if(inVar){
            variable[varCount++] = src[i];
        }else if(!inVar){
            if(highlight)
                words[wordCount].flags |= DialogWindow::Highlight;
            dest[copyDest] = src[i]; 
            copyDest++;
            words[wordCount].length++;
            if(src[i] != ' ')
                prevIsSpace = false;
        }
    }
    dest[copyDest] = '\0';
    words[wordCount+1].word = NULL;

    for(int i = 0; i <= wordCount; i++){
        words[i].width = drawer.getTextWidth(words[i].word, drawer.font);
    }

    return copyDest;
}

void Game::addCrewVariable(const char* name, Crew* crew, bool global){
    Variable* existingVariable = getVariable(name);
    if(existingVariable){
        existingVariable->crew = crew;
        return;
    }
    Variable* variable = new Variable(name, global);
    variable->crew = crew;
    variables.push_back(variable);
}

void Game::addSystemVariable(const char* name, System* system, bool global){
    Variable* existingVariable = getVariable(name);
    if(existingVariable){
        existingVariable->system = system;
        return;
    }
    Variable* variable = new Variable(name, global);
    variable->system = system;
    variables.push_back(variable);
}

void Game::addIntVariable(const char* name, int i, bool global){
    Variable* existingVariable = getVariable(name);
    if(existingVariable){
        existingVariable->i = i;
        return;
    }
    Variable* variable = new Variable(name, global);
    variable->i = i;
    variables.push_back(variable);
}

void Game::addStringVariable(const char* name, const char* str, bool global){
    Variable* existingVariable = getVariable(name);
    if(existingVariable){
        delete existingVariable->str;
        existingVariable->str = new char[strlen(str)+1];
        strcpy(existingVariable->str, str);
        return;
    }
    Variable* variable = new Variable(name, global);
    variable->str = new char[strlen(str)+1];
    strcpy(variable->str, str);
    variables.push_back(variable);
}

Variable* Game::getVariable(const char* name){
    for(int i = 0; i < variables.size(); i++){
        Variable* variable = variables[i];
        if(strcmp(variable->name, name) == 0)
            return variable;
    }
    return NULL;
}

void Game::removeVariable(const char* name){
    auto var = variables.begin();
    while(var != variables.end()){
        if(strcmp((*var)->name, name) == 0){
            delete *var;
            var = variables.erase(var);
            break;
        }else{
            var++;
        }
    }
}

void Game::clearVariables(bool all){
    auto var = variables.begin();
    while(var != variables.end()){
        if(all || !(*var)->global){
            delete *var;
            var = variables.erase(var);
        }else{
            var++;
        }
    }
}

void Game::addOption(const char* name, const char* str, const char* callback, const std::vector<const char*>* variables){
    if(str == NULL || (name != NULL && strcmp(stringLibrary.language, "en") != 0))
        str = stringLibrary.getString(name, StringLibrary::str_Encounter);

    copyAndReplaceVariables(dialogWindow.options[dialogWindow.numOptions].text, dialogWindow.options[dialogWindow.numOptions].words, str);
    if(callback == NULL)
        dialogWindow.options[dialogWindow.numOptions].callback[0] = '\0';
    else
        strcpy(dialogWindow.options[dialogWindow.numOptions].callback, callback);
    dialogWindow.options[dialogWindow.numOptions].crewSelect = 0;
    dialogWindow.numOptions++;
}

void Game::addCrewSelectOption(const char* name, const char* str, int numCrew, const char* callback, const std::vector<const char*>* variables){
    addOption(name, str, callback);
    dialogWindow.options[dialogWindow.numOptions - 1].crewSelect = numCrew;
}

void Game::showDialog(const char* name, const char* str, const std::vector<const char*>* variables){
    if(str == NULL || (name != NULL && strcmp(stringLibrary.language, "en") != 0))
        str = stringLibrary.getString(name, StringLibrary::str_Encounter);

    showWindow(dialogWindow);
    dialogWindow.optionSelection = 0;
    dialogWindow.encounter = NULL;
    dialogWindow.openTime = realFrameTime;
    dialogWindow.animateText = true;

    dialogWindow.dialogLen = copyAndReplaceVariables(dialogWindow.dialog, dialogWindow.dialogWords, str, variables);
    // If no options add a default Close option
    if(dialogWindow.numOptions == 0){
        addOption(NULL, "Close", "");
    }
}

/**
 * Does player or ai have a target on given ship tile.
 * @param player Player or AI
 */
bool Game::hasTarget(Ship* ship, bool player, int x, int y) const{
    const Tile& tile = ship->getTile(x, y);
    System* system = ship->getTileSystem(x, y);
    if(system && ship->playerShip != player && system->health != 0)
        return true;
    Door* door = NULL;
    if(ship->playerShip != player && (door = ship->getDoor(x, y)) && door->isOpen == false && door->health != 0)
        return true;
    if(tile.isFire() && (player || (player == false && ship->playerShip == false)))
        return true;
    Crew* tileCrew = NULL;
    if((tileCrew = ship->getTileCrew(x, y)) &&
       tileCrew->playerCrew != player)
        return true;
    return false;
}

void Game::calculateMoveMap(const Crew* crew, bool canRepair, bool canHeal){
    // Clear move map
    moveMap.clear();
    if(crew->lockedToStation){
        return;
    }

    Ship* ship = crew->ship;

    // Find all visible attackable targets on ship and see if they are
    // in field of view of this crew
    glm::vec2 entPos(crew->position.x, crew->position.y);
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            const Tile& tile = ship->getTile(x, y);
            if(!moveMap.isAttackable(x, y) && tile.isVisible(crew->playerCrew) &&
               !crew->ship->getTile(x, y).isEmpty() && glm::distance(entPos, glm::vec2(x, y)) < 8){
                if(hasTarget(ship, crew->playerCrew, x, y)){
                    // Duplicated code from ship visibility
                    SDL_Point delta = {abs(x - crew->position.x), abs(y - crew->position.y)};
                    SDL_Point check = {crew->position.x, crew->position.y};
                    int numPoints = 1 + delta.x + delta.y;
                    int increaseX = (x > crew->position.x) ? 1 : -1;
                    int increaseY = (y > crew->position.y) ? 1 : -1;
                    int next = delta.x - delta.y;
                    delta.x *= 2;
                    delta.y *= 2;
                    bool blocked = false;
                    for(; numPoints > 0; numPoints--){
                        if(crew->ship->getTile(check).isEmpty()){
                            blocked = true;
                            break;
                        }

                        const Door* door = crew->ship->getDoor(check.x, check.y);
                        if(door && door->isOpen == false && !(check.x == x && check.y == y)){
                            blocked = true;
                            break;
                        }

                        if(hasTarget(ship, crew->playerCrew, check.x, check.y)){
                            moveMap.setAction(check.x, check.y, MoveMap::Attackable);
                        }

                        if(next > 0){
                            check.x += increaseX;
                            next -= delta.y;
                        }else{
                            check.y += increaseY;
                            next += delta.x;
                        }
                    }
                    if(!blocked){
                        moveMap.setAction(x, y, MoveMap::Attackable);
                    }
                }
            }
        }
    }

    int walkDistance = crew->walkDistance+1;
    if(isSafe && crew->playerCrew)
        walkDistance = 254;
    std::queue<std::pair<SDL_Point,uint8_t> > queue;
    SDL_Point start = {crew->position.x, crew->position.y};
    queue.push(std::make_pair(start,0));
    while(!queue.empty()){
        std::pair<SDL_Point, uint8_t> entry = queue.front();
        int x = entry.first.x;
        int y = entry.first.y;
        uint8_t distance = entry.second;
        queue.pop();
        if(distance > walkDistance)
            continue;
        if(moveMap.isActionable(x, y) || moveMap.getDistance(x, y))
            continue;

        const Tile& tile = ship->getTile(x, y);
        if(!tile.isVisible(crew->playerCrew))
            continue;
        if(tile.isFire())
            continue;

        const Crew* tileCrew = ship->getTileCrew(x, y);
        if(tileCrew && canHeal && tileCrew->health != tileCrew->maxHealth){
            moveMap.setDistance(x, y, distance);
            moveMap.setAction(x, y, MoveMap::Healable);
        }else if(!tile.isBlocked() && distance != walkDistance){
            const Door* door = ship->getDoor(x, y);
            if(tileCrew && tileCrew->playerCrew != crew->playerCrew){
               continue;
            }else if(tileCrew){
                moveMap.setDistance(x, y, distance);
            }else if(door){
                if(ship->playerShip != crew->playerCrew && door->isOpen == false)
                    continue;
                else if(canRepair && ship->playerShip == crew->playerCrew && door->health != door->maxHealth){
                    moveMap.setAction(x, y, MoveMap::Repairable);
                    moveMap.setDistance(x, y, distance);
                }else{
                    moveMap.setDistance(x, y, distance);
                    moveMap.setAction(x, y, MoveMap::Movable);
                }
            }else{
                moveMap.setDistance(x, y, distance);
                moveMap.setAction(x, y, MoveMap::Movable);
            }
        }else if(canRepair && tile.isSystem()){
            const System* system = ship->getTileSystem(x, y);
            if(ship->playerShip == crew->playerCrew){
                if(system && system->health != system->maxHealth){
                    moveMap.setAction(x, y, MoveMap::Repairable);
                }
            }
            continue;
        }else{
            continue;
        }

        if(x > 0){
            SDL_Point newPoint = {x-1, y};
            queue.push(std::make_pair(newPoint,distance+1));
        }
        if(y > 0){
            SDL_Point newPoint = {x, y-1};
            queue.push(std::make_pair(newPoint,distance+1));
        }
        if(x < crew->ship->width-1){
            SDL_Point newPoint = {x+1, y};
            queue.push(std::make_pair(newPoint,distance+1));
        }
        if(y < crew->ship->height-1){
            SDL_Point newPoint = {x, y+1};
            queue.push(std::make_pair(newPoint,distance+1));
        }
    }

    // Set own position to movable
    if(canHeal && crew->health != crew->maxHealth)
        moveMap.setAction(crew->position.x, crew->position.y, MoveMap::Healable);
    else
        moveMap.setAction(crew->position.x, crew->position.y, MoveMap::Movable);
    moveMap.setDistance(crew->position.x, crew->position.y, 0);

    // All repairable/healable tiles need the crew to be able to move next to it
    // Check tiles and make sure there is an adjacent movable tile
    // If no such tile exists, then unmark it as movable
    for(int y = 0; y < ship->height; y++){
        for(int x = 0; x < ship->width; x++){
            if(moveMap.isRepairable(x, y) ||
               (moveMap.isHealable(x, y) && (x != crew->position.x || y != crew->position.y))){
                SDL_Point closestPoint = findClosestAdjacentMovePoint(crew, x, y);
                if(closestPoint.x == -1){
                    moveMap.setAction(x, y, MoveMap::None);
                }
            }
        }
    }
}

int Game::getPlayerMouseX() const{
    return Controls::instance()->getMouseX() * playerView.zoom;
}

int Game::getPlayerMouseY() const{
    return Controls::instance()->getMouseY() * playerView.zoom; 
}

bool Game::mouseInEnemyView() const{
    return enemyShip && Controls::instance()->getMouseX() > drawer.displayWidth*playerViewSize;
}

int Game::getEnemyMouseX() const{
    return (Controls::instance()->getMouseX() - drawer.displayWidth*playerViewSize)*enemyView.zoom;
}

int Game::getEnemyMouseY() const{
    return Controls::instance()->getMouseY() * enemyView.zoom;
}

int Game::getUIMouseX() const{
#ifdef ANDROID
    return Controls::instance()->getMouseX() * 0.5f;
#else
    return Controls::instance()->getMouseX();
#endif
}

int Game::getUIMouseY() const{
#ifdef ANDROID
    return Controls::instance()->getMouseY() * 0.5f;
#else
    return Controls::instance()->getMouseY();
#endif
}

void Game::drawShip(const Ship* ship, int mouseX, int mouseY, bool isPlayerView){

    const Sprite* fireFrame = NULL;
    fireFrame = cachedSprites.fire->getFrame(realStartTime, realFrameTime);
    int offsetX = (int)ship->view->offset.x;
    int offsetY = (int)ship->view->offset.y;

    // Draw shield
    if(ship->shield && ship->shield->shield != 0){
        if(ship->view->isShaking)
            SDL_SetTextureAlphaMod(cachedSprites.shield->texture, 255);
        else
            SDL_SetTextureAlphaMod(cachedSprites.shield->texture, 180);

        int width = ship->width*TILESIZE;
        int height = ship->height*TILESIZE;
        
        SDL_Rect dest = {offsetX - (int)(width*0.25f)/2, offsetY - (int)(height*0.25f)/2, (int)(width*1.25f), (int)(height*1.25f)};
        SDL_RenderCopy(drawer.renderer, cachedSprites.shield->texture, &cachedSprites.shield->rect, &dest);

        SDL_SetTextureAlphaMod(cachedSprites.shield->texture, 255);
    }

    // Ship floor tiles
    bool rowAlternate = false;
    for(int y = 0; y < ship->height; y++){
        rowAlternate = !rowAlternate;
        bool colAlternate = rowAlternate;
        for(int x = 0; x < ship->width; x++){
            colAlternate = !colAlternate;
            const Tile& tile = ship->getTile(x, y);
            if(!tile.isEmpty()){
                // Draw floor
                SDL_Rect src = {0, 0, 32, 32};
                if(colAlternate)
                    src.x = 64;
                if(tile.isExternal()){
                    SDL_SetTextureAlphaMod(drawer.tilesTexture, 36);
                }else if(tile.isVisible(isPlayerView) || debug){
                    SDL_SetTextureAlphaMod(drawer.tilesTexture, 255);
                }else if(tile.isUnexplored(isPlayerView)){
                    SDL_SetTextureAlphaMod(drawer.tilesTexture, 96);
                }else{
                    SDL_SetTextureAlphaMod(drawer.tilesTexture, 180);
                }
                    
                SDL_Point dest = {x*TILESIZE + offsetX, y*TILESIZE + offsetY};
                drawer.drawSprite(drawer.tilesTexture, &src, dest.x, dest.y);

                SDL_SetTextureAlphaMod(drawer.tilesTexture, 255);
                // Draw tile
                if(tile.tile != Tile::NoTile && (debug || tile.isExternal() || tile.isExplored(isPlayerView))){
                    src.x = (tile.tile % 4)*TILESIZE;
                    src.y = (tile.tile / 4)*TILESIZE;
                    drawer.drawSprite(drawer.tilesTexture, &src, dest.x, dest.y);
                }

                // Draw Door
                const Door* door = tile.getDoor();
                if(door && (debug || !tile.isUnexplored(isPlayerView))){
                    if((tile.isVisible(isPlayerView) || debug) && door->health != door->maxHealth)
                        drawer.drawProgressBar(door->health, door->maxHealth, x*TILESIZE + offsetX, y*TILESIZE - 4 + offsetY, 30, 4, red, white);
                    if((!tile.isVisible(isPlayerView) && !debug) || door->isOpen == false){
                        if(ship->tiles[y][x-1].isBlocked()){
                            drawer.drawRect(dest.x, dest.y + HALFTILESIZE - 2, TILESIZE, 4, red);
                        }else if(ship->tiles[y-1][x].isBlocked()){
                            drawer.drawRect(dest.x + HALFTILESIZE - 2, dest.y, 4, TILESIZE, red);
                        }
                    }else if(door->isOpen == true){
                        if(ship->tiles[y][x-1].isBlocked()){
                            drawer.drawRect(dest.x, dest.y + HALFTILESIZE - 2, 4, 4, red);
                        }else if(ship->tiles[y-1][x].isBlocked()){
                            drawer.drawRect(dest.x + HALFTILESIZE - 2, dest.y, 4, 4, red);
                        }
                    }
                }

                int mouseTileX = (mouseX - offsetX)/32;
                int mouseTileY = (mouseY - offsetY)/32;
                if(teleporterCrew && teleporterCrew->ship == ship && y == teleporterCrew->position.y && x == teleporterCrew->position.x){
                    drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, blue_highlight);
                }else if(((selectedCrew && selectedCrew->ship == ship) || 
                         (debug && !playerTurn && selectedCrew == NULL)) && moveMap.isActionable(x, y)){
                    if((mouseX - offsetX) >= 0 && (mouseY - offsetY) > 0 &&
                       mouseTileX == x && mouseTileY == y){
                        drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, green_dark_highlight);
                        if(moveMap.isRepairable(x, y))
                            cursor = 9;
                        else if(moveMap.isAttackable(x, y))
                            cursor = 5;
                        else if(moveMap.isMovable(x, y))
                            cursor = 10;
                    }else{
                        if(moveMap.isRepairable(x, y) || moveMap.isHealable(x, y)){
                            drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, blue_highlight);
                        }else if(moveMap.isAttackable(x, y)){
                            drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, red_highlight);
                        }else if(moveMap.isMovable(x, y)){
                            drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, green_highlight);
                            if(debug){
                                drawer.drawTextf(dest.x, dest.y, Left, red, drawer.font, "%d", moveMap.getDistance(x, y));
                            }
                        }
                    }
                }else if(selectedCrew && selectedCrew->ship == ship && y == selectedCrew->position.y && x == selectedCrew->position.x){
                    drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, green_highlight);
                }else if((mouseX - offsetX) >= 0 && (mouseY - offsetY) > 0 && 
                         selectedWeapon && getTileDistance(mouseTileX, mouseTileY, x, y) < selectedWeapon->damageSize){
                    drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, red_highlight);
                }else if((mouseX - offsetX) >= 0 && (mouseY - offsetY) > 0 &&
                         mouseTileX == x && mouseTileY == y){
                    drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, red_highlight);
                }

                // Draw highlights for system install
                if(installingSystem && ship->playerShip && tile.room != 0){
                    const Room& room = ship->rooms[tile.room-1];
                    const SystemLayout* layout = installingSystem->layout;
                    if(room.system == NULL &&
                       ((room.isExternal == true && installingSystem->type == System::SystemType::Weapon) ||
                        (room.isExternal == false && installingSystem->type != System::SystemType::Weapon)) &&
                       room.width >= layout->width && room.height >= layout->height){
                        drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, green_highlight);
                    }else{
                        drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, red_highlight);
                    }
                }

                // Draw a red highlight for broken systems
                if((ship->playerShip || tile.isVisible(isPlayerView) || debug) && tile.room != 0 && tile.isSystem()){
                    const System* system = ship->getTileSystem(x, y);
                    if(system->isBroken()){
                        drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, red_highlight);
                    }
                }

                if(debug){
                    if(tile.isBlocked() && !tile.isEmpty()){
                        drawer.drawRect(dest.x, dest.y, TILESIZE, TILESIZE, red_highlight);
                    }
                    if(tile.room != 0){
                        drawer.drawTextf(dest.x + HALFTILESIZE, dest.y + 15, Center, blue, drawer.font, "%d", tile.room);
                    }
                    if(tile.isSystem()){
                        drawer.drawText(dest.x + 12, dest.y + 3, Left, red, drawer.font, "S");
                    }
                }
            }
        }
    }

    // Draw systems sprites
    for(int i = 0; i < ship->allSystems.size(); i++){
        const System* system = ship->allSystems[i];
        int room = -1;
        for(int r = 0; r < ship->numRooms; r++){
            if(ship->rooms[r].system == system){
                room = r;
                break;
            }
        }
        if(room != -1){
            room += 1;
            bool isVisible = ship->rooms[room-1].isExternal || debug || (system == selectedSystem || ship->playerShip);
            bool isPartiallyVisible = false;
            if(!isVisible){
                for(int y = system->posY/TILESIZE; y < system->posY/TILESIZE + system->layout->height && !isVisible; y++){
                    for(int x = system->posX/TILESIZE; x < system->posX/TILESIZE + system->layout->width; x++){
                        const Tile& tile = ship->getTile(x, y);
                        if(tile.isVisible(isPlayerView)){
                            isVisible = true;
                            break;
                        }else if(tile.isExplored(isPlayerView)){
                            isPartiallyVisible = true;
                        }
                    }
                }
            }
            if(isVisible){
                drawer.drawSprite(system->layout->sprite, system->posX + offsetX, system->posY + offsetY);
            }else if(isPartiallyVisible){
                drawer.drawSpriteAlpha(system->layout->sprite, system->posX + offsetX, system->posY + offsetY, 128);
            }
        }
    }

    // Draw movemap arrows for player movement
    if(isPlayerView && selectedCrew && selectedCrew->ship == ship && selectedCrew->moveMap.size() > 0){
        SDL_Point prev = {selectedCrew->position.x, selectedCrew->position.y};
        for(int i = selectedCrew->moveMap.size()-1; i > 0; i--){
            const SDL_Point& curr = selectedCrew->moveMap[i];
            const SDL_Point& next = selectedCrew->moveMap[i-1];
            if(next.x != prev.x && next.y == prev.y)
                drawer.drawSprite(cachedSprites.walk_arrow->getSprite(0), curr.x*TILESIZE + offsetX, curr.y*TILESIZE + offsetY, 90, SDL_FLIP_NONE);
            else if(next.x == prev.x && next.y != prev.y)
                drawer.drawSprite(cachedSprites.walk_arrow->getSprite(0), curr.x*TILESIZE + offsetX, curr.y*TILESIZE + offsetY);
            else if((curr.y < prev.y && curr.x > next.x) || (curr.y < next.y && curr.x > prev.x))
                drawer.drawSprite(cachedSprites.walk_arrow->getSprite(1), curr.x*TILESIZE + offsetX, curr.y*TILESIZE + offsetY);
            else if((curr.x < prev.x && curr.y < next.y) || (curr.x < next.x && curr.y < prev.y))
                drawer.drawSprite(cachedSprites.walk_arrow->getSprite(1), curr.x*TILESIZE + offsetX, curr.y*TILESIZE + offsetY, 0, SDL_FLIP_HORIZONTAL);
            else if((curr.y > prev.y && curr.x > next.x) || (curr.y > next.y && curr.x > prev.x))
                drawer.drawSprite(cachedSprites.walk_arrow->getSprite(1), curr.x*TILESIZE + offsetX, curr.y*TILESIZE + offsetY, 0, SDL_FLIP_VERTICAL);
            else if((curr.y > prev.y && curr.x < next.x) || (curr.y > next.y && curr.x < prev.x))
                drawer.drawSprite(cachedSprites.walk_arrow->getSprite(1), curr.x*TILESIZE + offsetX, curr.y*TILESIZE + offsetY, 180, SDL_FLIP_NONE);
            prev = curr;
        }
        SDL_Point point = selectedCrew->moveMap[0];
        if(point.y > prev.y)
            drawer.drawSprite(cachedSprites.walk_arrow->getSprite(2), point.x*TILESIZE + offsetX, point.y*TILESIZE + offsetY, 180, SDL_FLIP_NONE);
        else if(point.x > prev.x)
            drawer.drawSprite(cachedSprites.walk_arrow->getSprite(2), point.x*TILESIZE + offsetX, point.y*TILESIZE + offsetY, 90, SDL_FLIP_NONE);
        else if(point.x < prev.x)
            drawer.drawSprite(cachedSprites.walk_arrow->getSprite(2), point.x*TILESIZE + offsetX, point.y*TILESIZE + offsetY, -90, SDL_FLIP_NONE);
        else
            drawer.drawSprite(cachedSprites.walk_arrow->getSprite(2), point.x*TILESIZE + offsetX, point.y*TILESIZE + offsetY);
    }

    SDL_SetTextureAlphaMod(drawer.tilesTexture, 255);
    // Draw markers for player shots
    for(int i = 0; i < shots.size(); i++){
        Shot& shot = shots[i];
        if(shot.dest == ship && shot.source && shot.source->playerShip){
            drawer.drawSprite(cachedSprites.target, shot.target.x*TILESIZE + offsetX, shot.target.y*TILESIZE + offsetY);
        }
    }

    // Create a y position ordered list of crews
    std::list<const Crew*> crews;
    for(int i = 0; i < ship->crews.size(); i++){
        const Crew* crew = ship->crews[i];
        bool inserted = false;
        for(auto it = crews.begin(); it != crews.end(); ++it){
            if((*it)->drawPosition.y > crew->drawPosition.y){
                crews.insert(it, crew);
                inserted = true;
                break;
            }
        }
        if(!inserted){
            crews.push_back(crew);
        }
    }

    auto it = crews.begin();
    // Ship non floor tiles
    for(int y = 0; y < ship->height; y++){
        // Draw crew at this y height
        while(it != crews.end() && (*it)->drawPosition.y <= y*TILESIZE){
            const Crew* crew = *it;
            int posY = (int)crew->drawPosition.y;
            int posX = (int)crew->drawPosition.x;
            if(crew == selectedCrew)
                drawer.drawText(posX + 12 + offsetX, posY - 20 + offsetY, Left, red, drawer.font, "S");
            if(ship->getTile(crew->position).isLifeVisible(isPlayerView) || debug){
                SDL_Rect src = {crew->sprite*32, 0, TILESIZE, TILESIZE};
                SDL_Point dest = {posX + offsetX, posY - 8 + offsetY};
                if(!crew->playerCrew){
                    if(crew->hostile)
                        drawer.drawRect(dest.x, dest.y + 8, TILESIZE, TILESIZE, red_highlight);
                    else
                        drawer.drawRect(dest.x, dest.y + 8, TILESIZE, TILESIZE, yellow_highlight);
                }
                drawer.drawSprite(drawer.crewTexture, &src, dest.x, dest.y);
                if(crew->playerCrew &&
                   (crew->action == Crew::Repairing || crew->repeatAction == Crew::Repairing))
                    drawer.drawSprite(cachedSprites.repair, dest.x, dest.y - 32);
                if(crew->playerCrew &&
                   (crew->action == Crew::Healing || crew->repeatAction == Crew::Healing))
                    drawer.drawSprite(cachedSprites.heal, dest.x, dest.y - 32);
                if(crew->playerCrew &&
                   (crew->action == Crew::Attacking || crew->repeatAction == Crew::Attacking))
                    drawer.drawSprite(cachedSprites.target, dest.x, dest.y - 32);
                if(crew->health != crew->maxHealth)
                    drawer.drawProgressBar(crew->health, crew->maxHealth, dest.x+1, dest.y-3, 30, 4, red, white);
                if(crew->action == Crew::Attacking){
                    drawer.drawLine(dest.x + HALFTILESIZE, dest.y + HALFTILESIZE, crew->targetPosition.x*TILESIZE + offsetX + HALFTILESIZE, crew->targetPosition.y*TILESIZE + offsetY + HALFTILESIZE, red);
                }
            }
            it++;
        }
        for(int x = 0; x < ship->width; x++){
            const Tile& tile = ship->getTile(x, y);
            if(!tile.isUnexplored(isPlayerView) || debug){
                if(tile.isStation()){
                    SDL_Rect src = {32, 0, TILESIZE, TILESIZE};
                    SDL_Point dest = {x*TILESIZE + offsetX, y*TILESIZE + offsetY};
                    drawer.drawSprite(drawer.tilesTexture, &src, dest.x, dest.y);

                    const char* tileType = "";
                    if(tile.isCaptainStation())
                        tileType = "C";
                    else if(tile.isOpsStation())
                        tileType = "O";
                    else if(tile.isWeaponsStation())
                        tileType = "W";
                    drawer.drawText(dest.x + 13, dest.y + 21, Left, red, drawer.font, tileType);
                }
            }
            if((tile.isVisible(isPlayerView) || debug) && tile.isFire()){
                drawer.drawSprite(fireFrame, x*TILESIZE + offsetX, y*TILESIZE + offsetY);
            }
            if(debug && tile.fire){
                drawer.drawTextf(x*TILESIZE + offsetX + HALFTILESIZE, y*TILESIZE + offsetY + 12, Center, green, drawer.font, "%d", tile.fire);
            }
        }
    }

    // System Info
    for(int i = 0; i < ship->allSystems.size(); i++){
        const System* system = ship->allSystems[i];
        int room = -1;
        for(int r = 0; r < ship->numRooms; r++){
            if(ship->rooms[r].system == system){
                room = r;
                break;
            }
        }
        if(room != -1 && (system == selectedSystem || system->health != system->maxHealth)){
            room += 1;
            bool isVisible = debug || (system == selectedSystem || ship->playerShip);
            if(!isVisible){
                for(int y = 0; y < ship->height && !isVisible; y++){
                    for(int x = 0; x < ship->width; x++){
                        const Tile& tile = ship->getTile(x, y);
                        if(tile.room == room && tile.isVisible(isPlayerView) && tile.isSystem()){
                            isVisible = true;
                            break;
                        }
                    }
                }
            }
            if(isVisible){
                if(system == selectedSystem){
                    drawer.drawRect(system->posX + offsetX - 2, system->posY + offsetY + 3, drawer.getTextWidth(system->printName, drawer.font) + 4, drawer.fontHeight + 4,  black_highlight);
                    drawer.drawText(system->posX + offsetX, system->posY + offsetY + 5, Left, white, drawer.font, system->printName);
                }
                drawer.drawProgressBar(system->health, system->maxHealth, system->posX + offsetX, system->posY - 7 + offsetY, system->layout->sprite->rect.w, 4, red, white);
            }
        }
    }

    // Debug tile grid
    if(debug){
        for(int w = 0; w < ship->width+1; w++){
            drawer.drawLine(w*TILESIZE + offsetX, offsetY, w*TILESIZE + offsetX, ship->height*TILESIZE + offsetY, white);
        }
        for(int w = 0; w < ship->width; w++){
            drawer.drawTextf(w*TILESIZE + offsetX + 12, offsetY - 16, Left, white, drawer.font, "%d", w);
        }
        for(int h = 0; h < ship->height+1; h++){
            drawer.drawLine(offsetX, h*TILESIZE + offsetY, ship->width*TILESIZE + offsetX, h*TILESIZE + offsetY, white);
        }
        for(int h = 0; h < ship->height; h++){
            drawer.drawTextf(offsetX - 16, h*TILESIZE + offsetY + 12, Left, white, drawer.font, "%d", h);
        }
    }
}

void Game::drawMessages(const Ship* ship, int offsetX, int offsetY){
    for(int i = 0; i < messages.size(); i++){
        Message& message = messages[i];
        if(message.ship != ship)
            continue;

        int diff = frameTime - message.timer;
        int alpha = 0;
        if(diff < 1500)
            alpha = 255*ilerp(message.timer, frameTime, message.timer + 1500);
        int x = message.x;
        int y = message.y;
        x += offsetX;
        y += offsetY;

        int textW, textH;
        SDL_QueryTexture(message.texture, NULL, NULL, &textW, &textH);
        SDL_Rect textRect = {0, 0, textW, textH};
        SDL_Rect destRect = {x, y - diff/50, textW, textH};

        SDL_SetTextureAlphaMod(message.texture, alpha);
        SDL_RenderCopy(drawer.renderer, message.texture, &textRect, &destRect);
    }
}

void Game::drawEffects(const Ship* ship, int offsetX, int offsetY){
    for(int i = 0; i < effects.size(); i++){
        Effect& effect = effects[i];
        if(effect.ship != ship)
            continue;

        int x = effect.point.x + offsetX;
        int y = effect.point.y + offsetY;
        drawer.drawSprite(effect.list->getFrame(effect.timer, frameTime), x, y);
    }
}

void Game::drawParticles(const Ship* ship, int offsetX, int offsetY){
    for(int i = 0; i < particleManager.numParticles; i++){
        Particle* particle = particleManager.particles[i];
        if(particle->ship != ship)
            continue;
        
        glm::vec2 pos = particle->getPosition(frameTime);
        SDL_Color color = particle->color;
        int delta = (int)(frameTime - particle->start);
        if(delta < particle->duration)
            color.a = 255*ilerp(particle->start, frameTime, particle->start + particle->duration);
        drawer.drawRect(offsetX + (int)pos.x, offsetY + (int)pos.y, 2, 2, color);
    }
}

void Game::drawAsteroids(bool foreground){
    for(int i = 0; i < asteroids.size(); i++){
        Asteroid& asteroid = asteroids[i];
        if(asteroid.foreground != foreground)
            continue;
        asteroid.position.x += frameDelta/asteroid.speed;
        asteroid.angle += frameDelta/asteroid.rotationSpeed;
        if(asteroid.position.x > drawer.displayWidth){
            asteroid.position.x = -64;
            asteroid.position.y = random.max(drawer.displayHeight) - 32;
            asteroid.randomize(random);
        }
        drawer.drawSprite(asteroid.sprite, asteroid.position.x, asteroid.position.y, asteroid.angle);
    }
}

void Game::drawClouds(){
    for(int i = 0; i < clouds.size(); i++){
        Cloud& cloud = clouds[i];
        cloud.position += (cloud.speed*(float)frameDelta);
        if(cloud.alpha < 128)
            cloud.alpha++;
        if(cloud.position.x > drawer.displayWidth ||
           cloud.position.y > drawer.displayHeight ||
           cloud.position.x < -cloud.sprite->rect.w*3 ||
           cloud.position.y < -cloud.sprite->rect.h*3){
            cloud.randomize(random, drawer.displayWidth, drawer.displayHeight);
            cloud.alpha = 0;
        }
        SDL_SetTextureAlphaMod(cloud.sprite->texture, cloud.alpha);
        SDL_Rect dest = {(int)cloud.position.x, (int)cloud.position.y, cloud.sprite->rect.w*3, cloud.sprite->rect.h*3};
        drawer.drawSprite(cloud.sprite, &dest);
        SDL_SetTextureAlphaMod(cloud.sprite->texture, 255);
    }
}

void Game::drawShots(bool enemyView){
    for(int i = 0; i < shots.size(); i++){
        Shot& shot = shots[i];
        if(shot.waiting || shot.waitingToFire)
            continue;
        if(enemyView != shot.inEnemyView)
            continue;
        glm::vec2 offset;
        int drawWidth = 0;
        int drawHeight = 0;
        if(enemyView){
            drawWidth = (drawer.displayWidth*this->enemyView.zoom)/2;
            drawHeight = (drawer.displayHeight*this->enemyView.zoom);
            offset = this->enemyView.offset;
        }else{
            drawWidth = (int)(drawer.displayWidth*playerView.zoom);
            if(enemyShip)
                drawWidth *= playerViewSize;
            drawHeight = (int)(drawer.displayHeight*playerView.zoom);
            offset = playerView.offset;
        }

        glm::vec2 start(shot.start.x, shot.start.y);
        start += offset;
        // Shooting from source view
        if((shot.source == enemyShip && shot.inEnemyView) ||
           (shot.source == ship && !shot.inEnemyView)){ 
            glm::vec2 dir(0, 1);
            glm::vec2 point = start + dir * (float)(frameTime - shot.startTime);
            drawer.drawSprite(cachedSprites.missile->getSprite(0), point.x, point.y);

            if(point.y > drawHeight+50){
                shot.start.x = random.max(drawWidth);
                if(shot.source != enemyShip){
                    shot.start.y = -100 - this->enemyView.offset.y;
                }else{
                    shot.start.y = -100 - playerView.offset.y;
                }
                shot.inEnemyView = !shot.inEnemyView;
                shot.waiting = true;
                shot.distance = UINT32_MAX;
            }
        }else{ // Shooting to destination
            glm::vec2 end(shot.target.x*TILESIZE, shot.target.y*TILESIZE);
            end += offset;
            glm::vec2 dir = glm::normalize(end - start);
            glm::vec2 point = (dir * (float)(frameTime - shot.startTime)) + start;
            if(shot.asteroid){
                drawer.drawSprite(cachedSprites.asteroids->getSprite(0), point.x, point.y);
            }else{
                drawer.drawSprite(cachedSprites.missile->getSprite(0), point.x, point.y);
            }
            float newDist = glm::distance(point,end);
            if(newDist > shot.distance)
                shot.hit = true;
            if(shot.missed && (point.y > drawHeight || point.x < 0 || point.x > drawWidth))
                shot.destroy = true;
            shot.distance = newDist;
        }
    }
}

void Game::drawLasers(bool enemyView){
    for(int i = 0; i < lasers.size(); i++){
        const Laser& laser = lasers[i];
        glm::vec2 start(laser.sourceStart.x, laser.sourceStart.y); 
        if(enemyView)
            start += this->enemyView.offset;
        else
            start += playerView.offset;

        if(laser.fireAtShot != NULL &&
           ((enemyView && laser.source != enemyShip) ||
            (!enemyView && laser.source != ship)))
            continue;

        SDL_Rect src = {32, 192, 32, 32};
        SDL_Point center = {16, 16};

        if(enemyView){
            if(laser.source == enemyShip){
                glm::vec2 start(laser.sourceStart.x, laser.sourceStart.y); 
                start += this->enemyView.offset;
                SDL_Rect dest = {(int)start.x, (int)start.y, 32, 3000};
                SDL_RenderCopyEx(drawer.renderer, drawer.tilesTexture, &src, &dest, 0, &center, SDL_FLIP_NONE);
            }else if(laser.source == ship){
                glm::vec2 start(laser.targetEnd.x*TILESIZE, laser.targetEnd.y*TILESIZE); 
                start += this->enemyView.offset;
                if(laser.missed)
                    start.y = drawer.displayHeight;
                SDL_Rect dest = {(int)start.x, (int)start.y, 32, 3000};
                SDL_RenderCopyEx(drawer.renderer, drawer.tilesTexture, &src, &dest, 180+laser.targetAngle, &center, SDL_FLIP_NONE);
            }
        }else{
            if(laser.source == enemyShip){
                glm::vec2 start(laser.targetEnd.x*TILESIZE, laser.targetEnd.y*TILESIZE); 
                start += playerView.offset;
                if(laser.missed)
                    start.y = drawer.displayHeight;
                SDL_Rect dest = {(int)start.x, (int)start.y, 32, 3000};
                SDL_RenderCopyEx(drawer.renderer, drawer.tilesTexture, &src, &dest, 180+laser.targetAngle, &center, SDL_FLIP_NONE);
            }else if(laser.source == ship){
                glm::vec2 start(laser.sourceStart.x, laser.sourceStart.y); 
                start += playerView.offset;
                SDL_Rect dest = {(int)start.x, (int)start.y, 32, 3000};
                SDL_RenderCopyEx(drawer.renderer, drawer.tilesTexture, &src, &dest, 0, &center, SDL_FLIP_NONE);
            }

        }
    }
}

void Game::drawIncoming(bool enemyView, int x){
    int shotCounter = 0;
    int height = 12;
    for(int i = 0; i < shots.size(); i++){
        const Shot& shot = shots[i];
        if(shot.waiting && shot.inEnemyView == enemyView)
            height += 36;
    }
    if(height > 12){
        drawer.drawRectBorder(x-1, 24-1, 64+2, height+2, 1, black, gray);
        drawer.drawText(x + 32, 24, Center, red, drawer.font, cachedStrings.incoming);
        for(int i = 0; i < shots.size(); i++){
            Shot& shot = shots[i];
            if(shot.waiting && shot.inEnemyView == enemyView){
                if(shot.asteroid){
                    drawer.drawSprite(cachedSprites.asteroids->getSprite(0), x + 12, 32 + 36*shotCounter);
                }else{
                    drawer.drawSprite(cachedSprites.missile->getFrame(shot.startTime, frameTime), x + 20, 32 + 36*shotCounter);
                }
                shotCounter++;
            }
        }
    }
}

void Game::draw(){
    cursor = 11;

    if(currentView == GameView::Menu ||
       currentView == GameView::Options ||
       currentView == GameView::Settings){
        // Draw UI
        drawer.clear();
        drawer.prepareUI();

        std::vector<MenuButton>* drawList = NULL;
        int selection = 0;
        if(currentView == GameView::Menu){
            drawList = &mainButtons;
            selection = mainSelection;
        }else if(currentView == GameView::Options){
            drawList = &optionsButtons;
            selection = optionsSelection;
        }else if(currentView == GameView::Settings){
            drawList = &settingsButtons;
            selection = settingsSelection;
        }
        for(int i = 0; i < drawList->size(); i++){
            const MenuButton& button = (*drawList)[i];
            SDL_Color color = white;
#ifndef ANDROID
            if(i == selection)
                color = yellow;
#endif
            if(button.visible)
                drawer.drawText(button.point.x, button.point.y, Center, color, drawer.messageFont, button.text);
        }

        // Draw UI to screen
        drawer.presentUI();

        SDL_RenderPresent(drawer.renderer);
        return;
    }else if(currentView == GameView::Hangar){
        drawHangar();
        return;
    }

    SDL_SetRenderTarget(drawer.renderer, drawer.drawTexture);
    SDL_SetRenderDrawColor(drawer.renderer, 0, 0, 0, 0);
    SDL_RenderClear(drawer.renderer);

    // Cleanup expired effects
    auto it = effects.begin();
    while(it != effects.end()){
        Effect& effect = *it;
        if(frameTime - effect.timer >= effect.duration - 1){
            it = effects.erase(it);
        }else{
            it++;
        }
    }

    particleManager.update(frameTime);

    // Draw World
    drawShip(ship, getPlayerMouseX(), getPlayerMouseY(), debugPlayerView);
    drawShots(false);
    drawLasers(false);
    drawMessages(ship, playerView.offset.x, playerView.offset.y);
    drawEffects(ship, playerView.offset.x, playerView.offset.y);
    drawParticles(ship, playerView.offset.x, playerView.offset.y);

    if(selectedScanner && !mouseInEnemyView()){
        int size = ship->scanner->getSize();
        drawer.drawRect(getPlayerMouseX() - (size*TILESIZE) - 16, getPlayerMouseY() - (size*TILESIZE) - 16,
                 size*TILESIZE*2+TILESIZE, size*TILESIZE*2+TILESIZE, blue_highlight);
    }

    if(frameTime - flashStart < flashDuration){
        SDL_Color color = flashColor;
        if(frameTime - flashStart < flashDuration/2){
            color.a = 255*lerp(flashStart, frameTime, flashStart + flashDuration/2);
        }else{
            color.a = 255*(1.0f - lerp(flashStart + flashDuration/2, frameTime, flashStart + flashDuration));
        }
        drawer.drawRect(0, 0, drawer.displayWidth, drawer.displayHeight, color);
    }

    if(enemyShip){
        SDL_SetRenderTarget(drawer.renderer, drawer.enemyTexture);
        SDL_SetRenderDrawColor(drawer.renderer, 0, 0, 0, 0);
        SDL_RenderClear(drawer.renderer);

        drawShip(enemyShip, getEnemyMouseX(), getEnemyMouseY(), debugPlayerView);
        drawShots(true);
        drawLasers(true);
        drawMessages(enemyShip, enemyView.offset.x, enemyView.offset.y);
        drawEffects(enemyShip, enemyView.offset.x, enemyView.offset.y);
        drawParticles(enemyShip, enemyView.offset.x, enemyView.offset.y);
        if(selectedScanner && mouseInEnemyView()){
            int size = ship->scanner->getSize();
            drawer.drawRect(getEnemyMouseX() - (size*TILESIZE) - 16, getEnemyMouseY() - (size*TILESIZE) - 16,
                     size*TILESIZE*2+TILESIZE, size*TILESIZE*2+TILESIZE, blue_highlight);
        }
    }

    drawer.prepareUI();

    // Split screen divider
    if(enemyShip)
        drawer.drawLine(drawer.uiWidth*playerViewSize, 0, drawer.uiWidth*playerViewSize, drawer.uiHeight, gray);

    // Draw UI
    // Portraits
    for(int i = 0; i < playerCrews.size(); i++){

        const Crew *crew = playerCrews[i];

        if(crew->turnDone || playerTurn == false)
            SDL_SetTextureAlphaMod(cachedSprites.portrait->texture, 128);
        else if(crew->lockedToStation)
            SDL_SetTextureAlphaMod(cachedSprites.portrait->texture, 192);
        else
            SDL_SetTextureAlphaMod(cachedSprites.portrait->texture, 255);

        int destX = 6 + i*70;
        int destY = drawer.uiHeight-62-24;

        drawer.drawSprite(cachedSprites.portrait, destX, destY);
        drawer.drawProgressBar(crew->health, crew->maxHealth, destX + 1, destY + 64, 62, 7, red, white);
        drawer.drawText(destX + 32, destY + 72, Center, white, drawer.font, crew->name);

        if(crew == selectedCrew){
            drawer.drawText(destX, destY - 12, Left, red, drawer.font, "SELECTED");
        }else{
            const Tile& tile = ship->getTile(crew->position);
            if(tile.isStation()){
                const char* station = "";
                if(tile.isCaptainStation())
                    station = "C";
                if(tile.isOpsStation())
                    station = "O";
                if(tile.isWeaponsStation())
                    station = "W";
                drawer.drawText(destX + 32, destY - 13, Center, red, drawer.font, station);
            }
        }
        SDL_SetTextureAlphaMod(cachedSprites.portrait->texture, 255);
    }

    // Draw player ship info
    int borderSize = cachedSprites.window->getSprite(0)->rect.w;
    int cornerSize = cachedSprites.corner->rect.w;
    drawer.drawRect(0, 0, 335-borderSize, 36, black);
    drawer.drawRect(0, 36, 335-cornerSize, cornerSize, black);
    drawer.drawSprite(cachedSprites.corner, 335-cornerSize, 36);
    SDL_Rect infoSide = {335-borderSize, 0, borderSize, 36};
    drawer.drawSprite(cachedSprites.window->getSprite(4), &infoSide);
    infoSide = {0, 36+cornerSize-borderSize, 335-cornerSize, borderSize};
    drawer.drawSprite(cachedSprites.window->getSprite(5), &infoSide);

    drawer.drawProgressBar(ship->armor, ship->armorMax, 5, 5, 320, 18, red, gray);
    drawer.drawTextf(5 + 160, 10, Center, white, drawer.font, "%d/%d", ship->armor, ship->armorMax);
    if(ship->shield){
        int regen = ship->shield->shield + (ship->getShieldRegen()/ship->shield->shieldRegenTurns)*(ship->shield->shieldRegenTurns - ship->shield->shieldRegenCooldown);
        if(regen > ship->shield->shieldMax)
            regen = ship->shield->shieldMax;
        drawer.drawProgressBar(ship->shield->shield, regen, ship->shield->shieldMax, 5, 25, 320, 10, green, shield_regen_green, gray);
    }else{
        drawer.drawProgressBar(0, 1, 5, 25, 320, 10, green, gray);
    }
    int infoOffsetX = 5;
    infoOffsetX += drawer.drawTextf(infoOffsetX, 38, Left, white, drawer.font, "RM:%d ", getRM());
    infoOffsetX += drawer.drawTextf(infoOffsetX, 38, Left, white, drawer.font, "F:%d ", getFuel());
    infoOffsetX += drawer.drawTextf(infoOffsetX, 38, Left, yellow, drawer.font, "EV:%.2d%% ", ship->getEvasion());

    if(ship->mode == Ship::ShipMode::Offensive)
        drawer.drawText(infoOffsetX, 38, Left, white, drawer.font, "OFFENSIVE");
    else if(ship->mode == Ship::ShipMode::Defensive)
        drawer.drawText(infoOffsetX, 38, Left, white, drawer.font, "DEFENSIVE");
    else if(ship->mode == Ship::ShipMode::Evasive)
        drawer.drawText(infoOffsetX, 38, Left, white, drawer.font, "EVASIVE");

    if(!inWarp){
        if(isSafe){
            drawer.drawSprite(cachedSprites.safe, drawer.uiWidth/2 - cachedSprites.safe->rect.w/2, 10);
            drawer.drawText(drawer.uiWidth/2, cachedSprites.safe->rect.h + 10 + 5, Center, green, drawer.font, cachedStrings.safe);
        }else{
            drawer.drawSprite(cachedSprites.unsafe, drawer.uiWidth/2 - cachedSprites.safe->rect.w/2, 10);
            drawer.drawText(drawer.uiWidth/2, cachedSprites.safe->rect.h + 10 + 5, Center, red, drawer.font, cachedStrings.danger);
        }
    }

    drawIncoming(false, enemyShip ? drawer.uiWidth*playerViewSize - drawer.getTextWidth(8, drawer.font) : drawer.uiWidth - 64);

    if(enemyShip){
        // Draw enemy ship info
        int infoOffsetX = drawer.uiWidth - 325;
        drawer.drawRect(infoOffsetX-4, 0, 325+4, 36, black);
        drawer.drawRect(infoOffsetX+cornerSize-borderSize-4, 36, 325, cornerSize, black);
        drawer.drawSprite(cachedSprites.corner, infoOffsetX - borderSize - 4, 36, 0, SDL_FLIP_HORIZONTAL);
        infoSide = {infoOffsetX-4-borderSize, 0, borderSize, 36};
        drawer.drawSprite(cachedSprites.window->getSprite(6), &infoSide);
        int subInfoOffsetX = infoOffsetX-4-borderSize+cornerSize;
        infoSide = {subInfoOffsetX, 36+cornerSize-borderSize, 335-cornerSize, borderSize};
        drawer.drawSprite(cachedSprites.window->getSprite(7), &infoSide);

        drawer.drawProgressBar(enemyShip->armor, enemyShip->armorMax, infoOffsetX, 5, 320, 18, red, gray);
        drawer.drawTextf(drawer.uiWidth - 325 + 160, 10, Center, white, drawer.font, "%d/%d", enemyShip->armor, enemyShip->armorMax);
        if(enemyShip->shield){
            int regen = enemyShip->shield->shield + (enemyShip->getShieldRegen()/enemyShip->shield->shieldRegenTurns)*(enemyShip->shield->shieldRegenTurns - enemyShip->shield->shieldRegenCooldown);
            if(regen > enemyShip->shield->shieldMax)
                regen = enemyShip->shield->shieldMax;
            drawer.drawProgressBar(enemyShip->shield->shield, regen, enemyShip->shield->shieldMax, infoOffsetX, 25, 320, 10, green, shield_regen_green, gray);
        }else{
            drawer.drawProgressBar(0, 1, infoOffsetX, 25, 320, 10, green, gray);
        }
        subInfoOffsetX += drawer.drawTextf(subInfoOffsetX, 38, Left, yellow, drawer.font, "EV:%.2d%% ", enemyShip->getEvasion());

        if(enemyShip->mode == Ship::ShipMode::Offensive)
            drawer.drawText(subInfoOffsetX, 38, Left, white, drawer.font, "OFFENSIVE");
        else if(enemyShip->mode == Ship::ShipMode::Defensive)
            drawer.drawText(subInfoOffsetX, 38, Left, white, drawer.font, "DEFENSIVE");
        else if(enemyShip->mode == Ship::ShipMode::Evasive)
            drawer.drawText(subInfoOffsetX, 38, Left, white, drawer.font, "EVASIVE");

        drawIncoming(true, drawer.uiWidth*playerViewSize + 1);
    }

    // Draw bottom right action buttons
    if(playerTurn){
        int offset = drawer.uiWidth - 70*4;
        int offsetY = drawer.uiHeight - 86;
        if(station != NULL && isSafe){
            drawer.drawRectBorder(offset, offsetY, 64, 64, 2, black, gray);
            drawer.drawSprite(cachedSprites.buttons->getSprite(3), offset + 2, offsetY);
            drawer.drawText(offset + 32, offsetY + 66, Center, white, drawer.font, cachedStrings.store);
        }
        offset += 70;
        drawer.drawRectBorder(offset, offsetY, 64, 64, 2, black, gray);
        drawer.drawSprite(cachedSprites.buttons->getSprite(2), offset + 2, offsetY);
        drawer.drawText(offset + 32, offsetY + 66, Center, white, drawer.font, cachedStrings.cargo);
        offset += 70;
        drawer.drawRectBorder(offset, offsetY, 64, 64, 2, black, gray);
        drawer.drawText(offset + 32, offsetY + 66, Center, white, drawer.font, cachedStrings.starmap);
        if(ship->engine == NULL || !ship->engine->isAvailable() || ship->engine->cooldown != 0 || inWarp){
            drawer.drawSpriteAlpha(cachedSprites.buttons->getSprite(1), offset + 2, offsetY, 128);
        }else{
            drawer.drawSprite(cachedSprites.buttons->getSprite(1), offset + 2, offsetY);
        }
        offset += 70;
        drawer.drawRectBorder(offset, offsetY, 64, 64, 2, black, gray);
        if(isSafe)
            drawer.drawSpriteAlpha(cachedSprites.buttons->getSprite(0), offset + 2, offsetY, 128);
        else
            drawer.drawSprite(cachedSprites.buttons->getSprite(0), offset + 2, offsetY);
        drawer.drawText(offset + 32, offsetY + 66, Center, isSafe ? red : white, drawer.font, cachedStrings.end_turn);
    }


    if(selectedCrew && actionsWindow.actions.size() != 0){
        drawer.prepareWindowTexture();
        actionsWindow.height = 72;
        actionsWindow.width = actionsWindow.actions.size() * 64 + 12;
        actionsWindow.offsetX = drawer.uiWidth/2 - actionsWindow.width/2;
        actionsWindow.offsetY = drawer.uiHeight - actionsWindow.height;
        drawer.drawText(drawer.getTextCentered(cachedStrings.actions, drawer.font, actionsWindow.width), 1, Left, white, drawer.font, cachedStrings.actions);
        for(int i = 0; i < actionsWindow.actions.size(); i++){
            Action& action = actionsWindow.actions[i];
            int posX = 4 + i*64 + 12;
            if(action.icon){
                if(!action.available)
                    drawer.drawSpriteAlpha(action.icon, posX, 15, 128);
                else
                    drawer.drawSprite(action.icon, posX, 15);
            }else{
                drawer.drawRect(posX, 15, 40, 40, action.available ? white : gray);
            }
            
            drawer.drawText(posX + drawer.getTextCentered(action.name, drawer.font, 40), 15 + 40 + 4,
                            Left, i == selectedAction ? red : action.available ? white : gray,
                            drawer.font , action.name);
#ifndef ANDROID
            drawer.drawTextf(posX - drawer.fontHeight, 15, Left, white, drawer.font, "%d", i+1);
#endif
        }
        drawWindow(actionsWindow);
    }

    // Draw player/enemy turn notification
    if(!gameOver && frameTime - turnTimer < 500){
        uint64_t diff = frameTime - turnTimer;
        uint8_t alpha = 0;
        if(diff >= 100 && diff <= 400)
            alpha = 255;
        else if(diff > 400)
            alpha = (1.0f - (((float)diff-400) / 100.0f))*255;
        else if(diff < 100)
            alpha = (((float)diff) / 100.0f)*255;

        SDL_Color color = green;
        if(!playerTurn)
            color = red;
        color.a = alpha;
        if(enemyShip == NULL){
            drawer.drawRect(0, drawer.uiHeight/3, drawer.uiWidth, drawer.uiHeight/4, color);
            if(playerTurn)
                drawer.drawText(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8 - 12, Center, white, drawer.messageFont, cachedStrings.player_turn);
            else
                drawer.drawText(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8 - 12, Center, white, drawer.messageFont, cachedStrings.enemy_turn);
        }else{
            if(playerTurn){
                int width = drawer.uiWidth*playerViewSize;
                drawer.drawRect(0, drawer.uiHeight/3, width, drawer.uiHeight/4, color);
                drawer.drawText(width/2, drawer.uiHeight/3 + drawer.uiHeight/8 - 12, Center, white, drawer.messageFont, cachedStrings.player_turn);
            }else{
                int width = drawer.uiWidth - drawer.uiWidth*playerViewSize;
                drawer.drawRect(drawer.uiWidth*playerViewSize+1, drawer.uiHeight/3, width, drawer.uiHeight/4, color);
                drawer.drawText(width/2 + drawer.uiWidth*playerViewSize, drawer.uiHeight/3 + drawer.uiHeight/8 - 12, Center, white, drawer.messageFont, cachedStrings.enemy_turn);
            }
        }
    }

#ifndef ANDROID
    if(luaWindow.visible){
        drawer.prepareWindowTexture();
        luaWindow.height = 51*drawer.fontHeight;
        luaWindow.width = drawer.uiWidth*0.8f;
        centerWindow(luaWindow);
        for(int i = 0; i < luaWindow.history.size(); i++){
            drawer.drawText(6, i*drawer.fontHeight, Left, white, drawer.font, luaWindow.history[i].c_str());
        }
        int textOffset = drawer.drawText(6, 50*drawer.fontHeight, Left, white, drawer.font, "Command: ");
        drawer.drawText(6 + textOffset, 50*drawer.fontHeight, Left, white, drawer.font, luaWindow.luaCommand);
        drawWindow(luaWindow);
    }
#endif

    if(systemWindow.visible){
        drawer.prepareWindowTexture();
        const System* system = systemWindow.system;
        systemWindow.width = 250;
        systemWindow.height = 64 + system->upgradeItems->size()*33;
        if(system->upgradeRM != 0){
            systemWindow.height += 33;
            systemWindow.height += 24; // Upgrade button
        }else if(system->upgradeItems->size() > 0){
            systemWindow.height += 24; // Upgrade button
        }
        centerWindow(systemWindow);
        drawer.drawText(1, 1, Left, white, drawer.font, system->printName);

        drawer.drawRect(1, 15, 32, 32, gray);
        drawer.drawTextf(34, 15, Left, white, drawer.font, "%s: %.2d/%.2d", cachedStrings.health, system->health, system->maxHealth); 
        drawer.drawTextf(34, 27, Left, white, drawer.font, "%s:  %.2d/%.2d", cachedStrings.power, system->power, system->maxPower); 

        if(system->upgrade != NULL){
            drawer.drawText(1, 50, Left, white, drawer.font, cachedStrings.upgrade);

            int upgradeCounter = 0;
            if(system->upgradeRM != 0){
                drawer.drawRect(1, 48 + 13, 32, 32, gray);
                drawer.drawIcon(0, 1, 48 + 13);
                drawer.drawText(1 + 33, 48 + 15, Left, white, drawer.font, cachedStrings.recycled_material);
                drawer.drawTextf(1 + 33, 48 + 28, Left, white, drawer.font, "%s: %d", cachedStrings.amount, system->upgradeRM);
                upgradeCounter++;
            }

            for(int i = 0; i < system->upgradeItems->size(); i++){
                const Item& item = (*system->upgradeItems)[i];
                drawer.drawRect(1, 48 + 13 + 33*upgradeCounter, 32, 32, gray);
                drawer.drawIcon(item.icon, 1, 48 + 13 + 33*upgradeCounter);
                drawer.drawText(1 + 33, 48 + 15 + 33*upgradeCounter, Left, white, drawer.font, item.printName);
                drawer.drawTextf(1 + 33, 48 + 28 + 33*upgradeCounter, Left, white, drawer.font, "%s: %d", cachedStrings.amount, item.amount);
                upgradeCounter++;
            }
            drawer.drawText(1, systemWindow.height - 16, Left, white, drawer.font, cachedStrings.upgrade);
        }else{
            drawer.drawText(1, 50, Left, white, drawer.font, "No Upgrade");
        }
        drawer.drawText(systemWindow.width, systemWindow.height - 16, Right, white, drawer.font, "Uninstall");
        drawWindow(systemWindow);
    }

    if(stationWindow.visible){
        drawer.prepareWindowTexture();
        stationWindow.width = 512;
        stationWindow.height = 300;
        centerWindow(stationWindow);
        drawer.drawText(1, 1, Left, white, drawer.font, "STORE");
        drawer.drawTextf(1+75, 1, Left, white, drawer.font, "AVAILABLE RM: %d", getRM());
        int itemCounter = 0;
        // Todo: Redesign and refactor the station window view
        // For now really repetitive draw calls
        if(station->repairPrice != 0 || station->healPrice != 0 || station->mapPrice != 0){
            drawer.drawText(1, 14, Left, white, drawer.font, "SERVICES");
            if(station->repairPrice != 0){
                drawer.drawText(1, 27+46*itemCounter, Left, white, drawer.font, "REPAIRS");
                drawer.drawText(1 + 34, 27+13+46*itemCounter, Left, white, drawer.font, "REPAIR 15 ARMOR");
                drawer.drawTextf(1 + 34, 27+30+46*itemCounter, Left, white, drawer.font, "%d RM", station->repairPrice);
                SDL_Color color = white;
                if(getRM() < station->repairPrice)
                    color = red;
                drawer.drawText(1 + 224, 27+46*itemCounter + 22, Left, color, drawer.font, "BUY");
                drawer.drawRect(1, 27+46*itemCounter+12, 32, 32, white);
                itemCounter++;
            }
            if(station->healPrice != 0){
                drawer.drawText(1, 27+46*itemCounter, Left, white, drawer.font, "HEALTH");
                drawer.drawText(1 + 34, 27+13+46*itemCounter, Left, white, drawer.font, "HEAL ENTIRE CREW");
                drawer.drawTextf(1 + 34, 27+30+46*itemCounter, Left, white, drawer.font, "%d RM", station->healPrice);
                SDL_Color color = white;
                if(getRM() < station->healPrice)
                    color = red;
                drawer.drawText(1 + 224, 27+46*itemCounter + 22, Left, color, drawer.font, "BUY");
                drawer.drawRect(1, 27+46*itemCounter+12, 32, 32, white);
                itemCounter++;
            }
            if(station->mapPrice != 0){
                drawer.drawText(1, 27+46*itemCounter, Left, white, drawer.font, "STAR CHART");
                drawer.drawText(1 + 34, 27+13+46*itemCounter, Left, white, drawer.font, "PURCHASE NEARBY CHART");
                drawer.drawTextf(1 + 34, 27+30+46*itemCounter, Left, white, drawer.font, "%d RM", station->mapPrice);
                SDL_Color color = white;
                if(getRM() < station->mapPrice)
                    color = red;
                drawer.drawText(1 + 224, 27+46*itemCounter + 22, Left, color, drawer.font, "BUY");
                drawer.drawRect(1, 27+46*itemCounter+12, 32, 32, white);
                itemCounter++;
            }
        }
        drawer.drawLine(1+254, 0, 1+254, stationWindow.height, red);
        if(station->items.size() != 0 || station->fuelPrice != 0){
            drawer.drawText(1 + 256, 14, Left, white, drawer.font, "ITEMS");
            int itemCounter = 0;
            if(station->fuelPrice != 0){
                drawer.drawText(1 + 256, 27 + 46*itemCounter, Left, white, drawer.font, cachedStrings.fuel);
                drawer.drawRect(1 + 256, 27+46*itemCounter+12, 32, 32, white);
                drawer.drawTextf(1 + 34 + 256, 27+13+46*itemCounter, Left, white, drawer.font, "AMOUNT: %d", station->fuelAmount);
                drawer.drawTextf(1 + 34 + 256, 27+30+46*itemCounter, Left, white, drawer.font, "%d RM EA", station->fuelPrice);
                SDL_Color color = white;
                if(getRM() < station->fuelPrice || station->fuelAmount == 0)
                    color = red;
                drawer.drawText(1 + 224 + 256, 27+46*itemCounter+ 22, Left, color, drawer.font, "BUY");
                itemCounter++;
            }
            for(int i = 0; i < station->items.size(); i++){
                Item& item = station->items[i];
                drawer.drawText(1 + 256, 27 + 46*itemCounter, Left, white, drawer.font, item.printName);
                drawer.drawRect(1 + 256, 27+46*itemCounter+12, 32, 32, white);
                drawer.drawTextf(1 + 34 + 256, 27+13+46*itemCounter, Left, white, drawer.font, "AMOUNT: %d", item.amount);
                drawer.drawTextf(1 + 34 + 256, 27+30+46*itemCounter, Left, white, drawer.font, "%d RM EA", item.price);
                SDL_Color color = white;
                if(getRM() < item.price || item.amount == 0)
                    color = red;
                drawer.drawText(1 + 224 + 256, 27+46*itemCounter+ 22, Left, color, drawer.font , "BUY");
                itemCounter++;
            }
        }
        drawWindow(stationWindow);
    }

    if(inventoryWindow.visible){
        drawer.prepareWindowTexture();
        inventoryWindow.width = 33 * 10 + 1;
        inventoryWindow.height = 33 * 6 + drawer.fontHeight + 1;
        centerWindow(inventoryWindow);
        if(inventoryWindow.selection != -1)
            inventoryWindow.height += 33;
        drawer.drawText(1, 1, Left, white, drawer.font, cachedStrings.cargo);

        drawer.drawTextf(80, 1, Left, white, drawer.font, "%s: %d", cachedStrings.recycled_material, getRM());
        drawer.drawTextf(260, 1, Left, white, drawer.font, "%s: %d", cachedStrings.fuel, getFuel()); 

        if(inventoryWindow.selection != -1){
            const Item& item = items[inventoryWindow.selection];
            drawer.drawRect(1, drawer.fontHeight + 33*6, 32, 32, gray);
            if(debug)
                drawer.drawText(34, drawer.fontHeight + 3 + 33*6, Left, white, drawer.font, item.name);
            else
                drawer.drawText(34, drawer.fontHeight + 3 + 33*6, Left, white, drawer.font, item.printName);
            
            drawer.drawTextf(34, drawer.fontHeight + 3 + 33*6 + drawer.fontHeight, Left, white, drawer.font, "%s: %d", cachedStrings.amount, item.amount);

            drawer.drawIcon(item.icon, inventoryWindow.offsetX + 1, inventoryWindow.offsetY + 13 + 33*6);
            
            drawer.drawText(270, inventoryWindow.height - 24, Left, white, drawer.font, cachedStrings.recycle);
            if(item.system != NULL)
                drawer.drawText(200, inventoryWindow.height - 24, Left, white, drawer.font, cachedStrings.install);
        }
        int inventoryCounter = 0;
        for(int y = 0; y < 6; y++){
            for(int x = 0; x < 10; x++){
                drawer.drawRect(x*33, y*33 + drawer.fontHeight, 32, 32, gray);

                if(inventoryCounter < items.size()){
                    const Item& item = items[inventoryCounter];
                    drawer.drawIcon(item.icon, x*33, y*33 + drawer.fontHeight);
                    drawer.drawTextf(x*33, y*33 + 32, Left, white, drawer.font, "%d", item.amount);
                }
                
                inventoryCounter++;
            }
        }
        drawWindow(inventoryWindow);
    }

    if(mapWindow.visible){
        drawer.prepareWindowTexture();
        mapWindow.width = ((int)(drawer.uiWidth*0.85f)/MAPTILESIZE)*MAPTILESIZE;
        mapWindow.height = ((int)(drawer.uiHeight*0.85f)/MAPTILESIZE)*MAPTILESIZE;

        centerWindow(mapWindow);
        if(mapWindow.doneAnimation){
            mapWindow.center(mapPosition);
        }

        int offsetX = mapWindow.mapOffsetX%MAPTILESIZE;
        int offsetY = mapWindow.mapOffsetY%MAPTILESIZE;
        SDL_Point mousePos = getWindowMousePos(mapWindow);
        mousePos.x = (mousePos.x + offsetX)/MAPTILESIZE;
        mousePos.y = (mousePos.y + offsetY)/MAPTILESIZE;

        int nearDistance = 1;
        if(ship->scanner)
            nearDistance = ship->scanner->nearVisibility;
        for(int x = 0; x < mapWindow.width/MAPTILESIZE+1; x++){
            for(int y = 0; y < mapWindow.height/MAPTILESIZE+1; y++){

                int mapX = x + mapWindow.mapOffsetX/MAPTILESIZE;
                int mapY = y + mapWindow.mapOffsetY/MAPTILESIZE;
                if(mapX >= starMap->width || mapY >= starMap->height)
                    break;
                if(mapX == mapPosition.x && mapY == mapPosition.y)
                    drawer.drawRect(x*MAPTILESIZE - offsetX, y*MAPTILESIZE - offsetY, MAPTILESIZE, MAPTILESIZE, red_highlight);
                const StarMap::StarTile& tile = starMap->getTile(SDL_Point{mapX, mapY});
                if(!tile.isUnexplored() || debug){
                    if(tile.isStar()){
                        drawer.drawSprite(cachedSprites.sun, x*MAPTILESIZE+3-offsetX, y*MAPTILESIZE+3-offsetY);
                    }
                    if(tile.encounter != NULL)
                        drawer.drawText(x*MAPTILESIZE + MAPTILESIZE/2 - offsetX, y*MAPTILESIZE + 12-offsetY, Center, red, drawer.font, "!");
                    if(tile.isVisible() || debug){
                        if(tile.isAsteroids()){
                            drawer.drawSprite(cachedSprites.mapasteroid, x*MAPTILESIZE-offsetX, y*MAPTILESIZE-offsetY);
                        }
                        if(tile.isStation()){
                            drawer.drawText(x*MAPTILESIZE + MAPTILESIZE/2 - offsetX, y*MAPTILESIZE + 12 - offsetY, Center, white, drawer.font, "S");
                        }
                    }else{
                        SDL_Color fog = {128, 128, 128, 104};
                        drawer.drawRect(x*MAPTILESIZE - offsetX, y*MAPTILESIZE - offsetY, MAPTILESIZE, MAPTILESIZE, fog);
                    }
                }else{
                    SDL_Color fog = {128, 128, 128, 128};
                    drawer.drawRect(x*MAPTILESIZE - offsetX, y*MAPTILESIZE - offsetY, MAPTILESIZE, MAPTILESIZE, fog);
                }
                drawer.drawSprite(cachedSprites.grid->getSprite(0), x*MAPTILESIZE - offsetX, y*MAPTILESIZE - offsetY);
                if(x == mousePos.x && y == mousePos.y){
                    drawer.drawSprite(cachedSprites.grid->getSprite(2), x*MAPTILESIZE-1 - offsetX, y*MAPTILESIZE-1 - offsetY);
                }else if(mapX == mapWindow.selection.x && mapY == mapWindow.selection.y){
                    drawer.drawSprite(cachedSprites.grid->getSprite(2), x*MAPTILESIZE-1 - offsetX, y*MAPTILESIZE-1 - offsetY);
                }else if(getTileDistance(mapPosition.x, mapPosition.y, mapX, mapY) < nearDistance){
                    drawer.drawSprite(cachedSprites.grid->getSprite(1), x*MAPTILESIZE-1 - offsetX, y*MAPTILESIZE-1 - offsetY);
                }
            }
        }
        SDL_Point closestEncounter = {-1, -1};
        for(int x = 0; x < starMap->width; x++){
            for(int y = 0; y < starMap->height; y++){
                SDL_Point p = {x, y};
                const StarMap::StarTile& tile = starMap->getTile(p);
                if(tile.encounter != NULL){
                    if(closestEncounter.x == -1){
                        closestEncounter = p;
                    }else if(getDistance(closestEncounter, mapPosition) > getDistance(p, mapPosition)){
                        closestEncounter = p;
                    }
                } 
            }
        }
        auto drawMapLine = [&](const SDL_Point& src, const SDL_Point& dest, const SDL_Color& color){
            int targetX = dest.x*MAPTILESIZE + MAPTILESIZE/2 - (mapWindow.mapOffsetX/MAPTILESIZE)*MAPTILESIZE - offsetX;
            int targetY = dest.y*MAPTILESIZE + MAPTILESIZE/2 - (mapWindow.mapOffsetY/MAPTILESIZE)*MAPTILESIZE - offsetY;
            int srcX = src.x*MAPTILESIZE + MAPTILESIZE/2 - (mapWindow.mapOffsetX/MAPTILESIZE)*MAPTILESIZE - offsetX;
            int srcY = src.y*MAPTILESIZE + MAPTILESIZE/2 - (mapWindow.mapOffsetY/MAPTILESIZE)*MAPTILESIZE - offsetY;
            drawer.drawLine(srcX, srcY, targetX, targetY, color);
        };
        if(closestEncounter.x != -1){
            drawMapLine(mapPosition, closestEncounter, gray);
        }

        auto points = mapWindow.reachable.begin();
        while(points != mapWindow.reachable.end()){
            SDL_Point& p = *points;
            drawMapLine(p, mapPosition, white);
            points++;
        }
        points = mapWindow.secondary.begin();
        while(points != mapWindow.secondary.end()){
            SDL_Point& p = *points;
            drawMapLine(mapWindow.selection, p, gray);
            points++;
        }
        int warpDistance = getWarpDistance();
        if(mapWindow.selection.x != -1 && mapWindow.selection.y != -1){
            drawMapLine(mapPosition, mapWindow.selection, mapWindow.distance > warpDistance ? red : green);
        }

        char str[100];
        if(mapWindow.selection.x != -1){
            sprintf(str, "[%.3d,%.3d] - [%.3d,%.3d] : %d %s", mapPosition.x, mapPosition.y, mapWindow.selection.x, mapWindow.selection.y, mapWindow.distance, cachedStrings.fuel);
        }else{
            sprintf(str, "[%.3d,%.3d]", mapPosition.x, mapPosition.y);
        }
        int width = drawer.getTextWidth(str, drawer.font);
        SDL_Color background = {32, 32, 32, 160};
        drawer.drawRect(drawer.fontHeight - 5, mapWindow.height - drawer.fontHeight*2 - 5, width + 10, drawer.fontHeight + 10, background);
        drawer.drawText(drawer.fontHeight, mapWindow.height - drawer.fontHeight*2, Left, white, drawer.font, str);

        if(enemyShip == NULL && mapWindow.reachable.size() == 0){
            int x = mapWindow.width - 70;
            int y = mapWindow.height - 70 - drawer.fontHeight;
            drawer.drawRectBorder(x, y, 64, 64, 2, black, gray);
            drawer.drawSpriteAlpha(cachedSprites.buttons->getSprite(4), x + 2, y + 2, 128);
            drawer.drawText(x+32, y + 66, Center, white, drawer.font, "HELP");
        }else if(!inWarp && mapWindow.selection != SDL_Point{-1, -1} && mapWindow.selection != mapPosition){
            int x = mapWindow.width - 70;
            int y = mapWindow.height - 70 - drawer.fontHeight;
            drawer.drawRectBorder(x, y, 64, 64, 2, black, gray);
            uint8_t alpha = 128;
            if(ship->engine && ship->engine->isAvailable() && ship->engine->cooldown == 0 && mapWindow.distance <= warpDistance && mapWindow.distance != 0 &&
               getFuel() >= mapWindow.distance){
                alpha = 255;
                drawer.drawText(x + 32, y + 66, Center, white, drawer.font, cachedStrings.warp);
            }else if(ship->engine == NULL || !ship->engine->isAvailable())
                drawer.drawText(x + 32, y + 66, Center, red, drawer.font, "ENGINE UNAVAILABLE");
            else if(ship->engine->cooldown != 0)
                drawer.drawText(x + 32, y + 66, Center, red, drawer.font, "COOLDOWN");
            else if(mapWindow.distance > ship->engine->warp)
                drawer.drawText(x + 32, y + 66, Center, red, drawer.font, "TOO FAR");
            else if(getFuel() < mapWindow.distance)
                drawer.drawText(x + 32, y + 66, Center, red, drawer.font, "NO FUEL");
            drawer.drawSpriteAlpha(cachedSprites.buttons->getSprite(4), x + 2, y + 2, alpha);
        }

        drawWindow(mapWindow);
    }

    if(powerWindow.visible){
        drawer.prepareWindowTexture();
        powerWindow.height = powerWindow.systems.size() * 46 + 15 + 12;
        powerWindow.width = 600;
        centerWindow(powerWindow);
        drawer.drawText(1, 1, Left, white, drawer.font, "POWER MANAGEMENT");
        drawer.drawTextf(powerWindow.width/2, 0, Center, white, drawer.font, "UNASSIGNED POWER AVAILABLE: %d", ship->freePower);

        // System info
        for(int i = 0; i < powerWindow.systems.size(); i++){
            System* system = powerWindow.systems[i];
            drawer.drawRect(0, 15 + i*46, powerWindow.width, 46, system_bg);
            drawer.drawRect(1, 15 + i*46 + 13, 32, 32, white);
            drawer.drawText(2, 15 + i*46, Left, powerWindow.selection == i? yellow : white, drawer.font, system->printName);

            drawer.drawTextf(36, 15 + i*46 + 12, Left, white, drawer.font, "POWER: %d/%d", system->power, system->maxPower);
            drawer.drawText(powerWindow.width - 46, 15 + i*46 + 16, Left, (system->power == system->maxPower || ship->freePower == 0) ? red : white, drawer.font, "+");
            drawer.drawText(powerWindow.width - 92, 15 + i*46 + 16, Left, system->power == 0 ? red : white, drawer.font, "-");
        }
        drawWindow(powerWindow);
    }

    if(crewWindow.visible){
        drawer.prepareWindowTexture();
        crewWindow.width = 320;
        crewWindow.height = 120;
        centerWindow(crewWindow);
        drawer.drawText(1, 1, Left, white, drawer.font, crewWindow.crew->name);
        drawer.drawText(1, 13, Left, white, drawer.font, "SKILLS");

        const Crew* crew = crewWindow.crew;
        drawer.drawTextf(1, 25, Left, skillColors[Crew::Combat], drawer.font, "COMBAT     LEVEL: %.2d  EXP: %.5d/%.5d", crew->skills[Crew::Combat].level,
                  crew->skills[Crew::Combat].exp, crew->skills[Crew::Combat].levelExp);

        drawer.drawTextf(1, 37, Left, skillColors[Crew::Command], drawer.font, "COMMAND    LEVEL: %.2d  EXP: %.5d/%.5d", crew->skills[Crew::Command].level,
                  crew->skills[Crew::Command].exp, crew->skills[Crew::Command].levelExp);

        drawer.drawTextf(1, 49, Left, skillColors[Crew::Operations], drawer.font, "OPERATIONS LEVEL: %.2d  EXP: %.5d/%.5d", crew->skills[Crew::Operations].level,
                  crew->skills[Crew::Operations].exp, crew->skills[Crew::Operations].levelExp);

        drawer.drawTextf(1, 61, Left, skillColors[Crew::Engineering], drawer.font, "ENGINEER   LEVEL: %.2d  EXP: %.5d/%.5d", crew->skills[Crew::Engineering].level,
                  crew->skills[Crew::Engineering].exp, crew->skills[Crew::Engineering].levelExp);

        drawer.drawTextf(1, 73, Left, skillColors[Crew::Science], drawer.font, "SCIENCE    LEVEL: %.2d  EXP: %.5d/%.5d", crew->skills[Crew::Science].level,
                  crew->skills[Crew::Science].exp, crew->skills[Crew::Science].levelExp);

        drawer.drawText(1, 85, Left, white, drawer.font, "PROFICIENCIES");
        int offset = 0;
        for(int i = 0; i < crew->proficiencies.size(); i++){
            Proficiency& proficiency = proficiencies[crew->proficiencies[i]];
            drawer.drawText(1 + offset, 97, Left, skillColors[proficiency.skill], drawer.font, proficiency.printName);
            offset += strlen(proficiencies[crew->proficiencies[i]].name)*8+ 3;
        }
        drawWindow(crewWindow);
    }

    if(dialogWindow.visible){
        drawer.prepareWindowTexture();
        dialogWindow.width = 800;
        dialogWindow.height = 400;
        centerWindow(dialogWindow);

        int pos = realFrameTime - dialogWindow.openTime;
        if(dialogWindow.animateText && pos > 0)
            pos = pos/15;
        else if(!dialogWindow.animateText)
            pos = dialogWindow.dialogLen+1;

        int wordCount = 0;
        int charCount = 0;
        int offset = 2;
        int yoffset = 0;
        while(dialogWindow.dialogWords[wordCount].word != NULL){
            DialogWindow::Word& word = dialogWindow.dialogWords[wordCount];
            if(word.width + offset >= (dialogWindow.width - 2)){
                offset = 2;
                yoffset += drawer.fontHeight;
            }

            int wordPos = pos - charCount;
            char copy = '\0';
            if(wordPos <= word.length){
                copy = word.word[wordPos];
                word.word[wordPos] = '\0';
            }

            SDL_Color textColor = white;
            if((word.flags & DialogWindow::Highlight) != 0)
                textColor = red;

            drawer.drawText(1 + offset, 13 + yoffset, Left, textColor, drawer.font, word.word);
            if(wordPos <= word.length){
                word.word[wordPos] = copy;
                break;
            }
            if(word.flags & DialogWindow::NewLine){
                offset = 2;
                yoffset += drawer.fontHeight*2;
            }else
                offset += word.width + (stringLibrary.spacesInDialog ? 8 : 0);
            charCount += word.length;
            wordCount++; 
        }

        if(dialogWindow.animateText == false || realFrameTime - dialogWindow.openTime >= dialogWindow.dialogLen*15){

            int gainedOffset = yoffset + 13 + drawer.fontHeight;
            if(gainedRM != 0){
                drawer.drawTextf(dialogWindow.width/2, gainedOffset, Center, gainedRM > 0 ? green : red, drawer.font, "%d RM", gainedRM);
                gainedOffset += drawer.fontHeight;
            }
            if(gainedFuel != 0){
                drawer.drawTextf(dialogWindow.width/2, gainedOffset, Center, gainedFuel > 0 ? green : red, drawer.font, "%d FUEL", gainedFuel);
                gainedOffset += drawer.fontHeight;
            }
            if(gainedArmor != 0){
                drawer.drawTextf(dialogWindow.width/2, gainedOffset, Center, gainedArmor > 0 ? green : red, drawer.font, "%d ARMOR", gainedArmor);
                gainedOffset += drawer.fontHeight;
            }
            for(int i = 0; i < gainedItems.size(); i++){
                Item& item = gainedItems[i];
                drawer.drawTextf(dialogWindow.width/2, gainedOffset, Center, item.amount > 0 ? green : red, drawer.font, "%s - %d", item.printName, std::abs(item.amount));
                gainedOffset += drawer.fontHeight;
            }

            for(int i = 0; i < dialogWindow.numOptions ; i++){
                int wordCount = 0;
                int offset = 2;    
                int yPos = dialogWindow.height - (dialogWindow.numOptions - i)*40;
#ifndef ANDROID
                char num[3];
                sprintf(num, "%d.", i+1);
                drawer.drawText(offset, yPos + (20-drawer.fontHeight/2), Left, white, drawer.font, num);
                offset += 8*3;
#endif
                while(dialogWindow.options[i].words[wordCount].word != NULL){
                    SDL_Color textColor = white;
#ifndef ANDROID
                    if(dialogWindow.optionSelection == i)
                        textColor = yellow;
#endif
                    if((dialogWindow.options[i].words[wordCount].flags & DialogWindow::Highlight) != 0)
                        textColor = red;
                    drawer.drawLine(0, yPos, dialogWindow.width, yPos, gray);
                    drawer.drawText(offset, yPos + (20-drawer.fontHeight/2), Left, textColor, drawer.font, dialogWindow.options[i].words[wordCount].word);
                    offset += (dialogWindow.options[i].words[wordCount].length + 1)*8;
                    wordCount++;
                }
            }
        }
        drawWindow(dialogWindow);
    }

    if(crewSelectionWindow.visible){
        drawer.prepareWindowTexture();
        crewSelectionWindow.width = 170*4+10;
        crewSelectionWindow.height = 400;
        centerWindow(crewSelectionWindow);
        drawer.drawTextf(crewSelectionWindow.width/2, 1, Center, white, drawer.font, "SELECT CREW %d/%d", crewSelectionWindow.numSelectedCrews, crewSelectionWindow.numSelect);

        for(int i = 0; i < playerCrews.size(); i++){
            const Crew* crew = playerCrews[i];
            bool selected = crewSelectionWindow.selectedCrews[i];

            int destX = 5 + (i%4)*170;
            int destY = drawer.fontHeight + (64+drawer.fontHeight)*(i/4);

            drawer.drawSprite(cachedSprites.portrait, destX, destY);
            drawer.drawText(destX + 71, destY, Left, selected ? red : white, drawer.font, crew->name);

            for(int p = 0; p < crew->proficiencies.size(); p++){
                const Proficiency& prof = proficiencies[crew->proficiencies[p]];
                drawer.drawText(destX + 71, destY + drawer.fontHeight + drawer.fontHeight*p, Left, skillColors[prof.skill], drawer.font, prof.printName);
            }
        }
    
        SDL_Color selectColor = gray;
        if(crewSelectionWindow.numSelectedCrews >= 1)
            selectColor = white;
        drawer.drawText(crewSelectionWindow.width/2, crewSelectionWindow.height - drawer.fontHeight*2, Center, selectColor, drawer.font, "SELECT");

        drawWindow(crewSelectionWindow);
    }
    
    drawer.drawTextf(drawer.uiWidth-64, 45 + drawer.fontHeight, Left, white, drawer.font, "FPS: %d", fps);
    drawer.drawTextf(drawer.uiWidth-72, 45 + drawer.fontHeight*2, Left,  white, drawer.font, "TURN: %d", turnCounter);

    drawMessages(NULL);
    drawEffects(NULL);

    auto messageIt = messages.begin();
    while(messageIt != messages.end()){
        Message& message = *messageIt;
        if(frameTime - message.timer > 1500){
            SDL_DestroyTexture(message.texture);
            messageIt = messages.erase(messageIt);
        }else{
            messageIt++;
        }
    }

    if(displayVictory){
        drawer.drawLine(0, drawer.uiHeight/3-1, drawer.uiWidth, drawer.uiHeight/3 -1, gray);
        drawer.drawRect(0, drawer.uiHeight/3, drawer.uiWidth, drawer.uiHeight/3, black);
        drawer.drawLine(0, (drawer.uiHeight/3)*2, drawer.uiWidth, (drawer.uiHeight/3)*2, gray);
        drawer.drawText(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8, Center, white, drawer.messageFont, cachedStrings.victory);
        int counter = 0;
        if(gainedRM != 0){
            drawer.drawTextf(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8 + 33, Center, white, drawer.font, "%s - %d", cachedStrings.recycled_material, gainedRM);
            counter++;
        }
        if(gainedFuel!= 0){
            drawer.drawTextf(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8 + 33 + 13*counter, Center, white, drawer.font, "%s - %d", cachedStrings.fuel, gainedFuel);
            counter++;
        }
        for(int i = 0; i < gainedItems.size(); i++){
            drawer.drawTextf(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8 + 33 + 13*counter, Center, white, drawer.font, "%s - %d", gainedItems[i].printName, gainedItems[i].amount);
            counter++;
        }
    }

    if(dialogWindow.visible == false && gameOver){
        drawer.drawLine(0, drawer.uiHeight/3-1, drawer.uiWidth, drawer.uiHeight/3 -1, gray);
        drawer.drawRect(0, drawer.uiHeight/3, drawer.uiWidth, drawer.uiHeight/3, black);
        drawer.drawLine(0, (drawer.uiHeight/3)*2, drawer.uiWidth, (drawer.uiHeight/3)*2, gray);
        drawer.drawText(drawer.uiWidth/2, drawer.uiHeight/3 + drawer.uiHeight/8, Center, white, drawer.messageFont, cachedStrings.game_over);

        drawer.drawTextf(drawer.uiWidth/2, drawer.uiHeight / 3 + drawer.uiHeight / 8 + 25, Center, white, drawer.font, "%s %d - %d", cachedStrings.record_encounters, currentRecord.encounterCounter, alltimeRecord.encounterCounter);
        drawer.drawTextf(drawer.uiWidth/2, drawer.uiHeight / 3 + drawer.uiHeight / 8 + 25 + drawer.fontHeight, Center, white, drawer.font, "%s %d - %d", cachedStrings.record_ships, currentRecord.shipDestroyedCounter, alltimeRecord.shipDestroyedCounter);
        drawer.drawTextf(drawer.uiWidth/2, drawer.uiHeight / 3 + drawer.uiHeight / 8 + 25 + drawer.fontHeight*2, Center, white, drawer.font, "%s %d - %d", cachedStrings.record_distance, currentRecord.distance, alltimeRecord.distance);
    }

    if(isPaused){
        drawer.drawText(drawer.uiWidth/2, drawer.uiHeight/2, Center, red, drawer.messageFont, cachedStrings.paused);
    }

    // No cursor on phone
#ifndef ANDROID
    if(selectedWeapon){
        cursor = 5;
    }else if(selectedTeleporter){
        cursor = 12;
    }

    if(cursor != -1){
        SDL_Rect src = {(cursor % 4)*32, (cursor / 4)*32, 32, 32};
        SDL_Rect dest = {getUIMouseX()-16, getUIMouseY()-16, 32, 32};
        SDL_RenderCopy(drawer.renderer, drawer.tilesTexture, &src, &dest);
    }
#endif

    // Draw background
    SDL_SetRenderTarget(drawer.renderer, NULL);
    for(int x = 0; x < drawer.displayWidth/512 + 1; x++){
        for(int y = 0; y < drawer.displayHeight/512 + 1; y++){
            SDL_Rect src = {0, 0, 512, 512};
            SDL_Rect dest = {x*512, y*512, 512, 512};
            SDL_RenderCopy(drawer.renderer, drawer.backgroundTexture, &src, &dest);
        }
    }
    drawAsteroids(false);
    drawClouds();

    // Draw game texture to screen
    if(enemyShip){
        SDL_Rect src = {0, 0, (int)(drawer.displayWidth*playerView.zoom*playerViewSize), (int)(drawer.displayHeight*playerView.zoom)};
        SDL_Rect dest = {0, 0, (int)(drawer.displayWidth*playerViewSize), drawer.displayHeight};
        SDL_RenderCopy(drawer.renderer, drawer.drawTexture, &src, &dest);

        if((inWarp && !playerWarp) || (destroyingShip && !playerDestroy)){
            float mult = ilerp(shipAnimationTimer, frameTime, shipAnimationTimer + 1000);
            SDL_SetTextureAlphaMod(drawer.enemyTexture, (int)(255.0f*mult));
        }else{
            SDL_SetTextureAlphaMod(drawer.enemyTexture, 255);
        }
        SDL_Rect enemySrc = {0, 0, (int)(drawer.displayWidth*enemyView.zoom), (int)(drawer.displayHeight*enemyView.zoom)};
        SDL_Rect enemyDest = {(int)(drawer.displayWidth*playerViewSize), 0, drawer.displayWidth, drawer.displayHeight};
        SDL_RenderCopy(drawer.renderer, drawer.enemyTexture, &enemySrc, &enemyDest);
    }else{
        SDL_Rect src = {0, 0, (int)(drawer.displayWidth*playerView.zoom), (int)(drawer.displayHeight*playerView.zoom)};
        SDL_Rect dest = {0, 0, drawer.displayWidth, drawer.displayHeight};
        if(inWarp && playerWarp){
            float mult = 1.0f;
            if(frameTime - shipAnimationTimer < 500)
                mult = ilerp(shipAnimationTimer, frameTime, shipAnimationTimer + 500);
            else
                mult = lerp(shipAnimationTimer+500, frameTime, shipAnimationTimer+1000);
            SDL_SetTextureAlphaMod(drawer.drawTexture, (int)(255.0f*mult));
        }
        SDL_RenderCopy(drawer.renderer, drawer.drawTexture, &src, &dest);
    }
    drawAsteroids(true);

    // Draw UI to screen
    drawer.presentUI();

    SDL_RenderPresent(drawer.renderer);
}

void Game::drawSelectionCrews(const std::vector<CrewDescription>& crews, int x, int y){
    for(int i = 0; i < crews.size(); i++){
        const CrewDescription& crew = crews[i];

        int destX = x + 6 + i*(CREW_SELECTION_WIDTH+5);
        int destY = y + drawer.fontHeight;

        SDL_Color selected_bg = {26, 64, 26, 255};
        drawer.drawRectBorder(destX, destY, CREW_SELECTION_WIDTH, CREW_SELECTION_HEIGHT, 1, crew.selected ? selected_bg : black, gray);
        drawer.drawSprite(cachedSprites.portrait, destX + 5, destY + 5);
        drawer.drawText(destX + 76, destY + 5, Left, white, drawer.font, crew.name);

        for(int p = 0; p < crew.proficiencies.size(); p++){
            const Proficiency& prof = proficiencies[crew.proficiencies[p]];
            drawer.drawText(destX + 76, destY + 5 + drawer.fontHeight + drawer.fontHeight*p, Left, skillColors[prof.skill], drawer.font, prof.printName);
        }
    }
}

void Game::drawHangar(){
    SDL_SetRenderTarget(drawer.renderer, drawer.drawTexture);
    SDL_SetRenderDrawColor(drawer.renderer, 0, 0, 0, 255);
    SDL_RenderClear(drawer.renderer);

    playerView.offset.x = 64;
    playerView.offset.y = (((float)drawer.displayHeight/2)*playerView.zoom - (ship->height*HALFTILESIZE));
    drawShip(ship, -1, -1, true);

    drawer.prepareUI();

    drawer.drawText(drawer.uiWidth/2, 5, Center, white, drawer.messageFont, "NEW GAME");
    drawer.drawTextf(drawer.uiWidth/2, 30, Center, white, drawer.font, "CREW AVAILABLE: %d", 5 - (int)playerCrews.size());

    int offsetX = ((ship->width*TILESIZE + 128)/playerView.zoom)*drawer.uiZoom;

    drawer.drawTextf(offsetX, 45, Left, skillColors[Crew::Command], drawer.font, "%s %d/2", cachedStrings.command, selectedCrews[0]);
    drawSelectionCrews(crews[0], offsetX, 45);

    drawer.drawTextf(offsetX, 75 + 64, Left, skillColors[Crew::Operations], drawer.font, "%s %d/2", cachedStrings.operations, selectedCrews[1]);
    drawSelectionCrews(crews[1], offsetX, 75 + 64);

    drawer.drawTextf(offsetX, 105 + 128, Left, skillColors[Crew::Engineering], drawer.font, "%s %d/2", cachedStrings.engineering, selectedCrews[2]);
    drawSelectionCrews(crews[2], offsetX, 105 + 128);

    drawer.drawTextf(offsetX, 135 + 196, Left, skillColors[Crew::Science], drawer.font, "%s %d/2", cachedStrings.science, selectedCrews[3]);
    drawSelectionCrews(crews[3], offsetX, 135 + 196);

    offsetX = drawer.uiWidth - 96;
    int offsetY = drawer.uiHeight - 96;
    drawer.drawRectBorder(offsetX, offsetY, 64, 64, 1, black, gray);
    drawer.drawSpriteAlpha(cachedSprites.buttons->getSprite(0), offsetX, offsetY, playerCrews.size() == 5 ? 255 : 128);
    drawer.drawText(offsetX + 32, offsetY + 66, Center, playerCrews.size() == 5 ? white : red, drawer.font, cachedStrings.start);

    offsetX -= 72;
    drawer.drawRectBorder(offsetX, offsetY, 64, 64, 1, black, gray);
    drawer.drawSprite(cachedSprites.buttons->getSprite(5), offsetX, offsetY);
    drawer.drawText(offsetX + 32, offsetY + 66, Center, white, drawer.font, cachedStrings.randomize);

    SDL_SetRenderTarget(drawer.renderer, NULL);
    SDL_Rect src = {0, 0, (int)(drawer.displayWidth*playerView.zoom), (int)(drawer.displayHeight*playerView.zoom)};
    SDL_Rect dest = {0, 0, drawer.displayWidth, drawer.displayHeight};
    SDL_RenderCopy(drawer.renderer, drawer.drawTexture, &src, &dest);
    // Draw UI to screen
    drawer.presentUI();

    SDL_RenderPresent(drawer.renderer);
}

void Game::addEffect(const SpriteList* list, const Ship* ship, int x, int y, int duration){
    Effect effect;
    effect.ship = ship;
    effect.point.x = x;
    effect.point.y = y;
    effect.list = list;
    effect.timer = frameTime;
    if(duration == 0)
        effect.duration = list->framerate * list->sprites.size();
    else
        effect.duration = duration;
    effects.push_back(effect);
}

void Game::scanEffect(Ship* ship, int x, int y, int size, SDL_Color color){
    addMessage(ship, x*TILESIZE + HALFTILESIZE, y*TILESIZE + HALFTILESIZE, stringLibrary.getString("scanning"));

    for(int i = -size*TILESIZE; i < size*2*TILESIZE; i+= 4){
        for(int j = 0; j < size*3*TILESIZE; j+= 4){
            glm::vec2 pos(x*TILESIZE + i, y*TILESIZE - size*TILESIZE + j);
            glm::vec2 vel(0, -0.01f);
            particleManager.addParticle(new Particle(ship, pos, vel, frameTime, 2500, color));
        }
    }
}

void Game::addMessage(const char* text){
    SDL_Log("Message: '%s'", text);
    Message message;
    message.ship = NULL;
    message.x = drawer.getTextCentered(text, drawer.messageFont, drawer.uiWidth);
    message.y = drawer.uiHeight/2;
    SDL_Surface* textSurface = TTF_RenderUTF8_Solid(drawer.messageFont, text, red);
    message.texture = SDL_CreateTextureFromSurface(drawer.renderer, textSurface);
    SDL_FreeSurface(textSurface);
    message.timer = frameTime;
    messages.push_back(message);
}

void Game::addMessage(const Ship* ship, int x, int y, const char* text){
    SDL_Log("Message: '%s'", text);
    Message message;
    message.ship = ship;
    message.x = x + drawer.getTextCentered(text, drawer.messageFont, 0);
    message.y = y;
    SDL_Surface* textSurface = TTF_RenderUTF8_Solid(drawer.messageFont, text, red);
    message.texture = SDL_CreateTextureFromSurface(drawer.renderer, textSurface);
    SDL_FreeSurface(textSurface);
    message.timer = frameTime;
    messages.push_back(message);
}

void Game::flash(int duration, SDL_Color color){
    flashColor = color;
    flashStart = frameTime;
    flashDuration = duration;
}

bool Game::isMouseInUIRect(int x, int y, int w, int h) const{
    SDL_Point mouse = {getUIMouseX(), getUIMouseY()};
    return isPointInRect(mouse, x, y, w, h);
}

void Game::showMapWindow(){
    showWindow(mapWindow);
    mapWindow.center(mapPosition);
    mapWindow.reachable.clear();
    mapWindow.secondary.clear();
    int warpDistance = getWarpDistance();
    int distance = getFuel();
    if(distance > warpDistance)
        distance = warpDistance;
    findReachableMapPoints(mapPosition, distance, !debug, mapWindow.reachable);
}

void Game::findReachableMapPoints(const SDL_Point& position, int distance, bool explored, std::vector<SDL_Point>& list){
    for(int x = 0; x < starMap->width; x++){
        for(int y = 0; y < starMap->height; y++){
            if(x == position.x && y == position.y)
                continue;
            StarMap::StarTile& tile = starMap->tiles[x][y];
            if((tile.encounter != NULL || tile.isStar()) && (!explored || !tile.isUnexplored())){
                int pointDistance = getDistance(position, SDL_Point{x, y});
                if(pointDistance <= distance){
                    list.push_back(SDL_Point{x, y});
                }
            }
        }
    }
}

void Game::showWindow(Window& window){
    window.visible = true;
    window.animate = true;
    window.animationWidth = 0;
    audioManager.playSound("select");
}

void Game::centerWindow(Window& window){
    window.offsetX = drawer.uiWidth/2 - window.width/2;
    window.offsetY = drawer.uiHeight/2 - window.height/2;
}

bool Game::isInWindow(const Window& window) const{
    return isMouseInUIRect(window.offsetX, window.offsetY, window.width, window.height);
}

SDL_Point Game::getWindowMousePos(const Window& window) const{
    SDL_Point pos = {getUIMouseX() - window.offsetX, getUIMouseY() - window.offsetY};
    return pos;
}

Window* Game::getOpenWindow(){
    if(powerWindow.visible)
        return &powerWindow;
    if(mapWindow.visible)
        return &mapWindow;
    if(inventoryWindow.visible)
        return &inventoryWindow;
    if(systemWindow.visible)
        return &systemWindow;
    if(stationWindow.visible)
        return &stationWindow;
    if(crewWindow.visible)
        return &crewWindow;
    if(dialogWindow.visible)
        return &dialogWindow;
    if(crewSelectionWindow.visible)
        return &crewSelectionWindow;
    return NULL;
}

void Game::updateMenuSelection(std::vector<MenuButton>& buttons, int& menuSelection){
    int direction = 0;
    if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_DOWN))
        direction = 1;
    else if(Controls::instance()->keyIsRegistered(SDL_SCANCODE_UP))
        direction = -1;

    if(direction != 0){
        audioManager.playSound("select");
        menuSelection += direction;
    }

    // Run unconditionally incase default selection is invalid
    while(true){
        if(menuSelection < 0){
            menuSelection = buttons.size()-1;
            continue;
        }else if(menuSelection > buttons.size()){
            menuSelection = 0;
            continue;
        }else if(buttons[menuSelection].visible == false){
            if(direction != 0)
                menuSelection += direction;
            else
                menuSelection++;
            continue;
        }
        break;
    }
}

bool Game::isInButton(const MenuButton& button) const{
    int width = drawer.getTextWidth(button.len, drawer.messageFont);
    int mouseX = getUIMouseX();
    int mouseY = getUIMouseY();

    return (mouseX >= button.point.x - width/2 && mouseX < button.point.x + width/2 &&
            mouseY >= button.point.y && mouseY < button.point.y + 30);
}

void Game::drawWindow(const Window& window){
    // Switch target to UI
    SDL_SetRenderTarget(drawer.renderer, drawer.uiTexture);

    int width = window.width;
    int height = window.height;

    if(window.animate){
        width = window.animationWidth;
    }
    int offset = window.width/2 - width/2;

    // Draw window borders
    int borderSize = cachedSprites.window->getSprite(0)->rect.w;
    drawer.drawSprite(cachedSprites.window->getSprite(0), window.offsetX-borderSize+offset, window.offsetY-borderSize);
    drawer.drawSprite(cachedSprites.window->getSprite(1), window.offsetX + width+offset, window.offsetY-borderSize);
    drawer.drawSprite(cachedSprites.window->getSprite(2), window.offsetX + width+offset, window.offsetY + window.height);
    drawer.drawSprite(cachedSprites.window->getSprite(3), window.offsetX-borderSize+offset, window.offsetY + window.height );

    SDL_Rect side = {window.offsetX - borderSize+offset, window.offsetY, borderSize, window.height};
    drawer.drawSprite(cachedSprites.window->getSprite(4), &side);
    side.x = window.offsetX + offset + width;
    drawer.drawSprite(cachedSprites.window->getSprite(6), &side);

    side.x = window.offsetX + offset;
    side.y = window.offsetY-borderSize;
    side.w = width;
    side.h = borderSize;
    drawer.drawSprite(cachedSprites.window->getSprite(5), &side);
    side.y = window.offsetY + window.height;
    drawer.drawSprite(cachedSprites.window->getSprite(7), &side);

    // Window background
    drawer.drawRect(window.offsetX+offset, window.offsetY, width, window.height, black);

    // Draw window texture to UI texture
    SDL_Rect src = {offset, 0, width, height};
    SDL_Rect dest = {window.offsetX + offset, window.offsetY, width, height};
    SDL_RenderCopy(drawer.renderer, drawer.windowTexture, &src, &dest);
}

void MoveMap::clear(){
    hasRepair = false;
    hasHeal = false;
    for(int y = 0; y < MAXMAPHEIGHT; y++){
        for(int x = 0; x < MAXMAPWIDTH; x++){
            map[y][x].distance = 0;
            map[y][x].action = MoveMap::None;
        }
    }
}

void MapWindow::center(const SDL_Point& point){
    mapOffsetY = std::max(0, point.y*MAPTILESIZE - height/2);
    mapOffsetX = std::max(0, point.x*MAPTILESIZE - width/2);
}
