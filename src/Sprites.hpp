#ifndef _SPRITES_HPP
#define _SPRITES_HPP

#include "SDLIncludes.hpp"
#include <vector>
#include <string>
#include <map>

#include "FileManager.hpp"

class Sprite{
public:
    char* name;
    SDL_Rect rect;
    SDL_Texture* texture;

    Sprite(const char* n);
};

class SpriteList{
public:
    int framerate;
    std::vector<const Sprite*> sprites;

    const Sprite* getFrame(int64_t startTime, int64_t time) const;
    const Sprite* getSprite(const char* name) const;
    const Sprite* getSprite(int i) const;
};

class SpriteManager{
private:
    std::vector<const Sprite*> sprites;
    std::map<std::string, SDL_Texture*> textures;
    std::map<std::string, const SpriteList*> lists;
public:
    void load(const char* fileName, SDL_Renderer* renderer);
    const Sprite* loadSprite(rapidxml::xml_node<>* node, SDL_Renderer* renderer);

    void addTexture(SDL_Texture* texture, const char* name);
    SDL_Texture* getTexture(const char* name);
    void addSprite(const Sprite* sprite);
    const Sprite* getSprite(const char* name);

    void addList(const SpriteList* list, const char* name);
    const SpriteList* getList(const char* name);
};

#endif
