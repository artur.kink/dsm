#ifndef _SHIP_HPP
#define _SHIP_HPP

#include "SDLIncludes.hpp"
#include "FileManager.hpp"

#include <cstring>
#include <cstdint>
#include <vector>

#include "System.hpp"
#include "Crew.hpp"

#define MAXMAPWIDTH 100
#define MAXMAPHEIGHT 100

class Door{
public:
    int health;
    int maxHealth;
    bool isOpen;
    bool forceOpen;
    bool turnDone;

    Door(){
        health = 50;
        maxHealth = 50;
        isOpen = false;
        forceOpen = false;
    }

    void open(){
        isOpen = true;
        turnDone = true;
    }

    void close(){
        isOpen = false;
        turnDone = true;
    }
};

class Tile{
public:
    enum TileType{
        Empty = 0x00, ///< Not a valid tile
        Blocked = 0x01,
        Walkable = 0x02,
        //Available = 0x04,
        External = 0x08, ///< A non walkable tile but can be used for systems
        System = 0x10,
        Station_Mask = 0xE0,
        Station_Captain = 0x20,
        Station_Ops = 0x40,
        Station_Weapons = 0x60,
    };

    enum VisibilityFlags{
        None = 0x00,
        Explored = 0x01, ///< Explored but no visibility
        Life = 0x02, ///< Show living crew
        Full = 0x03,
        Enemy_Explored = 0x04,
        Enemy_Life = 0x08,
        Enemy_Full = 0x0C
    };

    static const uint8_t NoTile = 0xFF;
    uint8_t tile;
    uint8_t type;
    uint8_t room;
    uint8_t visibility;
    Door* door;

    static const int fireCooldown = 4;
    static const int fireLength = 10;
    uint8_t fire;

    Tile(){
        tile = NoTile;
        type = 0;
        room = 0;
        visibility = 0;
        door = NULL;
        fire = 0;
    }

    bool isRoom() const{
        return room != 0;
    }

    bool isUnexplored(bool player = true) const{
        if(player)
            return (visibility & Full) == 0;
        else
            return (visibility & Enemy_Full) == 0;
    }

    bool isExplored(bool player = true) const{
        if(player)
            return visibility & Explored;
        else
            return visibility & Enemy_Explored;
    }

    bool isVisible(bool player = true) const{
        if(player)
            return (visibility & Full) == Full;
        else
            return (visibility & Enemy_Full) == Enemy_Full;
    }

    bool isLifeVisible(bool player = true) const{
        if(player)
            return visibility & Life;
        else
            return visibility & Enemy_Life;
    }

    bool isEmpty() const{
        return type == 0;
    }

    bool isBlocked() const{
        return type == 0 || type & Blocked || type & External;
    }

    bool isExternal() const{
        return type & External;
    }

    Door* getDoor(){
        return door;
    }

    const Door* getDoor() const{
        return door;
    }

    bool isSystem() const{
        return type & System;
    }

    bool isStation() const{
        return type & Station_Mask;
    }

    bool isWeaponsStation() const{
        return (type & Station_Mask) == Station_Weapons;
    }

    bool isOpsStation() const{
        return (type & Station_Mask) == Station_Ops;
    }

    bool isCaptainStation() const{
        return (type & Station_Mask) == Station_Captain;
    }

    bool canCreateFire() const{
        return fire == 0;
    }

    bool isFire() const{
        return fire > fireCooldown;
    }
    bool isNewFire() const{
        return fire == fireLength + fireCooldown;
    }

    bool isFireCooldown() const{
        return fire > 0 && fire <= fireCooldown;
    }

    void setFire(){
        fire = fireLength + fireCooldown;
    }

};

class Room{
public:
    int width;
    int height;
    int x;
    int y;
    System* system;
    bool isExternal;
    Room(){
        width = height = -1;
        x = y = -1;
        system = NULL;
        isExternal = false;
    }
};

class ShipView;

class Ship{
public:

    char name[20];

    Tile** tiles;
    int8_t width;
    int8_t height;

    int8_t numRooms;
    Room* rooms;

    int armor;
    int armorMax;

    uint8_t power;
    uint8_t freePower;

    SDL_Point captainPosition;
    SDL_Point opsPosition;
    bool playerShip;

    Scanner* scanner;
    Engine* engine;
    Shield* shield;
    System* teleporter;
    Sickbay* sickbay;
    std::vector<System*> allSystems;
    std::vector<Weapon*> weapons;
    std::vector<System*> systems;

    std::vector<Crew*> crews;
    std::vector<Door*> doors;
    int fireArmorDamageCooldown;

    enum ShipMode{
        None,
        Evasive,
        Defensive,
        Offensive
    };
    ShipMode mode;
    int modeCooldown;

    ShipView* view;

    Ship(int8_t w, int8_t h);
    ~Ship();

    int getEvasion() const;
    int getShieldRegen() const;
    int getShield() const;
    int getShieldMax() const;

    void setMode(ShipMode m);
    void endTurn();

    void addSystem(System* system, uint8_t room);
    void replaceSystem(System* a, System* b);
    void removeSystem(System* system);

    void addCrew(Crew* Crew);
    void removeCrew(Crew* Crew);

    void addDoor(Door* door, int x, int y);
    void directPower(System* system, int8_t power);
    void setNumRooms(uint8_t num);

    System* getSystem(const char* name);
    Tile& getTile(int x, int y);
    Tile& getTile(const SDL_Point& point);
    const Tile& getTile(int x, int y) const;
    const Tile& getTile(const SDL_Point& point) const;
    System* getTileSystem(int x, int y);
    const System* getTileSystem(int x, int y) const;
    Crew* getTileCrew(int x, int y);

    Door* getDoor(int x, int y);
    const Door* getDoor(int x, int y) const;
};

#endif
