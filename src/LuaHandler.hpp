#ifndef _LUAHANDLER_HPP
#define _LUAHANDLER_HPP

#include "lua.hpp"

class Ship;

class LuaHandler{
private:

public:
    lua_State* luaState;
    LuaHandler();

    void init();
    bool loadScript(const char* name);
    bool getLuaFunction(const char* name);
    int executeCommand(const char* command);

    void setPlayerShip(Ship* ship);
    void setEnemyShip(Ship* ship);

    //Lua bindings
    static int l_removeEnemyShip(lua_State* state);
    static int l_spawnEnemyShip(lua_State* state);
    static int l_warpEnemyShip(lua_State* state);
    static int l_showDialog(lua_State* state);
    static int l_addOption(lua_State* state);
    static int l_addCrewSelectOption(lua_State* state);
    
    static int l_onHail(lua_State* state); 
    static int l_onDestroy(lua_State* state); 
    static int l_onFlee(lua_State* state); 
    static int l_onAttack(lua_State* state); 
    static int l_onScan(lua_State* state); 
    static int l_addTurnCallback(lua_State* state);
    static int l_removeTurnCallback(lua_State* state);
    static int l_createEncounter(lua_State* state);
    static int l_cancelWarp(lua_State* state);
    static int l_beginEncounter(lua_State* state);
    static int l_removeEncounters(lua_State* state);

    // Items
    static int l_addItem(lua_State* state);
    static int l_useItem(lua_State* state);
    static int l_useRM(lua_State* state);
    static int l_useFuel(lua_State* state);
    static int l_addRM(lua_State* state);
    static int l_addFuel(lua_State* state);
    static int l_giveReward(lua_State* state);
    static int l_getItemAmount(lua_State* state);
    static int l_getRM(lua_State* state);
    static int l_getFuel(lua_State* state);

    // Crew
    static int l_setEnemyHostile(lua_State* state);
    static int l_getCrewName(lua_State* state);
    static int l_findProficiencyCrew(lua_State* state);
    static int l_getPlayerCrew(lua_State* state);
    static int l_addCrewVariable(lua_State* state);
    static int l_getCrewVariable(lua_State* state);
    static int l_addSystemVariable(lua_State* state);
    static int l_addIntVariable(lua_State* state);
    static int l_getIntVariable(lua_State* state);
    static int l_addStringVariable(lua_State* state);
    static int l_getStringVariable(lua_State* state);
    static int l_removeVariable(lua_State* state);
    static int l_killCrew(lua_State* state);
    static int l_getCrewHealth(lua_State* state);
    static int l_damageCrew(lua_State* state);
    static int l_healCrew(lua_State* state);
    static int l_hasProficiency(lua_State* state);
    static int l_getCrewProficiencyLevel(lua_State* state);
    static int l_increaseCrewProficiencyLevel(lua_State* state);
    static int l_createPlayerCrew(lua_State* state);
    static int l_spawnEnemyCrew(lua_State* state);

    // Ship
    static int l_damageShip(lua_State* state);
    static int l_damageShipArmor(lua_State* state);
    static int l_damageShipShield(lua_State* state);
    static int l_repairShip(lua_State* state);
    static int l_regenShipShield(lua_State* state);
    static int l_getShipArmor(lua_State* state);
    static int l_getShipShield(lua_State* state);
    static int l_spawnFire(lua_State* state);
    static int l_removeFire(lua_State* state);
    static int l_spawnAsteroid(lua_State* state);

    // Systems
    static int l_damageSystem(lua_State* state);
    static int l_repairSystem(lua_State* state);
    static int l_getShieldSystem(lua_State* state);
    static int l_getScannerSystem(lua_State* state);
    static int l_getEngineSystem(lua_State* state);
    static int l_getWeaponSystems(lua_State* state);

    // Sector (Star system)
    static int l_setSafe(lua_State* state);
    static int l_setSpawnEnemyShips(lua_State* state);
    static int l_isAsteroids(lua_State* state);
    static int l_setAsteroids(lua_State* state);
    static int l_isClouds(lua_State* state);
    static int l_setClouds(lua_State* state);
    static int l_isStar(lua_State* state);
    static int l_setStar(lua_State* state);
    static int l_exploreStarMap(lua_State* state);

    // Other
    static int l_playSound(lua_State* state);
    static int l_flash(lua_State* state);
    static int l_random(lua_State* state);
    static int l_getTurnNumber(lua_State* state);

    // Lua Debug functions
    static int l_changeLanguage(lua_State* state);
    static int l_increaseDifficulty(lua_State* state);
    static int l_reset(lua_State* state);
    static int l_endTurn(lua_State* state);
    static int l_setTimeout(lua_State* state);
    static int l_stop(lua_State* state);
    static int l_saveGame(lua_State* state);
    static int l_loadGame(lua_State* state);
    static int l_newGame(lua_State* state);
};

#endif
