#ifndef _AUDIOMANAGER_HPP
#define _AUDIOMANAGER_HPP

#include "SDLIncludes.hpp"

#include <map>
#include <string>

#include "FileManager.hpp"

class AudioManager{
private:
    std::map<std::string, Mix_Music*> music;
    std::map<std::string, Mix_Chunk*> sounds;

    bool autoPlayMusic;
    int musicCounter = 0;
    bool initialized;
    bool soundEnabled;
public:

    static const int CHANNEL_ANY = -1;
    static const int CHANNEL_WEAPON = 0;
    static const int CHANNEL_DAMAGE = 1;
    static const int RESERVE_CHANNELS = 2;

    AudioManager();
    ~AudioManager();

    void init();
    void load(const char* fileName);
    void cleanup();

    void addMusic(const char* name, Mix_Music* m);
    void addSound(const char* name, Mix_Chunk* sound);

    void update();
    void playAllMusic();
    void stopMusic();
    bool isMusicPlaying();
    void playMusic(const char* name);

    bool isSoundEnabled();
    void enableSound(bool enable);
    void playSound(const char* name, int channel = CHANNEL_ANY, bool checkAvailable = false);

};

#endif
