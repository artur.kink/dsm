#ifndef _STRINGLIBRARY_HPP
#define _STRINGLIBRARY_HPP

#include "SDLIncludes.hpp"
#include <vector>
#include <string>

#include "rapidxml.hpp"

#include "FileManager.hpp"

class String{
public:
    std::string name;
    char* str;
};

class StringLibrary{
public:

    static StringLibrary* instance;

    TTF_Font* font;
    TTF_Font* messageFont;

    char* language;
    bool spacesInDialog;
    enum StringType{
        str_Source = 0,
        str_Item = 1,
        str_System = 2,
        str_Proficiency = 3,
        str_Encounter = 4,
        str_NumTypes = 5
    };
    std::vector<String*> strings[(int)StringType::str_NumTypes];

    StringLibrary();
    const char* getString(const char* name, StringType type = StringType::str_Source);
    void load(const char* fileName, bool noDraw);
    void setLanguage(const char* lang);
};

#endif
