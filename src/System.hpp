#ifndef _SYSTEM_HPP
#define _SYSTEM_HPP

#include "SDLIncludes.hpp"
#include "FileManager.hpp"
#include <algorithm>


class Sprite;

class SystemLayout{
public:
    char* name;
    uint8_t width;
    uint8_t height;

    uint8_t** type;

    const Sprite* sprite;

    SystemLayout(uint8_t w, uint8_t h){
        width = w;
        height = h;
        name = NULL;
        sprite = NULL;

        type = new uint8_t*[height];
        for(int y = 0; y < height; y++){
            type[y] = new uint8_t[width];
            for(int x = 0; x < width; x++){
                type[y][x] = 1; // Blocked, factor this into a new file
            }
        }
    }

};

class Item;
class SystemDescription;

class System{
public:
    char name[20];
    const char* printName;

    enum SystemType{
        Default,
        Scanner,
        Shield,
        Engine,
        Weapon,
        Sickbay,
        Teleporter
    };
    SystemType type;

    int posX;
    int posY;

    uint8_t power;
    uint8_t maxPower;
    uint8_t directedPower; // How much power was directed to system

    uint8_t health;
    uint8_t maxHealth;

    const SystemDescription* upgrade;
    int upgradeRM;
    const std::vector<Item>* upgradeItems;

    const SystemLayout* layout;

    const float breakPercent = 0.25f;
    bool isBroken() const{
        return health <= maxHealth*breakPercent;
    }

    /**
     * Is system available.
     */
    virtual bool isAvailable() const{
        return !isBroken() && power != 0;
    }

    /**
     * Is sytem available to be used.
     */
    virtual bool isUsable() const{
        return isAvailable();
    }

    virtual void saveData(FileManager* file){
        file->saveInt8(power);
        file->saveInt8(health);
    }

    virtual void loadData(FileManager* file){
        power = file->loadInt8();
        directedPower = power;
        health = file->loadInt8();
    }
    
    System(const char* n, const char* pn, uint8_t p, uint8_t h, const SystemLayout* l) :
    power(0), maxPower(p),
    health(h), maxHealth(h), layout(l){
        strncpy(name, n, 20);
        printName = pn;
        type = Default;
        upgrade = NULL;
        upgradeRM = 0;
        upgradeItems = NULL;
    }
};

class Weapon : public System{
public:
    enum WeaponType{
        Laser = 0,
        Projectile = 1
    };
    WeaponType weaponType;
    int damage;
    int fireChance;
    int shots;
    int damageSize;
    int turns;
    int cooldown;
    SDL_Point fireOffset;

    Weapon(const char* n, const char* pn, WeaponType w, int d, int fc, int s, int ds, int t, int p, int h, SDL_Point o,
           const SystemLayout* l) :
    System(n, pn, p, h, l), weaponType(w), damage(d), fireChance(fc), shots(s), damageSize(ds), turns(t), cooldown(0), fireOffset(o){
        type = SystemType::Weapon;
    }

    void beginCooldown(){
        cooldown = turns + 1;
    }

    virtual bool isUsable() const{
        return isAvailable() && cooldown == 0;
    }

    int getDamage() const{
        return std::max(0, damage + (power - maxPower));
    }
    
    int getDamagePerTurn() const{
        return (getDamage()*shots)/(turns+1);
    }

    virtual void saveData(FileManager* file){
        System::saveData(file);
        file->saveInt8(cooldown);
    }

    virtual void loadData(FileManager* file){
        System::loadData(file);
        cooldown = file->loadInt8();
    }
};

class Scanner : public System{
public:
    uint8_t scanX;
    uint8_t scanY;
    bool isScanning;
    uint8_t turns;
    uint8_t cooldown;
    int nearVisibility;
    int farVisibility;
    int size;
    Scanner(const char* n, const char* pn, uint8_t p, uint8_t h, const SystemLayout* l) :
    System(n, pn, p, h, l){
        scanX = 0;
        scanY = 0;
        isScanning = false;
        cooldown = 0;
        size = 3;
        turns = 2;
        type = SystemType::Scanner;
        nearVisibility = 1;
        farVisibility = 1;
    }

    int getSize() const{
        return size + (power - maxPower);
    }

    virtual bool isUsable() const{
        return isAvailable() && cooldown == 0;
    }

    virtual void saveData(FileManager* file){
        System::saveData(file);
        file->saveInt8(scanX);
        file->saveInt8(scanY);
        file->saveInt8(cooldown);
        file->saveBool(isScanning);
    }

    virtual void loadData(FileManager* file){
        System::loadData(file);
        scanX = file->loadInt8();
        scanY = file->loadInt8();
        cooldown = file->loadInt8();
        isScanning = file->loadBool();
    }
};

class Engine : public System{
public:
    int evasion;
    int cooldown;
    int warp;

    Engine(const char* n, const char* pn, int e, int w, uint8_t p, uint8_t h, const SystemLayout* l) :
    System(n, pn, p, h, l){
        warp = w;
        evasion = e;
        cooldown = 0;
        type = SystemType::Engine;
    }
};

class Shield : public System{
public:
    int shield;
    int shieldMax;
    int shieldRegen;
    int shieldRegenTurns;
    int shieldRegenCooldown;

    Shield(int max, int regen, int cooldown, const char* n, const char* pn, uint8_t p, uint8_t h, const SystemLayout* l) :
    System(n, pn, p, h, l){
        shield = shieldMax = max;
        shieldRegen = regen;
        shieldRegenTurns = cooldown;
        shieldRegenCooldown = cooldown;
        type = SystemType::Shield;
    }

    virtual void saveData(FileManager* file){
        System::saveData(file);
        file->saveInt32(shield);
        file->saveInt32(shieldRegenCooldown);
    }

    virtual void loadData(FileManager* file){
        System::loadData(file);
        shield = file->loadInt32();
        shieldRegenCooldown = file->loadInt32();
    }
};

class Sickbay : public System{
public:
    int8_t heal;
    Sickbay(const char* n, const char* pn, int8_t ha, uint8_t p, uint8_t h, const SystemLayout* l) :
    System(n, pn, p, h, l){
        heal = ha;
        type = SystemType::Sickbay;
    }
};


#endif
