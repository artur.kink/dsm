#include "EncounterManager.hpp"

void EncounterManager::load(const char* fileName){
    // Load encounter definitions
    char* text = FileManager::loadFileText(fileName);
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);
    rapidxml::xml_node<>* encountersNode = doc.first_node("encounters");
    for(rapidxml::xml_node<>* encounterNode = encountersNode->first_node("encounter"); encounterNode; encounterNode = encounterNode->next_sibling("encounter")){
        addEncounter(loadEncounter(encounterNode));
    }

    for(rapidxml::xml_node<>* listNode = encountersNode->first_node("list"); listNode; listNode = listNode->next_sibling("list")){
        std::vector<Encounter*>* encounterList = new std::vector<Encounter*>();
        addEncounterList(listNode->first_attribute("name")->value(), encounterList);

        for(rapidxml::xml_node<>* encounterNode = listNode->first_node("encounter"); encounterNode; encounterNode = encounterNode->next_sibling()){
            encounterList->push_back(loadEncounter(encounterNode));
        }
    }

    delete text;
}

Encounter* EncounterManager::loadEncounter(rapidxml::xml_node<>* encounterNode){
    rapidxml::xml_attribute<>* load = encounterNode->first_attribute("load");
    if(load){
        return getEncounter(load->value());
    }

    Encounter* encounter = new Encounter();
    
    rapidxml::xml_attribute<>* name = encounterNode->first_attribute("name");
    if(name){
        encounter->name = new char[strlen(name->value()) + 1];
        strcpy(encounter->name, name->value());
    }else{
        encounter->name = NULL;
    }

    return encounter;
}


void EncounterManager::addEncounter(Encounter* encounter){
    encounters.push_back(encounter);
}

Encounter* EncounterManager::getEncounter(const char* name){
    for(int i = 0; i < encounters.size(); i++){
        if(strcmp(encounters[i]->name, name) == 0)
            return encounters[i];
    }
    return NULL;
}

void EncounterManager::addEncounterList(const char* name, std::vector<Encounter*>* list){
    encounterLists.insert(std::pair<std::string, std::vector<Encounter*>*>(name, list));
}

std::vector<Encounter*>* EncounterManager::getEncounterList(const char* name){
    auto it = encounterLists.find(name);
    if(it != encounterLists.end())
        return it->second;
    return NULL;
}
