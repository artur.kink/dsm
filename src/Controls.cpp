#include "Controls.hpp"

Controls* Controls::controls = NULL;

Controls::Controls(){
    for(int i = 0; i < MOUSE_NUMBUTTONS; i++){
        buttons[i].state = BUTTON_UP;
        buttons[i].pressedTime = 0;
        buttons[i].doubleClickOnRelease = false;
    }

    numFingers = 0;
    isMultiTouch = false;

    SDL_GetKeyboardState(&keySize);
    keys = new uint8_t[keySize];
    keyTime = new int64_t[keySize];
}

Controls* Controls::instance(){
    if(controls == NULL)
        controls = new Controls();
    return controls;
}

void Controls::update(int64_t time){

    isMultiTouch = numFingers > 1 || (isMultiTouch && numFingers != 0);
    pinchDist = 0;

    SDL_PumpEvents();

    const uint8_t* newKeys = SDL_GetKeyboardState(&keySize);
    for(int i = 0; i < keySize; i++){
        if(newKeys[i] && keys[i] == KEY_UNPRESSED){
            keys[i] = KEY_PRESSED;
            keyTime[i] = time;
        }else if(!newKeys[i]){
            keys[i] = KEY_UNPRESSED;
        }else if(keys[i] && time - keyTime[i] > 250){
            keys[i] = KEY_REGISTERED;
            keyTime[i] = time;
        }else{
            keys[i] = KEY_DOWN;
        }
    }

    int x, y;
    uint32_t mouseState = SDL_GetMouseState(&x, &y);
    previousMouseX = mouseX;
    previousMouseY = mouseY;
    mouseX = x;
    mouseY = y;

    updateMouseButtonState(MOUSE_LEFT, mouseState & SDL_BUTTON(SDL_BUTTON_LEFT));
    updateMouseButtonState(MOUSE_RIGHT, mouseState & SDL_BUTTON(SDL_BUTTON_RIGHT));
}

void Controls::updateMouseButtonState(MOUSE_BUTTON button, bool isPressed){
    if(isPressed){
        if(!isMouseButtonDown(button)){
            if(SDL_GetTicks() - buttons[button].pressedTime < doublePressTime){
                buttons[button].doubleClickOnRelease = true;
            }
            buttons[button].state = BUTTON_PRESSED;
            buttons[button].pressedTime = SDL_GetTicks();
        }else{
            buttons[button].state = BUTTON_DOWN;
        }
    }else{
        if(ignoreMousePress){
            buttons[button].state = BUTTON_UP;
            ignoreMousePress = false;
            buttons[button].doubleClickOnRelease = false;
        }else if(isMouseButtonDown(button)){
            if(buttons[button].doubleClickOnRelease){
                buttons[button].state = BUTTON_DOUBLEPRESSED;
                buttons[button].doubleClickOnRelease = false;
            }else
                buttons[button].state = BUTTON_RELEASED;
        }else{
            buttons[button].state = BUTTON_UP;
        }
    }
}

int Controls::getMouseX() const{
    return mouseX;
}

int Controls::getMouseY() const{
    return mouseY;
}

int Controls::getMouseXDiff() const{
    return (mouseX - previousMouseX);
}

int Controls::getMouseYDiff() const{
    return (mouseY - previousMouseY);
}

void Controls::addFinger(){
    numFingers++;
}

void Controls::removeFinger(){
    numFingers--;
    if(numFingers < 0){
        SDL_Log("Potential ERROR, Fingers < 0?");
        numFingers = 0;
    }
}

void Controls::setPinch(float d){
    if(numFingers < 2){
        SDL_Log("Potential ERROR, Pinch with less than 2 fingers");
    }
    pinchDist = d;
    ignoreMousePress = true;
}

bool Controls::getIsMultiTouch() const{
    return isMultiTouch;
}

int Controls::getNumFingers() const{
    return numFingers;
}

float Controls::getPinchDist() const{
    return pinchDist;
}

void Controls::setIgnoreMousePress(){
    ignoreMousePress = true;
}

uint64_t Controls::getMouseButtonPressLength(MOUSE_BUTTON button) const{
    return SDL_GetTicks() - buttons[button].pressedTime;
}

uint8_t Controls::getMouseButtonState(MOUSE_BUTTON button) const{
    return buttons[button].state;
}

bool Controls::isMouseButtonDown(MOUSE_BUTTON button) const{
    return buttons[button].state & BUTTON_DOWN;
}

bool Controls::wasMouseButtonPressed(MOUSE_BUTTON button) const{
    return buttons[button].state == BUTTON_PRESSED;
}

bool Controls::wasMouseButtonReleased(MOUSE_BUTTON button) const{
    return buttons[button].state & BUTTON_RELEASED;
}

/**
 * Mouse button has been released a second time quickly.
 * Also registers as a release.
 */
bool Controls::wasMouseButtonDoubleClicked(MOUSE_BUTTON button) const{
    return buttons[button].state == BUTTON_DOUBLEPRESSED;
}

bool Controls::keyWasPressed(SDL_Scancode key) const{
    return keys[key] == KEY_PRESSED;
}

bool Controls::keyIsDown(SDL_Scancode key) const{
    return keys[key];
}

bool Controls::keyIsRegistered(SDL_Scancode key) const{
    return keys[key] == KEY_REGISTERED || keys[key] == KEY_PRESSED;
}
