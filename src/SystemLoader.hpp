#ifndef _SYSTEMLOADER_HPP
#define _SYSTEMLOADER_HPP

#include <vector>
#include "SDLIncludes.hpp"

#include "FileManager.hpp"
#include "System.hpp"
#include "StringLibrary.hpp"
#include "Item.hpp"
#include "ItemLibrary.hpp"
#include "Sprites.hpp"

#include "rapidxml.hpp"

struct SystemDescription{
    System::SystemType type;
    char* name;
    char* layoutName;
    const SystemLayout* layout;
    char* upgradeName;
    const SystemDescription* upgrade;
    int health;
    int power;
    int scansize;
    int near_visibility;
    int far_visibility;
    int cooldown;
    int damage;
    int fireChance;
    int shots;
    int damageSize;
    Weapon::WeaponType weaponType;
    int shield;
    int regen;
    int evasion;
    SDL_Point fireOffset;
    int heal;
    int warp;
    int difficulty;

    int upgradeRM;
    std::vector<Item> upgradeItems;

    int getDamagePerTurn() const{
        return (shots*damage)/(cooldown+1);
    }

    int getShieldPerTurn() const{
        return regen/(cooldown+1);
    }
};

class SystemLoader{
public:
    std::vector<SystemDescription*> systems;
    std::vector<SystemDescription*> weapons;
    std::vector<SystemDescription*> shields;
    std::vector<SystemDescription*> engines;
    std::vector<const SystemLayout*> layouts;

    void load(const char* fileName, ItemLibrary& itemLibrary, SpriteManager& spriteManager);
    const SystemLayout* getLayout(const char* name);
    System* getSystem(const char* name);
    System* getSystem(const SystemDescription* description);
    const SystemDescription* getSystemDescription(const char* name);
};

#endif
