#ifndef _FILEMANAGER_HPP
#define _FILEMANAGER_HPP

#include "SDLIncludes.hpp"
#include <cstring>

#include "rapidxml.hpp"

class FileManager{
public:
    SDL_RWops* file;

    FileManager(const char* path, const char* ops){
        file =  SDL_RWFromFile(path, ops);
    }

    ~FileManager(){
        if(file){
            SDL_RWclose(file);
        }
    }
  
    bool isOpen(){
        return file != NULL;
    }

    void saveSDLPoint(const SDL_Point& point){
        int32_t p = point.x;
        SDL_RWwrite(file, &p, sizeof(p), 1);
        p = point.y;
        SDL_RWwrite(file, &p, sizeof(p), 1);
    }

    SDL_Point loadSDLPoint(){
        SDL_Point point;
        int32_t p = 0;
        SDL_RWread(file, &p, sizeof(p), 1);
        point.x = p;
        SDL_RWread(file, &p, sizeof(p), 1);
        point.y = p;
        return point;
    }

    void saveInt64(int64_t val){
        SDL_RWwrite(file, &val, sizeof(val), 1);
    }

    int64_t loadInt64(){
        int64_t val = 0;
        SDL_RWread(file, &val, sizeof(val), 1);
        return val;
    }

    void saveInt32(int32_t val){
        SDL_RWwrite(file, &val, sizeof(val), 1);
    }

    int32_t loadInt32(){
        int32_t val = 0;
        SDL_RWread(file, &val, sizeof(val), 1);
        return val;
    }

    void saveInt16(int16_t val){
        SDL_RWwrite(file, &val, sizeof(val), 1);
    }

    int16_t loadInt16(){
        int16_t val = 0;
        SDL_RWread(file, &val, sizeof(val), 1);
        return val;
    }

    void saveInt8(int8_t val){
        SDL_RWwrite(file, &val, sizeof(val), 1);
    }

    int8_t loadInt8(){
        int8_t val = 0;
        SDL_RWread(file, &val, sizeof(val), 1);
        return val;
    }

    void saveBool(bool val){
        int8_t saveVal = 0;
        if(val)
            saveVal = 1;
        SDL_RWwrite(file, &saveVal, sizeof(saveVal), 1);
    }

    bool loadBool(){
        int8_t val = 0;
        SDL_RWread(file, &val, sizeof(val), 1);
        return val;
    }

    void saveString(const char* str){
        int16_t len = strlen(str);
        SDL_RWwrite(file, &len, sizeof(len), 1);
        if(len > 0)
            SDL_RWwrite(file, str, 1, len);
    }

    void saveRawString(const char* str){
        SDL_RWwrite(file, str, 1, strlen(str));
    }

    /**
     * Assumes str has preallocated enough memory
     */
    void loadString(char* str){
        int16_t len = 0;
        SDL_RWread(file, &len, sizeof(len), 1);
        if(len > 0)
            SDL_RWread(file, str, 1, len);
        str[len] = '\0';
    }

    char* loadString(){
        int16_t len = 0;
        SDL_RWread(file, &len, sizeof(len), 1);
        char* str = new char[len+1];
        if(len > 0)
            SDL_RWread(file, str, 1, len);
        str[len] = '\0';
        return str;
    }

    void saveBytes(const char* buffer, int len){
        SDL_RWwrite(file, buffer, 1, len);
    }

    /**
     * Caller must delete returned memory.
     */
    static char* loadFileText(const char* fileName){
        SDL_RWops* file = SDL_RWFromFile(fileName, "r");
        if(file){
            int64_t length = SDL_RWseek(file, 0, RW_SEEK_END);
            SDL_RWseek(file, 0, RW_SEEK_SET);
            char* text = new char[length+1];
            SDL_RWread(file, text, length, 1);
            text[length] = 0;
            SDL_RWclose(file);
            return text;
        }
        return NULL;
    }

};

#endif
