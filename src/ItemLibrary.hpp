#ifndef _ITEMLIBRARY_HPP
#define _ITEMLIBRARY_HPP

#include <vector>

#include "SDLIncludes.hpp"

#include "rapidxml.hpp"

#include "FileManager.hpp"
#include "Item.hpp"

class ItemLibrary{
public:
    std::vector<Item> items;
    void load(const char* fileName);
    Item findItemDefinition(const char* name) const;
    Item findSystemItem(const char* system);
};

#endif
