#include "Particle.hpp"

ParticleManager::ParticleManager(){
    particles = new Particle*[MAX_PARTICLES];
    numParticles = 0;
}

void ParticleManager::update(int64_t frameTime){
    for(int i = 0; i < numParticles; i++){
        Particle* particle = particles[i];
        if(frameTime - particle->start >= particle->duration){
            numParticles--;
            delete particle;
            if(numParticles > 0){
                particles[i] = particles[numParticles];
                i--;
            }
        }
    }
}

void ParticleManager::addParticle(Particle* particle){
    if(numParticles < MAX_PARTICLES)
        particles[numParticles++] = particle;
    else
        delete particle;
}
