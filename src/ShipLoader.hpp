#ifndef _SHIPLOADER_HPP
#define _SHIPLOADER_HPP

#include <vector>
#include <map>
#include <string>
#include <cstdint>
#include <cerrno>

#include "SDLIncludes.hpp"
#include "FileManager.hpp"

#include "Ship.hpp"

struct ShipLayout{
    char* name;
    int width;
    int height;
    int **tiles;

    std::map<char, SDL_Rect> rooms;
    std::vector<SDL_Point> doors;
};

struct ShipDescription{

    char* name;
    const ShipLayout* layout;
    int armor;
    int crew;
    int power;

    struct ShipSystem{
        char* name;
        int room;
        int power;
    };
    std::vector<ShipSystem> systems;
};


class ShipLoader{
private:
    std::vector<const ShipLayout*> layouts;

    std::vector<const ShipDescription*> ships;
    std::map<std::string, std::vector<const ShipDescription*>*> shipLists;
public:

    void load(const char* fileName);

    Ship* loadShipLayout(const ShipLayout* layout);

    const ShipDescription* getShipDescription(const char* name);
    std::vector<const ShipDescription*>* getShipList(const char* name);
};

#endif
