#include "Ship.hpp"
#include <algorithm>

Ship::Ship(int8_t w, int8_t h){
    width = w;
    height = h;

    armor = armorMax = 100;
    fireArmorDamageCooldown = 0;
    scanner = NULL;
    shield = NULL;
    engine = NULL;
    teleporter = NULL;
    sickbay = NULL;

    power = 0;
    freePower = 0;

    tiles = new Tile*[h];
    for(uint8_t i = 0; i < h; i++){
        tiles[i] = new Tile[w];
    }

    rooms = NULL;
    numRooms = 0;
    mode = None;
}

void Ship::setMode(ShipMode m){
    mode = m;
    if(mode != ShipMode::None){
        modeCooldown = 3;
    }else{
        modeCooldown = 1;
    } 
}

System* Ship::getSystem(const char* name){
    for(int i = 0; i < systems.size(); i++){
        if(strcmp(systems[i]->name, name) == 0){
            return systems[i];
        }
    }
    return NULL;
}

int Ship::getEvasion() const{
    if(engine == NULL)
        return 0;

    if(!engine->isAvailable())
        return 0;

    int evasion = engine->evasion;
    if(mode == Evasive)
        evasion += 3;
    else if(mode != None)
        evasion -= 3;
    if(evasion < 0)
        return 0;
    return evasion;
}

int Ship::getShield() const{
    return shield->shield;
}

int Ship::getShieldMax() const{
    return shield->shieldMax;
}

int Ship::getShieldRegen() const{
    if(shield == NULL)
        return 0;
    if(!shield->isAvailable())
        return 0;
    if(shield->power == shield->maxPower)
        return shield->shieldRegen;

    return shield->shieldRegen + (shield->shieldRegen/shield->maxPower)*(shield->power - shield->maxPower);
}

void Ship::endTurn(){
    for(int i = 0; i < doors.size(); i++){
        doors[i]->turnDone = false;
    }
    if(shield && shield->shield < shield->shieldMax){
        if(shield->shieldRegenCooldown == 0){
            shield->shield += getShieldRegen();
            if(shield->shield > shield->shieldMax)
                shield->shield = shield->shieldMax;
            shield->shieldRegenCooldown = shield->shieldRegenTurns;
        }else{
            shield->shieldRegenCooldown--;
        }
    }else if(shield && shield->shield == shield->shieldMax)
        shield->shieldRegenCooldown = shield->shieldRegenTurns;

    for(int i = 0; i < weapons.size(); i++){
        if(weapons[i]->cooldown > 0)
            weapons[i]->cooldown--;
    }
    if(scanner && scanner->cooldown > 0){
        scanner->cooldown--;
        if(scanner->cooldown == 0)
            scanner->isScanning = false;
    }
    if(engine && engine->cooldown > 0){
        engine->cooldown--;
    }
    if(mode != ShipMode::None){
        assert(modeCooldown > 0);
        modeCooldown--;
        if(modeCooldown == 0){
            mode = ShipMode::None;
        }
    }else if(modeCooldown > 0)
        modeCooldown--;
}

void Ship::addSystem(System* system, uint8_t room){
    if(system->type == System::SystemType::Weapon)
        weapons.push_back((Weapon*)system);
    else
        systems.push_back(system);
    allSystems.push_back(system);
    rooms[room-1].system = system;
    int roomX = 0;
    int roomY = 0;
    bool found = false;
    for(int y = 0; y < height && !found; y++){
        for(int x = 0; x < width; x++){
            if(tiles[y][x].room == room){
                roomX = x;
                roomY = y;
                found = true;
                break;
            }
        }
    }

    system->posX = roomX*32;
    system->posY = roomY*32;

    for(int y = 0; y < system->layout->height; y++){
        for(int x = 0; x < system->layout->width; x++){
            tiles[roomY + y][roomX + x].type |= system->layout->type[y][x];
        } 
    }
    switch(system->type){
        case System::SystemType::Scanner:
            scanner = (Scanner*)system;
            break;
        case System::SystemType::Shield:
            shield = (Shield*)system;
            break;
        case System::SystemType::Engine:
            engine = (Engine*)system;
            break;
        case System::SystemType::Teleporter:
            teleporter = system;
            break;
        case System::SystemType::Sickbay:
            sickbay = (Sickbay*)system;
            break;
        default:
            break;
    }
}

void Ship::replaceSystem(System* origSys, System* newSys){
    if(origSys->type == System::SystemType::Weapon){
        weapons.erase(std::remove(weapons.begin(), weapons.end(), origSys), weapons.end());
    }else{
        systems.erase(std::remove(systems.begin(), systems.end(), origSys), systems.end());
    }
    allSystems.erase(std::remove(allSystems.begin(), allSystems.end(), origSys), allSystems.end());

    for(int i = 0; i < numRooms; i++){
        if(rooms[i].system == origSys){
            addSystem(newSys, i+1);
            break;
        }
    }

    int8_t power = origSys->power;
    directPower(origSys, -power);
    directPower(newSys, power);
    delete origSys;
}

void Ship::removeSystem(System* system){
    if(system->type == System::SystemType::Weapon){
        weapons.erase(std::remove(weapons.begin(), weapons.end(), system), weapons.end());
    }else{
        systems.erase(std::remove(systems.begin(), systems.end(), system), systems.end());
    }
    allSystems.erase(std::remove(allSystems.begin(), allSystems.end(), system), allSystems.end());

    for(int i = 0; i < numRooms; i++){
        if(rooms[i].system == system){
            rooms[i].system = NULL;
            for(int y = 0; y < height; y++){
                for(int x = 0; x < width; x++){
                    if(tiles[y][x].room == i+1){
                        tiles[y][x].type &= ~(Tile::Blocked | Tile::System);
                    }
                } 
            }
            break;
        }
    }

    directPower(system, -system->power);

    switch(system->type){
        case System::SystemType::Scanner:
            scanner = NULL;
            break;
        case System::SystemType::Shield:
            shield = NULL;
            break;
        case System::SystemType::Engine:
            engine = NULL;
            break;
        case System::SystemType::Teleporter:
            teleporter = NULL;
            break;
        case System::SystemType::Sickbay:
            sickbay = NULL;
            break;
        default:
            break;
    }
}

void Ship::addCrew(Crew* crew){
    crews.push_back(crew);
}

void Ship::removeCrew(Crew* crew){
    for(int i = 0; i < crews.size(); i++){
        if(crews[i] == crew){
            crews.erase(crews.begin() + i);
            return;
        }
    }
}

void Ship::addDoor(Door* door, int x, int y){
    doors.push_back(door);
    getTile(x, y).door = door;
}

void Ship::directPower(System* system, int8_t p){
    if(system->isBroken() && p > 0){
        assert(system->power == 0);
        return;
    }

    if(p > 0 && p <= freePower && system->power + p <= system->maxPower){
        system->power += p;
        freePower -= p;
    }else if(p > 0 && p > freePower && system->power + p <= system->maxPower){
        system->power += freePower;
        freePower = 0;
    }else if(p < 0 && system->power + p >= 0){
        system->power += p;
        freePower -= p;
    }

    if(system == shield && system->power == 0){
        shield->shield = 0;
    }
    if(system->power != 0){
        system->directedPower = system->power;
    }
}

void Ship::setNumRooms(uint8_t num){
    rooms = new Room[num];
    numRooms = num;
    for(uint8_t room = 0; room < num; room++)
        rooms[room].system = NULL;
}

Tile& Ship::getTile(int x, int y){
    return tiles[y][x];
}

Tile& Ship::getTile(const SDL_Point& point){
    return tiles[point.y][point.x];
}

const Tile& Ship::getTile(int x, int y) const{
    return tiles[y][x];
}

const Tile& Ship::getTile(const SDL_Point& point) const{
    return tiles[point.y][point.x];
}

Crew* Ship::getTileCrew(int x, int y){
    if(tiles[y][x].isBlocked())
        return NULL;
    for(int i = 0; i < crews.size(); i++){
        if(crews[i]->position.x == x && crews[i]->position.y == y)
            return crews[i];
    }
    return NULL;
}

const System* Ship::getTileSystem(int x, int y) const{
    if(tiles[y][x].isSystem() && tiles[y][x].room != 0 && rooms[tiles[y][x].room-1].system)
        return rooms[tiles[y][x].room-1].system;
    return NULL;
}

System* Ship::getTileSystem(int x, int y){
    if(tiles[y][x].isSystem() && tiles[y][x].room != 0 && rooms[tiles[y][x].room-1].system)
        return rooms[tiles[y][x].room-1].system;
    return NULL;
}

Door* Ship::getDoor(int x, int y){
    return tiles[y][x].getDoor();
}

const Door* Ship::getDoor(int x, int y) const{
    return tiles[y][x].getDoor();
}

Ship::~Ship(){
    for(uint8_t y = 0; y < height; y++){
        delete tiles[y];
    }
    delete tiles;

    if(rooms != NULL)
        delete rooms;
}
