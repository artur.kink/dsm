#ifndef _CONTROLS_HPP
#define _CONTROLS_HPP

#include "SDLIncludes.hpp"
#include <iostream>
#include <cstdint>

class Controls{
public:
    enum MOUSE_BUTTON{
        MOUSE_LEFT = 0,
        MOUSE_RIGHT = 1,
        MOUSE_NUMBUTTONS = 2
    };

    enum BUTTON_STATE{
        BUTTON_UP = 0x00,
        BUTTON_DOWN = 0x01,
        BUTTON_RELEASED = 0x02, ///< BUTTON_UP bit is 0
        BUTTON_PRESSED = 0x05, ///< BUTTON_DOWN bit is 1
        BUTTON_DOUBLEPRESSED = 0x0A ///< BUTTON_UP bit is 0 BUTTON_RELEASED bit is 1
    };

private:
    static Controls* controls;

    int mouseX;
    int mouseY;

    int previousMouseX;
    int previousMouseY;

    struct ButtonState{
    public:
        uint8_t state;
        uint64_t pressedTime;
        bool doubleClickOnRelease;
    };
    ButtonState buttons[MOUSE_NUMBUTTONS];

    enum KEY_STATE{
        KEY_UNPRESSED = 0x00,
        KEY_PRESSED = 0x01,
        KEY_REGISTERED = 0x02,
        KEY_DOWN = 0x03
    };
    int keySize;
    uint8_t* keys;
    int64_t* keyTime;

    Controls();
    void updateMouseButtonState(MOUSE_BUTTON button, bool isPressed);

    static const int doublePressTime = 300;

    /** Ignore mouse press to avoid registerring a mouse release after a drag */
    bool ignoreMousePress = false;

    /** Phone multitouch support */
    bool isMultiTouch;

    int numFingers;
    /** Distance of multi touch pinch */
    float pinchDist;
public:

    static Controls* instance();

    int getMouseX() const;
    int getMouseY() const;

    int getMouseXDiff() const;
    int getMouseYDiff() const;

    void addFinger();
    void removeFinger();
    bool getIsMultiTouch() const;
    int getNumFingers() const;
    float getPinchDist() const;
    void setPinch(float d);

    void setIgnoreMousePress();

    void update(int64_t time);
    uint8_t getMouseButtonState(MOUSE_BUTTON button) const;
    uint64_t getMouseButtonPressLength(MOUSE_BUTTON button) const;
    bool isMouseButtonDown(MOUSE_BUTTON button) const;
    bool wasMouseButtonPressed(MOUSE_BUTTON button) const;
    bool wasMouseButtonReleased(MOUSE_BUTTON button) const;
    bool wasMouseButtonDoubleClicked(MOUSE_BUTTON button) const;

    bool keyWasPressed(SDL_Scancode key) const;
    bool keyIsDown(SDL_Scancode key) const;
    bool keyIsRegistered(SDL_Scancode key) const;

};

#endif
