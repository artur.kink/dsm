#ifndef _HELPERS_HPP
#define _HELPERS_HPP

#include "SDLIncludes.hpp"

inline bool operator ==(const SDL_Point& a, const SDL_Point& b){
    return a.x == b.x && a.y == b.y;
}

inline bool operator !=(const SDL_Point& a, const SDL_Point& b){
    return a.x != b.x || a.y != b.y;
}

inline int getDistance(const SDL_Point& a, const SDL_Point& b){
    return (int)sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y-a.y));
}

inline int getTileDistance(int x1, int y1, int x2, int y2){
    return abs(x1 - x2) + abs(y1 - y2);
}

inline bool isPointInRect(const SDL_Point& point, int x, int y, int w, int h){
    return point.x > x && point.x < x + w &&
           point.y > y && point.y < y + h;
}

inline float lerp(int start, int current, int end){
    int duration = end - start;
    int point = current - start;
    return (float)point/(float)duration;
}

inline float ilerp(int start, int current, int end){
    return 1.0f - lerp(start, current, end);
}

#include "Random.hpp"
#include <vector>
template <typename T>
class RandomVectorIterator{
private:
    int start;
    int cur;
    bool isValid;
public:
    const std::vector<T>& vector;
    
    RandomVectorIterator(std::vector<T>& v, Random& rand):
        vector(v){
        if(v.size() == 0){
            start = 0;
            cur = 0;
            isValid = false;
        }
        else{
            start = rand.max(vector.size()); 
            cur = start;
            isValid = true;
        }
    }

    bool hasNext(){
        return isValid;
    }
    
    T getNext(){
        int c = cur;
        cur++;
        if(cur >= vector.size())
            cur = 0;
        if(cur == start)
            isValid = false;
        return vector[c];
    }

};

#endif
