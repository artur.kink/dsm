#include "SystemLoader.hpp"

void SystemLoader::load(const char* fileName, ItemLibrary& itemLibrary, SpriteManager& spriteManager){
    char* text = FileManager::loadFileText(fileName);
    if(text == NULL)
        return;

    rapidxml::xml_document<> doc;
    doc.parse<0>(text);

    rapidxml::xml_node<>* systemsNode = doc.first_node("systems");
    for(rapidxml::xml_node<>* layoutNode = systemsNode->first_node("layout"); layoutNode; layoutNode = layoutNode->next_sibling("layout")){
        int width = atoi(layoutNode->first_attribute("width")->value());
        int height = atoi(layoutNode->first_attribute("height")->value());
        SystemLayout* layout = new SystemLayout(width, height); 
        const char* nameAttr = layoutNode->first_attribute("name")->value();
        layout->name = new char[strlen(nameAttr)+1];
        strcpy(layout->name, nameAttr);

        layout->sprite = spriteManager.getList("systems")->getSprite(layoutNode->first_attribute("sprite")->value());

        const char* body = layoutNode->first_node("map")->value();
        int row = 0;
        while(*body){
            while(*body && *body != '[')
                ++body;
            if(*body == '\0')
                break;
            ++body;
            if(row >= layout->height){
                SDL_Log("System layout '%s' width mismatch", layout->name);
                break;
            }
            int col = 0;
            while(*body && *body != ']'){
                //fprintf(stderr, "%c", *body);
                if(col >= layout->width){
                    SDL_Log("System layout '%s' width mismatch", layout->name);
                    break;
                }
                if(*body == 'b'){
                    layout->type[row][col] = 0x01 | 0x10;
                }else if(*body == 'e'){
                    layout->type[row][col] = 0x02;
                }else if(*body == 's'){
                    layout->type[row][col] = 0x10 | 0x02;
                }
                col++;
                ++body;
            }
            //fprintf(stderr, "\n");
            row++;
        }


        layouts.push_back(layout); 
    }

    for(rapidxml::xml_node<>* system = systemsNode->first_node(); system; system = system->next_sibling()){
        if(strcmp(system->name(), "layout") == 0){
            continue;
        }
        SystemDescription* description = new SystemDescription();
        // Common attributes to all systems
        description->layout = NULL;
        const char* nameAttr = system->first_attribute("name")->value();
        description->name = new char[strlen(nameAttr)+1];
        strcpy(description->name, nameAttr);
        const char* layoutAttr = system->first_attribute("layout")->value();
        description->layoutName = new char[strlen(layoutAttr)+1];
        strcpy(description->layoutName, layoutAttr);
        description->layout = getLayout(description->layoutName);
        description->health = atoi(system->first_attribute("health")->value());
        description->power = atoi(system->first_attribute("power")->value());
        rapidxml::xml_attribute<>* difficulty = system->first_attribute("difficulty");
        if(difficulty){
            description->difficulty = atoi(difficulty->value());
        }else{
            description->difficulty = -1;
        }

        // System type specific attributes
        if(strcmp(system->name(), "engine") == 0){
            description->type = System::SystemType::Engine;
            description->evasion = atoi(system->first_attribute("evasion")->value());
            description->warp = atoi(system->first_attribute("warp")->value());
            engines.push_back(description);
        }else if(strcmp(system->name(), "weapon") == 0){
            description->type = System::SystemType::Weapon;
            description->damage = atoi(system->first_attribute("damage")->value());
            description->cooldown = atoi(system->first_attribute("cooldown")->value());
            description->fireOffset.x = atoi(system->first_attribute("fireoffsetx")->value());
            description->fireOffset.y = atoi(system->first_attribute("fireoffsety")->value());
            rapidxml::xml_attribute<>* type = system->first_attribute("type");
            if(strcmp(type->value(), "laser") == 0){
                description->weaponType = Weapon::Laser;
                description->shots = 1;
                description->damageSize = 1;
            }else{
                description->weaponType = Weapon::Projectile;
                rapidxml::xml_attribute<>* shots = system->first_attribute("shots");
                if(shots)
                    description->shots = atoi(shots->value());
                else
                    description->shots = 1;
                rapidxml::xml_attribute<>* size = system->first_attribute("size");
                if(size)
                    description->damageSize = atoi(size->value());
                else
                    description->damageSize = 1;
                rapidxml::xml_attribute<>* fire = system->first_attribute("fire");
                if(fire)
                    description->fireChance = atoi(fire->value());
                else
                    description->fireChance = 0;
            }
            weapons.push_back(description);
        }else if(strcmp(system->name(), "scanner") == 0){
            description->type = System::SystemType::Scanner;
            description->cooldown = atoi(system->first_attribute("cooldown")->value());
            description->scansize = atoi(system->first_attribute("scansize")->value());
            description->near_visibility = atoi(system->first_attribute("near_visibility")->value());
            description->far_visibility = atoi(system->first_attribute("far_visibility")->value());
        }else if(strcmp(system->name(), "shield") == 0){
            description->type = System::SystemType::Shield;
            description->cooldown = atoi(system->first_attribute("cooldown")->value());
            description->shield = atoi(system->first_attribute("shield")->value());
            description->regen = atoi(system->first_attribute("regen")->value());
            shields.push_back(description);
        }else if(strcmp(system->name(), "sickbay") == 0){
            description->type = System::SystemType::Sickbay;
            description->heal = atoi(system->first_attribute("heal")->value());
        }else if(strcmp(system->name(), "system") == 0){
            description->type = System::SystemType::Default;
            if(strcmp(description->name, "teleporter") == 0)
                description->type = System::SystemType::Teleporter;
        }else{
            SDL_Log("Potential Error, Unexpected System: '%s'", system->name());
        }

        // System upgrade
        description->upgradeName = NULL;
        description->upgrade = NULL;
        rapidxml::xml_node<>* upgrade = system->first_node("upgrade");
        if(upgrade){
            description->upgradeName = new char[strlen(upgrade->first_attribute("name")->value())+1];
            strcpy(description->upgradeName, upgrade->first_attribute("name")->value());
            rapidxml::xml_attribute<>* rm = upgrade->first_attribute("rm");
            if(rm)
                description->upgradeRM = atoi(rm->value());
            else
                description->upgradeRM = 0;
            for(rapidxml::xml_node<>* item = upgrade->first_node("item"); item; item = item->next_sibling("item")){
                Item i = itemLibrary.findItemDefinition(item->first_attribute("name")->value());
                i.amount = atoi(item->first_attribute("amount")->value());
                description->upgradeItems.push_back(i);
            }
        }

        systems.push_back(description);
    }

    for(int i = 0; i < systems.size(); i++){
        SystemDescription* description = systems[i];
        if(description->upgradeName != NULL){
            description->upgrade = getSystemDescription(description->upgradeName);
            assert(description->upgrade != NULL);
        }
    }

    delete text;
}

System* SystemLoader::getSystem(const char* name){
    for(int i = 0; i < systems.size(); i++){
        SystemDescription* description = systems[i];
        if(strcmp(description->name, name) == 0){
            return getSystem(systems[i]);
        }
    }
    SDL_Log("Could not find system '%s'", name);
    return NULL;
}

System* SystemLoader::getSystem(const SystemDescription* description){
    StringLibrary* strings = StringLibrary::instance;

    if(description->type == System::SystemType::Scanner){
        Scanner* scanner = new Scanner(description->name, strings->getString(description->name, StringLibrary::str_System), description->power, description->health, description->layout);
        scanner->turns = description->cooldown;
        scanner->size = description->scansize;
        scanner->nearVisibility = description->near_visibility;
        scanner->farVisibility = description->far_visibility;
        scanner->upgrade = description->upgrade;
        scanner->upgradeItems = &description->upgradeItems;
        scanner->upgradeRM = description->upgradeRM;
        return scanner;
    }else if(description->type == System::SystemType::Weapon){
        Weapon* weapon = new Weapon(description->name, strings->getString(description->name, StringLibrary::str_System), description->weaponType, description->damage, description->fireChance, description->shots, description->damageSize, description->cooldown, description->power, description->health, description->fireOffset, description->layout);
        weapon->upgrade = description->upgrade;
        weapon->upgradeItems = &description->upgradeItems;
        weapon->upgradeRM = description->upgradeRM;
        return weapon;
    }else if(description->type == System::SystemType::Shield){
        Shield* shield = new Shield(description->shield, description->regen, description->cooldown, description->name, strings->getString(description->name, StringLibrary::str_System), description->power, description->health, description->layout);
        shield->upgrade = description->upgrade;
        shield->upgradeItems = &description->upgradeItems;
        shield->upgradeRM = description->upgradeRM;
        return shield;
    }else if(description->type == System::SystemType::Engine){
        Engine* engine = new Engine(description->name, strings->getString(description->name, StringLibrary::str_System), description->evasion, description->warp, description->power, description->health, description->layout);
        engine->upgrade = description->upgrade;
        engine->upgradeItems = &description->upgradeItems;
        engine->upgradeRM = description->upgradeRM;
        return engine;
    }else if(description->type == System::SystemType::Sickbay){
        Sickbay* sickbay = new Sickbay(description->name, strings->getString(description->name, StringLibrary::str_System), description->heal, description->power, description->health, description->layout);
        sickbay->upgrade = description->upgrade;
        sickbay->upgradeItems = &description->upgradeItems;
        sickbay->upgradeRM = description->upgradeRM;
        return sickbay;
    }else{
        System* system = new System(description->name, strings->getString(description->name, StringLibrary::str_System), description->power, description->health, description->layout);
        system->upgrade = description->upgrade;
        system->upgradeItems = &description->upgradeItems;
        system->upgradeRM = description->upgradeRM;
        system->type = description->type;
        return system;
    }
    return NULL;
}

const SystemLayout* SystemLoader::getLayout(const char* name){
    for(int i = 0; i < layouts.size(); i++){
        if(strcmp(name, layouts[i]->name) == 0){
            return layouts[i];
        }
    }
    return NULL;
}


const SystemDescription* SystemLoader::getSystemDescription(const char* name){
    for(int i = 0; i < systems.size(); i++){
        if(strcmp(systems[i]->name, name) == 0){
            return systems[i];
        }
    }
    return NULL;
}
