# DSM
DSM is a turn based spaceship management roguelike game.

Working builds for Linux, Windows, macOS and Android.

# Screenshot
![screenshot](screenshot.png)

# Libraries
Required Libraries:

    SDL2
    SDL2_image
    SDL2_ttf
    SDL2_mixer
    glm
    lua
    rapidxml

rapidxml is packaged with the project in extern/rapidxml.tar and is unpackaged automatically if using cmake.

# Linux
To build run cmake in the root directory:

    cmake .
    make

The executable will be bin/game

# Android
Currently working with SDL2-2.0.9, SDL2_mixer-2.0.4, SDL2_image-2.0.4, SDL2_ttf-2.0.14

The project requires the full source of the external libraries, which includes their Android projects. 

The library source folders as listed above must be added to android/app/src/main/jni/ and should look like:

    android/app/src/main/jni
      > SDL2
      > SDL2_image
      > SDL2_ttf
      > SDL2_mixer
      > glm
      > lua

Additionally the Java files in SDL2 must be copied or linked as follows:

    android/app/src/main/java/org -> android/app/src/main/jni/SDL2/android-project/app/src/main/java/org

SDL2_image should set SUPPORT_JPEG and SUPPORT_WEBP to false in it's Android.mk.

SDL2_mixer should set SUPPORT_FLAC, SUPPORT_MP3_MPG123, SUPPORT_MOD_PLUG and SUPPORT_MID_TIMIDITY to false in it's Android.mk.

The rapidxml source files should be copied directly to android/app/src/main/jni.

Additionally the bin directory which includes the game assets must be copied or linked as the android assets directory:

    android/app/src/main/assets -> bin/

# Windows
The windows project expects SDL2, SDL2_image, SDL2_ttf, SDL2_mixer, glm, lua and rapidxml source in the project root directory.

Copy the provided dlls in the SDL lib directories and the lua precompiled dll to the bin folder to run the executable.

# macOS
Can be built using brew. brew install the required libraries along with cmake and pkg-config. Then run cmake in the root directory:

    cmake .
    make

# Font
The font, kongtext, can be found at www.1001fonts.com/kongtext-font.html

It is free to use and redistribute in any form.

The unicode font microhei from [WenQuanYi](http://wenq.org/en) is licensed under GPL and free to redistribute.

# Music and Sounds
The audio files:

    LightYears.ogg
    When_Machines_Dream.ogg
    explosion.wav
    launch.wav
    laser.wav
    warp.wav

are from http://soundimage.org/ by Eric Matyas

They are free to use and redistribute under attribution.

Other sounds were generated using [jfxr](jfxr.frozenfractal.com)

# Graphics

Action icons are from [game-icons](https://game-icons.net) provided under Creative Commmons license.
