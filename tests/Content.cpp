#include "ItemLibrary.hpp"
#include "SystemLoader.hpp"
#include "ShipLoader.hpp"

int main(int argc, char** argv){

    ItemLibrary items;
    items.load("items.xml");

    ShipLoader ships;
    ships.load("ships.xml");

    std::vector<const ShipDescription*>* enemyShips = ships.getShipList("enemy");
    for(int i = 0; i < enemyShips->size(); i++){
        const ShipDescription* description = (*enemyShips)[i];
        Ship* ship = ships.loadShipLayout(description->layout, true);
        delete ship;
    }

    SystemLoader systems;
    systems.load("systems.xml", items);

    return 0;
}
