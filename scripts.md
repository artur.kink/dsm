# Global variables
PlayerShip is the player's Ship
EnemyShip is the current enemy Ship

# Functions

### void addOption(string name, string text, string callback)
Add an option to the dialog window.

    name: Name of text to display, used for language changes.
    text: Text to display for option, english only.
    callback: String in the form of "module.function" to call when
    the option is selected. Can be empty string to do nothing.

### void addCrewSelectOption(string name, string text, int numCrew, string callback)
Add a crew selection window to the dialog window. When the option
is selected, a crew selection window is displayed. Once the player
selects numCrew crews, the callback function is called.

    name: Name of text to display, used for language changes.
    text: Text to display for option, english only.
    numCrews: Number of crew selectable.
    callback: String in the form of "module.function" to call when
    the option is selected. Can be empty string to do nothing.

### void showDialog(string name, string dialog, string ...)
Show dialog window.

    name: Name of text to display, used for language changes.
    dialog: Text to display in dialog window.

### void onHail(string callback)
Callback function to call if player performs a hail.

    callback: Callback to call in form of "module.function".

### void onDestroy(string callback)
Callback function to call if enemy ship is destroyed.

    callback: Callback to call in form of "module.function".

### void onFlee(string callback)
Callback function to call if player attempts to warp.

    callback: Callback to call in form of "module.function".

### void onAttack(string callback)
Callback function to call if player attacks enemy ship.

    callback: Callback to call in form of "module.function".

### void addTurnCallback(string  name, string callback, int turns)
Add a callback to call in n turns.

### void removeTurnCallback(string name)
Remove turn callback with given name.

### void createEncounter(string name, int minDistance, int maxDistance)
Create given encounter somewhere within given distance range.

### void removeEncounter(string name)
Remove all encounters on map with given name

### void canceWarp()
Cancels player's warp. This is used when the player tries to flee
on the onFlee event.

### bool beginEncounter(string name)
Begin encounter with given name. Returns false if encountr not found
or encounter did not meet start requirements.

### void setEnemyHostile(bool hostile)
Controls whether the enemy is hostile or not.

## Variables
Variable functions. Variables are used to preserve state between encounters and encounter functions.
Since the game can be closed at any point, local variables within lua are not preservable.
To preserve game state, any value that must persist between any lua encounter function must be saved
using the following variable functions.

### void addCrewVariable(string name, Crew crew, bool global)
Add a variable with given name storing crew.

    name: Variable name
    crew: Crew value
    global: Preserve post encounter

## Item
Item functions

### void addItem(string name, int amount)
Add an item to player cargo.

    name: Name of item.
    amount: Amount to add.

### void useItem(string name, int amount)
Remove item from player cargo.

    name: Name of item.
    amount: Amount to remove.

### void useRM(int amount)
Remove RM from player.

    amount: Amount to remove.

### void useFuel(int amount)
Remove fuel from player.

    amount: Amount to remove.

### void addRM(int amount)
Add RM to player.

    amount: Amount to add.

### void addFuel(int amount)
Add fuel to player.
    amount: Amount to add.

### void giveAward()
Give standard award items to player.

### int getItemAmount(string name)
Get number of items of given name the player has.

    name: Name of item.

### int getRM()
Get number of RM player has.

### int getFuel()
Get number of fuel player has.

## Crew
Crew functions

### Crew findProficiencyCrew(string name)
Find a crew member with given proficiency

### Crew getRandomCrew()
Get a random player crew member

### Crew[] getPlayerCrew()
Get a table of all player's crew

### void killCrew(Crew crew)
Kill crew

### int getCrewHealth(Crew crew)
Get health of crew

### void damageCrew(Crew crew, int amount)
Damage crew

### void healCrew(Crew crew, int amount)
Heal crew

### bool hasProficiency(Crew crew, string name)
Does given crew have proficiency

### int getCrewProficiencyLevel(Crew crew, string proficiency)
Get the crew's skill level associated with the proficiency.
0 If the crew does not have the proficiency.

### void increaseCrewProficiencyLevel(Crew crew, string proficiency, int amount)
Increase the crew's skill level associated with the proficiency.
Does not apply if the crew does not have the proficiency.

### Crew createPlayerCrew()
Add a crew to the player

## Ship
Ship functions

### void spawnEnemyShip(string name)
Spawns an enemy ship. If no name is provided then a random enemy ship is spawned.
If an enemy ship already exists, this will remove that ship.
Will set the global variable EnemyShip to spawned ship.

    name: Ship name to spawn. If none provided a random ship is spawned.

### void warpEnemyShip()
Cause enemy ship to warp, removing it.

### void damageShip(Ship ship, int amount)
Apply damage to ship, damage is first applied to shield, then armor

    ship: Ship to damage.
    amount: Amount to damage.

### void damageShipArmor(Ship ship, int amount)
Damage ship armor.

    ship: Ship to damage.
    amount: Amount to damage.
	
### void damageShipShield(Ship ship, int amount)
Damage ship shield.

    ship: Ship's shield to damage.
    amount: Amount to damage.
	
### void repairShip(Ship ship, int amount)
Repair ship armor.

    ship: Ship to repair.
    amount: Amount to repair.
	
### void regenShipShield(Ship ship, int amount)
Regenerate ship's shield.

    ship: Ship's shield to regen.
    amount: Amount to regen.
	
### int getShipArmor(Ship ship)
Get armor of ship.

### int getShipShield(Ship ship)
Get shield of ship.

### void spawnFire(Ship ship, int amount)
Create fire on given ship.

    ship: Ship to spawn fire on.
    amount: Amount of fires to spawn.

### void removeFire(Ship ship)
Remove all fire on given ship.

### void spawnAsteroid(Ship ship)
Spawn asteroid aimed at given ship.

## System
Functions for ship systems.

### void damageSystem(System system, int amount)
Damage system

### void repairSystem(System system, int amount)
Repair system

### System getShieldSystem(Ship ship)
Get the shield system on ship

### System getScannerSystem(Ship ship)
Get the scanner system on ship

### System getEngineSystem(Ship ship)
Get the engine system on ship

### System[] getWeaponSystems(Ship ship)
Get the weapon systems on ship

## Sector
Functions for the current sector the player is in.

### void setSpawnEnemyShips(bool spawn)
Set whether to randomly spawn enemy ships if no enemy ship exists.
Reset when warped.

### void setSafe(bool safe)
Mark sector as safe or unsafe.
Reset when warped.

### bool isAsteroids()
Does the current sector have asteroids?

### void setAstroids(bool set)
Set current sector to have asteroids if set is true, remove if false

### bool isClouds()
Does the current sector have clouds?

### void setClouds(bool set)
Set current sector to have clouds if set is true, remove if false

### bool isStart()
Does the current sector have a star?

### void setStar(bool set)
Set current sector to have star if set is true, remove if false

## Other
Other functions

### void playSound(name)
Play sound

    name: Name of sound to play

### void flash(duration, r, g, b)
Flash screen with color for given duration

    duration: Time to flash
    r: Color's red value
    g: Color's green value
    b: Color's blue value

## Debug
Debug only functions

### void removeEnemyShip()
Removes the current enemy ship. Will set EnemyShip to nil.
