
# Uncomment this if you're using STL in your project
# See CPLUSPLUS-SUPPORT.html in the NDK documentation for more information
APP_STL := c++_static

APP_CPPFLAGS := -std=gnu++11 -Wno-narrowing -O2

#APP_ABI := armeabi armeabi-v7a x86
APP_ABI := armeabi-v7a

# Min SDK level
APP_PLATFORM=android-11
