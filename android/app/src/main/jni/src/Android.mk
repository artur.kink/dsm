LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include $(LOCAL_PATH)/.. $(LOCAL_PATH)/../SDL2_image/ $(LOCAL_PATH)/../SDL2_ttf/ $(LOCAL_PATH)/../SDL2_mixer/ $(LOCAL_PATH)/../lua/

LOCAL_CFLAGS := -DANDROID -DRAPIDXML_NO_EXCEPTIONS

# Add your application source files here...
LOCAL_SRC_FILES := ../../../../../../src/main.cpp \
    ../../../../../../src/Game.cpp \
    ../../../../../../src/Particle.cpp \
    ../../../../../../src/StringLibrary.cpp \
    ../../../../../../src/ItemLibrary.cpp \
    ../../../../../../src/ShipLoader.cpp \
    ../../../../../../src/SystemLoader.cpp \
    ../../../../../../src/Ship.cpp \
    ../../../../../../src/Controls.cpp \
    ../../../../../../src/Crew.cpp \
    ../../../../../../src/EncounterManager.cpp \
    ../../../../../../src/Sprites.cpp \
    ../../../../../../src/LuaHandler.cpp \
    ../../../../../../src/Drawer.cpp \
    ../../../../../../src/AudioManager.cpp

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_ttf SDL2_mixer lua

LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog

include $(BUILD_SHARED_LIBRARY)
