MY_LOCAL_PATH := $(call my-dir)

LOCAL_PATH := $(MY_LOCAL_PATH)

include $(CLEAR_VARS)
LOCAL_MODULE    := lua
LOCAL_CFLAGS    := -Wall -DLUA_COMPAT_ALL -DLUA_USE_C89 -Dlua_getlocaledecpoint\(\)=\'.\'
LOCAL_SRC_FILES := lapi.c \
                   lauxlib.c \
                   lbaselib.c \
                   lbitlib.c \
                   lcode.c \
                   lcorolib.c \
                   lctype.c \
                   ldblib.c \
                   ldebug.c \
                   ldo.c \
                   ldump.c \
                   lfunc.c \
                   lgc.c \
                   linit.c \
                   liolib.c \
                   llex.c \
                   lmathlib.c \
                   lmem.c \
                   loadlib.c \
                   lobject.c \
                   lopcodes.c \
                   loslib.c \
                   lparser.c \
                   lstate.c \
                   lstring.c \
                   lstrlib.c \
                   ltable.c \
                   ltablib.c \
                   ltm.c \
                   lutf8lib.c \
                   lundump.c \
                   lvm.c \
                   lzio.c

LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)

include $(BUILD_SHARED_LIBRARY)
